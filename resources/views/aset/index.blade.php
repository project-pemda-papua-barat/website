@push('styles')
<link href="{{ asset('lib/sweetalert/sweetalert.css') }}" rel="stylesheet" />
<link href="https://cdn.datatables.net/1.12.1/css/jquery.dataTables.min.css" rel="stylesheet" />
<link href="https://cdn.datatables.net/buttons/2.2.3/css/buttons.dataTables.min.css" rel="stylesheet" />
@endpush


<x-header-auth title="Aset" />
<x-sidebar-auth />
<div id="right-panel" class="right-panel">
    <x-navbar-auth />
    <div class="content">
        <div class="animated fadeIn">
            <div class="row">
                <div class="col-xl-12 col-md-12 col-12">
                    <div class="card">
                        <div class="card-header">
                            <strong class="card-title">Master Data Aset</strong>
                        </div>
                        <div class="card-body" id="content"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <x-modal title="Tambah Data Aset I" idModal="modalAsetI" buttonAdd="Tambah" buttonEdit="Edit" actionAdd="addAsetI()" actionEdit="editAsetI()">
        @push('modal-content')
        <div class="row form-group">
            <input type="hidden" id="id_aset_i" name="id_aset_i" />
            <div class="col col-md-3"><label for="nama_aset_i" class=" form-control-label">Nama Aset 1</label></div>
            <div class="col-12 col-md-9"><input type="text" id="nama_aset_i" name="nama_aset_i" placeholder="Nama Asset 1" class="form-control"><small class="form-text  text-danger" id="nama_aset_i_validation"></small></div>
        </div>
        <div class="row form-group">
            <div class="col col-md-3"><label for="kode_aset_i" class=" form-control-label">kode Aset 1</label></div>
            <div class="col-12 col-md-9"><input type="text" id="kode_aset_i" name="kode_aset_i" placeholder="Kode Asset 1" class="form-control"><small class="form-text  text-danger" id="kode_aset_i_validation"></small></div>
        </div>
        @endpush
    </x-modal>

    <x-aset-component title="Tambah Data Aset II" idModal="modalAsetII" buttonAdd="Tambah" buttonEdit="edit" actionAdd="addAsetII()" actionEdit="editAsetII()" idAddButton="button-add-aset-ii" idEditButton="button-edit-aset-ii">
        @push('modal-aset-ii-content')
        <div class="row form-group">
            <input type="hidden" id="lv2_asetI_id" name="lv2_asetI_id" />
            <input type="hidden" id="id_aset_ii" name="id_aset_ii" />
            <div class="col col-md-3"><label for="nama_aset_i" class=" form-control-label">Nama Aset 2</label></div>
            <div class="col-12 col-md-9"><input type="text" id="nama_aset_ii" name="nama_aset_ii" placeholder="Nama Asset 2" class="form-control"><small class="form-text  text-danger" id="nama_aset_ii_validation"></small></div>
        </div>
        <div class="row form-group">
            <div class="col col-md-3"><label for="kode_aset_i" class=" form-control-label">kode Aset 2</label></div>
            <div class="col-12 col-md-9"><input type="text" id="kode_aset_ii" name="kode_aset_ii" placeholder="Kode Asset 2" class="form-control"><small class="form-text  text-danger" id="kode_aset_ii_validation"></small></div>
        </div>
        @endpush
    </x-aset-component>
</div>

<x-aset-component title="Tambah Data Aset III" idModal="modalAsetIII" buttonAdd="Tambah" buttonEdit="edit" actionAdd="addAsetIII()" actionEdit="editAsetIII()" idAddButton="button-add-aset-iii" idEditButton="button-edit-aset-iii">
    @push('modal-aset-iii-content')
    <div class="row form-group">
        <input type="hidden" id="lv3_asetI_id" name="lv3_asetI_id" />
        <input type="hidden" id="lv3_asetII_id" name="lv3_asetII_id" />
        <input type="hidden" id="id_aset_iii" name="id_aset_iii" />
        <div class="col col-md-3"><label for="nama_aset_iii" class=" form-control-label">Nama Aset 3</label></div>
        <div class="col-12 col-md-9"><input type="text" id="nama_aset_iii" name="nama_aset_iii" placeholder="Nama Asset 3" class="form-control"><small class="form-text  text-danger" id="nama_aset_iii_validation"></small></div>
    </div>
    <div class="row form-group">
        <div class="col col-md-3"><label for="kode_aset_iii" class=" form-control-label">kode Aset 3</label></div>
        <div class="col-12 col-md-9"><input type="text" id="kode_aset_iii" name="kode_aset_iii" placeholder="Kode Asset 3" class="form-control"><small class="form-text  text-danger" id="kode_aset_iii_validation"></small></div>
    </div>
    @endpush
</x-aset-component>

<x-aset-component title="Tambah Data Aset IV" idModal="modalAsetIV" buttonAdd="Tambah" buttonEdit="edit" actionAdd="addAsetIV()" actionEdit="editAsetIV()" idAddButton="button-add-aset-iv" idEditButton="button-edit-aset-iv">
    @push('modal-aset-iv-content')
    <div class="row form-group">
        <input type="hidden" id="lv4_asetI_id" name="lv4_asetI_id" />
        <input type="hidden" id="lv4_asetII_id" name="lv4_asetII_id" />
        <input type="hidden" id="lv4_asetIII_id" name="lv4_asetIII_id" />
        <input type="hidden" id="id_aset_iv" name="id_aset_iv" />
        <div class="col col-md-3"><label for="nama_aset_iv" class=" form-control-label">Nama Aset 4</label></div>
        <div class="col-12 col-md-9"><input type="text" id="nama_aset_iv" name="nama_aset_iv" placeholder="Nama Asset 4" class="form-control"><small class="form-text  text-danger" id="nama_aset_iv_validation"></small></div>
    </div>
    <div class="row form-group">
        <div class="col col-md-3"><label for="kode_aset_iv" class=" form-control-label">kode Aset 4</label></div>
        <div class="col-12 col-md-9"><input type="text" id="kode_aset_iv" name="kode_aset_iv" placeholder="Kode Asset 4" class="form-control"><small class="form-text  text-danger" id="kode_aset_iv_validation"></small></div>
    </div>
    @endpush
</x-aset-component>

<x-aset-component title="Tambah Data Aset V" idModal="modalAsetV" buttonAdd="Tambah" buttonEdit="edit" actionAdd="addAsetV()" actionEdit="editAsetV()" idAddButton="button-add-aset-v" idEditButton="button-edit-aset-v">
    @push('modal-aset-v-content')
    <div class="row form-group">
        <input type="hidden" id="lv5_asetI_id" name="lv5_asetI_id" />
        <input type="hidden" id="lv5_asetII_id" name="lv5_asetII_id" />
        <input type="hidden" id="lv5_asetIII_id" name="lv5_asetIII_id" />
        <input type="hidden" id="lv5_asetIV_id" name="lv5_asetIV_id" />
        <input type="hidden" id="id_aset_v" name="id_aset_v" />
        <div class="col col-md-3"><label for="nama_aset_v" class=" form-control-label">Nama Aset 5</label></div>
        <div class="col-12 col-md-9"><input type="text" id="nama_aset_v" name="nama_aset_v" placeholder="Nama Asset 5" class="form-control"><small class="form-text  text-danger" id="nama_aset_v_validation"></small></div>
    </div>
    <div class="row form-group">
        <div class="col col-md-3"><label for="kode_aset_v" class=" form-control-label">kode Aset 5</label></div>
        <div class="col-12 col-md-9"><input type="text" id="kode_aset_v" name="kode_aset_v" placeholder="Kode Asset 5" class="form-control"><small class="form-text  text-danger" id="kode_aset_v_validation"></small></div>
    </div>
    @endpush
</x-aset-component>

<x-aset-component title="Tambah Data Aset VI" idModal="modalAsetVI" buttonAdd="Tambah" buttonEdit="edit" actionAdd="addAsetVI()" actionEdit="editAsetVI()" idAddButton="button-add-aset-vi" idEditButton="button-edit-aset-vi">
    @push('modal-aset-vi-content')
    <div class="row form-group">
        <input type="hidden" id="lv6_asetI_id" name="lv6_asetI_id" />
        <input type="hidden" id="lv6_asetII_id" name="lv6_asetII_id" />
        <input type="hidden" id="lv6_asetIII_id" name="lv6_asetIII_id" />
        <input type="hidden" id="lv6_asetIV_id" name="lv6_asetIV_id" />
        <input type="hidden" id="lv6_asetV_id" name="lv6_asetV_id" />
        <input type="hidden" id="id_aset_vi" name="id_aset_vi" />
        <div class="col col-md-3"><label for="nama_aset_vi" class=" form-control-label">Nama Aset 6</label></div>
        <div class="col-12 col-md-9"><input type="text" id="nama_aset_vi" name="nama_aset_vi" placeholder="Nama Asset 6" class="form-control"><small class="form-text  text-danger" id="nama_aset_vi_validation"></small></div>
    </div>
    <div class="row form-group">
        <div class="col col-md-3"><label for="kode_aset_vi" class=" form-control-label">kode Aset 6</label></div>
        <div class="col-12 col-md-9"><input type="text" id="kode_aset_vi" name="kode_aset_vi" placeholder="Kode Asset 6" class="form-control"><small class="form-text  text-danger" id="kode_aset_vi_validation"></small></div>
    </div>
    @endpush
</x-aset-component>

<x-aset-component title="Tambah Data Aset VII" idModal="modalAsetVII" buttonAdd="Tambah" buttonEdit="edit" actionAdd="addAsetVII()" actionEdit="editAsetVII()" idAddButton="button-add-aset-vii" idEditButton="button-edit-aset-vii">
    @push('modal-aset-vii-content')
    <div class="row form-group">
        <input type="hidden" id="lv7_asetI_id" name="lv7_asetI_id" />
        <input type="hidden" id="lv7_asetII_id" name="lv7_asetII_id" />
        <input type="hidden" id="lv7_asetIII_id" name="lv7_asetIII_id" />
        <input type="hidden" id="lv7_asetIV_id" name="lv7_asetIV_id" />
        <input type="hidden" id="lv7_asetV_id" name="lv7_asetV_id" />
        <input type="hidden" id="lv7_asetVI_id" name="lv7_asetVI_id" />
        <input type="hidden" id="id_aset_vii" name="id_aset_vii" />
        <div class="col col-md-3"><label for="nama_aset_vii" class=" form-control-label">Nama Aset 7</label></div>
        <div class="col-12 col-md-9"><input type="text" id="nama_aset_vii" name="nama_aset_vii" placeholder="Nama Asset 7" class="form-control"><small class="form-text  text-danger" id="nama_aset_vii_validation"></small></div>
    </div>
    <div class="row form-group">
        <div class="col col-md-3"><label for="kode_aset_vii" class=" form-control-label">kode Aset 7</label></div>
        <div class="col-12 col-md-9"><input type="text" id="kode_aset_vii" name="kode_aset_vii" placeholder="Kode Asset 7" class="form-control"><small class="form-text  text-danger" id="kode_aset_vii_validation"></small></div>
    </div>
    <div class="row form-group">
        <div class="col col-md-3"><label for="data_id" class=" form-control-label">ID Data</label></div>
        <div class="col-12 col-md-9"><input type="text" id="data_id" name="data_id" placeholder="ID Data" class="form-control"><small class="form-text  text-danger" id="data_id_validation"></small></div>
    </div>
    @endpush
</x-aset-component>
</div>

@push('scripts')
<script src="{{asset('dist/assets/js/lib/data-table/datatables.min.js')}}"></script>
<script src="{{asset('dist/assets/js/lib/data-table/dataTables.bootstrap.min.js')}}"></script>
<script src="{{asset('dist/assets/js/lib/data-table/dataTables.buttons.min.js')}}"></script>
<script src="{{asset('dist/assets/js/lib/data-table/buttons.bootstrap.min.js')}}"></script>
<script src="{{asset('dist/assets/js/lib/data-table/jszip.min.js')}}"></script>
<script src="{{asset('dist/assets/js/lib/data-table/vfs_fonts.js')}}"></script>
<script src="{{asset('dist/assets/js/lib/data-table/buttons.html5.min.js')}}"></script>
<script src="{{asset('dist/assets/js/lib/data-table/buttons.print.min.js')}}"></script>
<script src="{{asset('dist/assets/js/lib/data-table/buttons.colVis.min.js')}}"></script>
<script src="{{asset('dist/assets/js/init/datatables-init.js')}}"></script>
<script src="{{asset('lib/sweetalert/sweetalert.min.js')}}"></script>
<script src="{{asset('js/action/authValidation.js')}}"></script>
<script src="{{asset('js/action/aset.js')}}"></script>
@endpush
<x-footer-auth />