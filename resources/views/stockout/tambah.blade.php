@push('styles')
<link href="{{ asset('lib/sweetalert/sweetalert.css') }}" rel="stylesheet" />
<link href="https://cdn.datatables.net/1.12.1/css/jquery.dataTables.min.css" rel="stylesheet" />
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
@endpush
<x-header-auth title="Stock In" />
<x-sidebar-auth />
<div id="right-panel" class="right-panel">
    <x-navbar-auth />
    <div class="content">
        <div class="animated fadeIn">
            <div class="card">
                <div class="card-header">
                    <div class="row">
                        <div class="col-xl-8 col-md-8">
                            <div class="form-group row">
                                <label for="no_transaksi" class=" col-sm-6 font-weight-bold">Nomor Transaksi: </label>
                                <div class="col-sm-6">
                                    <input type="text" id="no_transaksi" name="no_transaksi" placeholder="no_transaksi" class="form-control-plaintext">
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-4 col-md-4 col-4">
                            <span class="float-right font-weight-bold">Stok Keluar > Tambah Data</span>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <div class="container">
                        <div class="row">
                            <div class="col-xl-4 col-md-4 col-12">
                                <div class="form-group">
                                    <label for="no_berita_acara">Nomor Berita Acara</label>
                                    <input type="text" id="no_berita_acara" name="no_berita_acara" placeholder="Masukan Nomor Berita Acara" class="form-control" /><small class="form-text  text-danger" id="no_berita_acara_validation"></small>
                                </div>
                            </div>
                            <div class="col-xl-4 col-md-4 col-12">
                                <div class="form-group">
                                    <label for="no_dokumen_pengiriman">Nomor Dokumen Pengiriman</label>
                                    <input type="text" id="no_dokumen_pengiriman" name="no_dokumen_pengiriman" placeholder="Masukan Nomor Pengiriman" class="form-control"><small class="form-text  text-danger" id="no_dokumen_pengiriman_validation"></small>
                                </div>
                            </div>
                            <div class="col-xl-4 col-md-4 col-12">
                                <div class="form-group">
                                    <label for="tanggal_dokumen">Tanggal Dokumen</label>
                                    <input type="date" id="tanggal_dokumen" name="tanggal_dokumen" class="form-control"><small class="form-text  text-danger" id="tanggal_dokumen_validation"></small>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4 col-12">
                                <label for="dokumen_berita_acara" class=" form-control-label">Dokumen Berita Acara</label>
                                <input type="file" id="dokumen_berita_acara" name="dokumen_berita_acara" class="form-control"><small class="form-text  text-danger" id="dokumen_berita_acara_validation"></small>
                            </div>
                            <div class="col-lg-4 col-md-4 col-12">
                                <label for="dokumen_expedisi" class=" form-control-label">Dokumen Expedisi</label>
                                <input type="file" id="dokumen_expedisi" name="dokumen_expedisi" class="form-control"><small class="form-text  text-danger" id="dokumen_expedisi_validation"></small>
                            </div>
                            <div class="col-lg-4 col-md-4 col-12">
                                <label for="dokumen_pendukung" class=" form-control-label">Dokumen Pendukung</label>
                                <input type="file" id="dokumen_pendukung" name="dokumen_pendukung" class="form-control"><small class="form-text  text-danger" id="dokumen_pendukung_validation"></small>
                            </div>
                            <div class="col-lg-4 col-md-4 col-12">
                                <label for="aset" class=" form-control-label">Distributor</label>
                                <select class="form-control" name="distributor_id_select" id="distributor_id_select" style="width: 100%">
                                    <option value="">-- Pilih Distributor -- </option>
                                    @foreach ($listDistributor as $dist)
                                        <option value="{{$dist->id}}">{{$dist->text}}</option>                                        
                                    @endforeach
                                </select>
                                <small class="form-text  text-danger" id="distributor_id_validation"></small>
                            </div>
                            <div class="col-lg-4 col-md-4 col-12">
                                <label for="aset" class=" form-control-label">Unit Tujuan</label>
                                <select class="form-control" name="unit_id_select" id="unit_id_select" style="width: 100%">
                                    <option value="">-- Pilih Unit Tujuan -- </option>
                                    @foreach ($unit as $u)
                                        <option value="{{$u->id}}">{{$u->text}}</option>                                        
                                    @endforeach
                                </select>
                                <small class="form-text  text-danger" id="to_unit_id_validation"></small>
                            </div>
                            <div class="col-xl-4 col-md-4 col-12">
                                <div class="form-group">
                                    <label for="keterangan">Keterangan</label>
                                    <input type="text" id="keterangan" name="keterangan" placeholder="Keterangan" class="form-control">
                                </div>
                            </div>
                            <div class="col-xl-12 col-md-12 col-12">
                                <div class="my-2">
                                    <button type="button" class="btn btn-danger mx-2" onclick="back()">Kembali</button>
                                    <button type="button" class="btn btn-success mx-2" onclick="addData()">Simpan</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@push('scripts')
<script src="{{asset('dist/assets/js/lib/data-table/datatables.min.js')}}"></script>
<script src="{{asset('dist/assets/js/lib/data-table/dataTables.bootstrap.min.js')}}"></script>
<script src="{{asset('dist/assets/js/lib/data-table/jszip.min.js')}}"></script>
<script src="{{asset('dist/assets/js/lib/data-table/vfs_fonts.js')}}"></script>
<script src="{{asset('dist/assets/js/init/datatables-init.js')}}"></script>
<script src="{{asset('lib/sweetalert/sweetalert.min.js')}}"></script>
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
<script src="{{asset('js/action/stockout.js')}}"></script>
<x-footer-auth />