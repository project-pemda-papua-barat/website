@push('styles')
<link href="{{ asset('lib/sweetalert/sweetalert.css') }}" rel="stylesheet" />
<link href="https://cdn.datatables.net/1.12.1/css/jquery.dataTables.min.css" rel="stylesheet" />
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
@endpush
<x-header-auth title="Stock In Draft" />
<x-sidebar-auth />
<div id="right-panel" class="right-panel">
    <x-navbar-auth />
    <div class="content">
        <div class="animated fadeIn">
            <div class="card" id="content-draft"></div>
        </div>
    </div>
</div>

<x-moda-edit title="Edit Data Detail Stock Out" idModal="modalDataEditDetailStock" buttonAdd="Tambah" buttonEdit="Edit" actionEdit="editDataDetailStock()">
    @push('modal-content-edit')
    <div class="row">
        <div class="col-lg-12 col-md-12 col-12">
            <input type="hidden" id="id_value" value=""/>
            <input type="hidden" id="stock_out_id_value" value=""/>
            <select class="form-control" name="sotcked_data_select_edit" id="sotcked_data_select_edit" style="width: 100%">
                <option value="">-- Pilih Aset --</option>
           </select><small class="form-text  text-danger" id="sotcked_data_edit_select_validation"></small>
        </div>
        <div class="col-lg-12 col-md-12 col-12">
            <label for="aset" class=" form-control-label">Jumlah</label>
            <input type="text" id="jumlah_edit" name="jumlah_edit" placeholder="Jumlah" class="form-control"><small class="form-text  text-danger" id="jumlah_edit_validation"></small>
        </div>
    </div>
    @endpush
</x-moda-edit>

<x-modal title="Tambah Data Stock Keluar" idModal="modalData" buttonAdd="Tambah" buttonEdit="Edit" actionAdd="addDataDetailStock()" actionEdit="">
    @push('modal-content')
    <div class="row">
        <div class="col-lg-12 col-md-12 col-12">
            <select class="form-control" name="sotcked_data_select" id="sotcked_data_select" style="width: 100%">
                <option value="">Pilih Aset</option>
           </select><small class="form-text  text-danger" id="sotcked_data_select_validation"></small>
        </div>
        <div class="col-lg-6 col-md-6">
            <div id="list-content"></div>
        </div>
        <div class="col-lg-6 col-md-6 col-12">
            <label for="aset" class=" form-control-label">Jumlah</label>
            <input type="text" id="jumlah" name="jumlah" placeholder="Jumlah" class="form-control"><small class="form-text  text-danger" id="jumlah_validation"></small>
        </div>
    </div>
    @endpush
</x-modal>

<x-dokumen-aset idModal="modalUpload" title="Edit Data Stock Out" titleButton="Edit Transaksi">
    @push('modal-content-upload')
    <div class="row">
        <div class="col-lg-12 col-md-12 col-12">
            <label for="aset" class=" form-control-label">No Berita Acara</label>
            <input type="text" id="no_berita_acara" name="no_berita_acara" placeholder="Masukan Nomor Berita Acara" class="form-control" /><small class="form-text  text-danger" id="no_berita_acara_validation"></small>
        </div>
        <div class="col-lg-6 col-md-6 col-12">
            <label for="aset" class=" form-control-label">Nomor Dokumen Pengiriman</label>
            <input type="text" id="no_dokumen_pengiriman" name="no_dokumen_pengiriman" placeholder="Masukan Nomor Dokumen Pengiriman" class="form-control" /><small class="form-text  text-danger" id="no_dokumen_pengiriman_validation"></small>
        </div>
        <div class="col-lg-6 col-md-6 col-12">
            <label for="aset" class=" form-control-label">Distributor</label>
            <select class="form-control" name="distributor_id_select" id="distributor_id_select" style="width: 100%">
                <option value="">Pilih Distributor</option>
                @foreach($listSDistributor as $dist)
                <option value="{{$dist->id}}">{{$dist->text}}</option>
                @endforeach
            </select><small class="form-text  text-danger" id="distributor_id_validation"></small>
        </div>
        <div class="col-lg-6 col-md-6 col-12">
            <label for="aset" class=" form-control-label">Unit Tujuan</label>
            <select class="form-control" name="to_unit_id_select" id="to_unit_id_select" style="width: 100%">
                <option value="">Pilih Distributor</option>
                @foreach($unit as $u)
                <option value="{{$u->id}}">{{$u->text}}</option>
                @endforeach
            </select><small class="form-text  text-danger" id="to_unit_id_select_validation"></small>
        </div>
        <div class="col-lg-6 col-md-4 col-12">
            <label for="tanggal_dokumen">Tanggal Dokumen</label>
            <input type="date" id="tanggal_dokumen" name="tanggal_dokumen" class="form-control"><small class="form-text  text-danger" id="tanggal_dokumen_validation"></small>
        </div>
        <div class="col-lg-6 col-md-4 col-12">
            <label for="tanggal_dokumen">Keterangan</label>
            <input type="text" id="keterangan" name="keterangan" class="form-control"><small class="form-text  text-danger" id="keterangan_validation"></small>
        </div>
        <div class="col-lg-6 col-md-6 col-12">
            <label for="dokumen_berita_acara" class=" form-control-label">Dokumen Berita Acara</label>
            <input type="file" id="dokumen_berita_acara" name="dokumen_berita_acara" class="form-control"><small class="form-text  text-danger" id="dokumen_berita_acara_validation"></small>
        </div>
        <div class="col-lg-6 col-md-12 col-12">
            <label for="dokumen_expedisi" class=" form-control-label">Dokumen Expedisi</label>
            <input type="file" id="dokumen_expedisi" name="dokumen_expedisi" class="form-control"><small class="form-text  text-danger" id="dokumen_expedisi_validation"></small>
        </div>
        <div class="col-lg-6 col-md-12 col-12">
            <label for="dokumen_pendukung" class=" form-control-label">Dokumen Pendukung</label>
            <input type="file" id="dokumen_pendukung" name="dokumen_pendukung" class="form-control"><small class="form-text  text-danger" id="dokumen_pendukung_validation"></small>
        </div>
    </div>
    @endpush
</x-dokumen-aset>
</div>
@push('scripts')
<script src="{{asset('dist/assets/js/lib/data-table/datatables.min.js')}}"></script>
<script src="{{asset('dist/assets/js/lib/data-table/dataTables.bootstrap.min.js')}}"></script>
<script src="{{asset('dist/assets/js/lib/data-table/jszip.min.js')}}"></script>
<script src="{{asset('dist/assets/js/lib/data-table/vfs_fonts.js')}}"></script>
<script src="{{asset('dist/assets/js/init/datatables-init.js')}}"></script>
<script src="{{asset('lib/sweetalert/sweetalert.min.js')}}"></script>
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
<script src="{{asset('js/action/stockoutdraft.js')}}"></script>
@endpush
<x-footer-auth />