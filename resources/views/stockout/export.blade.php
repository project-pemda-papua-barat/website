<!DOCTYPE html>
<html>

<head>
    <title>InventoryApp::Report Stock OUT {{ $date }} </title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>

<body>
    <div class="card">
        <div class="card-header">
            <div class="card-title">
                <p class="text-center font-weight-bold">{{ $title }}</p>
                <hr />
                <div class="d-flex justify-content-between">
                    <div>
                        <p class="text-muted">Nomor Transaksi &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: 
                            <?=  $data['no_transaksi']; ?>
                        </p>
                        <p class="text-muted">dialamatkan kepada &nbsp;: <?= $data['unit_tujuan'] ;?>
                        </p>
                        <p class="text-muted">Alamat
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: <?= $data['alamat_unit_tujuan'] ;?>
                        </p>
                    </div>
                    <div class="">
                        <p class="text-muted">Tanggal: <?= $data['tanggal_dokumen']; ?></p>
                        <p class="text-muted">Dikirim melalui / diambil sendiri oleh: <?= $data['nama_distributor'] ;?>
                            </p>
                    </div>
                </div>
            </div>
        </div>
        <div class="cord-body">
            <h5 class="text-center font-weight-bold my-2">Pelaksanaan Pengeluaran Barang</h5>
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th scope="col">NO</th>
                        <th scope="col">NAMA BARANG</th>
                        <th scope="col">JUMLAH</th>
                        <th scope="col">SUMBER</th>
                        <th scope="col">HARGA </th>
                        <th scope="col">JUMLAH HARGA </th>
                        <th scope="col">KETERANGAN</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($data['detail'] as $item => $key)
                    <tr>
                        <th scope="row">{{$loop->iteration}}</th>
                        <td>{{ $key['nama_detail_aset'] }} - {{ $key['nomor_aset'] }} </td>
                        <td>{{ $key['qty_out'] }} {{ $key['nama_satuan'] }}</td>
                        <td>{{ $key['nama_sumber_dana'] }}</td>
                        <td>Rp {{ number_format($key['harga_satuan'],0, ',', '.') }}</td>
                        <td>Rp {{ number_format($key['total'],0, ',', '.') }}</td>
                        <td>{{ $key['keterangan'] }}</td>
                    </tr>
                    @endforeach
                    <tr>
                        <td colspan="6"><span class="font-weight-bold">Total Harga</span></td>
                        <td class="font-weight-bold">: Rp {{ number_format($data['total_harga'],0, ',', '.')}} </td>
                    </tr>
                    <tr>
                        <td colspan="3">
                            <div class="d-flex flex-column ">
                                <div class="font-weight-bold" style="margin-bottom: 100px">Staf Pengelola Instalasi
                                    Farmasi
                                </div>
                                @foreach($signer as $sign)
                                    @if($sign->jabatan == 'Staf Pengelola Instalasi Farmasi')
                                        <div>
                                            Nama : {!! $sign->nama_penanda_tangan !!}   <br />
                                            NIP. {!! $sign->nip !!}
                                        </div>
                                    @endif
                                @endforeach
                            </div>
                        </td>
                        <td colspan="3">
                            <div class="d-flex flex-column ">
                                <span class="font-weight-bold" style="margin-bottom: 50px">Dikirim / Diambil sendiri
                                    Oleh</span>
                                <br />
                                <i class="my-5 text-muted">Ttd, stample / cap tiga jari </i>
                                <i class="text-muted">Nama <br />NIP/Jabatan </i>
                            </div>
                        </td>
                        <td colspan="3">
                            <div class="font-weight-bold" style="margin-bottom: 90px">
                                Mengetahui, <br />
                                Kepala Bidang Sumber Daya Kesehatan
                            </div>
                            @foreach($signer as $sign)
                                @if($sign->jabatan == 'Kepala Bidang Sumber Daya Kesehatan')
                                    <div>
                                        Nama : {!! $sign->nama_penanda_tangan !!}   <br />
                                        NIP. {!! $sign->nip !!}
                                    </div>
                                @endif
                            @endforeach
                        </td>
                    </tr>
                    <tr>
                        <td colspan="9">
                            <i class="font-weight-bold">Barang-barang tersebut telah dihitung satu  persatu dan diterima dengan baik dan lengkap. Penerima Mengetahui</i>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            <div class="d-flex flex-column ">
                                <div class="d-flex justify-content-between">
                                    <span class="font-weight-bold">Penerima <br />Tanggal : {!! $date !!}
                                    </span>
                                    <span class="font-weight-bold">Jam: {!! $waktu !!}</span>
                                </div>
                                <i class="my-5 text-muted">Ttd, stample / cap tiga jari </i>
                                <i class="text-muted">Nama <br />NIP/Jabatan </i>
                            </div>
                        </td>
                        <td colspan="5">
                            <div class="d-flex justify-content-center">
                                <div class="d-flex flex-column ">
                                    <div class="font-weight-bold" style="margin-bottom: 90px">
                                        Mengetahui, <br />
                                        Kepala Dinas Kesehatan MayBrat
                                    </div>
                                    @foreach($signer as $sign)
                                        @if($sign->jabatan == 'Kepala Dinas Kesehatan MayBrat')
                                            <div>
                                                Nama : {!! $sign->nama_penanda_tangan !!}   <br />
                                                NIP. {!! $sign->nip !!}
                                            </div>
                                        @endif
                                    @endforeach
                                </div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="9">
                            <i class="font-weight-bold text-center">Komoditas Tersebut Digunakan Untuk Peningkatan
                                Kesehatan
                                Masyarakat dan Tidak Untuk di Perjual Belikan, Jika Barang diterima rusak/pecah/kurang,
                                agar diberi catatan pada Kolom Keterangan di SBBK ini dan
                                kalau tidak tercatat apa-apa maka barang dianggap diterima dalam keadaan baik dan
                                lengkap.</i>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</body>

</html>