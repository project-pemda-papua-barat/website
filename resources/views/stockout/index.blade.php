@push('styles')
<link href="{{ asset('lib/sweetalert/sweetalert.css') }}" rel="stylesheet" />
<link href="https://cdn.datatables.net/1.12.1/css/jquery.dataTables.min.css" rel="stylesheet" />
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
@endpush
<x-header-auth title="Stock Out::Tambah" />
<x-sidebar-auth />
<div id="right-panel" class="right-panel">
    <x-navbar-auth />
    <div class="content">
        <div class="animated fadeIn">
            <div class="row">
                <div class="col-xl-12 col-md-12 col-12">
                    <div class="card">
                        <div class="card-header">
                            <strong class="card-title">Transaksi Keluar</strong>
                            <div class="row">
                                <div class="col-10"></div>
                                <div class="col-2">
                                    <a href="stok-keluar/tambah" class="btn btn-primary"  id="role-user">Tambah Data ></a>
                                </div>
                            </div>
                        </div>
                        <div class="card-body">
                            <table id="stock-out-table" class="table table-striped table-bordered" width="100%">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Nomor Transaksi / Waktu</th>
                                        <th>No Berita Acara</th>
                                        <th>Unit Pelayanan</th>
                                        <th>Status</th>
                                        <th>Petugas / keterangan</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <x-detail-aset title="List Detail Aset" idModal="listAset">
        @push('modal-content-detail-aset')
        <div id="content-list">
            <div class="row" id="tag-download"></div>
            <div class="container my-2" id="list-detail-aset"></div>
        </div>
        @endpush
    </x-detail-aset>

</div>
@push('scripts')
<script src="{{asset('dist/assets/js/lib/data-table/datatables.min.js')}}"></script>
<script src="{{asset('dist/assets/js/lib/data-table/dataTables.bootstrap.min.js')}}"></script>
<script src="{{asset('dist/assets/js/lib/data-table/jszip.min.js')}}"></script>
<script src="{{asset('dist/assets/js/lib/data-table/vfs_fonts.js')}}"></script>
<script src="{{asset('dist/assets/js/init/datatables-init.js')}}"></script>
<script src="{{asset('lib/sweetalert/sweetalert.min.js')}}"></script>
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
<script src="{{asset('js/action/authValidationStockout.js')}}"></script>
<x-footer-auth />