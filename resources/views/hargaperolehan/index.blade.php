@push('styles')
<link href="{{ asset('lib/sweetalert/sweetalert.css') }}" rel="stylesheet" />
<link href="https://cdn.datatables.net/1.12.1/css/jquery.dataTables.min.css" rel="stylesheet" />
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
<link href="https://cdn.datatables.net/buttons/2.2.3/css/buttons.dataTables.min.css" rel="stylesheet" />
@endpush

<x-header-auth title="Harga Perolehan" />
<x-sidebar-auth />
<div id="right-panel" class="right-panel">
    <x-navbar-auth />
    <div class="content">
        <div class="animated fadeIn">
            <div class="row">
                <div class="col-xl-12 col-md-12 col-12">
                    <div class="card">
                        <div class="card-header">
                            <strong class="card-title">Master Data Detail Aset dan Harga Perolehan</strong>
                        </div>
                        <div class="card-body">
                            <table id="harga-perolehan-table" class="table table-striped table-bordered">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Nomor Aset</th>
                                        <th>Nama Aset</th>
                                        <th>Data ID</th>
                                        <th>Nama Detail Aset</th>
                                        <th>Keterangan Aset</th>
                                        <th>Gambar Aset</th>
                                        <th>creator / terakhir diupdate oleh</th>
                                        <th> Update Harga</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody id="content"></tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <x-moda-edit title="Edit Data Detail Aset" idModal="modalDetailAsetEdit" buttonAdd="Tambah" buttonEdit="Edit" actionAdd="" actionEdit="editDetailAsetAction()">
        @push('modal-content-edit')
        <input type="hidden" id="detai_aset_id_value" value=""/>
        <div class="row form-group">
            <div class="col col-md-3"><label for="aset_id" class=" form-control-label">Pilih Aset</label></div>
            <div class="col-12 col-md-9"><select class="form-control" id="aset_id_select_edit" name="aset_id_select_edit" style="width: 100%">
                <option value="">-- Pilih Aset --</option>
                @foreach ($listAset as $aset => $key)
                    <option value="{{$key['id']}}">{{$key['nomor_aset']}} - {{$key['nama_aset']}} - ({{$key['id_aset']}})</option>
                @endforeach
            </select><small class="form-text  text-danger" id="aset_id_select_edit_validation"></small></div>
        </div>
        <div class="row form-group">
            <div class="col col-md-3"><label for="nama_detail_aset_edit" class=" form-control-label">Nama Detail Aset</label></div>
            <div class="col-12 col-md-9"><input type="text" id="nama_detail_aset_edit" name="nama_detail_aset_edit" placeholder="Nama Detail/ spesifikasi Aset" class="form-control"><small class="form-text  text-danger" id="nama_detail_aset_edit_validation"></small></div>
        </div>
        <div class="row form-group">
            <div class="col col-md-3"><label for="keterangan_edit" class=" form-control-label">Keterangan</label></div>
            <div class="col-12 col-md-9"><input type="text" id="keterangan_edit" name="keterangan_edit" placeholder="Detail Aset Merk, lokasi, ukuran dll" class="form-control"><small class="form-text  text-danger" id="keterangan_edit_validation"></small></div>
        </div>
        <div class="row form-group">
            <div class="col col-md-3"><label for="aset_image_edit" class=" form-control-label">Gambar Aset</label></div>
            <div class="col-12 col-md-9"><input type="file" id="aset_image_edit" name="aset_image_edit" class="form-control"><small class="form-text  text-danger" id="aset_image_edit_validation"></small></div>
        </div>
        <div class="row form-group">
            <div class="col col-md-3"><label for="is_expired_edit" class=" form-control-label">Apakah Aset ini memiliki tanggal Expired?</label></div>
            <div class="col-12 col-md-9"><select id="is_expired_edit" name="is_expired_edit" class="form-control">
                    <option value="">Pilih Opsi </option>
                    <option value="true">"Ya" </option>
                    <option value="false">"Tidak" </option>
                </select><small class="form-text  text-danger" id="is_expired_edit_validation"></small></div>
        </div>
        @endpush
    </x-moda-edit>

    <x-modal title="Tambah Data Detail Aset" idModal="modalDetailAset" buttonAdd="Tambah" buttonEdit="Edit" actionAdd="addDetailAset()" actionEdit="editHargaPerolehan()">
        @push('modal-content')
        <div class="row form-group">
            <div class="col col-md-3"><label for="aset_id" class=" form-control-label">Pilih Aset</label></div>
            <div class="col-12 col-md-9"><select class="form-control" id="aset_id_select" name="aset_id_select" style="width: 100%">
                <option value="">-- Pilih Aset --</option>
                @foreach ($listAset as $aset => $key)
                    <option value="{{$key['id']}}">{{$key['nomor_aset']}} - {{$key['nama_aset']}} - ({{$key['id_aset']}})</option>
                @endforeach
            </select><small class="form-text  text-danger" id="aset_id_select_validation"></small></div>
        </div>
        <div class="row form-group">
            <div class="col col-md-3"><label for="nama_detail_aset" class=" form-control-label">Nama Detail Aset</label></div>
            <div class="col-12 col-md-9"><input type="text" id="nama_detail_aset" name="nama_detail_aset" placeholder="Nama Detail/ spesifikasi Aset" class="form-control"><small class="form-text  text-danger" id="nama_detail_aset_validation"></small></div>
        </div>
        <div class="row form-group">
            <div class="col col-md-3"><label for="keterangan" class=" form-control-label">Keterangan</label></div>
            <div class="col-12 col-md-9"><input type="text" id="keterangan" name="keterangan" placeholder="Detail Aset Merk, lokasi, ukuran dll" class="form-control"><small class="form-text  text-danger" id="keterangan_validation"></small></div>
        </div>
        <div class="row form-group">
            <div class="col col-md-3"><label for="aset_image" class=" form-control-label">Gambar Aset</label></div>
            <div class="col-12 col-md-9"><input type="file" id="aset_image" name="aset_image" class="form-control"><small class="form-text  text-danger" id="aset_image_validation"></small></div>
        </div>
        <div class="row form-group">
            <div class="col col-md-3"><label for="is_expired" class=" form-control-label">Apakah Aset ini memiliki tanggal Expired?</label></div>
            <div class="col-12 col-md-9"><select id="is_expired" name="is_expired" class="form-control">
                    <option value="">Pilih Opsi </option>
                    <option value="true">"Ya" </option>
                    <option value="false">"Tidak" </option>
                </select><small class="form-text  text-danger" id="is_expired_validation"></small></div>
        </div>
        @endpush
    </x-modal>

    <x-modal-unit title="Tambah Data Harga Perolehan <span id='detail-title'></span>" idModal="modalTambahHargaPerolehan" buttonAdd="TambahPerubahanHarga" buttonEdit="Tambah Harga" actionAdd="addDataPerolehan()" actionEdit="editHargaPerolehan()">
        @push('modal-unit-content')
        <input type="hidden" id="detail_aset_id" name="detail_aset_id">
        <div class="row form-group">
            <div class="col col-md-3"><label for="tanggal" class=" form-control-label">Tanggal</label></div>
            <div class="col-12 col-md-9"><input type="date" id="tanggal" name="tanggal" placeholder="Nama Detail/ spesifikasi Aset" class="form-control"><small class="form-text  text-danger" id="tanggal_validation"></small></div>
        </div>
        <div class="row form-group">
            <div class="col col-md-3"><label for="perubahan_harga" class=" form-control-label">Perubahan Harga</label></div>
            <div class="col-12 col-md-9"><input type="text" id="perubahan_harga" name="perubahan_harga" placeholder="Perubahan Harga" class="form-control"><small class="form-text  text-danger" id="perubahan_harga_validation"></small></div>
        </div>
        @endpush
    </x-modal-unit>

    <x-list-harga title="List Data Harga Perolehan" idModal="ListTambahHargaPerolehan">
        @push('modal-content-list')
        <div id="content-list"></div>
        @endpush
    </x-list-harga>

</div>
@push('scripts')
<script src="{{asset('dist/assets/js/lib/data-table/datatables.min.js')}}"></script>
<script src="{{asset('dist/assets/js/lib/data-table/dataTables.bootstrap.min.js')}}"></script>
<script src="{{asset('dist/assets/js/lib/data-table/dataTables.buttons.min.js')}}"></script>
<script src="{{asset('dist/assets/js/lib/data-table/buttons.bootstrap.min.js')}}"></script>
<script src="{{asset('dist/assets/js/lib/data-table/jszip.min.js')}}"></script>
<script src="{{asset('dist/assets/js/lib/data-table/vfs_fonts.js')}}"></script>
<script src="{{asset('dist/assets/js/lib/data-table/buttons.html5.min.js')}}"></script>
<script src="{{asset('dist/assets/js/lib/data-table/buttons.print.min.js')}}"></script>
<script src="{{asset('dist/assets/js/lib/data-table/buttons.colVis.min.js')}}"></script>
<script src="{{asset('dist/assets/js/init/datatables-init.js')}}"></script>
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
<script src="{{asset('lib/sweetalert/sweetalert.min.js')}}"></script>
<script src="{{asset('js/action/authValidation.js')}}"></script>
<script src="{{asset('js/action/hargaperolehan.js')}}"></script>
@endpush
<x-footer-auth />