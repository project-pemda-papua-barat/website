@push('styles')
<link href="{{ asset('lib/sweetalert/sweetalert.css') }}" rel="stylesheet" />
<link href="https://cdn.datatables.net/1.12.1/css/jquery.dataTables.min.css" rel="stylesheet" />
<link href="https://cdn.datatables.net/buttons/2.2.3/css/buttons.dataTables.min.css" rel="stylesheet" />
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
@endpush
<x-header-auth title="Unit" />
<x-sidebar-auth />
<div id="right-panel" class="right-panel">
    <x-navbar-auth />
    <div class="content">
        <div class="animated fadeIn">
            <div class="row">
                <div class="col-xl-12 col-md-12 col-12">
                    <div class="card">
                        <div class="card-header">
                            <strong class="card-title">Tambah Unit</strong>
                        </div>
                        <div class="card-body">
                            <div class="row form-group" id="is_active_unit_status"> </div>
                            <div class="row form-group">
                                <input type="hidden" id="id_unit" name="id_unit" />
                                <div class="col col-md-3"><label for="nama_unit" class=" form-control-label">Nama Unit</label></div>
                                <div class="col-12 col-md-9">
                                    <input type="text" id="nama_unit" name="nama_unit" placeholder="Nama Unit" class="form-control">
                                    <small class="form-text  text-danger" id="nama_unit_validation"></small>
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col col-md-3"><label for="" class=" form-control-label">Pilih Area</label></div>
                                <div class="col-12 col-md-9">
                                    <select class="form-control" name=" " id="area_id_select" style="width: 100%"></select>
                                    <small class="form-text  text-danger" id="area_id_validation"></small>
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col col-md-3"><label for="keterangan" class=" form-control-label">Keterangan</label></div>
                                <div class="col-12 col-md-9"><input type="text" id="keterangan" name="keterangan" placeholder="Keterangan" class="form-control"></div>
                            </div>
                            <div class="row form-group">
                                <div class="col col-md-3"><label for="" class=" form-control-label">Alamat</label></div>
                                <div class="col-12 col-md-9">
                                    <input id="address" name="address" class="form-control" type="text" placeholder="Alamat" />
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col col-md-3"><label for="" class=" form-control-label">Detail Alamat</label></div>
                                <div class="col-12 col-md-9">
                                    <input id="detail_alamat" class="form-control" type="text" placeholder="Nama Gedung, patokan blok dll" />
                                </div>
                            </div>
                            <input type="hidden" id="lat" name="lat">
                            <input type="hidden" name="long" id="long">
                            <div class="col-lg-12 col-md-12 col-12">
                                <div class="address-map-container" id="address-map-container">
                                    <div id="map"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@push('scripts')
<script src="{{asset('dist/assets/js/lib/data-table/datatables.min.js')}}"></script>
<script src="{{asset('dist/assets/js/lib/data-table/dataTables.bootstrap.min.js')}}"></script>
<script src="{{asset('dist/assets/js/lib/data-table/dataTables.buttons.min.js')}}"></script>
<script src="{{asset('dist/assets/js/lib/data-table/buttons.bootstrap.min.js')}}"></script>
<script src="{{asset('dist/assets/js/lib/data-table/jszip.min.js')}}"></script>
<script src="{{asset('dist/assets/js/lib/data-table/vfs_fonts.js')}}"></script>
<script src="{{asset('dist/assets/js/lib/data-table/buttons.html5.min.js')}}"></script>
<script src="{{asset('dist/assets/js/lib/data-table/buttons.print.min.js')}}"></script>
<script src="{{asset('dist/assets/js/lib/data-table/buttons.colVis.min.js')}}"></script>
<script src="{{asset('dist/assets/js/init/datatables-init.js')}}"></script>
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
<script src="{{asset('lib/sweetalert/sweetalert.min.js')}}"></script>
<script src="{{asset('js/action/authValidation.js')}}"></script>
<script src="{{asset('js/action/area.js')}}"></script>
<script src="{{asset('js/action/unit.js')}}"></script>
<script type="text/javascript" src="https://maps.google.com/maps/api/js?key={{ env('GOOGLE_MAP_KEY') }}&libraries=places"></script>

<script>
    google.maps.event.addDomListener(window, 'load', initialize);

    function initialize() {
        var input = document.getElementById('address');
        var autocomplete = new google.maps.places.Autocomplete(input);
        autocomplete.addListener('place_changed', function() {
            var place = autocomplete.getPlace();
            $('#lat').val(place.geometry['location'].lat());
            $('#long').val(place.geometry['location'].lng());
        });
    }
</script>
@endpush
<x-footer-auth />