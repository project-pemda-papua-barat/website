@push('styles')
<link href="{{ asset('lib/sweetalert/sweetalert.css') }}" rel="stylesheet" />
<link href="https://cdn.datatables.net/1.12.1/css/jquery.dataTables.min.css" rel="stylesheet" />
<link href="https://cdn.datatables.net/buttons/2.2.3/css/buttons.dataTables.min.css" rel="stylesheet" />
@endpush

<x-header-auth title="Distributor" />
<x-sidebar-auth />
<div id="right-panel" class="right-panel">
    <x-navbar-auth />
    <div class="content">
        <div class="animated fadeIn">
            <div class="row">
                <div class="col-xl-12 col-md-12 col-12">
                    <div class="card">
                        <div class="card-header">
                            <strong class="card-title">Master Data Distributor</strong>
                        </div>
                        <div class="card-body">
                            <table id="distributor-table" class="table table-striped table-bordered">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Nama Distributor</th>
                                        <th>Alamat</th>
                                        <th>Detail Alamat</th>
                                        <th>Nomor Kontak</th>
                                        <th>Keterangan</th>
                                        <th>Status</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <x-modal title="Tambah Data Distributor" idModal="modalDistributor" buttonAdd="Tambah" buttonEdit="Edit" actionAdd="addDistributor()" actionEdit="editDistributor()">
        @push('modal-content')
        <div class="row form-group" id="is_active_status"> </div>
        <div class="row form-group">
            <input type="hidden" id="id_distributor" name="id_distributor" />
            <div class="col col-md-3"><label for="nama_distributor" class=" form-control-label">Nama distributor</label></div>
            <div class="col-12 col-md-9"><input type="text" id="nama_distributor" name="nama_distributor" placeholder="Nama distributor" class="form-control"><small class="form-text  text-danger" id="nama_distributor_validation"></small></div>
        </div>
        <div class="row form-group">
            <div class="col col-md-3"><label for="detail_alamat" class=" form-control-label">Detail Alamat distributor</label></div>
            <div class="col-12 col-md-9"><input type="text" name="detail_alamat" id="detail_alamat" rows="9" placeholder="Nama gedung, lantai dll" class="form-control" /><small class="form-text  text-danger" id="detail_alamat_distributor_validation"></small></div>
        </div>
        <div class="row form-group">
            <div class="col col-md-3"><label for="alamat_distributor" class=" form-control-label">Alamat distributor</label></div>
            <div class="col-12 col-md-9"><input type="text" name="alamat_distributor" id="alamat_distributor" rows="9" placeholder="Alamat distributor..." class="form-control" /><small class="form-text  text-danger" id="alamat_distributor_validation"></small></div>
        </div>
        <div class="row form-group">
            <div class="col col-md-3"><label for="no_contact_distributor" class=" form-control-label">Nomor Kontak distributor</label></div>
            <div class="col-12 col-md-9"><input type="text" id="no_contact_distributor" name="no_contact_distributor" placeholder="Nomor Kontak distributor" class="form-control"><small class="form-text  text-danger" id="no_contact_distributor_validation"></small></div>
        </div>
        <input type="hidden" id="lat" name="lat">
        <input type="hidden" name="long" id="long">
        <div class="row form-group">
            <div class="col col-md-3"><label for="keterangan" class=" form-control-label">Keterangan</label></div>
            <div class="col-12 col-md-9"><input type="text" id="keterangan" name="keterangan" placeholder="Keterangan" class="form-control"></div>
        </div>
        <div class="col-lg-12 col-md-12 col-12">
            <div class="address-map-container" id="address-map-container">
                <div id="map"></div>
            </div>
        </div>
        @endpush
    </x-modal>
</div>

@push('scripts')
<script src="{{asset('dist/assets/js/lib/data-table/datatables.min.js')}}"></script>
<script src="{{asset('dist/assets/js/lib/data-table/dataTables.bootstrap.min.js')}}"></script>
<script src="{{asset('dist/assets/js/lib/data-table/dataTables.buttons.min.js')}}"></script>
<script src="{{asset('dist/assets/js/lib/data-table/buttons.bootstrap.min.js')}}"></script>
<script src="{{asset('dist/assets/js/lib/data-table/jszip.min.js')}}"></script>
<script src="{{asset('dist/assets/js/lib/data-table/vfs_fonts.js')}}"></script>
<script src="{{asset('dist/assets/js/lib/data-table/buttons.html5.min.js')}}"></script>
<script src="{{asset('dist/assets/js/lib/data-table/buttons.print.min.js')}}"></script>
<script src="{{asset('dist/assets/js/lib/data-table/buttons.colVis.min.js')}}"></script>
<script src="{{asset('dist/assets/js/init/datatables-init.js')}}"></script>
<script src="{{asset('lib/sweetalert/sweetalert.min.js')}}"></script>
<script src="{{asset('js/action/authValidation.js')}}"></script>
<script src="{{asset('js/action/distributor.js')}}"></script>
<script type="text/javascript" src="https://maps.google.com/maps/api/js?key={{ env('GOOGLE_MAP_KEY') }}&libraries=places"></script>

<script>
    google.maps.event.addDomListener(window, 'load', initialize);

    function initialize() {
        var input = document.getElementById('alamat_distributor');
        var autocomplete = new google.maps.places.Autocomplete(input);
        autocomplete.addListener('place_changed', function() {
            var place = autocomplete.getPlace();
            $('#lat').val(place.geometry['location'].lat());
            $('#long').val(place.geometry['location'].lng());
        });
    }
</script>
@endpush

<x-footer-auth />