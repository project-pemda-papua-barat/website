@push('styles')
<link href="{{ asset('lib/sweetalert/sweetalert.css') }}" rel="stylesheet" />
<link href="https://cdn.datatables.net/1.12.1/css/jquery.dataTables.min.css" rel="stylesheet" />
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
@endpush
<x-header-auth title="Stock In Draft" />
<x-sidebar-auth />
<div id="right-panel" class="right-panel">
    <x-navbar-auth />
    <div class="content">
        <div class="animated fadeIn">
            <div class="card" id="content-draft"></div>
        </div>
    </div>
</div>

<x-moda-edit title="Edit Data Detail Stock In" idModal="modalDataEditDetailStock" buttonAdd="Tambah" buttonEdit="Edit"
    actionEdit="editDataDetailStock()">
    @push('modal-content-edit')
    <div class="row">
        <div class="col-lg-6 col-md-6 col-12">
            <label for="aset" class=" form-control-label">Aset </label>
            @csrf
            <input type="hidden" id="id_value" name="id_value" />
            <select class="form-control" name="aset_id_select_edit" id="aset_id_select_edit" style="width: 100%">
                <option value="">Pilih Aset</option>
                @foreach($detailAset as $detail)
                <option value="{{$detail['id']}}" S>{{$detail['text']}}</option>
                @endforeach
            </select><small class="form-text  text-danger" id="aset_id_edit_validation"></small>
        </div>
        <div class="col-lg-6 col-md-6 col-12">
            <label for="aset" class=" form-control-label">Penyedia</label>
            <select class="form-control" name="penyedia_id_select_edit" id="penyedia_id_select_edit"
                style="width: 100%">
                <option value="">Pilih Penyedia</option>
                @foreach($penyedias as $penyedia)
                <option value="{{$penyedia->id}}">{{$penyedia->text}}</option>
                @endforeach
            </select><small class="form-text  text-danger" id="penyedia_id_edit_validation"></small>
        </div>
        <div class="col-lg-6 col-md-6 col-12">
            <label for="aset" class=" form-control-label">Satuan</label>
            <select class="form-control" name="satuan_id_select_edit" id="satuan_id_select_edit" style="width: 100%">
                <option value="">Pilih Satuan</option>
                @foreach($satuans as $satuan)
                <option value="{{$satuan->id}}">{{$satuan->text}}</option>
                @endforeach
            </select><small class="form-text  text-danger" id="satuan_id_edit_validation"></small>
        </div>
        <div class="col-lg-6 col-md-6 col-12">
            <label for="aset" class=" form-control-label">Kemasan</label>
            <select class="form-control" name="kemasan_id_select_edit" id="kemasan_id_select_edit" style="width: 100%">
                <option value="">Pilih Kemasan</option>
                @foreach($kemasans as $kemasan)
                <option value="{{$kemasan->id}}">{{$kemasan->text}}</option>
                @endforeach
            </select><small class="form-text  text-danger" id="kemasan_id_edit_validation"></small>
        </div>
        <div class="col-lg-6 col-md-6 col-12">
            <label for="aset" class=" form-control-label">Jumlah</label>
            <input type="text" id="jumlah_edit" name="jumlah_edit" placeholder="Jumlah" class="form-control"><small
                class="form-text  text-danger" id="jumlah_edit_validation"></small>
        </div>
        <div class="col-lg-6 col-md-6 col-12">
            <label for="aset" class=" form-control-label">Kode</label>
            <input type="text" id="kode_edit" name="kode_edit" placeholder="Kode" class="form-control"><small
                class="form-text  text-danger" id="kode_edit_validation"></small>
        </div>
        <div class="col-lg-6 col-md-6 col-12">
            <label for="batch_edit" class=" form-control-label">Batch</label>
            <input type="text" id="batch_edit" name="batch_edit" placeholder="Batch" class="form-control"><small
                class="form-text  text-danger" id="batch_edit_validation"></small>
        </div>
        <div class="col-lg-6 col-md-6 col-12">
            <label for="aset" class=" form-control-label">Tanggal Expired</label>
            <input type="date" id="expired_date_edit" name="expired_date_edit" placeholder="expired date"
                class="form-control"><small class="form-text  text-danger" id="expired_date_edit_validation"></small>
        </div>
        <div class="col-lg-6 col-md-6 col-12">
            <label for="aset" class=" form-control-label">Harga Satuan</label>
            <input type="text" id="harga_satuan_edit" name="harga_satuan_edit" placeholder="Harga Satuan"
                class="form-control"><small class="form-text  text-danger" id="harga_satuan_edit_validation"></small>
        </div>
    </div>
    @endpush
</x-moda-edit>

<x-modal title="Tambah Data Stock" idModal="modalData" buttonAdd="Tambah" buttonEdit="Edit"
    actionAdd="addDataDetailStock()" actionEdit="">
    @push('modal-content')
    <div class="row">

        <div class="col-lg-6 col-md-6 col-12">
            <label for="aset" class=" form-control-label">Aset</label>
            <select class="form-control" name="aset_id_select" id="aset_id_select" style="width: 100%">
                <option value="">-- Pilih Aset --</option>
                @foreach($detailAset as $detail)
                <option value="{{$detail['id']}}" title="{{$detail['is_expired']}}">{{$detail['text']}}</option>
                @endforeach
            </select>
            <small class="form-text  text-danger" id="aset_id_validation"></small>
        </div>
        <div class="col-lg-6 col-md-6 col-12">
            <label for="aset" class=" form-control-label">Penyedia</label>
            <select class="form-control" name="penyedia_id_select" id="penyedia_id_select" style="width: 100%">
                <option value="">Pilih Penyedia</option>
                @foreach($penyedias as $penyedia)
                <option value="{{$penyedia->id}}">{{$penyedia->text}}</option>
                @endforeach
            </select>
            <small class="form-text  text-danger" id="penyedia_id_validation"></small>
        </div>
        <div class="col-lg-6 col-md-6 col-12">
            <label for="aset" class=" form-control-label">Satuan</label>
            <select class="form-control" name="satuan_id_select" id="satuan_id_select" style="width: 100%">
                <option value="">Pilih Satuan</option>
                @foreach($satuans as $satuan)
                <option value="{{$satuan->id}}">{{$satuan->text}}</option>
                @endforeach
            </select>
            <small class="form-text  text-danger" id="satuan_id_validation"></small>
        </div>
        <div class="col-lg-6 col-md-6 col-12">
            <label for="aset" class=" form-control-label">Kemasan</label>
            <select class="form-control" name="kemasan_id_select" id="kemasan_id_select" style="width: 100%">
                <option value="">Pilih Kemasan</option>
                @foreach($kemasans as $kemasan)
                <option value="{{$kemasan->id}}">{{$kemasan->text}}</option>
                @endforeach
            </select>
            <small class="form-text  text-danger" id="kemasan_id_validation"></small>
        </div>
        <div class="col-lg-6 col-md-6 col-12">
            <label for="aset" class=" form-control-label">Jumlah</label>
            <input type="text" id="jumlah" name="jumlah" placeholder="Jumlah" class="form-control"><small
                class="form-text  text-danger" id="jumlah_validation"></small>
        </div>
        <div class="col-lg-6 col-md-6 col-12">
            <label for="aset" class=" form-control-label">Kode</label>
            <input type="text" id="kode" name="kode" placeholder="Kode" class="form-control"><small
                class="form-text  text-danger" id="kode_validation"></small>
        </div>
        <div class="col-lg-6 col-md-6 col-12">
            <label for="aset" class=" form-control-label">Batch</label>
            <input type="text" id="batch" name="batch" placeholder="Batch" class="form-control"><small
                class="form-text  text-danger" id="batch_validation"></small>
        </div>
        <div class="col-lg-6 col-md-6 col-12">
            <label for="aset" class=" form-control-label">Tanggal Expired</label>
            <input type="date" id="expired_date" name="expired_date" placeholder="expired_date"
                class="form-control"><small class="form-text  text-danger" id="expired_date_validation"></small>
            <small class="form-text  text-danger" id="notice_expired_validation"></small>
        </div>
        <div class="col-lg-6 col-md-6 col-12">
            <label for="aset" class=" form-control-label">Harga Satuan</label>
            <input type="text" id="harga_satuan" name="harga_satuan" placeholder="Harga Satuan"
                class="form-control"><small class="form-text  text-danger" id="harga_satuan_validation"></small>
            <span class="text-muted" id="update-harga-terakhir"></span>
        </div>
    </div>
    @endpush
</x-modal>

<x-dokumen-aset idModal="modalUpload" title="Edit Data Stock In" titleButton="Edit Transaksia">
    @push('modal-content-upload')
    <div class="row">
        <div class="col-lg-6 col-md-6 col-12">
            <label for="aset" class=" form-control-label">Nomor Berita Acara</label>
            <input type="text" id="no_berita_acara" name="no_berita_acara" placeholder="Masukan Nomor Berita Acara"
                class="form-control" /><small class="form-text  text-danger" id="no_berita_acara_validation"></small>
        </div>
        <div class="col-lg-6 col-md-6 col-12">
            <label for="aset" class=" form-control-label">Nomor Dokumen Pengiriman</label>
            <input type="text" id="no_dokumen_pengiriman" name="no_dokumen_pengiriman"
                placeholder="Masukan Nomor Dokumen Pengiriman" class="form-control" /><small
                class="form-text  text-danger" id="no_dokumen_pengiriman_validation"></small>
        </div>
        <div class="col-lg-6 col-md-6 col-12">
            <label for="aset" class=" form-control-label">Tahun Pengadaan</label>
            <select id="tahun_pengadaan" name="tahun_pengadaan" placeholder="Masukan Nomor Dokumen Pengiriman"
                class="form-control"></select><small class="form-text  text-danger"
                id="tahun_pengadaan_validation"></small>
        </div>
        <div class="col-lg-6 col-md-6 col-12">
            <label for="aset" class=" form-control-label">Jenis</label>
            <select class="form-control" name="jenis_id_select" id="jenis_id_select" style="width: 100%">
                <option value="">Pilih Jenis Program</option>
                @foreach($listJenis as $jenis)
                <option value="{{$jenis->id}}">{{$jenis->text}}</option>
                @endforeach
            </select><small class="form-text  text-danger" id="jenis_id_validation"></small>
        </div>
        <div class="col-lg-6 col-md-6 col-12">
            <label for="aset" class=" form-control-label">Distributor</label>
            <select class="form-control" name="distributor_id_select" id="distributor_id_select" style="width: 100%">
                <option value="">Pilih Distributor</option>
                @foreach($listSDistributor as $dist)
                <option value="{{$dist->id}}">{{$dist->text}}</option>
                @endforeach
            </select><small class="form-text  text-danger" id="distributor_id_validation"></small>
        </div>
        <div class="col-lg-6 col-md-6 col-12">
            <label for="aset" class=" form-control-label">Sumber Dana</label>
            <select class="form-control" name="sumber_dana_id_select" id="sumber_dana_id_select" style="width: 100%">
                <option value="">Pilih Sumber Dana</option>
                @foreach($listSumberDana as $sumber)
                <option value="{{$sumber->id}}">{{$sumber->text}}</option>
                @endforeach
            </select><small class="form-text  text-danger" id="sumber_dana_id_validation"></small>
        </div>
        <div class="col-lg-6 col-md-4 col-12">
            <label for="tanggal_dokumen">Tanggal Dokumen</label>
            <input type="date" id="tanggal_dokumen" name="tanggal_dokumen" class="form-control"><small
                class="form-text  text-danger" id="tanggal_dokumen_validation"></small>
        </div>
        <div class="col-lg-6 col-md-4 col-12">
            <label for="tanggal_dokumen">Keterangan</label>
            <input type="text" id="keterangan" name="keterangan" class="form-control"><small
                class="form-text  text-danger" id="keterangan_validation"></small>
        </div>
        <div class="col-lg-6 col-md-6 col-12">
            <label for="dokumen_berita_acara" class=" form-control-label">Dokumen Berita Acara</label>
            <input type="file" id="dokumen_berita_acara" name="dokumen_berita_acara" class="form-control"><small
                class="form-text  text-danger" id="dokumen_berita_acara_validation"></small>
        </div>
        <div class="col-lg-6 col-md-12 col-12">
            <label for="dokumen_expedisi" class=" form-control-label">Dokumen Expedisi</label>
            <input type="file" id="dokumen_expedisi" name="dokumen_expedisi" class="form-control"><small
                class="form-text  text-danger" id="dokumen_expedisi_validation"></small>
        </div>
        <div class="col-lg-6 col-md-12 col-12">
            <label for="dokumen_pendukung" class=" form-control-label">Dokumen Pendukung</label>
            <input type="file" id="dokumen_pendukung" name="dokumen_pendukung" class="form-control"><small
                class="form-text  text-danger" id="dokumen_pendukung_validation"></small>
        </div>
    </div>
    @endpush
</x-dokumen-aset>
</div>
@push('scripts')
<script src="{{asset('dist/assets/js/lib/data-table/datatables.min.js')}}"></script>
<script src="{{asset('dist/assets/js/lib/data-table/dataTables.bootstrap.min.js')}}"></script>
<script src="{{asset('dist/assets/js/lib/data-table/jszip.min.js')}}"></script>
<script src="{{asset('dist/assets/js/lib/data-table/vfs_fonts.js')}}"></script>
<script src="{{asset('dist/assets/js/init/datatables-init.js')}}"></script>
<script src="{{asset('lib/sweetalert/sweetalert.min.js')}}"></script>
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
<script src="{{asset('js/action/stockindraft.js')}}"></script>
@endpush
<x-footer-auth />