@push('styles')
<link href="{{ asset('lib/sweetalert/sweetalert.css') }}" rel="stylesheet" />
<link href="https://cdn.datatables.net/1.12.1/css/jquery.dataTables.min.css" rel="stylesheet" />
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
<link href="https://cdn.datatables.net/buttons/2.2.3/css/buttons.dataTables.min.css" rel="stylesheet" />
@endpush

<x-header-auth title="Management User" />
<x-sidebar-auth />
<div id="right-panel" class="right-panel">
    <x-navbar-auth />
    <div class="content">
        <div class="animated fadeIn">
            <div class="row">
                <div class="col-xl-12 col-md-12 col-12">
                    <div class="card">
                        <div class="card-header">
                            <strong class="card-title">Master Data Jenis Program Masuk</strong>
                        </div>
                        <div class="card-body">
                            <table id="list-user-table" class="table table-striped table-bordered">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Nama </th>
                                        <th>Username</th>
                                        <th>Email</th>
                                        <th>Role</th>
                                        <th>Unit / Squad</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <x-modal title="Tambah User" idModal="modalUser" buttonAdd="Tambah" buttonEdit="Edit" actionAdd="addUser()" actionEdit="editUser()">
        @push('modal-content')
        <div class="row form-group" id="is_active_status"> </div>
        <div class="row form-group">
            <input type="hidden" id="id_user" name="id_user" />
            <div class="col col-md-3"><label for="name" class=" form-control-label">Nama User</label></div>
            <div class="col-12 col-md-9"><input type="text" id="name" name="name" placeholder="Nama User" class="form-control"><small class="form-text  text-danger" id="name_validation"></small></div>
        </div>
        <div class="row form-group">
            <div class="col col-md-3"><label for="username" class=" form-control-label">Username</label></div>
            <div class="col-12 col-md-9"><input type="text" id="username" username="username" placeholder="Username" class="form-control"><small class="form-text  text-danger" id="username_validation"></small></div>
        </div>
        <div class="row form-group">
            <div class="col col-md-3"><label for="password" class=" form-control-label">Password</label></div>
            <div class="col-12 col-md-9"><input type="password" id="password" password="password" placeholder="password" class="form-control"><small class="form-text  text-danger" id="password_validation"></small></div>
        </div>
        <div class="row form-group">
            <div class="col col-md-3"><label for="email" class=" form-control-label">Email</label></div>
            <div class="col-12 col-md-9"><input type="email" id="email" name="email" placeholder="email" class="form-control"><small class="form-text  text-danger" id="email_validation"></small></div>
        </div>
        <div class="row form-group">
            <div class="col col-md-3"><label for="squad_id_select" class=" form-control-label">Unit / Squad</label></div>
            <div class="col-12 col-md-9"><select id="squad_id_select" name="squad_id_select" class="form-control" style="width: 100%"></select><small class="form-text  text-danger" id="squad_id_validation"></small></div>
        </div>
        <div class="row form-group">
            <div class="col col-md-3"><label for="role_id" class=" form-control-label">Role</label></div>
            <div class="col-12 col-md-9"><select id="role_id" name="role_id" class="form-control">
                    <option value="">Pilih Role</option>
                    <option value="1">Super Admin</option>
                    <option value="2">Supervisor Pusat</option>
                    <option value="3">Supervisor Area</option>
                    <option value="4">Supervisor Unit</option>
                    <option value="5">User Unit</option>
                </select><small class="form-text  text-danger" id="role_id_validation"></small></div>
        </div>
        <div class="row form-group">
            <div class="col col-md-3"><label for="profile_pict" class=" form-control-label">Profile Picture</label></div>
            <div class="col-12 col-md-9"><input type="file" id="profile_pict" name="profile_pict" class="form-control" />
                </select><small class="form-text  text-danger" id="profile_pict_validation"></small></div>
        </div>
        <div class="row form-group">
            <div class="col col-md-3"><label for="" class=" form-control-label">Alamat</label></div>
            <div class="col-12 col-md-9">
                <input id="address_user" name="address_user" class="form-control" type="text" placeholder="Alamat" /><small class="form-text  text-danger" id="address_user_validation"></small>
            </div>
        </div>
        <input type="hidden" name="lat" id="lat" />
        <input type="hidden" name="long" id="long" />
        @endpush
    </x-modal>
</div>

@push('scripts')
<script src="{{asset('dist/assets/js/lib/data-table/datatables.min.js')}}"></script>
<script src="{{asset('dist/assets/js/lib/data-table/dataTables.bootstrap.min.js')}}"></script>
<script src="{{asset('dist/assets/js/lib/data-table/dataTables.buttons.min.js')}}"></script>
<script src="{{asset('dist/assets/js/lib/data-table/buttons.bootstrap.min.js')}}"></script>
<script src="{{asset('dist/assets/js/lib/data-table/jszip.min.js')}}"></script>
<script src="{{asset('dist/assets/js/lib/data-table/vfs_fonts.js')}}"></script>
<script src="{{asset('dist/assets/js/lib/data-table/buttons.html5.min.js')}}"></script>
<script src="{{asset('dist/assets/js/lib/data-table/buttons.print.min.js')}}"></script>
<script src="{{asset('dist/assets/js/lib/data-table/buttons.colVis.min.js')}}"></script>
<script src="{{asset('dist/assets/js/init/datatables-init.js')}}"></script>
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
<script src="{{asset('lib/sweetalert/sweetalert.min.js')}}"></script>
<script src="{{asset('js/action/authValidationsuperuser.js')}}"></script>
<script src="{{asset('js/action/management-user.js')}}"></script>
<script type="text/javascript" src="https://maps.google.com/maps/api/js?key={{ env('GOOGLE_MAP_KEY') }}&libraries=places"></script>

<script>
    google.maps.event.addDomListener(window, 'load', initialize);

    function initialize() {
        var input = document.getElementById('address_user');
        var autocomplete = new google.maps.places.Autocomplete(input);
        autocomplete.addListener('place_changed', function() {
            var place = autocomplete.getPlace();
            $('#lat').val(place.geometry['location'].lat());
            $('#long').val(place.geometry['location'].lng());
        });
    }
</script>
@endpush
<x-footer-auth />