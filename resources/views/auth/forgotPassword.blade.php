<x-header />
<div class="container-fluid">
    <div class="row h-100 align-items-center justify-content-center">
        <div class="col-12 col-sm-8 col-md-6 col-lg-5 col-xl-7" style="height: 80vh;overflow-y: scroll">
            <div class="card">
                <div class="card-header bg-primary">
                    <div class="card-title">
                        <p class="h6 text-bold text-light text-center"> Pengumuman</p>
                    </div>
                </div>
                <div class="card-content">
                    <div class="d-flex justify-content-between px-4 py-2">
                        <p class="title">
                            SOP masuk telah terbit<br />
                            Oleh: Admin
                        </p>
                        <p class="title">16 Mei 2018, 21:53:27</p>
                    </div>
                    <p class="px-4 py-2">Lorem ipsum dolor sit amet consectetur adipisicing elit. Ratione consectetur vel tenetur blanditiis reiciendis magnam quo placeat saepe natus! Eaque iure, placeat itaque optio, earum impedit et autem necessitatibus nesciunt ad magni, exercitationem aliquid. Quos illo et sunt ad harum?</p>
                </div>
                <hr />
                <div class="card-content">
                    <div class="d-flex justify-content-between px-4 py-2">
                        <p class="title">
                            SOP masuk telah terbit<br />
                            Oleh: Admin
                        </p>
                        <p class="title">16 Mei 2018, 21:53:27</p>
                    </div>
                    <p class="px-4 py-2">Lorem ipsum dolor sit amet consectetur adipisicing elit. Ratione consectetur vel tenetur blanditiis reiciendis magnam quo placeat saepe natus! Eaque iure, placeat itaque optio, earum impedit et autem necessitatibus nesciunt ad magni, exercitationem aliquid. Quos illo et sunt ad harum?</p>
                </div>
                <hr />
                <div class="card-content">
                    <div class="d-flex justify-content-between px-4 py-2">
                        <p class="title">
                            SOP masuk telah terbit<br />
                            Oleh: Admin
                        </p>
                        <p class="title">16 Mei 2018, 21:53:27</p>
                    </div>
                    <p class="px-4 py-2">Lorem ipsum dolor sit amet consectetur adipisicing elit. Ratione consectetur vel tenetur blanditiis reiciendis magnam quo placeat saepe natus! Eaque iure, placeat itaque optio, earum impedit et autem necessitatibus nesciunt ad magni, exercitationem aliquid. Quos illo et sunt ad harum?</p>
                </div>
            </div>
        </div>
        <div class="col-12 col-sm-8 col-md-6 col-lg-5 col-xl-5  bg-info">
            <div class="card p-4 p-sm-5 my-4 mx-4">
                <div class="card-content">
                    <div class="text-center py-4">
                        <h5 class="text-info">Login</h5>
                        <p>Masukan Email dan Password</p>
                    </div>
                    <div class="form-floating mb-3">
                        <input type="email" class="form-control" id="floatingInput" placeholder="name@example.com">
                        <label for="floatingInput">Email address</label>
                    </div>
                    <div class="form-floating mb-4">
                        <input type="password" class="form-control" id="floatingPassword" placeholder="Password">
                        <label for="floatingPassword">Password</label>
                    </div>
                    <div class="d-flex align-items-center justify-content-between mb-4">
                        <div class="form-check">
                            <input type="checkbox" class="form-check-input" id="exampleCheck1">
                            <label class="form-check-label" for="exampleCheck1">ingat saya</label>
                        </div>
                    </div>
                    <button type="submit" class="btn btn-primary py-3 w-100 mb-4">Sign In</button>
                </div>
                <a class="text-center text-info" href="#">Lupa Password</a>
                <a class="text-center text-info" href="#">Download Manual Book</a>
            </div>
        </div>
    </div>
</div>

<x-footer />