<aside id="left-panel" class="left-panel">
    <nav class="navbar navbar-expand-sm navbar-default">
        <div id="main-menu" class="main-menu collapse navbar-collapse">
            <ul class="nav navbar-nav">
                <li class=" @if(url()->current() == url('/dashboard')) active @else '' @endif">
                    <a href="/dashboard"><i class="menu-icon fa fa-laptop"></i>Dashboard </a>
                </li>
                <li class="menu-item-has-children dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-exchange"></i>Transaksi</a>
                    <ul class="sub-menu children dropdown-menu">
                        <li><i class="fa fa-arrow-up"></i><a href="/stok-masuk">Stok Masuk</a></li>
                        <li><i class="fa fa-arrow-down"></i><a href="/stok-keluar">Stok Keluar</a></li>
                        <li><i class="fa fa-cart-plus"></i><a href="ui-badges.html">Pengajuan</a></li>
                    </ul>
                </li>
                <li class="menu-item">
                    <a href="index.html"><i class="menu-icon fa fa-line-chart"></i> Grafik </a>
                </li>
                <li class=" @if(url()->current() == url('/report/stok-gudang')) active @else '' @endif">
                    <a href="/report/stok-gudang"><i class="menu-icon fa fa-laptop"></i>Stok Gudang </a>
                </li>
                {{-- <li class="menu-item-has-children dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-file"></i>Laporan</a>
                    <ul class="sub-menu children dropdown-menu">
                        <li><i class="menu-icon fa fa-line-chart"></i><a href="/report/stok-gudang">Stok Gudang</a></li>
                        <li><i class="menu-icon fa fa-line-chart"></i><a href="forms-basic.html">Stok Gudang Per Program</a></li>
                        <li><i class="menu-icon fa fa-line-chart"></i><a href="forms-basic.html">Mutasi Stok Gudang</a></li>
                        <li><i class="menu-icon fa fa-users"></i><a href="forms-basic.html">Mutasi Aset</a></li>
                        <li><i class="menu-icon fa fa-users"></i><a href="forms-basic.html">Produk Expired</a></li>
                        <li><i class="menu-icon fa fa-users"></i><a href="forms-basic.html">Produk Expired Per Batch</a></li>
                        <li><i class="menu-icon fa fa-users"></i><a href="forms-basic.html">Stok Unit Pelayanan</a></li>
                        <li><i class="menu-icon fa fa-users"></i><a href="forms-basic.html">Kunjungan</a></li>
                    </ul>
                </li> --}}
                <li class="menu-item-has-children dropdown {{ url()->current() == url('/system/pengaturan-instansi') ? "active" : "" }} ">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-cogs"></i>System</a>
                    <ul class="sub-menu children dropdown-menu">
                        <li><i class="menu-icon fa fa-line-chart"></i><a href="charts-chartjs.html">Pengumunan</a></li>
                        <li><i class="menu-icon fa fa-area-chart"></i><a href="/management-user">Management User</a></li>
                        <li><i class="menu-icon fa fa-pie-chart"></i><a href="charts-peity.html">Manajemen Level</a></li>
                        <li><i class="menu-icon fa fa-pie-chart"></i><a href="/system/pengaturan-instansi">Pengaturan Instansi</a></li>
                        <li><i class="menu-icon fa fa-pie-chart"></i><a href="charts-peity.html">Module Generator</a></li>
                    </ul>
                </li>

                <li class="menu-item-has-children dropdown @if(url()->current() == url('/penyedia') || url()->current() == url('/distributor') || url()->current() == url('/sumber-dana') || url()->current() == url('/area') || url()->current() == url('/jenis-program-masuk') || url()->current() == url('/satuan') || url()->current() == url('/kemasan') || url()->current() == url('/aset')) active @else '' @endif">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="menu-icon fa fa-globe"></i> Master Data
                    </a>
                    <ul class="sub-menu children dropdown-menu">
                        <li>
                            <i class="menu-icon fa fa-sign-in"></i><a href="/penyedia">Penyedia</a>
                        </li>
                        <li>
                            <i class="menu-icon fa fa-sign-in"></i><a href="/distributor">Distributor</a>
                        </li>
                        <li>
                            <i class="menu-icon fa fa-sign-in"></i><a href="/sumber-dana">Sumber Dana</a>
                        </li>
                        <li>
                            <i class="menu-icon fa fa-sign-in"></i><a href="/area">Area</a>
                        </li>
                        <li>
                            <i class="menu-icon fa fa-sign-in"></i><a href="/jenis-program-masuk">Nama Program</a>
                        </li>
                        <li>
                            <i class="menu-icon fa fa-sign-in"></i><a href="/satuan">Satuan</a>
                        </li>
                        <li>
                            <i class="menu-icon fa fa-sign-in"></i><a href="/kemasan">Kemasan</a>
                        </li>
                        <li>
                            <i class="menu-icon fa fa-sign-in"></i><a href="/aset">Aset</a>
                        </li>
                        <li>
                            <i class="menu-icon fa fa-sign-in"></i><a href="/harga-perolehan">Detail Aset dan Harga Perolehan</a>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </nav>
</aside>