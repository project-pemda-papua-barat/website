<div class="modal fade" id="{{$idModal}}" tabindex="-1" role="dialog" aria-labelledby="staticModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="staticModalLabel">{!! $title !!}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                @stack('modal-unit-content')
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" id="button-unit-add" class="btn btn-primary" onclick="{{$actionAdd}}">{{$buttonAdd}}</button>
                <button type="button" id="button-unit-edit" class="btn btn-warning" onclick="{{$actionEdit}}">{{$buttonEdit}}</button>
            </div>
        </div>
    </div>
</div>