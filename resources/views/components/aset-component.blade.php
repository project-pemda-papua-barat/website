<div class="modal fade" id="{{$idModal}}" tabindex="-1" role="dialog" aria-labelledby="staticModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="staticModalLabel">{{$title}}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                @if($title == 'Tambah Data Aset II')
                @stack('modal-aset-ii-content')
                @elseif($title == 'Tambah Data Aset III')
                @stack('modal-aset-iii-content')
                @elseif($title == 'Tambah Data Aset IV')
                @stack('modal-aset-iv-content')
                @elseif($title == 'Tambah Data Aset V')
                @stack('modal-aset-v-content')
                @elseif($title == 'Tambah Data Aset VI')
                @stack('modal-aset-vi-content')
                @else
                @stack('modal-aset-vii-content')
                @endif
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" id="{{$idAddButton}}" class="btn btn-primary" onclick="{{$actionAdd}}">{{$buttonAdd}}</button>
                <button type="button" id="{{$idEditButton}}" class="btn btn-warning" onclick="{{$actionEdit}}">{{$buttonEdit}}</button>
            </div>
        </div>
    </div>
</div>