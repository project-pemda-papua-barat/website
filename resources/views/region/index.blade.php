@push('styles')
<link href="{{ asset('lib/sweetalert/sweetalert.css') }}" rel="stylesheet" />
<link href="{{ asset('lib/datatables/jquery.dataTables.min.css') }}" rel="stylesheet" />
@endpush

<x-header title="Region" />
<x-sidenav />
<div class="content" id="authvalidation">
    @push('scripts')
    <script src="{{asset('lib/datatables/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('lib/sweetalert/sweetalert.min.js')}}"></script>
    <script src="{{asset('js/action/region.js')}}"></script>
    @endpush
    <x-footer />