<div class="card">
    <div class="card-body">
        <button type="button"class="btn btn-info btn-rounded my-2" onclick="showFormAddData()">Tambah Data Staff</button>
        <table id="distributor-table" class="table table-striped table-bordered">
            <thead>
                <tr>
                    <th>No</th>
                    <th>Nama </th>
                    <th>NIP</th>
                    <th>Jabatan</th>
                    <th>Status</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody id="staff"></tbody>
        </table>
    </div>
    <div class="card-footer">
        <x-modal title="Tambah Data Penandatangan Staff Instalasi Farmasi" idModal="ModalStaff" buttonAdd="Tambah" buttonEdit="Edit" actionAdd="addStaff()" actionEdit="editPenyedia()">
            @push('modal-content')
                <div class="row form-group">
                    <input type="hidden" id="id_staff" name="id_staff" />
                    <div class="col col-md-3"><label for="nama_penanda_tangan" class=" form-control-label">Nama Penandatangan</label></div>
                    <div class="col-12 col-md-9"><input type="text" id="nama_penanda_tangan" name="nama_penanda_tangan" placeholder="Nama Penanda Tangan" class="form-control"><small class="form-text  text-danger" id="nama_penanda_tangan_validation"></small></div>
                </div>
                <div class="row form-group">
                    <div class="col col-md-3"><label for="nip" class=" form-control-label">NIP</label></div>
                    <div class="col-12 col-md-9"><input type="text" id="nip" name="nip" placeholder="NIP" class="form-control"><small class="form-text  text-danger" id="nip_validation"></small></div>
                </div>
            @endpush
        </x-modal>
    </div>
</div>