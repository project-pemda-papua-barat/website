<div class="card">
    <div class="card-body">
        <button type="button"class="btn btn-info btn-rounded my-2" onclick="showFormAddDataKabid()">Tambah Data KABID</button>
        <table id="distributor-table" class="table table-striped table-bordered">
            <thead>
                <tr>
                    <th>No</th>
                    <th>Nama </th>
                    <th>NIP</th>
                    <th>Jabatan</th>
                    <th>Status</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody id="kabid"></tbody>
        </table>
    </div>
    <div class="card-footer">
        <x-modal-unit title="Tambah Data Penandatangan Kepala Bidang" idModal="ModalKabid" buttonAdd="Tambah" buttonEdit="Edit" actionAdd="addKabid()" actionEdit="editKbid()">
            @push('modal-unit-content')
                <div class="row form-group">
                    <input type="hidden" id="id_kabid" name="id_kabid" />
                    <div class="col col-md-3"><label for="nama_penanda_tangan_kabid" class=" form-control-label">Nama Penandatangan</label></div>
                    <div class="col-12 col-md-9"><input type="text" id="nama_penanda_tangan_kabid" name="nama_penanda_tangan_kabid" placeholder="Nama Penanda Tangan" class="form-control"><small class="form-text  text-danger" id="nama_penanda_tangan_kabid_validation"></small></div>
                </div>
                <div class="row form-group">
                    <div class="col col-md-3"><label for="nip_kabid" class=" form-control-label">NIP</label></div>
                    <div class="col-12 col-md-9"><input type="text" id="nip_kabid" name="nip_kabid" placeholder="NIP" class="form-control"><small class="form-text  text-danger" id="nip_kabid_validation"></small></div>
                </div>
            @endpush
        </x-modal-unit>
    </div>
</div>