<div class="card">
    <div class="card-body">
        <button type="button"class="btn btn-info btn-rounded my-2" onclick="showFormAddDataKepalaDinas()">Tambah Data Kepala Dinas</button>
        <table id="distributor-table" class="table table-striped table-bordered">
            <thead>
                <tr>
                    <th>No</th>
                    <th>Nama </th>
                    <th>NIP</th>
                    <th>Jabatan</th>
                    <th>Status</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody id="kadis"></tbody>
        </table>
    </div>
    <div class="card-footer">
        <x-moda-edit title="Tambah Data Penandatangan Kepala Dinas" idModal="ModalKepalaDinas" buttonAdd="Tambah" buttonEdit="Tambah" actionEdit="addKepalaDinas()">
            @push('modal-content-edit')
                <div class="row form-group">
                    <input type="hidden" id="id_kadis" name="id_kadis" />
                    <div class="col col-md-3"><label for="nama_penanda_tangan_kadis" class=" form-control-label">Nama Penandatangan</label></div>
                    <div class="col-12 col-md-9"><input type="text" id="nama_penanda_tangan_kadis" name="nama_penanda_tangan_kadis" placeholder="Nama Penanda Tangan" class="form-control"><small class="form-text  text-danger" id="nama_penanda_tangan_kadis_validation"></small></div>
                </div>
                <div class="row form-group">
                    <div class="col col-md-3"><label for="nip_kadis" class=" form-control-label">NIP</label></div>
                    <div class="col-12 col-md-9"><input type="text" id="nip_kadis" name="nip_kadis" placeholder="NIP" class="form-control"><small class="form-text  text-danger" id="nip_kadis_validation"></small></div>
                </div>
            @endpush
        </x-moda-edit>
    </div>
</div>