<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>Home::Inventory App Papua Barat</title>
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <meta content="" name="keywords">
    <meta content="" name="description">

    <link href="{{asset('img/favicon.ico')}}" rel="icon">

    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Heebo:wght@400;500;600;700&display=swap" rel="stylesheet">

    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.10.0/css/all.min.css" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.4.1/font/bootstrap-icons.css" rel="stylesheet">

    <link href="{{asset('lib/owlcarousel/assets/owl.carousel.min.css')}}" rel="stylesheet">
    <link href="{{asset('lib/tempusdominus/css/tempusdominus-bootstrap-4.min.css')}}" rel="stylesheet" />

    <link href="{{asset('css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('lib/bxslider/jquery.bxslider.min.css')}}" rel="stylesheet" />
    <link href="{{asset('css/style.css')}}" rel="stylesheet">
    <link href="{{ asset('lib/sweetalert/sweetalert.css') }}" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css" />

</head>

<body>
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <div class="container-fluid">
            <a class="navbar-brand" href="#"><img src="{{asset('img/logo.png')}}" /></a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav me-auto mb-2 mb-lg-0"></ul>
                <form class="d-flex" id="profile"></form>
            </div>
        </div>
    </nav>
    <div class="bxslider">
        <main>
            <div class="card">
                <img src="https://www.jejakpiknik.com/wp-content/uploads/2019/03/dd-1-2-630x380.jpg" height="800" width="auto" class="img-fuild" />
            </div>
        </main>
        <main>
            <div class="card">
                <img src="https://upload.wikimedia.org/wikipedia/commons/3/30/Pegunungan_Arfak_from_Manokwari.jpg" height="800" width="auto" class="img-fuild" />
            </div>
        </main>
        <main>
            <div class="card">
                <img src="https://jejakpiknik.com/wp-content/uploads/2019/03/pantai-di-papua-yang-terkenal-dengan-keindahan-bawah-lautnya-adalah.jpg" height="800" width="auto" class="img-fuild" />
            </div>
        </main>
        <main>
            <div class="card">
                <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/3/33/Pegunungan_Arfak_from_the_Pond_of_Rendani.jpg/1024px-Pegunungan_Arfak_from_the_Pond_of_Rendani.jpg" height="800" width="auto" class="img-fuild" />
            </div>
        </main>
    </div>
    <div class="container-fluid">
        <h4 class="my-4"><i class="fa fa-folder-open text-primary"></i> Pengumuman edit</h4>
        <div id="card-info">
            <div class="card shadow mx-2">
                <!-- <img class="card-img-top" src="..." alt="Card image cap"> -->
                <div class="card-body">
                    <h6 class="card-title">INSTRUKSI GUBERNUR PAPUA BARAT NOMOR : 440/11/TAHUN 2021</h6>
                    <p class="card-text">16 Mei 2018, 21:53:27</p>
                    <p class="card-text">Ac at diam tincidunt lorem non ultrices. Malesuada tortor ullamcorper velit, nibh. Ac at diam tincidunt lorem non ultrices. Malesuada tortor ullamcorper velit, nibh.Ac at diam tincidunt lorem non ultrices. Malesuada tortor ullamcorper velit, nibh.</p>

                </div>
            </div>
            <div class="card shadow mx-2">
                <!-- <img class="card-img-top" src="..." alt="Card image cap"> -->
                <div class="card-body">
                    <h6 class="card-title">INSTRUKSI GUBERNUR PAPUA BARAT NOMOR : 440/11/TAHUN 2021</h6>
                    <p class="card-text">16 Mei 2018, 21:53:27</p>
                    <p class="card-text">Ac at diam tincidunt lorem non ultrices. Malesuada tortor ullamcorper velit, nibh. Ac at diam tincidunt lorem non ultrices. Malesuada tortor ullamcorper velit, nibh.Ac at diam tincidunt lorem non ultrices. Malesuada tortor ullamcorper velit, nibh.</p>

                </div>
            </div>
            <div class="card shadow mx-2">
                <!-- <img class="card-img-top" src="..." alt="Card image cap"> -->
                <div class="card-body">
                    <h6 class="card-title">INSTRUKSI GUBERNUR PAPUA BARAT NOMOR : 440/11/TAHUN 2021</h6>
                    <p class="card-text">16 Mei 2018, 21:53:27</p>
                    <p class="card-text">Ac at diam tincidunt lorem non ultrices. Malesuada tortor ullamcorper velit, nibh. Ac at diam tincidunt lorem non ultrices. Malesuada tortor ullamcorper velit, nibh.Ac at diam tincidunt lorem non ultrices. Malesuada tortor ullamcorper velit, nibh.</p>

                </div>
            </div>
            <div class="card shadow mx-2">
                <!-- <img class="card-img-top" src="..." alt="Card image cap"> -->
                <div class="card-body">
                    <h6 class="card-title">INSTRUKSI GUBERNUR PAPUA BARAT NOMOR : 440/11/TAHUN 2021</h6>
                    <p class="card-text">16 Mei 2018, 21:53:27</p>
                    <p class="card-text">Ac at diam tincidunt lorem non ultrices. Malesuada tortor ullamcorper velit, nibh. Ac at diam tincidunt lorem non ultrices. Malesuada tortor ullamcorper velit, nibh.Ac at diam tincidunt lorem non ultrices. Malesuada tortor ullamcorper velit, nibh.</p>

                </div>
            </div>
        </div>
    </div>

    <div class="container-fluid bg-info text-center text-light mb-0 mt-2">
        <p class="py-4 mb-0">Pemerintah Provinsi Papua Barat <span id="year"></span> © copyright </p>
    </div>



    </main>

    @push('scripts')
    <script src="{{asset('lib/sweetalert/sweetalert.min.js')}}"></script>
    <script src="{{asset('lib/bxslider/jquery.bxslider.min.js')}}"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
    <script>
        var endpoint = `${window.location.origin}/service`;
        var token = localStorage.getItem('token');
        var date = new Date();
        var dateNow = date.getFullYear();

        $(document).ready(function() {
            $('#year').html(dateNow);
            $('.bxslider').bxSlider({
                auto: true,
                slideMargin: 100,
                useCSS: true,

            });
            $('#card-info').slick({
                centerMode: true,
                centerPadding: '60px',
                slidesToShow: 3,
                arrows: false,
                responsive: [{
                        breakpoint: 768,
                        settings: {
                            arrows: false,
                            centerMode: true,
                            centerPadding: '100px',
                            slidesToShow: 3
                        }
                    },
                    {
                        breakpoint: 480,
                        settings: {
                            arrows: false,
                            centerMode: true,
                            centerPadding: '40px',
                            slidesToShow: 1
                        }
                    }
                ]
            });

            $('#profile').html("Loading...");
            $.ajax({
                type: 'get',
                url: `${endpoint}/profile`,
                cache: false,
                processData: false,
                contentType: false,
                headers: {
                    "Authorization": `Bearer ${token}`,
                },
                success: function(result) {
                    $('#profile').html(`
                    <div class="nav-item dropdown">
                <a href="#" class="nav-link dropdown-toggle" data-bs-toggle="dropdown">
                    <img class="rounded-circle me-lg-2" src="img/user.jpg" alt="" style="width: 40px; height: 40px;">
                    <span class="d-none d-lg-inline-flex">${result.name}</span>
                </a>
                <div class="dropdown-menu dropdown-menu-end bg-light border-0 rounded-0 rounded-bottom m-0">
                    <a href="/dashboard" class="dropdown-item">Dashboard</a>
                    <a href="#" class="dropdown-item">My Profile</a>
                    <a href="#" class="dropdown-item">Settings</a>
                    <a href="#" onclick="logout()" class="dropdown-item">Log Out</a>
                </div>
            </div>
                    `);
                },
                error: function(err) {
                    $('#profile').html(`<a class="btn btn-info text-light" href="/login">Sign In</a>`);
                }
            });
        });

        function logout() {
            swal({
                title: 'peringatan!',
                text: `Apakah anda yakin akan melakukan logout?`,
                icon: 'warning',
                button: 'OK'
            }).then(function(result) {
                if (result) {
                    localStorage.removeItem('token');
                    window.location.href = "/login";
                }
            });

        }
    </script>
    @endpush

    <x-footer />