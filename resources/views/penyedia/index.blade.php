@push('styles')
<link href="{{ asset('lib/sweetalert/sweetalert.css') }}" rel="stylesheet" />
<link href="https://cdn.datatables.net/1.12.1/css/jquery.dataTables.min.css" rel="stylesheet" />
<link href="https://cdn.datatables.net/buttons/2.2.3/css/buttons.dataTables.min.css" rel="stylesheet" />
@endpush

<x-header-auth title="Penyedia" />
<x-sidebar-auth />
<div id="right-panel" class="right-panel">
    <x-navbar-auth />
    <div class="content">
        <div class="animated fadeIn">
            <div class="row">
                <div class="col-xl-12 col-md-12 col-12">
                    <div class="card">
                        <div class="card-header">
                            <strong class="card-title">Master Data Penyedia</strong>
                        </div>
                        <div class="card-body">
                            <table id="penyedia-table" class="table table-striped table-bordered">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Nama Penyedia</th>
                                        <th>Alamat</th>
                                        <th>Detail Alamat</th>
                                        <th>Nomor Kontak</th>
                                        <th>Keterangan</th>
                                        <th>Status</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <x-modal title="Tambah Data Penyedia" idModal="modalPenyedia" buttonAdd="Tambah" buttonEdit="Edit" actionAdd="addPeyedia()" actionEdit="editPenyedia()">
        @push('modal-content')
        <div class="row form-group" id="is_active_status"> </div>
        <div class="row form-group">
            <input type="hidden" id="id_penyedia" name="id_penyedia" />
            <div class="col col-md-3"><label for="nama_penyedia" class=" form-control-label">Nama Penyedia</label></div>
            <div class="col-12 col-md-9"><input type="text" id="nama_penyedia" name="nama_penyedia" placeholder="Nama Penyedia" class="form-control"><small class="form-text  text-danger" id="nama_penyedia_validation"></small></div>
        </div>
        <div class="row form-group">
            <div class="col col-md-3"><label for="alamat_penyedia" class=" form-control-label">Alamat Penyedia</label></div>
            <div class="col-12 col-md-9"><input type="text" name="alamat_penyedia" id="alamat_penyedia" rows="9" placeholder="Alamat Penyedia..." class="form-control" /><small class="form-text  text-danger" id="alamat_penyedia_validation"></small></div>
        </div>
        <div class="row form-group">
            <div class="col col-md-3"><label for="alamat_penyedia" class=" form-control-label">Detail Alamat Penyedia</label></div>
            <div class="col-12 col-md-9"><input type="text" name="detail_alamat" id="detail_alamat" rows="9" placeholder="Nama Gedung, lantai dll" class="form-control" /><small class="form-text  text-danger" id="detail_alamat_penyedia_validation"></small></div>
        </div>
        <div class="row form-group">
            <div class="col col-md-3"><label for="no_contact_penyedia" class=" form-control-label">Nomor Kontak Penyedia</label></div>
            <div class="col-12 col-md-9"><input type="text" id="no_contact_penyedia" name="no_contact_penyedia" placeholder="Nomor Kontak Penyedia" class="form-control"><small class="form-text  text-danger" id="no_contact_penyedia_validation"></small></div>
        </div>
        <div class="row form-group">
            <div class="col col-md-3"><label for="keterangan" class=" form-control-label">Nomor Kontak Penyedia</label></div>
            <div class="col-12 col-md-9"><input type="text" id="keterangan" name="keterangan" placeholder="Keterangan" class="form-control"></div>
        </div>
        <input type="hidden" name="lat" id="lat" />
        <input type="hidden" name="long" id="long" />
        @endpush
    </x-modal>
</div>
@push('scripts')
<script src="{{asset('dist/assets/js/lib/data-table/datatables.min.js')}}"></script>
<script src="{{asset('dist/assets/js/lib/data-table/dataTables.bootstrap.min.js')}}"></script>
<script src="{{asset('dist/assets/js/lib/data-table/dataTables.buttons.min.js')}}"></script>
<script src="{{asset('dist/assets/js/lib/data-table/buttons.bootstrap.min.js')}}"></script>
<script src="{{asset('dist/assets/js/lib/data-table/jszip.min.js')}}"></script>
<script src="{{asset('dist/assets/js/lib/data-table/vfs_fonts.js')}}"></script>
<script src="{{asset('dist/assets/js/lib/data-table/buttons.html5.min.js')}}"></script>
<script src="{{asset('dist/assets/js/lib/data-table/buttons.print.min.js')}}"></script>
<script src="{{asset('dist/assets/js/lib/data-table/buttons.colVis.min.js')}}"></script>
<script src="{{asset('dist/assets/js/init/datatables-init.js')}}"></script>
<script src="{{asset('lib/sweetalert/sweetalert.min.js')}}"></script>
<script src="{{asset('js/action/authValidation.js')}}"></script>
<script src="{{asset('js/action/penyedia.js')}}"></script>
<script type="text/javascript" src="https://maps.google.com/maps/api/js?key={{ env('GOOGLE_MAP_KEY') }}&libraries=places"></script>

<script>
    google.maps.event.addDomListener(window, 'load', initialize);

    function initialize() {
        var input = document.getElementById('alamat_penyedia');
        var autocomplete = new google.maps.places.Autocomplete(input);
        autocomplete.addListener('place_changed', function() {
            var place = autocomplete.getPlace();
            $('#lat').val(place.geometry['location'].lat());
            $('#long').val(place.geometry['location'].lng());
        });
    }
</script>
@endpush
<x-footer-auth />