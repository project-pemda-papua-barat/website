@push('styles')
<link href="{{ asset('lib/sweetalert/sweetalert.css') }}" rel="stylesheet" />
<link href="https://cdn.datatables.net/1.12.1/css/jquery.dataTables.min.css" rel="stylesheet" />
<link href="https://cdn.datatables.net/buttons/2.2.3/css/buttons.dataTables.min.css" rel="stylesheet" />
@endpush

<x-header-auth title="Kemasan" />
<x-sidebar-auth />
<div id="right-panel" class="right-panel">
    <x-navbar-auth />
    <div class="content">
        <div class="animated fadeIn">
            <div class="row">
                <div class="col-xl-12 col-md-12 col-12">
                    <div class="card">
                        <div class="card-header">
                            <strong class="card-title">Master Data Kemasan</strong>
                        </div>
                        <div class="card-body">
                            <table id="kemasan-table" class="table table-striped table-bordered">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Nama Kemasan</th>
                                        <th>Keterangan</th>
                                        <th>Status</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <x-modal title="Tambah Data Kemasan" idModal="modalKemasan" buttonAdd="Tambah" buttonEdit="Edit" actionAdd="addKemasan()" actionEdit="editKemasan()">
        @push('modal-content')
        <div class="row form-group" id="is_active_status"> </div>
        <div class="row form-group">
            <input type="hidden" id="id_kemasan" name="id_kemasan" />
            <div class="col col-md-3"><label for="nama_kemasan" class=" form-control-label">Nama kemasan</label></div>
            <div class="col-12 col-md-9"><input type="text" id="nama_kemasan" name="nama_kemasan" placeholder="Nama kemasan" class="form-control"><small class="form-text  text-danger" id="nama_kemasan_validation"></small></div>
        </div>
        <div class="row form-group">
            <div class="col col-md-3"><label for="keterangan" class=" form-control-label">Nomor Kontak Penyedia</label></div>
            <div class="col-12 col-md-9"><input type="text" id="keterangan" name="keterangan" placeholder="Keterangan" class="form-control"></div>
        </div>
        @endpush
    </x-modal>
</div>
@push('scripts')
<script src="{{asset('dist/assets/js/lib/data-table/datatables.min.js')}}"></script>
<script src="{{asset('dist/assets/js/lib/data-table/dataTables.bootstrap.min.js')}}"></script>
<script src="{{asset('dist/assets/js/lib/data-table/dataTables.buttons.min.js')}}"></script>
<script src="{{asset('dist/assets/js/lib/data-table/buttons.bootstrap.min.js')}}"></script>
<script src="{{asset('dist/assets/js/lib/data-table/jszip.min.js')}}"></script>
<script src="{{asset('dist/assets/js/lib/data-table/vfs_fonts.js')}}"></script>
<script src="{{asset('dist/assets/js/lib/data-table/buttons.html5.min.js')}}"></script>
<script src="{{asset('dist/assets/js/lib/data-table/buttons.print.min.js')}}"></script>
<script src="{{asset('dist/assets/js/lib/data-table/buttons.colVis.min.js')}}"></script>
<script src="{{asset('dist/assets/js/init/datatables-init.js')}}"></script>
<script src="{{asset('lib/sweetalert/sweetalert.min.js')}}"></script>
<script src="{{asset('js/action/authValidation.js')}}"></script>
<script src="{{asset('js/action/kemasan.js')}}"></script>
@endpush
<x-footer-auth />