<!DOCTYPE html>
<html>
<head>
    <title>InventoryApp::Report Stock Gudang</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <style>
            .page-break {
                page-break-after: always;
            }
            </style>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/2.5.1/jspdf.umd.min.js"></script>
</head>
<body>
    <div class="page-break"  id="report-gudang-table" >
        <h5 class="text-center">Report {!! $data !!}</h5>
        <table class="table table-striped table-bordered" width="100%">
            <thead>
                <tr>
                    <th>No</th>
                    <th>Nomor Transaksi / Waktu</th>
                    <th>Nomor Aset</th>
                    <th>Nama Detail Aset</th>
                    <th>Sumber Dana / Tahun</th>
                    <th>Jenis Program</th>
                    <th>Penyedia</th>
                    <th>Distributor</th>
                    <th>Jenis Transaksi</th>
                    <th>Tanggal Expired</th>
                    <th>Harga satuan</th>
                    <th>Jumlah</th>
                    <th>Total</th>
                </tr>
            </thead>
            <tbody id="result-content">
            </tbody>
        </table>
    </div>
    @push('scripts')
    <script defer>
        var table = $('#result-content'); 
        var dataPrint = localStorage.getItem('dataPrint')
        $(document).ready(function () {
            try {  table.html(`${dataPrint}`); } catch (e) { window.onload = window.print; }
        });
        const doc = new jsPDF({
            orientation: "landscape",
            unit: "in",
            format: [4, 2]
        });
        var source = window.document.getElementsByTagName("body")[0];
        doc.text($('#report-gudang-table'), 1, 1);
        doc.save("two-by-four.pdf");
    </script>
    @endpush
<x-footer />