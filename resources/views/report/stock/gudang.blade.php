@push('styles')
<link href="{{ asset('lib/sweetalert/sweetalert.css') }}" rel="stylesheet" />
<link href="https://cdn.datatables.net/1.12.1/css/jquery.dataTables.min.css" rel="stylesheet" />
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
<style>
    .loader {
        border: 16px solid #f3f3f3;
        border-radius: 50%;
        border-top: 16px solid blue;
        border-right: 16px solid green;
        border-bottom: 16px solid red;
        width: 80px;
        height: 80px;
        -webkit-animation: spin 2s linear infinite;
        animation: spin 2s linear infinite;
    }

    @-webkit-keyframes spin {
        0% {
            -webkit-transform: rotate(0deg);
        }

        100% {
            -webkit-transform: rotate(360deg);
        }
    }

    @keyframes spin {
        0% {
            transform: rotate(0deg);
        }

        100% {
            transform: rotate(360deg);
        }
    }
</style>
@endpush
<x-header-auth title="Laporan::Stok Gudang" />
<x-sidebar-auth />
<div id="right-panel" class="right-panel">
    <x-navbar-auth />
    <div class="content">
        <div class="animated fadeIn">
            <div class="row">
                <div class="col-xl-12 col-md-12 col-12">
                    <div class="card">
                        <div class="card-header">
                            <strong class="card-title d-flex justify-content-center">Laporan Stok Gudang</strong><br />
                            <div class="row">
                                <div class="col-lg-4 col-md-12 col-12">
                                    <label for="start_date" class=" form-control-label">Pilih Tanggal Awal</label>
                                    <input type="date" class="form-control" name="start_date" id="start_date" />
                                    <small class="form-text  text-danger" id="start_date_validation"></small>
                                </div>
                                <div class="col-lg-4 col-md-12 col-12">
                                    <label for="end_date" class=" form-control-label">Pilih Tanggal Akhir</label>
                                    <input type="date" class="form-control" name="end_date" id="end_date" />
                                    <small class="form-text  text-danger" id="end_date_validation"></small>
                                </div>
                                <div class="col-lg-4 col-md-12 col-12">
                                    <label for="unit_id" class=" form-control-label">Pilih Unit</label>
                                    <select type="select" class="form-control" name="unit_id" id="unit_id"  width="100%">
                                        <option value="">pililh unit</option>
                                        @foreach ($unit as $u)
                                        <option value="{{ $u->id }}">{{ $u->text }}</option>
                                        @endforeach
                                    </select>
                                    <small class="form-text  text-danger" id="unit_id_validation"></small>
                                </div>
                            </div>
                            <div class="row" id="filter-action">
                                <div class="col-lg-4 col-md-12 col-12 my-2">
                                    <select type="select" class="form-contro py-2" name="penyedia_id" id="penyedia_id" width="100%">
                                        <option value="">Filter By Penyedia</option>
                                        @foreach ($penyedia as $p)
                                        <option value="{{ $p->text }}">{{ $p->text }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-lg-4 col-md-12 col-12 my-2">
                                    <select type="select" class="form-control  py-2" name="distributor_id" id="distributor_id"
                                        width="100%">
                                        <option value="">Filter By Distributor</option>
                                        @foreach ($distributor as $d)
                                        <option value="{{ $d->text }}">{{ $d->text }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-lg-4 col-md-12 col-12 my-2">
                                    <select type="select" class="form-control" name="sumber_dana_id" id="sumber_dana_id"
                                        width="100%">
                                        <option value="">Filter By Sumber Dana</option>
                                        @foreach ($sumberDana as $s)
                                        <option value="{{ $s->text }}">{{ $s->text }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-lg-4 col-md-12 col-12 my-2">
                                    <select type="select" class="form-control" name="jenis_program_id"
                                        id="jenis_program_id" width="100%">
                                        <option value="">Filter By Jenis Program</option>
                                        @foreach ($jenisProgram as $j)
                                        <option value="{{ $j->text }}">{{ $j->text }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-lg-4 col-md-12 col-12 my-2">
                                    <select type="select" class="form-control" name="listAset"
                                        id="listAset" width="100%">
                                        <option value="">Filter By Detail Aset</option>
                                        @foreach ($listAset as $aset)
                                        <option value="{{ $aset['text'] }}">{{ $aset['text'] }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-12 col-md-12 col-12">
                                    <div class="d-flex justify-content-between my-2">
                                        <div class="d-flex justify-content-start">
                                            <button class="btn btn-info btn-rounded" id="subfilter">Sub
                                                Filter</button>
                                            <button class="btn btn-success btn-rounded mx-2" id="reset">Reset</button>
                                            <button class="btn btn-danger btn-rounded" id="print">Print</button>
                                        </div>
                                        <button class="btn btn-primary btn-rounded" id="filter">Filter</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-body" id="result-filter">
                            <div class="d-flex justify-content-center d-none">
                                <div class="loader d-none" id="loader"></div>
                            </div>
                            <table id="report-gudang-table" class="table table-striped table-bordered" width="100%">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Nomor Transaksi / Waktu</th>
                                        <th>Nomor Aset</th>
                                        <th>Nama Detail Aset</th>
                                        <th>Sumber Dana / Tahun</th>
                                        <th>Jenis Program</th>
                                        <th>Penyedia</th>
                                        <th>Distributor</th>
                                        <th>Jenis Transaksi</th>
                                        <th>Tanggal Expired</th>
                                        <th>Harga satuan</th>
                                        <th>Jumlah</th>
                                        <th>Total</th>
                                    </tr>
                                </thead>
                                <tbody id="result-content"></tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <x-detail-aset title="List Detail Aset" idModal="listAset">
        @push('modal-content-detail-aset')
        <div id="content-list">
            <div class="row" id="tag-download"></div>
            <div class="container my-2" id="list-detail-aset"></div>
        </div>
        @endpush
    </x-detail-aset>

</div>
@push('scripts')
<script src="{{asset('dist/assets/js/lib/data-table/datatables.min.js')}}"></script>
<script src="{{asset('dist/assets/js/lib/data-table/dataTables.bootstrap.min.js')}}"></script>
<script src="{{asset('dist/assets/js/lib/data-table/jszip.min.js')}}"></script>
<script src="{{asset('dist/assets/js/lib/data-table/vfs_fonts.js')}}"></script>
<script src="{{asset('dist/assets/js/init/datatables-init.js')}}"></script>
<script src="{{asset('lib/sweetalert/sweetalert.min.js')}}"></script>
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
{{-- <script src="{{asset('js/action/authValidationGudang.js')}}"></script> --}}
<script>
    var endpoint = `${window.location.origin}/service`;
var baseUrl = `${window.location.origin}`;
var unitName = '';
var token = localStorage.getItem("token");
var startDateValue = 0;
var endDateValue = 0;
var unitIdValue = 0;
var date = new Date();
var data = [];
var dataJSon = [];
var dataFilter = [];
var penyediaFilterValue = $('#penyedia_id');
var distributorFilterValue = $('#distributor_id');
var sumberDanaFilterValue = $('#sumber_dana_id');
var jenisProgramFilterValue = $('#jenis_program_id');
var asetFilterValue = $('#listAset');
var collectedDataFilter = {
    penyediaFilterValue: "",
    distributorFilterValue: "",
    sumberDanaFilterValue: "",
    jenisProgramFilterValue: "",
    asetFilterValue: ""
}

var formatRupiah = (money) => {
    return new Intl.NumberFormat("id-ID", {
        minimumFractionDigits: 0,
    }).format(money);
};

(function ($) {
    "use strict";
    var content = $("#authvalidation");

    $(document).ready(function () {
        $('#report-gudang-table').addClass('d-none');
        $('#filter-action').addClass('d-none');
        $('#subfilter').addClass('d-none');
        $('#reset').addClass('d-none');
        $('#print').addClass('d-none');
        $("#unit_id").select2();
        $("#penyedia_id").select2({dropdownAutoWidth: 'true', width: '100%'});
        $("#distributor_id").select2({dropdownAutoWidth: 'true', width: '100%'});
        $("#sumber_dana_id").select2({dropdownAutoWidth: 'true', width: '100%'});
        $("#jenis_program_id").select2({dropdownAutoWidth: 'true', width: '100%'});
        $("#listAset").select2({dropdownAutoWidth: 'true', width: '100%'});

        var year = date.getFullYear();
        $("#year").html(year);
        content.html("Loading...");
        $.ajax({
            type: "get",
            url: `${endpoint}/profile`,
            cache: false,
            processData: false,
            contentType: false,
            headers: {
                Authorization: `Bearer ${token}`,
            },
            success: function (result) {
                content.html(`<a href="#" class="dropdown-toggle active" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <img class="user-avatar rounded-circle active" src="${result.profile_pict}" alt="User Avatar">
                <span class="ml-2">${result.name}</span>
            </a>
            <div class="user-menu dropdown-menu">
                <a class="nav-link" href="#"><i class="fa fa- user"></i>My Profile</a>
                <a class="nav-link" href="#"><i class="fa fa- user"></i>Notifications <span class="count">0</span></a>
                <a class="nav-link" href="#"><i class="fa fa -cog"></i>Settings</a>
                <a class="nav-link" href="#" onclick="logout()"><i class="fa fa-power -off"></i>Logout</a>
            </div>
                    `);
                    $('#filter').attr('disabled', true);
            },
            error: function (err) {
                if (err.status == 401) {
                    window.location.href = "/login";
                }
            },
        });
    });

    $('#start_date').on('change', function () {
        let startDate = new Date($('#start_date').val());
        startDateValue = startDate.getTime();
        if(startDateValue != 0 && endDateValue != 0 && unitIdValue != 0)  $('#filter').attr('disabled', false);
    });    

    $('#end_date').on('change', function () {
        let endDate = new Date($('#end_date').val());
        endDateValue = endDate.getTime();
        if(startDateValue != 0 && endDateValue != 0 && unitIdValue != 0)  $('#filter').attr('disabled', false);
        console.log(endDateValue - startDateValue);
    });   

    $('#unit_id').on('change', function () {
        unitIdValue = $('#unit_id').val();
        unitName = $('#unit_id option:selected').text();
        if (startDateValue != 0 && endDateValue != 0 && unitIdValue != 0) $('#filter').attr('disabled', false);
    });

    $('#filter').on('click', function () {
        $('#loader').removeClass('d-none');
        $('#report-gudang-table').addClass('d-none');
        $('#filter-action').addClass('d-none');
        $('#subfilter').addClass('d-none');
        $('#reset').addClass('d-none');
        $('#print').addClass('d-none');
        if ((endDateValue - startDateValue) < 0) {
            swal({
                title: "Error!",
                text: `Tanggal awal dan akhir tidak valid! tanggal akhir harus lebih besar daripada tanggal awal!`,
                icon: "error",
                button: "OK",
            }).then(() => {
                $('#filter').attr('disabled', true);
                $('#result-filter').html('');
            })
        }

        if ((endDateValue - startDateValue) > 0) {
            let dataInput = {
                'start_date': $('#start_date').val(),
                'end_date': $('#end_date').val(),
                'unit_id' : unitIdValue
            }
            $.ajax({
                url: `${endpoint}/report/unit`,
                type: "POST",
                headers: {
                    Authorization: `Bearer ${token}`,
                    'Accept': 'Application/json'
                },
                data: dataInput,
                success: res => {
                    dataJSon = res;
                    $('#loader').addClass('d-none');
                    $('#filter-action').removeClass('d-none');
                    $('#subfilter').removeClass('d-none');
                    $('#reset').removeClass('d-none');
                    $('#print').removeClass('d-none');
                    $('#report-gudang-table').removeClass('d-none');
                    if (res.length > 0) {
                        data = dataJSon.map((ds, index, dataSOurce) => {
                            if (index === dataJSon.length - 1) {
                                return `<tr>
                                            <td>${index + 1}</td>
                                            <td>${ds.no_transaksi} / ${ds.created_at}</td>
                                            <td>${ds.nomor_aset}</td>
                                            <td>${ds.nama_detail_aset}</td>
                                            <td>${ds.nama_sumber_dana} / ${ds.tahun_pengadaan}</td>
                                            <td>${ds.nama_program}</td>
                                            <td>${ds.nama_penyedia}</td>
                                            <td>${ds.nama_distributor}</td>
                                            <td>${ds.qty < 0 ? '<i>Transaksi Stok Out</i>' : '<i>Transaksi Stok In</i>'}</td>
                                            <td>${ds.expired_date}</td>
                                            <td>Rp ${formatRupiah(ds.harga_satuan)}</td>
                                            <td>${ds.qty < 0 ? -(ds.qty) : ds.qty}</td>
                                            <td>Rp ${formatRupiah(ds.total < 0 ? -(ds.total) : ds.total)}</td>
                                        </tr>
                                        <tr>
                                            <td colspan="11"><span class="font-weight-bold">Total</span></td>
                                            <td>${dataSOurce.map(data => data.qty).reduce((a, b) =>a+b, 0)}</td>
                                            <td>Rp ${formatRupiah(dataSOurce.map(data => data.total).reduce((a, b) =>a+b, 0))}</td>
                                        </tr>`
                            }
                            return `<tr>
                                <td>${index + 1}</td>
                                <td>${ds.no_transaksi} / ${ds.created_at}</td>
                                <td>${ds.nomor_aset}</td>
                                <td>${ds.nama_detail_aset}</td>
                                <td>${ds.nama_sumber_dana} / ${ds.tahun_pengadaan}</td>
                                <td>${ds.nama_program}</td>
                                <td>${ds.nama_penyedia}</td>
                                <td>${ds.nama_distributor}</td>
                                <td>${ds.qty < 0 ? '<i>Transaksi Stok Out</i>' : '<i>Transaksi Stok In</i>'}</td>
                                <td>${ds.expired_date}</td>
                                <td>Rp ${formatRupiah(ds.harga_satuan)}</td>
                                <td>${ds.qty < 0 ? -(ds.qty) : ds.qty}</td>
                                <td>Rp ${formatRupiah(ds.total < 0 ? -(ds.total) : ds.total)}</td>
                            </tr>`
                        });
                        $('#result-content').html(data);
                        console.log(data)
                    }
                },
                err: err => console.log(err)
            });
        }
    });

    $("#reset").on("click", function () {
        $("#filter-action").removeClass("d-none");
        $("#subfilter").removeClass("d-none");
        $("#reset").removeClass("d-none");
        $("#report-gudang-table").removeClass("d-none");
        collectedDataFilter.penyediaFilterValue = "";
        penyediaFilterValue.val("")
        collectedDataFilter.distributorFilterValue = "";
        distributorFilterValue.val("")
        collectedDataFilter.sumberDanaFilterValue = "";
        sumberDanaFilterValue.val("")
        collectedDataFilter.jenisProgramFilterValue = "";
        jenisProgramFilterValue.val("");
        localStorage.setItem('dataPrint', dataJSon);
        let dataReset = dataJSon.map((ds, index, dataSOurce) => {
            if (index === dataJSon.length - 1) {
                return `<tr>
                            <td>${index + 1}</td>
                            <td>${ds.no_transaksi} / ${ds.created_at}</td>
                            <td>${ds.nomor_aset}</td>
                            <td>${ds.nama_detail_aset}</td>
                            <td>${ds.nama_sumber_dana} / ${ds.tahun_pengadaan}</td>
                            <td>${ds.nama_program}</td>
                            <td>${ds.nama_penyedia}</td>
                            <td>${ds.nama_distributor}</td>
                            <td>${ds.qty < 0 ? '<i>Transaksi Stok Out</i>' : '<i>Transaksi Stok In</i>'}</td>
                            <td>${ds.expired_date}</td>
                            <td>Rp ${formatRupiah(ds.harga_satuan)}</td>
                            <td>${ds.qty < 0 ? -(ds.qty) : ds.qty}</td>
                            <td>Rp ${formatRupiah(ds.total < 0 ? -(ds.total) : ds.total)}</td>
                        </tr>
                        <tr>
                            <td colspan="11"><span class="font-weight-bold">Total</span></td>
                            <td>${dataSOurce.map(data => data.qty).reduce((a, b) =>a+b, 0)}</td>
                            <td>Rp ${formatRupiah(dataSOurce.map(data => data.total).reduce((a, b) =>a+b, 0))}</td>
                        </tr>`
            }
            return `<tr>
                <td>${index + 1}</td>
                <td>${ds.no_transaksi} / ${ds.created_at}</td>
                <td>${ds.nomor_aset}</td>
                <td>${ds.nama_detail_aset}</td>
                <td>${ds.nama_sumber_dana} / ${ds.tahun_pengadaan}</td>
                <td>${ds.nama_program}</td>
                <td>${ds.nama_penyedia}</td>
                <td>${ds.nama_distributor}</td>
                <td>${ds.qty < 0 ? '<i>Transaksi Stok Out</i>' : '<i>Transaksi Stok In</i>'}</td>
                <td>${ds.expired_date}</td>
                <td>Rp ${formatRupiah(ds.harga_satuan)}</td>
                <td>${ds.qty < 0 ? -(ds.qty) : ds.qty}</td>
                <td>Rp ${formatRupiah(ds.total < 0 ? -(ds.total) : ds.total)}</td>
            </tr>`
        });
        $("#result-content").html(dataReset);
    });



    asetFilterValue.on('change', function () {
        collectedDataFilter.asetFilterValue = asetFilterValue.val().split("-")[0];
        console.log(collectedDataFilter)
    })

    jenisProgramFilterValue.on('change', function () {
        collectedDataFilter.jenisProgramFilterValue = jenisProgramFilterValue.val();
        console.log(collectedDataFilter)
    });
    sumberDanaFilterValue.on('change', function () {
        collectedDataFilter.sumberDanaFilterValue = sumberDanaFilterValue.val();
        console.log(collectedDataFilter)
    });

    distributorFilterValue.on('change', function () {
        collectedDataFilter.distributorFilterValue = distributorFilterValue.val();
        console.log(collectedDataFilter)
    });

    penyediaFilterValue.on('change', function () {
        collectedDataFilter.penyediaFilterValue = penyediaFilterValue.val();
        console.log(collectedDataFilter)
    });

    $('#subfilter').on('click', function () {
        if (penyediaFilterValue.val() === "" && distributorFilterValue.val() === "" && sumberDanaFilterValue.val() === "" && jenisProgramFilterValue.val() === "" && asetFilterValue.val() === "") swal({
            title: "peringatan!",
            text: "Anda belum megisi filter yang harus diisi!",
            icon: "warning",
            button: "OK",
        });

        dataFilter = dataJSon.filter(val => {
            return val.nama_penyedia === collectedDataFilter.penyediaFilterValue ||
                val.nama_distributor === collectedDataFilter.distributorFilterValue ||
                val.nama_sumber_dana === collectedDataFilter.sumberDanaFilterValue ||
                val.nama_program === collectedDataFilter.jenisProgramFilterValue ||
                val.nama_detail_aset === collectedDataFilter.asetFilterValue;
        });
        
        let datafilterShow = dataFilter.map((df, index) => {
            // console.log(dataFilterSOur)
            if (index === dataFilter.length - 1) {
                return `<tr>
                            <td>${index + 1}</td>
                            <td>${df.no_transaksi} / ${df.created_at}</td>
                            <td>${df.nomor_aset}</td>
                            <td>${df.nama_detail_aset}</td>
                            <td>${df.nama_sumber_dana} / ${df.tahun_pengadaan}</td>
                            <td>${df.nama_program}</td>
                            <td>${df.nama_penyedia}</td>
                            <td>${df.nama_distributor}</td>
                            <td>${df.qty < 0 ? '<i>Transaksi Stok Out</i>' : '<i>Transaksi Stok In</i>'}</td>
                            <td>${df.expired_date}</td>
                            <td>Rp ${formatRupiah(df.harga_satuan)}</td>
                            <td>${df.qty < 0 ? -(df.qty) : df.qty}</td>
                            <td>Rp ${formatRupiah(df.total < 0 ? -(df.total) : df.total)}</td>
                        </tr>
                        <tr>
                            <td colspan="11"><span class="font-weight-bold">Total</span></td>
                            <td>${dataFilter.map(data => data.qty).reduce((a, b) =>a+b, 0)}</td>
                            <td>Rp ${formatRupiah(dataFilter.map(data => data.total).reduce((a, b) =>a+b, 0))}</td>
                        </tr>`
            }
            return `<tr>
                <td>${index + 1}</td>
                <td>${df.no_transaksi} / ${df.created_at}</td>
                <td>${df.nomor_aset}</td>
                <td>${df.nama_detail_aset}</td>
                <td>${df.nama_sumber_dana} / ${df.tahun_pengadaan}</td>
                <td>${df.nama_program}</td>
                <td>${df.nama_penyedia}</td>
                <td>${df.nama_distributor}</td>
                <td>${df.qty < 0 ? '<i>Transaksi Stok Out</i>' : '<i>Transaksi Stok In</i>'}</td>
                <td>${df.expired_date}</td>
                <td>Rp ${formatRupiah(df.harga_satuan)}</td>
                <td>${df.qty < 0 ? -(df.qty) : df.qty}</td>
                <td>Rp ${formatRupiah(df.total < 0 ? -(df.total) : df.total)}</td>
            </tr>`
        });
        $('#result-content').html(datafilterShow);
    });

    $('#print').on('click', function () {
        window.open(`${baseUrl}/report/stok-gudang/print/${unitName}`, '_blank').focus();
    });

})(jQuery);

function logout() {
    swal({
        title: "peringatan!",
        text: `Apakah anda yakin akan melakukan logout?`,
        icon: "warning",
        button: "OK",
    }).then(function (result) {
        if (result) {
            localStorage.removeItem("token");
            window.location.href = "/login";
        }
    });
}
</script>
@endpush
<x-footer-auth />