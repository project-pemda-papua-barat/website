@push('styles')
<link href="{{ asset('lib/sweetalert/sweetalert.css') }}" rel="stylesheet" />
<link href="https://cdn.datatables.net/1.12.1/css/jquery.dataTables.min.css" rel="stylesheet" />
<link href="https://cdn.datatables.net/buttons/2.2.3/css/buttons.dataTables.min.css" rel="stylesheet" />
@endpush
<x-header-auth title="Sumber Dana" />
<x-sidebar-auth />
<div id="right-panel" class="right-panel">
    <x-navbar-auth />
    <div class="content">
        <div class="animated fadeIn">
            <div class="row">
                <div class="col-xl-12 col-md-12 col-12">
                    <div class="card">
                        <div class="card-header">
                            <strong class="card-title">Master Data Sumber Dana</strong>
                        </div>
                        <div class="card-body">
                            <table id="sumber-dana-table" class="table table-striped table-bordered">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Nama Sumber dana</th>
                                        <th>Jenis Sumber Dana</th>
                                        <th>Keterangan</th>
                                        <th>Status</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <x-modal title="Tambah Data Sumber Dana" idModal="modalSumberDana" buttonAdd="Tambah" buttonEdit="Edit" actionAdd="addSumberDana()" actionEdit="editSumberDana()">
        @push('modal-content')
        <div class="row form-group" id="is_active_status"> </div>
        <div class="row form-group">
            <input type="hidden" id="id_sumber_dana" name="id_sumber_dana" />
            <div class="col col-md-3"><label for="nama_sumber_dana" class=" form-control-label">Nama Sumber Dana</label></div>
            <div class="col-12 col-md-9"><input type="text" id="nama_sumber_dana" name="nama_sumber_dana" placeholder="Nama Sumber  Dana" class="form-control"><small class="form-text  text-danger" id="nama_sumber_dana_validation"></small></div>
        </div>
        <div class="row form-group">
            <div class="col col-md-3"><label for="jenis_sumber_dana" class=" form-control-label">Jenis Sumber Dana</label></div>
            <div class="col-12 col-md-9"><input type="text" id="jenis_sumber_dana" name="jenis_sumber_dana" placeholder="Jenis Sumber  Dana" class="form-control"><small class="form-text  text-danger" id="jenis_sumber_dana_validation"></small></div>
        </div>
        <div class="row form-group">
            <div class="col col-md-3"><label for="keterangan" class=" form-control-label">Keterangan</label></div>
            <div class="col-12 col-md-9"><input type="text" id="keterangan" name="keterangan" placeholder="Keterangan" class="form-control"></div>
        </div>
        @endpush
    </x-modal>
</div>
@push('scripts')
<script src="{{asset('dist/assets/js/lib/data-table/datatables.min.js')}}"></script>
<script src="{{asset('dist/assets/js/lib/data-table/dataTables.bootstrap.min.js')}}"></script>
<script src="{{asset('dist/assets/js/lib/data-table/dataTables.buttons.min.js')}}"></script>
<script src="{{asset('dist/assets/js/lib/data-table/buttons.bootstrap.min.js')}}"></script>
<script src="{{asset('dist/assets/js/lib/data-table/jszip.min.js')}}"></script>
<script src="{{asset('dist/assets/js/lib/data-table/vfs_fonts.js')}}"></script>
<script src="{{asset('dist/assets/js/lib/data-table/buttons.html5.min.js')}}"></script>
<script src="{{asset('dist/assets/js/lib/data-table/buttons.print.min.js')}}"></script>
<script src="{{asset('dist/assets/js/lib/data-table/buttons.colVis.min.js')}}"></script>
<script src="{{asset('dist/assets/js/init/datatables-init.js')}}"></script>
<script src="{{asset('lib/sweetalert/sweetalert.min.js')}}"></script>
<script src="{{asset('js/action/authValidation.js')}}"></script>
<script src="{{asset('js/action/sumberdana.js')}}"></script>
<x-footer-auth />