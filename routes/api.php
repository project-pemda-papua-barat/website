<?php

use App\Http\Controllers\Mobile\PaginationController;
use App\Http\Controllers\Services\AreaServicesController;
use App\Http\Controllers\Services\AsetIIIServiceContoller;
use App\Http\Controllers\Services\AsetIIServiceController;
use App\Http\Controllers\Services\AsetIServiceController;
use App\Http\Controllers\Services\AsetIVServiceController;
use App\Http\Controllers\Services\AsetVIIServiceController;
use App\Http\Controllers\Services\AsetVIServiceController;
use App\Http\Controllers\Services\AsetVServiceController;
use App\Http\Controllers\Services\AuthController;
use App\Http\Controllers\Services\DetailAsetServiceController;
use App\Http\Controllers\Services\DetailProfileServiceController;
use App\Http\Controllers\Services\DistributorServiceController;
use App\Http\Controllers\Services\JenisProgramMasukServiceController;
use App\Http\Controllers\Services\KemasanServiceController;
use App\Http\Controllers\Services\MasterStockServiceController;
use App\Http\Controllers\Services\PenandatanganServiceController;
use App\Http\Controllers\Services\PenyediaServiceController;
use App\Http\Controllers\Services\PerubahanHargaServicesController;
use App\Http\Controllers\Services\RegionServiceController;
use App\Http\Controllers\Services\ReportStockServiceController;
use App\Http\Controllers\Services\SatuanServiceController;
use App\Http\Controllers\Services\StockedCenterServiceController;
use App\Http\Controllers\Services\StockInDetailServiceController;
use App\Http\Controllers\Services\StockInServiceController;
use App\Http\Controllers\Services\StockOutDetailServiceController;
use App\Http\Controllers\Services\StockoutServiceController;
use App\Http\Controllers\Services\SumberDanaServiceController;
use App\Http\Controllers\Services\UnitServicesController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::prefix('/report')->group(function(){
    Route::post('/unit', [ReportStockServiceController::class, 'filter']);
});


Route::prefix('/signer')->group(function(){
    Route::post('/', [PenandatanganServiceController::class, 'store']);
    Route::delete('/{id}', [PenandatanganServiceController::class, 'destroy']);
    Route::get('/jabatan/{jabatan}', [PenandatanganServiceController::class, 'find']);
    Route::post('/asign-jabatan/{id}', [PenandatanganServiceController::class, 'storeToPaperConfig']);
    Route::get('/export', [PenandatanganServiceController::class, 'collectSingner']);
});

Route::prefix('stocked-center')->group(function () {
    Route::get('/', [StockedCenterServiceController::class, 'index']);
    Route::get('/{id}', [StockedCenterServiceController::class, 'detail']);
});

Route::prefix('stock-out')->group(function () {
    // POST
    Route::post('/', [StockoutServiceController::class, 'store']);
    Route::post('/store-status', [StockoutServiceController::class, 'storeStatus']);

    // GET
    Route::get('/detail/{no_trx}', [StockoutServiceController::class, 'index']);
    Route::get('/detail-id/{id}', [StockoutServiceController::class, 'detail']);
    Route::get('/unit', [StockoutServiceController::class, 'unit']);

    // DELETE
    Route::delete('/{id}', [StockoutServiceController::class, 'destroy']);

    // UPDATE
    Route::post('/detail-id/{id}', [StockoutServiceController::class, 'update']);
});

Route::prefix('stock-out-detail')->group(function () {
    // POST
    Route::post('/', [StockOutDetailServiceController::class, 'store']);
    Route::get('/', [StockOutDetailServiceController::class, 'index']);

    // GET Stockout detail by id stock out
    Route::get('/stock-out/{idStockOut}', [StockOutDetailServiceController::class, 'byIndexTrx']);
    Route::get('/{id}', [StockOutDetailServiceController::class, 'detail']);

    // DELETE
    Route::delete('/{id}', [StockOutDetailServiceController::class, 'destroy']);

    // EDIT
    Route::post('/edit/{id}', [StockOutDetailServiceController::class, 'update']);
});

Route::prefix('master-stock')->group(function () {
    // GET
    Route::get('/', [MasterStockServiceController::class, 'index']);
});

Route::prefix('stock-in')->group(function () {
    // post
    Route::post('/', [StockInServiceController::class, 'store']);

    // GET
    Route::get('/', [StockInServiceController::class, 'index']);
    Route::get('/detail/{no_trx}', [StockInServiceController::class, 'detailStockIn']);
    Route::get('/detail-id/{id}', [StockInServiceController::class, 'detail']);

    // EDIT stock-in
    Route::post('/detail-id/{id}', [StockInServiceController::class, 'editStockIn']);
    Route::post('/store-status', [StockInDetailServiceController::class, 'storeStatus']);

    // Delete Stock-in
    Route::delete('/{id}', [StockInServiceController::class, 'destroy']);
});

Route::prefix('stock-in-detail')->group(function () {
    Route::post('/', [StockInDetailServiceController::class, 'store']);
    Route::get('/{stockinid}', [StockInDetailServiceController::class, 'stockinwithDetail']);
    Route::delete('/{id}', [StockInDetailServiceController::class, 'destroy']);
    Route::get('/id/{id}', [StockInDetailServiceController::class, 'detail']);
    Route::post('/{id}', [StockInDetailServiceController::class, 'update']);
    Route::post('/count/detail', [StockInDetailServiceController::class, 'detailListAset']);
});

Route::prefix('detail-aset')->group(function () {
    Route::post('/', [DetailAsetServiceController::class, 'store']);
    Route::post('/{id}', [DetailAsetServiceController::class, 'update']);
    Route::get('/{id}', [DetailAsetServiceController::class, 'detail']);
    Route::get('/', [DetailAsetServiceController::class, 'index']);
    Route::get('/select', [DetailAsetServiceController::class, 'list']);
});

Route::prefix('perubahan-harga-aset')->group(function(){
    // update harga perolehan
    Route::post('/update-harga', [PerubahanHargaServicesController::class, 'store']);
    Route::get('/by-detail-aset/{id}', [PerubahanHargaServicesController::class, 'hargaByAset']);
    // get harga ter-update
    Route::get('last-update-harga/{id}', [PerubahanHargaServicesController::class, 'lastUpdatePrice']); 
});


Route::controller(AuthController::class)->group(function () {
    Route::post('auth', 'auth')->block();
    Route::post('register', 'register');
    Route::get('profile', 'profile');
    Route::get('users', 'users');
    Route::get('refresh', 'refresh');
    Route::post('logout', 'logout');
    Route::post('profile', 'update');
});

Route::prefix('mobile/pagination')->controller(PaginationController::class)->group(function () {
    Route::get('penyedia/{page}', 'penyedia');
    Route::get('distributor/{page}', 'distributor');
    Route::get('sumber-dana/{page}', 'sumberDana');
    Route::get('area/{page}', 'area');
    Route::get('unit/{page}', 'unit');
    Route::get('jenis-program-masuk/{page}', 'jenisProgramMasuk');
    Route::get('satuan/{page}', 'satuan');
    Route::get('kemasan/{page}', 'kemasan');
});


Route::prefix('list')->group(function () {
    Route::get('aset', [AsetVIIServiceController::class, 'list']);
    Route::get('jenis', [JenisProgramMasukServiceController::class, 'list']);
    Route::get('penyedia', [PenyediaServiceController::class, 'list']);
    Route::get('satuan', [SatuanServiceController::class, 'list']);
    Route::get('sumber-dana', [SumberDanaServiceController::class, 'list']);
    Route::get('distributor', [DistributorServiceController::class, 'list']);
    Route::get('unit', [UnitServicesController::class, 'listUnit']);
    Route::get('kemasan', [KemasanServiceController::class, 'list']);
});

Route::resources([
    'penyedia' => PenyediaServiceController::class,
    'distributor' => DistributorServiceController::class,
    'sumber-dana' => SumberDanaServiceController::class,
    'area' => AreaServicesController::class,
    'unit' => UnitServicesController::class,
    'jenis-program-masuk' => JenisProgramMasukServiceController::class,
    'satuan' => SatuanServiceController::class,
    'kemasan' => KemasanServiceController::class,
    'detail-profile' => DetailProfileServiceController::class,
    'aset-i' => AsetIServiceController::class,
]);

Route::get('aset-ii/{asetI_id}', [AsetIIServiceController::class, 'index']);
Route::post('aset-ii', [AsetIIServiceController::class, 'store']);

Route::get('aset-iii/{id_asetI}/{id_asetII}', [AsetIIIServiceContoller::class, 'index']);
Route::post('aset-iii', [AsetIIIServiceContoller::class, 'store']);

Route::get('aset-iv/{id_asetI}/{id_asetII}/{id_asetIII}', [AsetIVServiceController::class, 'index']);
Route::post('aset-iv', [AsetIVServiceController::class, 'store']);

Route::get('aset-v/{id_asetI}/{id_asetII}/{id_asetIII}/{id_asetIV}', [AsetVServiceController::class, 'index']);
Route::post('aset-v', [AsetVServiceController::class, 'store']);

Route::get('aset-vi/{id_asetI}/{id_asetII}/{id_asetIII}/{id_asetIV}/{id_asetV}', [AsetVIServiceController::class, 'index']);
Route::post('aset-vi', [AsetVIServiceController::class, 'store']);

Route::get('aset-vii/{id_asetI}/{id_asetII}/{id_asetIII}/{id_asetIV}/{id_asetV}/{id_asetVI}', [AsetVIIServiceController::class, 'index']);
Route::post('aset-vii', [AsetVIIServiceController::class, 'store']);

Route::get('region/{provinsi}', [RegionServiceController::class, 'index']);

Route::get('list-area', [UnitServicesController::class, 'listArea']);
