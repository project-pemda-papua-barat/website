<?php

use App\Http\Controllers\AreaController;
use App\Http\Controllers\AsetController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\ConfigController;
use App\Http\Controllers\DistributorController;
use App\Http\Controllers\HargaPerolehanController;
use App\Http\Controllers\JenisProgramMasukController;
use App\Http\Controllers\KemasanController;
use App\Http\Controllers\ManagementUserController;
use App\Http\Controllers\PenyediaController;
use App\Http\Controllers\RegionController;
use App\Http\Controllers\ReportController;
use App\Http\Controllers\SatuanController;
use App\Http\Controllers\StockInController;
use App\Http\Controllers\StockOutController;
use App\Http\Controllers\SumberDanaController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [HomeController::class, 'index']);
Route::get('/dashboard', [HomeController::class, 'dashboard']);
Route::get('/login', [AuthController::class, 'index']);
Route::get('/forgot-password', [AuthController::class, 'forgotPassword']);
Route::get('/penyedia', [PenyediaController::class, 'index']);
Route::get('/distributor', [DistributorController::class, 'index']);
Route::get('/sumber-dana', [SumberDanaController::class, 'index']);
Route::get('/area', [AreaController::class, 'index']);
Route::get('/tambah-unit', [AreaController::class, 'tambahUnit']);
Route::get('/edit-unit/{id}', [AreaController::class, 'editUnit']);
Route::get('/jenis-program-masuk', [JenisProgramMasukController::class, 'index']);
Route::get('/satuan', [SatuanController::class, 'index']);
Route::get('/kemasan', [KemasanController::class, 'index']);
Route::get('/region', [RegionController::class, 'index']);
Route::get('/aset', [AsetController::class, 'index']);
Route::get('/test', [HomeController::class, 'test']);
Route::get('/stok-masuk', [StockInController::class, 'index']);
Route::get('/stok-masuk/tambah', [StockInController::class, 'tambah']);
Route::get('/stok-masuk/tambah/draft/{no_transaksi}', [StockInController::class, 'tambahDraf']);
Route::get('/stok-keluar', [StockOutController::class, 'index']);
Route::get('/stok-keluar/tambah', [StockOutController::class, 'tambah']);
Route::get('/stok-keluar/tambah/draft/{no_transaksi}', [StockOutController::class, 'tambahDraf']);
Route::get('/harga-perolehan', [HargaPerolehanController::class, 'index']);
Route::get('/management-user', [ManagementUserController::class, 'index']);
Route::get('/stok-masuk/export/{id}/{unit_id}', [StockInController::class, 'export']);
Route::get('/stok-keluar/export/{id}/{unit_id}', [StockOutController::class, 'export']);

Route::prefix('report')->group(function () {
    Route::get('/stok-gudang/', [ReportController::class, 'stokGudang']);
    Route::get('/stok-gudang/print/{data}', [ReportController::class, 'print']);
});

Route::prefix('/system')->group(function(){
    Route::get('/pengaturan-instansi', [ConfigController::class, 'index']);

});


Route::get('/unauthorized', [HomeController::class, 'unauthorized']);
Route::get('/not-found', [HomeController::class, 'notFound']);
