<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('harga_perolehans', function (Blueprint $table) {
            $table->id();
            $table->foreignId('detail_aset_id')->constrained('detail_asets')->onUpdate('cascade')
                ->onDelete('cascade')->nullable();
            $table->date('tanggal');
            $table->double('perubahan_harga');
            $table->integer('created_by');
            $table->integer('updated_by');
            $table->integer('deleted_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('harga_perolehans');
    }
};
