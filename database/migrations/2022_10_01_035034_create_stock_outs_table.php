<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stock_outs', function (Blueprint $table) {
            $table->id();
            $table->string('no_transaksi');
            $table->string('no_berita_acara');
            $table->string('no_dokumen_pengiriman');
            $table->date('tanggal_dokumen');
            $table->string('dokumen_berita_acara')->nullable();
            $table->string('dokumen_expedisi')->nullable();
            $table->string('dokumen_pendukung')->nullable();
            $table->string('is_status');
            $table->string('keterangan')->nullable();
            $table->foreignId('distributor_id')->constrained('distributor_models')->onUpdate('cascade')->onDelete('cascade')->nullable();
            $table->foreignId('user_id')->constrained('users')->onUpdate('cascade')->onDelete('cascade')->nullable();
            $table->integer('created_by');
            $table->integer('updated_by');
            $table->foreignId('from_unit_id');
            $table->foreignId('to_unit_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stock_outs');
    }
};
