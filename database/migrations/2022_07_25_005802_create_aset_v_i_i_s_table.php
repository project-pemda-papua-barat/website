<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('aset_v_i_i_s', function (Blueprint $table) {
            $table->id();
            $table->integer('asetI_id');
            $table->integer('asetII_id');
            $table->integer('asetIII_id');
            $table->integer('asetIV_id');
            $table->integer('asetV_id');
            $table->integer('asetVI_id');
            $table->integer('kode_aset_vii');
            $table->string('nama_aset_vii');
            $table->string('data_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('aset_v_i_i_s');
    }
};
