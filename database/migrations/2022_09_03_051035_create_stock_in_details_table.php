<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stock_in_details', function (Blueprint $table) {
            $table->id();
            $table->foreignId('stock_in_id')->constrained('stock_ins')->onUpdate('cascade')->onDelete('cascade')->nullable();
            $table->foreignId('detail_aset_id')->constrained('detail_asets')->onUpdate('cascade')->onDelete('cascade')->nullable();
            $table->foreignId('satuan_id')->constrained('satuan_models')->onUpdate('cascade')
                ->onDelete('cascade')->nullable();
            $table->foreignId('kemasan_id')->constrained('kemasan_models')->onUpdate('cascade')
                ->onDelete('cascade')->nullable();
            $table->foreignId('penyedia_id')->constrained('penyedia_models')->onUpdate('cascade')
                ->onDelete('cascade')->nullable();
                $table->foreignId('unit_id')->constrained('unit_models')->onUpdate('cascade')
                ->onDelete('cascade')->nullable();
                $table->double('qty');
                $table->string('kode');
                $table->string('batch');
                $table->date('expired_date');
                $table->double('harga_satuan');
                $table->string('aset_image');
                $table->string('is_status');
                $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stock_in_details');
    }
};
