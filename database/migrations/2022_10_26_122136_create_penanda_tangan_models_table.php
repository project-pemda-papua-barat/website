<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('penanda_tangan_models', function (Blueprint $table) {
            $table->id();
            $table->foreignId('unit_id')->constrained('unit_models')->onUpdate('cascade')
                ->onDelete('cascade')->nullable();
            $table->string('nama_penanda_tangan');
            $table->string('nip');
            $table->enum('jabatan', ['Staf Pengelola Instalasi Farmasi', 'Kepala Bidang Sumber Daya Kesehatan', 'Kepala Dinas Kesehatan MayBrat']);
            $table->string('unique_id')->unique();
            $table->enum('selected', ['true', 'false']);
            $table->integer('created_by');
            $table->integer('updated_by');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('penanda_tangan_models');
    }
};
