<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stocked_centers', function (Blueprint $table) {
            $table->id();
            $table->foreignId('detail_asets_id')->constrained('detail_asets')->onUpdate('cascade')->onDelete('cascade')->nullable();
            $table->foreignId('jenis_id')->constrained('jenis_program_masuk_models')->onUpdate('cascade')
                ->onDelete('cascade')->nullable();
            $table->foreignId('satuan_id')->constrained('satuan_models')->onUpdate('cascade')
                ->onDelete('cascade')->nullable();
            $table->foreignId('penyedia_id')->constrained('penyedia_models')->onUpdate('cascade')
                ->onDelete('cascade')->nullable();
            $table->foreignId('sumber_dana_id')->constrained('sumber_dana_models')->onUpdate('cascade')
                ->onDelete('cascade')->nullable();
            $table->foreignId('distributor_id')->constrained('distributor_models')->onUpdate('cascade')->onDelete('cascade')->nullable();
            $table->foreignId('from_unit_id')->constrained('unit_models')->onUpdate('cascade')
                ->onDelete('cascade')->nullable();
            $table->foreignId('to_unit_id')->constrained('unit_models')->onUpdate('cascade')
                ->onDelete('cascade')->nullable();
            $table->string('kode');
            $table->string('batch');
            $table->date('expired_date');
            $table->double('harga_satuan');
            $table->string('is_expired');
            $table->double('qty');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stocked_centers');
    }
};
