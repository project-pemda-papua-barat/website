<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('detail_asets', function (Blueprint $table) {
            $table->id();
            $table->foreignId('aset_id')->constrained('aset_v_i_i_s')->onUpdate('cascade')
                ->onDelete('cascade')->nullable();
            $table->string('nama_detail_aset');
            $table->enum('is_expired', ['true', 'false']);
            $table->string('aset_image')->nullable();
            $table->string('keterangan');
            $table->integer('created_by');
            $table->integer('updated_by');
            $table->integer('deleted_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('detail_asets');
    }
};
