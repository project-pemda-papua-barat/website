<?php

namespace Database\Seeders;

use App\Models\RoleUser;
// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class RoleUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role = [
            ['role_name' => 'Super Admin'],
            ['role_name' => 'Supervisor'],
            ['role_name' => 'Admin'],
        ];

        foreach ($role as $key => $r) {
            RoleUser::create([
                'role_name' => $r['role_name'],
            ]);
        }
    }
}
