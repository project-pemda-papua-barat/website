<?php

namespace Database\Seeders;

use App\Models\Squad;
use Illuminate\Database\Seeder;

class SquadSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $squad = [
            [
                'squad_name' => 'Kantor Pusat',
                'squad_unit' => 'Gudang',
                'squad_address' => 'jl. Salmba raya no. 2 Jakarta Pusat',
                'squad_phone' => '081122334455',
            ],
            [
                'squad_name' => 'Kantor Cabang Bandung',
                'squad_unit' => 'Puskesmas',
                'squad_address' => 'jl. Terusan Suryani Bandung',
                'squad_phone' => '082211334455',
            ],
        ];

        foreach ($squad as $key => $s) {
            Squad::create([
                'squad_name' => $s['squad_name'],
                'squad_unit' => $s['squad_unit'],
                'squad_address' => $s['squad_address'],
                'squad_phone' => $s['squad_phone'],
            ]);
        }
    }
}
