var endpoint = `${window.location.origin}/service`;
var token = localStorage.getItem('token');
$(document).ready(function ($) {
    "use strict";
    var date = new Date();
    var year = date.getFullYear();
    $('#year').html(year);

    $('#area_id_select').select2({
        placeholder: "Pilih  Area",
        allowClear: true,
        ajax: {
            url: `${endpoint}/list-area`,
            type: 'GET',
            headers: {
                "Authorization": `Bearer ${token}`,
            },
            processResults: function (data) {
                // Transforms the top-level key of the response object from 'items' to 'results'
                return {
                    results: data
                };
            }
        }
    });



    $('#unit-table').DataTable({
        processing: true,
        serverSide: true,
        dom: 'Bfrtip',
        buttons: [{
            text: 'Tamba Data >',
            className: 'btn btn-primary',
            action: function (e, dt, node, config) {
                return window.location.href = "/tambah-unit"
            }
        }],
        ajax: {
            url: `${endpoint}/unit`,
            type: 'GET',
            headers: {
                "Authorization": `Bearer ${token}`,
            },
            // data: e => console.log(e),
            // success: e => console.log(e),
        },
        columns: [{
            data: "DT_RowIndex",
            name: "DT_RowIndex",
        },
        {
            data: 'nama_unit',
            name: 'nama_unit'
        },
        {
            data: 'area.nama_area',
            name: 'area.nama_area'
        },
        {
            data: 'address',
            name: 'address'
        },
        {
            data: 'detail_address',
            name: 'detail_address'
        },
        {
            data: 'is_active',
            render: function (data) {
                if (data == 0) {
                    return "Tidak Aktif"
                } else {
                    return "Aktif"
                }

            }
        },
        {
            data: 'action',
            name: 'action'
        },
        ],
    });
});

function addUnit() {
    var form = new FormData();
    form.append('nama_unit', $('#nama_unit').val());
    form.append('area_id_select', $('#area_id_select').val() === null ? "" : $('#area_id_select').val());
    form.append('region_id', $('#region_id').val() === null ? "" : $('#region_id').val());
    form.append('keterangan', $('#keterangan').val());
    $.ajax({
        type: 'post',
        url: `${endpoint}/unit`,
        cache: false,
        processData: false,
        contentType: false,
        data: form,
        headers: {
            "Authorization": `Bearer ${token}`,
        },
        success: function (result) {
            swal({
                title: 'Success!',
                text: `${result.message}`,
                icon: 'success',
                button: 'OK'
            }).then(function () {
                $('#unit-table').DataTable().ajax.reload();
            });
            console.log(result)
        },
        error: function (err) {
            var status = err.status;
            if (status === 422) {
                var message = err.responseJSON.errors;
                if (message.nama_unit !== undefined) {
                    $('#nama_unit_validation').html(`${message.nama_unit}`);
                } else {
                    $('#nama_unit_validation').html(``)
                }
                if (message.area_id_select !== undefined) {
                    $('#area_id_validation').html(`${message.area_id_select}`);
                } else {
                    $('#area_id_validation').html(``)
                }
                if (message.region_id !== undefined) {
                    $('#region_id_validation').html(`${message.region_id}`);
                } else {
                    $('#region_id_validation').html(``)
                }
                console.log(message);
            }

            console.log(status)
        }
    });
}

function editUnitShow(id) {
    return window.location.href = `${window.location.origin}/edit-unit/${id}`

}

// function editArea() {
//     var id = $('#id_area').val();
//
// }