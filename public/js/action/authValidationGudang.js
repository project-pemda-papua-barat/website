var endpoint = `${window.location.origin}/service`;
var baseUrl = `${window.location.origin}`;
var unitName = '';
var token = localStorage.getItem("token");
var startDateValue = 0;
var endDateValue = 0;
var unitIdValue = 0;
var date = new Date();
var data = [];
var dataJSon = [];
var dataFilter = [];
var penyediaFilterValue = $('#penyedia_id');
var distributorFilterValue = $('#distributor_id');
var sumberDanaFilterValue = $('#sumber_dana_id');
var jenisProgramFilterValue = $('#jenis_program_id');
var asetFilterValue = $('#listAset');
var collectedDataFilter = {
    penyediaFilterValue: "",
    distributorFilterValue: "",
    sumberDanaFilterValue: "",
    jenisProgramFilterValue: "",
    asetFilterValue: ""
}

var formatRupiah = (money) => {
    return new Intl.NumberFormat("id-ID", {
        minimumFractionDigits: 0,
    }).format(money);
};

(function ($) {
    "use strict";
    var content = $("#authvalidation");

    $(document).ready(function () {
        $('#report-gudang-table').addClass('d-none');
        $('#filter-action').addClass('d-none');
        $('#subfilter').addClass('d-none');
        $('#reset').addClass('d-none');
        $('#print').addClass('d-none');
        $("#unit_id").select2();
        $("#penyedia_id").select2({dropdownAutoWidth: 'true', width: '100%'});
        $("#distributor_id").select2({dropdownAutoWidth: 'true', width: '100%'});
        $("#sumber_dana_id").select2({dropdownAutoWidth: 'true', width: '100%'});
        $("#jenis_program_id").select2({dropdownAutoWidth: 'true', width: '100%'});
        $("#listAset").select2({dropdownAutoWidth: 'true', width: '100%'});

        var year = date.getFullYear();
        $("#year").html(year);
        content.html("Loading...");
        $.ajax({
            type: "get",
            url: `${endpoint}/profile`,
            cache: false,
            processData: false,
            contentType: false,
            headers: {
                Authorization: `Bearer ${token}`,
            },
            success: function (result) {
                content.html(`<a href="#" class="dropdown-toggle active" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <img class="user-avatar rounded-circle active" src="${result.profile_pict}" alt="User Avatar">
                <span class="ml-2">${result.name}</span>
            </a>
            <div class="user-menu dropdown-menu">
                <a class="nav-link" href="#"><i class="fa fa- user"></i>My Profile</a>
                <a class="nav-link" href="#"><i class="fa fa- user"></i>Notifications <span class="count">0</span></a>
                <a class="nav-link" href="#"><i class="fa fa -cog"></i>Settings</a>
                <a class="nav-link" href="#" onclick="logout()"><i class="fa fa-power -off"></i>Logout</a>
            </div>
                    `);
                    $('#filter').attr('disabled', true);
            },
            error: function (err) {
                if (err.status == 401) {
                    window.location.href = "/login";
                }
            },
        });
    });

    $('#start_date').on('change', function () {
        let startDate = new Date($('#start_date').val());
        startDateValue = startDate.getTime();
        if(startDateValue != 0 && endDateValue != 0 && unitIdValue != 0)  $('#filter').attr('disabled', false);
    });    

    $('#end_date').on('change', function () {
        let endDate = new Date($('#end_date').val());
        endDateValue = endDate.getTime();
        if(startDateValue != 0 && endDateValue != 0 && unitIdValue != 0)  $('#filter').attr('disabled', false);
        console.log(endDateValue - startDateValue);
    });   

    $('#unit_id').on('change', function () {
        unitIdValue = $('#unit_id').val();
        unitName = $('#unit_id option:selected').text();
        if (startDateValue != 0 && endDateValue != 0 && unitIdValue != 0) $('#filter').attr('disabled', false);
    });

    $('#filter').on('click', function () {
        $('#loader').removeClass('d-none');
        $('#report-gudang-table').addClass('d-none');
        $('#filter-action').addClass('d-none');
        $('#subfilter').addClass('d-none');
        $('#reset').addClass('d-none');
        $('#print').addClass('d-none');
        if ((endDateValue - startDateValue) < 0) {
            swal({
                title: "Error!",
                text: `Tanggal awal dan akhir tidak valid! tanggal akhir harus lebih besar daripada tanggal awal!`,
                icon: "error",
                button: "OK",
            }).then(() => {
                $('#filter').attr('disabled', true);
                $('#result-filter').html('');
            })
        }

        if ((endDateValue - startDateValue) > 0) {
            let dataInput = {
                'start_date': $('#start_date').val(),
                'end_date': $('#end_date').val(),
                'unit_id' : unitIdValue
            }
            $.ajax({
                url: `${endpoint}/report/unit`,
                type: "POST",
                headers: {
                    Authorization: `Bearer ${token}`,
                    'Accept': 'Application/json'
                },
                data: dataInput,
                success: res => {
                    dataJSon = res;
                    $('#loader').addClass('d-none');
                    $('#filter-action').removeClass('d-none');
                    $('#subfilter').removeClass('d-none');
                    $('#reset').removeClass('d-none');
                    $('#print').removeClass('d-none');
                    $('#report-gudang-table').removeClass('d-none');
                    if (res.length > 0) {
                        data = dataJSon.map((ds, index, dataSOurce) => {
                            if (index === dataJSon.length - 1) {
                                return `<tr>
                                            <td>${index + 1}</td>
                                            <td>${ds.no_transaksi} / ${ds.created_at}</td>
                                            <td>${ds.nomor_aset}</td>
                                            <td>${ds.nama_detail_aset}</td>
                                            <td>${ds.nama_sumber_dana} / ${ds.tahun_pengadaan}</td>
                                            <td>${ds.nama_program}</td>
                                            <td>${ds.nama_penyedia}</td>
                                            <td>${ds.nama_distributor}</td>
                                            <td>${ds.qty < 0 ? '<i>Transaksi Stok Out</i>' : '<i>Transaksi Stok In</i>'}</td>
                                            <td>${ds.expired_date}</td>
                                            <td>Rp ${formatRupiah(ds.harga_satuan)}</td>
                                            <td>${ds.qty < 0 ? -(ds.qty) : ds.qty}</td>
                                            <td>Rp ${formatRupiah(ds.total < 0 ? -(ds.total) : ds.total)}</td>
                                        </tr>
                                        <tr>
                                            <td colspan="11"><span class="font-weight-bold">Total</span></td>
                                            <td>${dataSOurce.map(data => data.qty).reduce((a, b) =>a+b, 0)}</td>
                                            <td>Rp ${formatRupiah(dataSOurce.map(data => data.total).reduce((a, b) =>a+b, 0))}</td>
                                        </tr>`
                            }
                            return `<tr>
                                <td>${index + 1}</td>
                                <td>${ds.no_transaksi} / ${ds.created_at}</td>
                                <td>${ds.nomor_aset}</td>
                                <td>${ds.nama_detail_aset}</td>
                                <td>${ds.nama_sumber_dana} / ${ds.tahun_pengadaan}</td>
                                <td>${ds.nama_program}</td>
                                <td>${ds.nama_penyedia}</td>
                                <td>${ds.nama_distributor}</td>
                                <td>${ds.qty < 0 ? '<i>Transaksi Stok Out</i>' : '<i>Transaksi Stok In</i>'}</td>
                                <td>${ds.expired_date}</td>
                                <td>Rp ${formatRupiah(ds.harga_satuan)}</td>
                                <td>${ds.qty < 0 ? -(ds.qty) : ds.qty}</td>
                                <td>Rp ${formatRupiah(ds.total < 0 ? -(ds.total) : ds.total)}</td>
                            </tr>`
                        });
                        $('#result-content').html(data);
                        localStorage.setItem('dataPrint', data);
                        
                        console.log(data)
                    }
                },
                err: err => console.log(err)
            });
        }
    });

    $("#reset").on("click", function () {
        $("#filter-action").removeClass("d-none");
        $("#subfilter").removeClass("d-none");
        $("#reset").removeClass("d-none");
        $("#report-gudang-table").removeClass("d-none");
        collectedDataFilter.penyediaFilterValue = "";
        penyediaFilterValue.val("")
        collectedDataFilter.distributorFilterValue = "";
        distributorFilterValue.val("")
        collectedDataFilter.sumberDanaFilterValue = "";
        sumberDanaFilterValue.val("")
        collectedDataFilter.jenisProgramFilterValue = "";
        jenisProgramFilterValue.val("");
        localStorage.setItem('dataPrint', dataJSon);
        let dataReset = dataJSon.map((ds, index, dataSOurce) => {
            if (index === dataJSon.length - 1) {
                return `<tr>
                            <td>${index + 1}</td>
                            <td>${ds.no_transaksi} / ${ds.created_at}</td>
                            <td>${ds.nomor_aset}</td>
                            <td>${ds.nama_detail_aset}</td>
                            <td>${ds.nama_sumber_dana} / ${ds.tahun_pengadaan}</td>
                            <td>${ds.nama_program}</td>
                            <td>${ds.nama_penyedia}</td>
                            <td>${ds.nama_distributor}</td>
                            <td>${ds.qty < 0 ? '<i>Transaksi Stok Out</i>' : '<i>Transaksi Stok In</i>'}</td>
                            <td>${ds.expired_date}</td>
                            <td>Rp ${formatRupiah(ds.harga_satuan)}</td>
                            <td>${ds.qty < 0 ? -(ds.qty) : ds.qty}</td>
                            <td>Rp ${formatRupiah(ds.total < 0 ? -(ds.total) : ds.total)}</td>
                        </tr>
                        <tr>
                            <td colspan="11"><span class="font-weight-bold">Total</span></td>
                            <td>${dataSOurce.map(data => data.qty).reduce((a, b) =>a+b, 0)}</td>
                            <td>Rp ${formatRupiah(dataSOurce.map(data => data.total).reduce((a, b) =>a+b, 0))}</td>
                        </tr>`
            }
            return `<tr>
                <td>${index + 1}</td>
                <td>${ds.no_transaksi} / ${ds.created_at}</td>
                <td>${ds.nomor_aset}</td>
                <td>${ds.nama_detail_aset}</td>
                <td>${ds.nama_sumber_dana} / ${ds.tahun_pengadaan}</td>
                <td>${ds.nama_program}</td>
                <td>${ds.nama_penyedia}</td>
                <td>${ds.nama_distributor}</td>
                <td>${ds.qty < 0 ? '<i>Transaksi Stok Out</i>' : '<i>Transaksi Stok In</i>'}</td>
                <td>${ds.expired_date}</td>
                <td>Rp ${formatRupiah(ds.harga_satuan)}</td>
                <td>${ds.qty < 0 ? -(ds.qty) : ds.qty}</td>
                <td>Rp ${formatRupiah(ds.total < 0 ? -(ds.total) : ds.total)}</td>
            </tr>`
        });
        $("#result-content").html(dataReset);
    });



    asetFilterValue.on('change', function () {
        collectedDataFilter.asetFilterValue = asetFilterValue.val().split("-")[0];
        console.log(collectedDataFilter)
    })

    jenisProgramFilterValue.on('change', function () {
        collectedDataFilter.jenisProgramFilterValue = jenisProgramFilterValue.val();
        console.log(collectedDataFilter)
    });
    sumberDanaFilterValue.on('change', function () {
        collectedDataFilter.sumberDanaFilterValue = sumberDanaFilterValue.val();
        console.log(collectedDataFilter)
    });

    distributorFilterValue.on('change', function () {
        collectedDataFilter.distributorFilterValue = distributorFilterValue.val();
        console.log(collectedDataFilter)
    });

    penyediaFilterValue.on('change', function () {
        collectedDataFilter.penyediaFilterValue = penyediaFilterValue.val();
        console.log(collectedDataFilter)
    });

    $('#subfilter').on('click', function () {
        if (penyediaFilterValue.val() === "" && distributorFilterValue.val() === "" && sumberDanaFilterValue.val() === "" && jenisProgramFilterValue.val() === "" && asetFilterValue.val() === "") swal({
            title: "peringatan!",
            text: "Anda belum megisi filter yang harus diisi!",
            icon: "warning",
            button: "OK",
        });

        dataFilter = dataJSon.filter(val => {
            return val.nama_penyedia === collectedDataFilter.penyediaFilterValue ||
                val.nama_distributor === collectedDataFilter.distributorFilterValue ||
                val.nama_sumber_dana === collectedDataFilter.sumberDanaFilterValue ||
                val.nama_program === collectedDataFilter.jenisProgramFilterValue ||
                val.nama_detail_aset === collectedDataFilter.asetFilterValue;
        });
        
        let datafilterShow = dataFilter.map((df, index) => {
            // console.log(dataFilterSOur)
            if (index === dataFilter.length - 1) {
                return `<tr>
                            <td>${index + 1}</td>
                            <td>${df.no_transaksi} / ${df.created_at}</td>
                            <td>${df.nomor_aset}</td>
                            <td>${df.nama_detail_aset}</td>
                            <td>${df.nama_sumber_dana} / ${df.tahun_pengadaan}</td>
                            <td>${df.nama_program}</td>
                            <td>${df.nama_penyedia}</td>
                            <td>${df.nama_distributor}</td>
                            <td>${df.qty < 0 ? '<i>Transaksi Stok Out</i>' : '<i>Transaksi Stok In</i>'}</td>
                            <td>${df.expired_date}</td>
                            <td>Rp ${formatRupiah(df.harga_satuan)}</td>
                            <td>${df.qty < 0 ? -(df.qty) : df.qty}</td>
                            <td>Rp ${formatRupiah(df.total < 0 ? -(df.total) : df.total)}</td>
                        </tr>
                        <tr>
                            <td colspan="11"><span class="font-weight-bold">Total</span></td>
                            <td>${dataFilter.map(data => data.qty).reduce((a, b) =>a+b, 0)}</td>
                            <td>Rp ${formatRupiah(dataFilter.map(data => data.total).reduce((a, b) =>a+b, 0))}</td>
                        </tr>`
            }
            return `<tr>
                <td>${index + 1}</td>
                <td>${df.no_transaksi} / ${df.created_at}</td>
                <td>${df.nomor_aset}</td>
                <td>${df.nama_detail_aset}</td>
                <td>${df.nama_sumber_dana} / ${df.tahun_pengadaan}</td>
                <td>${df.nama_program}</td>
                <td>${df.nama_penyedia}</td>
                <td>${df.nama_distributor}</td>
                <td>${df.qty < 0 ? '<i>Transaksi Stok Out</i>' : '<i>Transaksi Stok In</i>'}</td>
                <td>${df.expired_date}</td>
                <td>Rp ${formatRupiah(df.harga_satuan)}</td>
                <td>${df.qty < 0 ? -(df.qty) : df.qty}</td>
                <td>Rp ${formatRupiah(df.total < 0 ? -(df.total) : df.total)}</td>
            </tr>`
        });
        localStorage.setItem('dataPrint', dataFilter)
        $('#result-content').html(datafilterShow);
    });

    $('#print').on('click', function () {
        window.open(`${baseUrl}/report/stok-gudang/print/${unitName}`, '_blank').focus();
    });

})(jQuery);

function logout() {
    swal({
        title: "peringatan!",
        text: `Apakah anda yakin akan melakukan logout?`,
        icon: "warning",
        button: "OK",
    }).then(function (result) {
        if (result) {
            localStorage.removeItem("token");
            window.location.href = "/login";
        }
    });
}
