var endpoint = `${window.location.origin}/service`;
var baseUrl = `${window.location.origin}`;
var token = localStorage.getItem("token");
var formatRupiah = (money) => {
    return new Intl.NumberFormat("id-ID", {
        minimumFractionDigits: 0,
    }).format(money);
};

(function ($) {
    "use strict";
    var content = $("#authvalidation");

    $(document).ready(function () {
        content.html("Loading...");
        $.ajax({
            type: "get",
            url: `${endpoint}/profile`,
            cache: false,
            processData: false,
            contentType: false,
            headers: {
                Authorization: `Bearer ${token}`,
            },
            success: function (result) {
                content.html(`<a href="#" class="dropdown-toggle active" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <img class="user-avatar rounded-circle active" src="${result.profile_pict}" alt="User Avatar">
                <span class="ml-2">${result.name}</span>
            </a>
            <div class="user-menu dropdown-menu">
                <a class="nav-link" href="#"><i class="fa fa- user"></i>My Profile</a>
                <a class="nav-link" href="#"><i class="fa fa- user"></i>Notifications <span class="count">0</span></a>
                <a class="nav-link" href="#"><i class="fa fa -cog"></i>Settings</a>
                <a class="nav-link" href="#" onclick="logout()"><i class="fa fa-power -off"></i>Logout</a>
            </div>
                    `);

                if (result.role_name == "User Unit" || "Supervisor Unit") {
                    unitUser(result.role_name);
                } else {
                    window.location.href = "/unauthrorize";
                }
            },
            error: function (err) {
                if (err.status == 401) {
                    window.location.href = "/login";
                }
            },
        });
    });
})(jQuery);

function deleteData(id) {
    swal({
        title: "warning!",
        text: `Akah anda yakin akan menghapus data ini?!`,
        icon: "warning",
        button: "OK",
    }).then(function (value) {
        if (value) {
            $.ajax({
                type: "DELETE",
                url: `${endpoint}/stock-in/${id}`,
                cache: false,
                processData: false,
                contentType: false,
                headers: {
                    Authorization: `Bearer ${token}`,
                },
                success: function (event) {
                    swal({
                        title: "Success!",
                        text: `${event.message}`,
                        icon: "success",
                        button: "OK",
                    });
                    $("#stock-in-table").DataTable().ajax.reload();
                    console.log(event);
                },
                error: function (err) {
                    console.log(err.responseJSON);
                    swal({
                        title: "Error!",
                        text: `${err}`,
                        icon: "error",
                        button: "OK",
                    });
                },
            });
        }
    });
}

function unitUser(role) {
    // console.log(role)
    if (role === "Supervisor Unit") {
        $('#button-atas').addClass('d-none')
        $("#stock-in-table").DataTable({
            processing: true,
            serverSide: true,
            dom: "Bfrtip",
            ajax: {
                url: `${endpoint}/stock-in`,
                type: "GET",
                headers: {
                    Authorization: `Bearer ${token}`,
                },
                // data: e => console.log(e),
                // success: e => console.log(e),
            },

            columns: [
                {
                    data: "DT_RowIndex",
                    name: "DT_RowIndex",
                },
                {
                    data: "no_transaksi",
                    render: function (a, b, c) {
                        return `${c.no_transaksi} / ${c.created_at}`;
                    },
                },
                {
                    data: "no_berita_acara",
                    name: "no_berita_acara",
                },
                {
                    data: "stock_in_detail.total",
                    name: "stock_in_detail.total",
                },
                {
                    data: "nama_sumber_dana",
                    render: function (a, b, c) {
                        return `${c.nama_sumber_dana} / ${c.tahun_pengadaan}`;
                    },
                },
                {
                    data: "nama_program",
                    name: "nama_program",
                },
                {
                    data: "nama_distributor",
                    name: "nama_distributor",
                },
                {
                    data: "is_status",
                    render: function (a, b, row) {
                        if (row.is_status !== "can deleted") {
                            return `<span class="text-info">${row.is_status}</span>`;
                        }

                        return null;
                    },
                },
                {
                    data: "stock_in_detail.total_harga_perolehan",
                    render: (a, b, row) =>
                        `Rp ${formatRupiah(
                            row.stock_in_detail.total_harga_perolehan
                        )}`,
                },
                {
                    data: "petugas",
                    render: function (a, b, c) {
                        return `${c.petugas} / ${
                            c.keterangan === null ? "" : c.keterangan
                        }`;
                    },
                },
                {
                    data: "is_status",
                    render: function (a, b, c) {
                        if (
                            c.is_status === "can deleted" ||
                            c.is_status === "harus direvisi"
                        ) {
                            return `<a href="#" class="btn btn-rounded btn-secondary my-1"><i class="fa fa-eye" aria-hidden="true" disabled></i> View</a> <br/><a href="#" class="btn btn-rounded btn-secondary my-1" disabled><i class="fa fa-check-circle" aria-hidden="true"></i> approve</a><br/><a href="#"  class="btn btn-rounded btn-secondary my-1" disabled><i class="fa fa-ban" aria-hidden="true"></i> Decline</a>`;
                        }
                        if (c.is_status === "approved") {
                            return `<a href="#" onclick="viewDetail('${c.id}', '${c.dokumen_berita_acara}', '${c.dokumen_expedisi}', '${c.dokumen_pendukung}')" class="btn btn-rounded btn-warning my-1"><i class="fa fa-eye" aria-hidden="true"></i> View</a> <br/><a href="#" class="btn btn-rounded btn-outline-success my-1"><i class="fa fa-check-circle" aria-hidden="true"></i> approved</a><br/>`;
                        }
                        return `<a href="#" onclick="viewDetail('${c.id}', '${c.dokumen_berita_acara}', '${c.dokumen_expedisi}', '${c.dokumen_pendukung}')" class="btn btn-rounded btn-warning my-1"><i class="fa fa-eye" aria-hidden="true"></i> View</a> <br/><a href="#" onclick="approveBySpv('${c.id}')" class="btn btn-rounded btn-success my-1"><i class="fa fa-check-circle" aria-hidden="true"></i> approve</a><br/><a href="#" onclick="declineBySpv('${c.id}')" class="btn btn-rounded btn-danger my-1"><i class="fa fa-ban" aria-hidden="true"></i> Decline</a>`;
                    },
                },
            ],
            error: function (err) {
                if (err.status == 401) {
                    window.location.href = "/login";
                }
            },
        });
    } else if ("User Unit") {
        $("#stock-in-table").DataTable({
            processing: true,
            serverSide: true,
            dom: "Bfrtip",
            ajax: {
                url: `${endpoint}/stock-in`,
                type: "GET",
                headers: {
                    Authorization: `Bearer ${token}`,
                },
                // data: e => console.log(e),
                // success: e => console.log(e),
            },

            columns: [
                {
                    data: "DT_RowIndex",
                    name: "DT_RowIndex",
                },
                {
                    data: "no_transaksi",
                    render: function (a, b, c) {
                        return `${c.no_transaksi} / ${c.created_at}`;
                    },
                },
                {
                    data: "no_berita_acara",
                    name: "no_berita_acara",
                },
                {
                    data: "stock_in_detail.total",
                    name: "stock_in_detail.total",
                },
                {
                    data: "nama_sumber_dana",
                    render: function (a, b, c) {
                        return `${c.nama_sumber_dana} / ${c.tahun_pengadaan}`;
                    },
                },
                {
                    data: "nama_program",
                    name: "nama_program",
                },
                {
                    data: "nama_distributor",
                    name: "nama_distributor",
                },
                {
                    data: "is_status",
                    render: function (a, b, row) {
                        if (row.is_status !== "can deleted") {
                            return `<span class="text-info">${row.is_status}</span>`;
                        }

                        return null;
                    },
                },
                {
                    data: "stock_in_detail.total_harga_perolehan",
                    render: (a, b, row) =>
                        `Rp ${formatRupiah(
                            row.stock_in_detail.total_harga_perolehan
                        )}`,
                },
                {
                    data: "petugas",
                    render: function (a, b, c) {
                        return `${c.petugas} / ${
                            c.keterangan === null ? "" : c.keterangan
                        }`;
                    },
                },
                {
                    data: "is_status",
                    render: function (a, b, c) {
                        if (
                            c.is_status === "can deleted" ||
                            c.is_status === "harus direvisi"
                        ) {
                            return `<a href="#" onclick="editData('${c.no_transaksi}')" class="text-warning editData"><i class="fa fa-edit" aria-hidden="true"></i></a> <a href="#" onclick="deleteData('${c.id}')" class="text-danger"><i class="fa fa-trash" aria-hidden="true"></i></a> <br/><a href="#" onclick="viewDetail('${c.id}', '${c.dokumen_berita_acara}', '${c.dokumen_expedisi}', '${c.dokumen_pendukung}')" class="text-success my-1"><i class="fa fa-eye" aria-hidden="true"></i></a> <a href="#" class="text-secondary my-1" disabled><i class="fa fa-print" aria-hidden="true"></i></a>`;
                        }

                        return `<a href="#"  class="text-secondary" disabled><i class="fa fa-edit" aria-hidden="true"></i></a> <a href="#" class="text-secondary" disabled><i class="fa fa-trash" aria-hidden="true"></i></a><br/><a href="#" onclick="viewDetail('${c.id}', '${c.dokumen_berita_acara}', '${c.dokumen_expedisi}', '${c.dokumen_pendukung}')" class="text-success my-1"><i class="fa fa-eye" aria-hidden="true"></i></a> <a href="${baseUrl}/stok-masuk/export/${c.id}/${c.unit_id}" class="text-info my-1" target="_blank" ><i class="fa fa-print" aria-hidden="true"></i></a>`;
                    },
                },
            ],
            error: function (err) {
                if (err.status == 401) {
                    window.location.href = "/login";
                }
            },
        });
    }
}

function approveBySpv(id) {
    const form = new FormData();
    form.append("stock_in_id", id);
    form.append("is_status", "approved");
    swal({
        title: "warning!",
        text: `Apakah anda yakin meng-approve data ini?`,
        icon: "warning",
        button: "OK",
    }).then((value) => {
        if (value) {
            fetch(`${endpoint}/stock-in/store-status`, {
                method: "POST",
                headers: {
                    Accept: "application/json",
                    Authorization: `Bearer ${token}`,
                },
                body: form,
            })
                .then((response) => response.json())
                .then((val) => {
                    console.log(val);
                    swal({
                        title: "Success!",
                        text: `${val.message}`,
                        icon: "success",
                        button: "OK",
                    }).then(() =>
                        $("#stock-in-table").DataTable().ajax.reload()
                    );
                })
                .catch((err) => console.log(err));
        }
    });
}

function declineBySpv(id) {
    swal({
        title: "warning!",
        icon: "warning",
        text: `Apakah anda yakin data ini harus direvisi?!`,
        buttons: {
            cancel: true,
            confirm: true,
        },
        content: {
            element: "input",
            attributes: {
                placeholder: "isi Keterangan data ini harus direvisi?",
                type: "text",
                id: "keterangan_revisi",
                name: "keterangan_revisi",
            },
        },
    }).then((value) => {
        console.log(value);
        if (value === "") {
            swal({
                title: "warning!",
                icon: "warning",
                text: "Keterangan harus diisi!",
                buttons: true,
            });
        } else {
            const form = new FormData();
            form.append("stock_in_id", id);
            form.append("is_status", "harus direvisi");
            form.append("keterangan", value);
            fetch(`${endpoint}/stock-in/store-status`, {
                method: "POST",
                headers: {
                    Accept: "application/json",
                    Authorization: `Bearer ${token}`,
                },
                body: form,
            })
                .then((response) => response.json())
                .then((val) => {
                    console.log(val);
                    swal({
                        title: "Success!",
                        text: `${val.message}`,
                        icon: "success",
                        button: "OK",
                    }).then(() =>
                        $("#stock-in-table").DataTable().ajax.reload()
                    );
                })
                .catch((err) => console.log(err));
        }
    });
}

function viewDetail(
    no_transaksi,
    dokumen_berita_acara,
    dokumen_expedisi,
    dokumen_pendukung
) {
    $("#list-detail-aset").html(
        "<div class='text-center text-success'>loadiing...</div>"
    );
    $("#tag-download").html(`
    <div class="col-xl-4 col-md-4">
        <a href="${dokumen_berita_acara}" target="_blank" class="btn btn-outline-primary my-1 text-center">Dokumen Berita Acara</a>
    </div>
    <div class="col-xl-4 col-md-4">
        <a href="${dokumen_expedisi}" target="_blank" class="btn btn-outline-warning my-1  text-center">Dokumen Expedisi</a>
    </div>
    <div class="col-xl-4 col-md-4">
        <a href="${dokumen_pendukung}" target="_blank" class="btn btn-outline-success my-1  text-center">Dokumen Pendukung</a>
    </div>
    `);
    var myModal = new bootstrap.Modal(document.getElementById("listAset"), {
        keyboard: false,
    });
    myModal.show();
    $.ajax({
        type: "GET",
        url: `${endpoint}/stock-in-detail/${no_transaksi}`,
        cache: false,
        processData: false,
        contentType: false,
        headers: {
            Authorization: `Bearer ${token}`,
        },
        success: function (result) {
            var content = $.map(result.data, function (a) {
                return `<tr>
                    <td>${a.DT_RowIndex}</td>
                    <td>${a.nomor_aset}</td>
                    <td>${a.nama_detail_aset}</td>
                    <td>${a.batch}</td>
                    <td>${a.nama_satuan}</td>
                    <td>${a.nama_kemasan}</td>
                    <td>Rp ${formatRupiah(a.harga_satuan)}</td>
                    <td>${a.nama_penyedia}</td>
                    <td>${a.qty}</td>
                </tr>`;
            }).join(" ");
            $("#list-detail-aset")
                .html(`<table class="table table-striped table-bordered table-responsive" width="100%">
                <thead>
                    <th>No.</th>
                    <th>No. Aset</th>
                    <th>Nama Aset</th>
                    <th>Batch</th>
                    <th>Satuan</th>
                    <th>Kemasan</th>
                    <th>Harga Satuan</th>
                    <th>Penyedia</th>
                    <th>Jumlah.</th>
                </thead>
                <tbody >${content}</tbody>
            </table>`);
        },
        error: function (err) {
            console.log(err);
        },
    });
}

function editData(no_transaksi) {
    window.location.href = `${baseUrl}/stok-masuk/tambah/draft/${no_transaksi} `;
}

function logout() {
    swal({
        title: "peringatan!",
        text: `Apakah anda yakin akan melakukan logout?`,
        icon: "warning",
        button: "OK",
    }).then(function (result) {
        if (result) {
            localStorage.removeItem("token");
            window.location.href = "/login";
        }
    });
}
