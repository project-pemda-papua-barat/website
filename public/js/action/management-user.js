var endpoint = `${window.location.origin}/service`;
var token = localStorage.getItem('token');
var unitIdValue = "";
(function ($) {
    "use strict";

    $(document).ready(function () {

        $('#squad_id_select').select2({
            placeholder: "Pilih  Aset",
            dropdownParent: $('#modalUser'),
            allowClear: true,
            ajax: {
                url: `${endpoint}/list/unit`,
                type: 'GET',
                headers: {
                    "Authorization": `Bearer ${token}`,
                },
                processResults: function (data) {
                    return {
                        results: data
                    };
                }
            }
        }).on("select2:selecting", function (e) {
            console.log(e.params)
            unitIdValue = e.params.args.data.id;
            console.log(unitIdValue)
        }).on("select2:clear", function () {
            unitIdValue = "";
        });


        $('#list-user-table').DataTable({
            processing: true,
            serverSide: true,
            dom: 'Bfrtip',
            buttons: [{
                text: 'Tamba Data >',
                className: 'btn btn-primary',
                action: function (e, dt, node, config) {
                    $('#nama_kemasan').attr("disabled", false);
                    $('#keterangan').attr("disabled", false);
                    // $('#is_active_status').html(``);
                    $("#button-add").css("display", "block");
                    $("#button-edit").css("display", "none");
                    // $('#nama_kemasan').val("");
                    // $('#nama_kemasan_validation').html(``);
                    // $('#keterangan').val("");
                    var myModal = new bootstrap.Modal(document.getElementById('modalUser'), {
                        keyboard: false
                    });
                    myModal.show();
                }
            }],
            ajax: {
                url: `${endpoint}/users`,
                type: 'GET',
                headers: {
                    "Authorization": `Bearer ${token}`,
                },
                // data: e => console.log(e),
                // success: e => console.log(e),
            },
            columns: [{
                data: "DT_RowIndex",
                name: "DT_RowIndex",
            },
            {
                data: "name",
                name: "name",
            },
            {
                data: "username",
                name: "username",
            },
            {
                data: "email",
                name: "email",
            },
            {
                data: "role.role_name",
                name: "role.role_name",
            },
            {
                data: "squad.nama_unit",
                name: "squad.nama_unit",
                // render: function (_, a, row) {
                //     return row.squad == null ? "unit sudah di hapus/ pindah" : row.squad.squad
                // }
            },
            {
                data: 'action',
                name: 'action'
            },
            ],
        });
    });

})(jQuery);

function addUser() {
    var file_data = $('#profile_pict').prop('files')[0] === undefined ? null : $('#profile_pict').prop('files')[0];
    var form = new FormData();
    form.append('name', $('#name').val());
    form.append('username', $('#username').val());
    form.append('password', $('#password').val());
    form.append('email', $('#email').val());
    form.append('squad_id', unitIdValue);
    form.append('role_id', $('#role_id').val());
    form.append('address_user', $('#address_user').val());
    form.append('lat', $('#lat').val());
    form.append('long', $('#long').val());
    form.append('profile_pict', file_data);
    $.ajax({
        type: 'post',
        url: `${endpoint}/register`,
        cache: false,
        processData: false,
        contentType: false,
        data: form,
        headers: {
            "Authorization": `Bearer ${token}`,
        },
        success: function (result) {
            swal({
                title: 'Success!',
                text: `${result.message}`,
                icon: 'success',
                button: 'OK'
            }).then(function () {
                $('#list-user-table').DataTable().ajax.reload();
            });
            console.log(result)
        },
        error: function (err) {
            var status = err.status;
            if (status === 422) {
                var message = err.responseJSON.errors;
                if (message.role_id !== undefined) {
                    $('#role_id_validation').html(`${message.role_id}`);
                } else {
                    $('#role_id_validation').html(``)
                }
                if (message.email !== undefined) {
                    $('#email_validation').html(`${message.email}`);
                } else {
                    $('#email_validation').html(``)
                }
                if (message.name !== undefined) {
                    $('#name_validation').html(`${message.name}`);
                } else {
                    $('#name_validation').html(``)
                }
                if (message.password !== undefined) {
                    $('#password_validation').html(`${message.password}`);
                } else {
                    $('#password_validation').html(``)
                }
                if (message.squad_id !== undefined) {
                    $('#squad_id_validation').html(`${message.squad_id}`);
                } else {
                    $('#squad_id_validation').html(``)
                }
                if (message.username !== undefined) {
                    $('#username_validation').html(`${message.username}`);
                } else {
                    $('#username_validation').html(``)
                }
                if (message.address_user !== undefined) {
                    $('#address_user_validation').html(`${message.address_user}`);
                } else {
                    $('#address_user_validation').html(``)
                }
                console.log(message);
            }

            console.log(status)
        }
    });
}