var endpoint = `${window.location.origin}/service`;
var token = localStorage.getItem('token');

(function ($) {
    "use strict";

    var loading = `<div id="spinner" class="show bg-white position-fixed translate-middle w-100 vh-100 top-50 start-50 d-flex align-items-center justify-content-center">
    <div class="spinner-border text-primary" style="width: 3rem; height: 3rem;" role="status">
        <span class="sr-only">Loading...</span>
    </div>
</div>`;
    var loading2 = `<div id="spinner" class="show position-fixed translate-middle  d-flex  justify-content-center">
    <div class="spinner-border text-success   d-flex  justify-content-center" >
        <span class="sr-only">Loading...</span>
    </div>
</div>`;
    var content = $('#authvalidation');
    var sideNav = $(`#sidenav-auth`)


    $(document).ready(function () {
        content.html(loading);

        $.ajax({
            type: 'get',
            url: `${endpoint}/profile`,
            cache: false,
            processData: false,
            contentType: false,
            headers: {
                "Authorization": `Bearer ${token}`,
            },
            success: function (result) {

                sideNav.html(`<nav class="navbar bg-light navbar-light">
                <a href="index.html" class="navbar-brand mx-4 mb-3">
                    <h3 class="text-primary"><i class="fa fa-hashtag me-2"></i>Papua Barat</h3>
                </a>
                <div class="navbar-nav w-100">
                    <a href="/dashboard" class="nav-item nav-link active"><i class="fa fa-tachometer-alt me-2"></i>Dashboard</a>
                        <div class="nav-item dropdown">
                            <a href="#" class="nav-link dropdown-toggle" data-bs-toggle="dropdown"><i class="fa fa-laptop me-2"></i>Master Data</a>
                                <div class="dropdown-menu bg-transparent border-0 bg-light">
                                    <a href="/penyedia" class="dropdown-item py-1">Penyedia</a>
                                    <a href="/distributor" class="dropdown-item py-1">Distributor</a>
                                    <a href="/sumber-dana" class="dropdown-item py-1">Sumber Dana</a>
                                    <a href="/area" class="dropdown-item py-1">Area</a>
                                    <a href="/jenis-program-masuk" class="dropdown-item py-1">Jenis Program Masuk</a>
                                    <a href="/satuan" class="dropdown-item py-1">Satuan</a>
                                    <a href="/kemasan" class="dropdown-item py-1">Kemasan</a>
                                    <a href="/aset" class="dropdown-item py-1">Aset</a>
                                    <a href="/region" class="dropdown-item py-1">region</a>
                                </div>
                    </div>
                </div>
            </nav>`);
                content.html(`<nav class="navbar navbar-expand bg-light navbar-light sticky-top px-4 py-0">
                                                    <a href="index.html" class="navbar-brand d-flex d-lg-none me-4">
                                                        <h2 class="text-primary mb-0"><i class="fa fa-hashtag"></i></h2>
                                                    </a>
                                                    <a href="#" class="sidebar-toggler flex-shrink-0">
                                                        <i class="fa fa-bars"></i>
                                                    </a>
                                                    <form class="d-none d-md-flex ms-4">
                                                        <input class="form-control border-0" type="search" placeholder="Search">
                                                    </form>
                                                    <div class="navbar-nav align-items-center ms-auto">
                                                        <div class="nav-item dropdown">
                                                            <a href="#" class="nav-link dropdown-toggle" data-bs-toggle="dropdown">
                                                                <img class="rounded-circle me-lg-2" src="${result.profile_pict}" alt="profile" style="width: 40px; height: 40px;">
                                                                <span class="d-none d-lg-inline-flex">${result.name}</span>
                                                            </a>
                                                            <div class="dropdown-menu dropdown-menu-end bg-light border-0 rounded-0 rounded-bottom m-0">
                                                                <a href="#" class="dropdown-item">My Profile</a>
                                                                <a href="#" class="dropdown-item">Settings</a>
                                                                <a href="#" onclick="logout()" class="dropdown-item">Log Out</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </nav>
                                                <div class="container-fluid pt-4 px-4 ">
                                                                    <div class="row g-4">
                                                                        <div class="col-sm-12 col-xl-12">
                                                                            <div class="bg-light rounded py-2 px-2">
                                                                                <h6 class="text-center">Master Data Wilayah</h6>
                                                                                <div class="input-group mb-3">
                                                                                <span class="input-group-text" id="basic-addon1">Cari Wilayah</span>
                                                                                    <input type="text" class="form-control" placeholder="Provinisi" aria-label="Ketik Provinisi" aria-describedby="basic-addon1" id="provinsi" name="provinsi">
                                                                                </div>
                                                                                <div class="table-responsive" id="data"></div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                    </div>

                            <div class="modal fade" id="tambahPenyediaModal" tabindex="-1" role="dialog" aria-labelledby="tambahPenyediaModal" aria-hidden="true">
                            <div class="modal-dialog modal-dialog-centered" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title font-weight-normal" id="tambahPenyediaModalTitle"></h5>
                                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                    </div>
                                    <div class="modal-body" id="form-operasional">
                                    <span class="text-danger" id="nama_satuan_validation"></span>
                                         <input type="hidden" id="id_satuan"name="id_satuan"/>
                                        <div class="form-floating mb-4">
                                            <input type="text" class="form-control" id="nama_satuan" placeholder="Nama satuan" name="nama_satuan">
                                            <label for="nama_satuan">Nama satuan</label>
                                        </div>
                                    <span class="text-danger" id="lambang_satuan_validation"></span>
                                        <div class="form-floating mb-4">
                                            <input type="text" class="form-control" id="lambang_satuan" placeholder="Lambang satuan" name="lambang_satuan">
                                            <label for="lambang_satuan">Lambang satuan</label>
                                        </div>
                                        <div class="form-floating mb-4">
                                            <input type="text" class="form-control" id="keterangan" placeholder="Keterangan" name="keterangan">
                                            <label for="keterangan">Keterangan</label>
                                        </div>
                                    </div>
                                    <div class="modal-footer" id="form-footer">
                                    <button class="btn btn-success my-4" id="button-tambah" onclick="tambahMasterDataPenyediaAction()">Tambah</button>
                                    <button class="btn btn-primary my-4" id="button-edit" onclick="editMasterDataPenyediaAction()">Edit</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                                            `);

                console.log(result);

                $('input[name=provinsi]').change(function (e) {
                    $('#data').html(loading2);
                    var inputValue = e.target.value;
                    var inputLangValue = e.target.value.lang;

                    $.ajax({
                        type: 'GET',
                        url: `${endpoint}/region/${inputValue}`,
                        cache: false,
                        processData: false,
                        contentType: false,
                        headers: {
                            "Authorization": `Bearer ${token}`,
                            "Content-Type": "application/json"
                        },
                        success: function (result) {
                            var dataProvinsi = result.length > 0 ? $.map(result, function (dp, index) {
                                return `<tr>
                                    <td>${index + 1}</td>
                                    <td>${dp.kelurahan}</td>
                                    <td>${dp.kecamatan}</td>
                                    <td>${dp.kabupaten}</td>
                                    <td>${dp.kodepos}</td>
                                    <td>${dp.provinsi}</td>
                                </tr>`
                            }).join(",") : result;

                            if (result.length == 0) {
                                $('#data').html(`
                                        <table id="wilayah-table" class="table  table-bordered align-items-center my-4">
                                        <thead>
                                            <tr>
                                                <th>No</th>
                                                <th>Kelurahan / Desa</th>
                                                <th>Kecamatan</th>
                                                <th>Kota / Kabupaten</th>
                                                <th>Kode Pos</th>
                                                <th>Provinsi</th>
                                            </tr>
                                            <tr>
                                                <td colspan="6">
                                                 <h4 class="text-danger text-center">Maaf! data yang anda cari tidak ada!</h4>
                                                </td>
                                            </tr>
                                        </thead>
                                </table>`);
                            } else {

                                $('#data').html(`
                                <table id="wilayah-table" class="table  table-bordered align-items-center my-4">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Kelurahan / Desa</th>
                                        <th>Kecamatan</th>
                                        <th>Kota / Kabupaten</th>
                                        <th>Kode Pos</th>
                                        <th>Provinsi</th>
                                    </tr>
                                </thead>
                                    <tbody>
                                        ${dataProvinsi}
                                    </tbody>
                        </table>`);
                            }
                            $('#wilayah-table').DataTable();
                        },
                        error: function (err) {
                            var status = err.status;
                            $('#data').html("");
                            console.log(status)
                        }
                    });

                    console.log(e.target.value, e.target.value.length)
                });


            },
            error: function (err) {
                if (err.status == 401) {
                    window.location.href = "/login";
                }
            }
        });

    });



})(jQuery);




function tambahMasterDataPenyediaAction() {
    var form = new FormData();
    form.append('nama_satuan', $('#nama_satuan').val());
    form.append('lambang_satuan', $('#lambang_satuan').val());
    form.append('keterangan', $('#keterangan').val());
    $.ajax({
        type: 'post',
        url: `${endpoint}/satuan`,
        cache: false,
        processData: false,
        contentType: false,
        data: form,
        headers: {
            "Authorization": `Bearer ${token}`,
        },
        success: function (result) {
            swal({
                title: 'Success!',
                text: `${result.message}`,
                icon: 'success',
                button: 'OK'
            }).then(function () {
                $('#satuan-table').DataTable().ajax.reload();
            });
            console.log(result)
        },
        error: function (err) {
            var status = err.status;
            if (status === 422) {
                var message = err.responseJSON.errors;
                if (message.nama_satuan !== undefined) {
                    $('#nama_satuan_validation').html(`${message.nama_satuan}`);
                } else {
                    $('#nama_satuan_validation').html(``)
                }
                if (message.lambang_satuan !== undefined) {
                    $('#lambang_satuan_validation').html(`${message.lambang_satuan}`);
                } else {
                    $('#lambang_satuan_validation').html(``)
                }
                console.log(message);
            }

            console.log(status)
        }
    });
}

function editDataSatuan(id) {
    $('#tambahPenyediaModalTitle').html('Edit Data Penyedia');
    $('#nama_satuan_validation').html("");
    $('#lambang_satuan_validation').html("");
    $("#button-tambah").css("display", "none");
    $("#button-edit").css("display", "block");
    var myModal = new bootstrap.Modal(document.getElementById('tambahPenyediaModal'), {
        keyboard: false
    });
    myModal.show();
    $.ajax({
        type: 'get',
        url: `${endpoint}/satuan/${id}`,
        cache: false,
        processData: false,
        contentType: false,
        headers: {
            "Authorization": `Bearer ${token}`,
        },
        success: function (result) {
            $('#id_satuan').val(result.id);
            $('#nama_satuan').val(result.nama_satuan);
            $('#lambang_satuan').val(result.lambang_satuan);
            $('#keterangan').val(result.keterangan);
            console.log(result)
        },
        error: function (err) {
            console.log(err)
        }
    });
}

function logout() {
    swal({
        title: 'peringatan!',
        text: `Apakah anda yakin akan melakukan logout?`,
        icon: 'warning',
        button: 'OK'
    }).then(function (result) {
        if (result) {
            localStorage.removeItem('token');
            window.location.href = "/login";
        }
    });

}