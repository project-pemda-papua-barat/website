var endpoint = `${window.location.origin}/service`;
var token = localStorage.getItem('token');

$(document).ready(function () {
    "use strict";
    var date = new Date();
    var year = date.getFullYear();
    $('#year').html(year);

    $('#satuan-table').DataTable({
        processing: true,
        serverSide: true,
        dom: 'Bfrtip',
        buttons: [{
            text: 'Tamba Data >',
            className: 'btn btn-primary',
            action: function (e, dt, node, config) {
                $('#nama_satuan').attr("disabled", false);
                $('#kode_satuan').attr("disabled", false);
                $('#keterangan').attr("disabled", false);
                $('#is_active_status').html(``);
                $("#button-add").css("display", "block");
                $("#button-edit").css("display", "none");
                $('#nama_satuan').val("");
                $('#kode_satuan').val("");
                $('#nama_satuan_validation').html(``);
                $('#lambang_satuan_validation').html(``);
                $('#keterangan').val("");
                var myModal = new bootstrap.Modal(document.getElementById('modalSatuan'), {
                    keyboard: false
                });
                myModal.show();
            }
        }],
        ajax: {
            url: `${endpoint}/satuan`,
            type: 'GET',
            headers: {
                "Authorization": `Bearer ${token}`,
            },
            // data: e => console.log(e),
            // success: e => console.log(e),
        },
        columns: [{
            data: "DT_RowIndex",
            name: "DT_RowIndex",
        },
        {
            data: 'nama_satuan',
            name: 'nama_satuan'
        },
        {
            data: 'kode_satuan',
            name: 'kode_satuan'
        },
        {
            data: 'keterangan',
            name: 'keterangan'
        },
        {
            data: 'is_active',
            render: function (data) {
                if (data == 0) {
                    return "Tidak Aktif"
                } else {
                    return "Aktif"
                }

            }
        },
        {
            data: 'action',
            name: 'action'
        },
        ],
    });
});

function addSatuan() {
    var form = new FormData;
    form.append('nama_satuan', $('#nama_satuan').val())
    form.append('kode_satuan', $('#kode_satuan').val())
    form.append('keterangan', $('#keterangan').val())
    $.ajax({
        type: 'post',
        url: `${endpoint}/satuan`,
        cache: false,
        processData: false,
        contentType: false,
        data: form,
        headers: {
            "Authorization": `Bearer ${token}`
        },
        success: function (result) {
            swal({
                title: 'Success!',
                text: `${result.message}`,
                icon: 'success',
                button: 'OK'
            }).then(function () {
                $('#satuan-table').DataTable().ajax.reload();
            });
            console.log(result)
        },
        error: function (err) {
            var status = err.status;
            if (status === 422) {
                var message = err.responseJSON.errors;
                if (message.nama_satuan !== undefined) {
                    $('#nama_satuan_validation').html(`${message.nama_satuan}`);
                } else {
                    $('#nama_satuan_validation').html(``)
                }
                if (message.kode_satuan !== undefined) {
                    $('#kode_satuan_validation').html(`${message.kode_satuan}`);
                } else {
                    $('#kode_satuan_validation').html(``)
                }
                console.log(message);
            }

            console.log(status)
        }
    });
}

function editDataSatuan(id) {
    $("#button-add").css("display", "none");
    $("#button-edit").css("display", "block");
    var myModal = new bootstrap.Modal(document.getElementById('modalSatuan'), {
        keyboard: false
    });
    $.ajax({
        type: 'get',
        url: `${endpoint}/satuan/${id}`,
        cache: false,
        processData: false,
        contentType: false,
        headers: {
            "Authorization": `Bearer ${token}`,
        },
        success: function (result) {
            if (result.is_active == '0') {
                $('#nama_satuan').attr("disabled", true);
                $('#kode_satuan').attr("disabled", true);
                $('#keterangan').attr("disabled", true);
                $('#id_satuan').val(result.id);
                $('#nama_satuan').val(result.nama_satuan);
                $('#kode_satuan').val(result.kode_satuan);
                $('#keterangan').val(result.keterangan);
                $('#button-edit').attr("disabled", true);
            } else {
                $('#id_satuan').val(result.id);
                $('#nama_satuan').attr("disabled", false);
                $('#kode_satuan').attr("disabled", false);
                $('#keterangan').attr("disabled", false);
                $('#button-edit').attr("disabled", false);
                $('#id_satuan').val(result.id);
                $('#nama_satuan').val(result.nama_satuan);
                $('#kode_satuan').val(result.kode_satuan);
                $('#keterangan').val(result.keterangan);
            }

            $('#is_active_status').html(`
            <div class="col-12 col-md-3"><label for="is_active" class="switch switch-default switch-primary mr-2">Status Satuan</label></div>
            <div class="col-12 col-md-9"><input type="checkbox" class="switch-input" ${result.is_active == '1' ? 'checked' : null} id="is_active" name="is_active"> <span class="switch-label"></span> <span class="switch-handle"></span></div>
            `);
            $('#is_active').on('change', function (event) {
                swal({
                    title: 'peringatan!',
                    text: `Apakah anda yakin akan meng-update data satuan ${result.nama_satuan}?`,
                    icon: 'warning',
                    button: 'OK'
                }).then(function (results) {
                    if (results) {
                        $.ajax({
                            type: 'put',
                            url: `${endpoint}/satuan/${$('#id_satuan').val()}`,
                            cache: false,
                            processData: false,
                            contentType: false,
                            data: JSON.stringify({
                                'nama_satuan': result.nama_satuan,
                                'kode_satuan': result.kode_satuan,
                                'keterangan': result.keterangan,
                                'is_active': result.is_active == '0' ? 1 : 0
                            }),
                            headers: {
                                "Authorization": `Bearer ${token}`,
                                "Content-Type": "application/json"
                            },
                            success: function (result) {
                                swal({
                                    title: 'Success!',
                                    text: `${result.message}`,
                                    icon: 'success',
                                    button: 'OK'
                                }).then(function () {
                                    $('#satuan-table').DataTable().ajax.reload();
                                    myModal.hide();
                                });
                                console.log(result)
                            },
                        })

                    }

                });
            });
            console.log(result)
        },
        error: function (err) {
            console.log(err)
        }
    });

    myModal.show();
}

function editSatuan() {
    var id = $('#id_satuan').val();
    $.ajax({
        type: 'put',
        url: `${endpoint}/satuan/${id}`,
        cache: false,
        processData: false,
        contentType: false,
        data: JSON.stringify({
            'nama_satuan': $('#nama_satuan').val(),
            'kode_satuan': $('#kode_satuan').val(),
            'keterangan': $('#keterangan').val(),
        }),
        headers: {
            "Authorization": `Bearer ${token}`,
            "Content-Type": "application/json"
        },
        success: function (result) {
            swal({
                title: 'Success!',
                text: `${result.message}`,
                icon: 'success',
                button: 'OK'
            }).then(function () {
                $('#satuan-table').DataTable().ajax.reload();
                var myModal = new bootstrap.Modal(document.getElementById('modalSatuan'), {
                    keyboard: false
                });
                myModal.hide();
            });
            console.log(result)
        },
        error: function (err) {
            var status = err.status;
            if (status === 422) {
                var message = err.responseJSON.errors;
                if (message.nama_satuan !== undefined) {
                    $('#nama_satuan_validation').html(`${message.nama_satuan}`);
                } else {
                    $('#nama_satuan_validation').html(``)
                }
                console.log(message);
            }

            console.log(status)
        }
    });
}

function deleteDataSatuan(id) {
    swal({
        title: 'Peringatan!',
        text: `Apakah anda yakin akan menghapus data ini?`,
        icon: 'warning',
        button: 'OK'
    }).then(function (value) {
        if (value) {
            $.ajax({
                type: 'delete',
                url: `${endpoint}/satuan/${id}`,
                cache: false,
                processData: false,
                contentType: false,
                headers: {
                    "Authorization": `Bearer ${token}`,
                    "Content-Type": "application/json"
                },
                success: function (result) {
                    swal({
                        title: 'Success!',
                        text: `${result.message}`,
                        icon: 'success',
                        button: 'OK'
                    }).then(function () {
                        $('#satuan-table').DataTable().ajax.reload();
                    });
                    console.log(result)
                },
                error: function (err) {
                    var status = err.status;

                    console.log(status)
                }
            });
        }
    });

}