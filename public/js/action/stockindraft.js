var dataTemporary = [];
var asetIdValue = "";
var jenisProgramValue = "";
var satuanValue = "";
var penyediaValueId = "";
var kemasanValueId = "";
var jumlahValue = "";
var batchValue = "";
var expiredDateValue = "";
var hargaSatuanValue = "";
var kodeValue = "";
var sumberDanaValue = "";
var distributorValue = "";
var stock_in_id = "";
var endpoint = `${window.location.origin}/service`;
var token = localStorage.getItem("token");
var path = window.location.pathname.split("/");
var formatRupiah = (money) => {
    return new Intl.NumberFormat("id-ID", {
        minimumFractionDigits: 0,
    }).format(money);
};
(function ($) {
    "use strict";
    var content = $("#authvalidation");
    var date = new Date();
    var tahun = date.getFullYear();

    $(document).ready(function () {
        $("#year").html(tahun);
        let date = new Date();
        let year = date.getFullYear();
        let month = date.getMonth();
        let dates = date.getDate();
        let hour = date.getHours();
        let minute = date.getMinutes();
        let second = date.getSeconds();
        let num_txt = `${year}${month}${dates}${hour}${minute}${second}`;
        $("#no_transaksi").val(num_txt);
        let listTahun = [];
        listTahun.push(year, year + 1);
        console.log(listTahun);
        $.map(listTahun, function (e) {
            $("#tahun_pengadaan").append(
                $("<option>", {
                    value: e,
                    text: e,
                })
            );
        });

        // select kemasan
        $("#kemasan_id_select")
            .select2({
                placeholder: "Pilih  Kemasan",
                dropdownParent: $("#modalData"),
                allowClear: true,
            })
            .on("select2:selecting", function (e) {
                kemasanValueId = e.params.args.data.id;
            })
            .on("select2:clear", function (e) {
                kemasanValueId = "";
            });

        // select satuan

        $("#satuan_id_select")
            .select2({
                placeholder: "Pilih  Satuan",
                dropdownParent: $("#modalData"),
                allowClear: true,
            })
            .on("select2:selecting", function (e) {
                satuanValue = e.params.args.data.id;
            })
            .on("select2:clear", function (e) {
                satuanValue = "";
            });

        // select penyedia

        $("#penyedia_id_select")
            .select2({
                placeholder: "Pilih  Penyedia",
                dropdownParent: $("#modalData"),
                allowClear: true,
            })
            .on("select2:selecting", function (e) {
                penyediaValueId = e.params.args.data.id;
            })
            .on("select2:clear", function (e) {
                penyediaValueId = "";
            });

        // select aset
        $("#aset_id_select")
            .select2({
                placeholder: "Pilih  Aset",
                dropdownParent: $("#modalData"),
                allowClear: true,
            })
            .on("select2:selecting", function (e) {
                var id = e.params.args.data.id;
                var notice = e?.params?.args?.data?.title;
                asetIdValue = e.params.args.data.id;
                if (notice === "true") {
                    $("#notice_expired_validation").html(
                        `<span class="text-danger">Aset ini Memiliki tanggal expired/ jatuh tempo! pastikan tanggal Expirenya sesuai</span>`
                    );
                    $("#expired_date").val("");
                    $("#expired_date").attr("disabled", false);
                } else {
                    $("#notice_expired_validation").html(``);
                    $("#expired_date").val("2040-01-01");
                    $("#expired_date").attr("disabled", true);
                }
                $.ajax({
                    url: `${endpoint}/perubahan-harga-aset/last-update-harga/${id}`,
                    type: "GET",
                    headers: {
                        Authorization: `Bearer ${token}`,
                    },
                    success: (res) => {
                        console.log(res);
                        if (res !== "") {
                            $("#update-harga-terakhir").html(
                                `Harga Perolehan: Rp. ${res.perubahan_harga} <br/> Diperbaharui: ${res.tanggal}`
                            );
                            $("#harga_satuan").val(res.perubahan_harga);
                        } else {
                            $("#update-harga-terakhir").html(``);
                            $("#harga_satuan").val("");
                        }
                    },
                    error: (err) => console.log(err.responseJSON),
                });
            })
            .on("select2:clear", function () {
                $("#update-harga-terakhir").html(``);
                $("#notice_expired_validation").html(``);
                asetIdValue = "";
            });

        $("#content-draft").html(
            '<span class="text-success text-center py-4">loading...</span>'
        );
        // auth
        $.ajax({
            type: "get",
            url: `${endpoint}/profile`,
            cache: false,
            processData: false,
            contentType: false,
            headers: {
                Authorization: `Bearer ${token}`,
            },
            success: function (result) {
                if (result.role_name !== "User Unit") {
                    window.location.href = "/unauthorized";
                }
                content.html(`<a href="#" class="dropdown-toggle active" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <img class="user-avatar rounded-circle active" src="${result.profile_pict}" alt="User Avatar">
                <span class="ml-2">${result.name}</span>
            </a>
            <div class="user-menu dropdown-menu">
                <a class="nav-link" href="#"><i class="fa fa- user"></i>My Profile</a>
                <a class="nav-link" href="#"><i class="fa fa- user"></i>Notifications <span class="count">0</span></a>
                <a class="nav-link" href="#"><i class="fa fa -cog"></i>Settings</a>
                <a class="nav-link" href="#" onclick="logout()"><i class="fa fa-power -off"></i>Logout</a>
            </div>
                    `);

                console.log(result.role_name);
                if (result.role_name === "User Unit") {
                    userAdmin();
                }
            },
            error: function (err) {
                if (err.status == 401) {
                    window.location.href = "/login";
                }
            },
        });
    });
})(jQuery);

function userAdmin() {
    $.ajax({
        type: "GET",
        url: `${endpoint}/stock-in/detail/${path[4]}`,
        cache: false,
        processData: false,
        contentType: false,
        headers: {
            Authorization: `Bearer ${token}`,
        },
        success: (res) => {
            jenisProgramValue = res.nama_program;
            stock_in_id = res[0].id;
            if (
                res[0].is_status === "waiting approval" ||
                res[0].is_status === "approved"
            ) {
                back();
            }

            $("#content-draft").html(`
            <div class="card-header">
                <div class="row">
                    <div class="col-xl-6 col-md-6">
                        <div class="form-group row">
                            <label for="no_transaksi" class=" col-sm-6 font-weight-bold">Nomor Transaksi</label>
                            <div class="col-sm-6">
                                <p class="font-weight-bold">: ${
                                    res[0].no_transaksi
                                }</p>
                                <input type="hidden" id="id_trx" name="id_trx" value="${
                                    res[0].id
                                }"/>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-6 col-md-6">
                        <div class="form-group row">
                            <label for="no_transaksi" class=" col-sm-6 font-weight-bold">Tahun Pengadaan</label>
                            <div class="col-sm-6">
                                <p class="font-weight-bold">: ${
                                    res[0].tahun_pengadaan
                                }</p>
                                <input type="hidden" id="id_trx" name="id_trx"/>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-6 col-md-6">
                        <div class="form-group row">
                            <label for="no_transaksi" class=" col-sm-6 font-weight-bold">Nomor Berita Acara</label>
                            <div class="col-sm-6">
                                <p class="font-weight-bold">: ${
                                    res[0].no_berita_acara
                                }</p>
                                <input type="hidden" id="id_trx" name="id_trx"/>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-6 col-md-6">
                        <div class="form-group row">
                            <label for="no_transaksi" class=" col-sm-6 font-weight-bold">Nomor Dokumen Pengiriman</label>
                            <div class="col-sm-6">
                                <p class="font-weight-bold">: ${
                                    res[0].no_dokumen_pengiriman
                                }</p>
                                <input type="hidden" id="id_trx" name="id_trx"/>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-6 col-md-6">
                        <div class="form-group row">
                            <label for="no_transaksi" class=" col-sm-6 font-weight-bold">Nama Distributor</label>
                            <div class="col-sm-6">
                                <p class="font-weight-bold">: ${
                                    res[0].nama_distributor
                                }</p>
                                <input type="hidden" id="id_trx" name="id_trx"/>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-6 col-md-6">
                        <div class="form-group row">
                            <label for="no_transaksi" class=" col-sm-6 font-weight-bold">Sumber Dana</label>
                            <div class="col-sm-6">
                                <p class="font-weight-bold">: ${
                                    res[0].nama_sumber_dana
                                }</p>
                                <input type="hidden" id="id_trx" name="id_trx"/>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-6 col-md-6">
                        <div class="form-group row">
                            <label for="no_transaksi" class=" col-sm-6 font-weight-bold">Program</label>
                            <div class="col-sm-6">
                                <p class="font-weight-bold">: ${
                                    res[0].nama_program
                                }</p>
                                <input type="hidden" id="id_trx" name="id_trx"/>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-6 col-md-6">
                        <div class="form-group row">
                            <label for="no_transaksi" class=" col-sm-6 font-weight-bold">Unit</label>
                            <div class="col-sm-6">
                                <p class="font-weight-bold">: ${
                                    res[0].nama_unit
                                }</p>
                                <input type="hidden" id="id_trx" name="id_trx"/>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-6 col-md-6">
                        <div class="form-group row">
                            <label for="no_transaksi" class=" col-sm-6 font-weight-bold">Petugas</label>
                            <div class="col-sm-6">
                                <p class="font-weight-bold">: ${
                                    res[0].petugas
                                }</p>
                                <input type="hidden" id="id_trx" name="id_trx"/>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-6 col-md-6">
                        <div class="form-group row">
                            <label for="no_transaksi" class=" col-sm-6 font-weight-bold">Keterangan</label>
                            <div class="col-sm-6">
                                <p class="font-weight-bold">: ${
                                    res[0].keterangan
                                }</p>
                                <input type="hidden" id="id_trx" name="id_trx"/>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-8 col-md-12">
                        <div class="d-flex justify-content-start">
                            <a ${
                                res[0].dokumen_berita_acara ===
                                window.location.origin + "/upload/aset_dokumen"
                                    ? `onclick="notAsetUploaded()" href="#" `
                                    : `href="${res[0].dokumen_berita_acara}" target="_blank" `
                            } class="btn btn-outline-primary my-1">Dokumen Berita Acara</a>
                            <a ${
                                res[0].dokumen_expedisi ===
                                window.location.origin + "/upload/aset_dokumen"
                                    ? "onclick='notAsetUploaded()' href='#' "
                                    : `href="${res[0].dokumen_expedisi}" target="_blank" `
                            } class="btn btn-outline-warning my-1 mx-2">Dokumen Expedisi</a>
                            <a ${
                                res[0].dokumen_pendukung ===
                                window.location.origin + "/upload/aset_dokumen"
                                    ? `onclick='notAsetUploaded()' href='#' `
                                    : `href="${res[0].dokumen_pendukung}" target="_blank" `
                            } class="btn btn-outline-success my-1">Dokumen Pendukung</a>
                        </div>
                    </div>
                    <div class="col-xl-4 col-md-3">
                        <a href="#"  class="btn btn-outline-danger my-1 " onclick="editStockInMaster(${
                            res[0].id
                        })">Edit</a>
                    </div>
                </div>
            </div>
            <div class="card-body">
                    <div class="container">
                        <div id="content-sub-item">
                            <table id="drafts-table" class="table table-striped table-bordered py-4">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Nomor Aset</th>
                                        <th>Kode</th>
                                        <th>Nama</th>
                                        <th>Batch</th>
                                        <th>Harga Satuan</th>
                                        <th>Jumlah</th>
                                        <th>Tgl Expired</th>
                                        <th>Penyedia</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                            </table>
                            <div class="mt-3">
                                <div class="d-flex justify-content-start">
                                    <button class="btn btn-secondary" onclick="tambahData()">Tambah Data ke Daftar Masuk</button>
                                </div>
                                <div class="d-flex justify-content-end">
                                    <button type="button" class="btn btn-danger mx-2" onclick="back()">Kembali</button>
                                    <button class="btn btn-primary mx-2" type="button" onclick="saveAll()">Simpan</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            `);
            showDetailStockIn(res[0].id);
            // showDetailStockIn();
        },
        error: (err) => {
            console.log(err.status);
            if (err.status === 404) {
                window.location.href = "/not-found";
            }

            if (err.status === 401) {
                window.location.href = "/unauthorized";
            }
        },
    });
}

function notAsetUploaded() {
    return swal({
        title: "Dokumen tidak tersedia!",
        text: `Dokumen ini belum di upload, mohon upload terlebih dahulu!`,
        icon: "warning",
        button: "OK",
    });
}

function saveAll() {
    console.log("SAVE ALL");
    var body = {
        stock_in_id: stock_in_id,
        is_status: "waiting approval",
    };
    $.ajax({
        url: `${endpoint}/stock-in/store-status`,
        type: "POST",
        headers: {
            Accept: "application/json",
            Authorization: `Bearer ${token}`,
        },
        data: body,
        success: (res) => {
            console.log(res);
            swal({
                title: "warning!",
                text: `${res.message}`,
                icon: "info",
                button: "OK",
            }).then(() => back());
        },
        error: (err) => {
            var code = err.responseJSON;
            swal({
                title: "warning!",
                text: `${code.message}`,
                icon: "info",
                button: "OK",
            });
        },
    });
}

function editDataDetailStock() {
    var id = $("#id_value").val();

    var form = new FormData();
    form.append("detail_aset_id", $("#aset_id_select_edit").val());
    form.append("satuan_id", $("#satuan_id_select_edit").val());
    form.append("kemasan_id", $("#kemasan_id_select_edit").val());
    form.append("penyedia_id", $("#penyedia_id_select_edit").val());
    form.append("qty", $("#jumlah_edit").val());
    form.append("kode", $("#kode_edit").val());
    form.append("batch", $("#batch_edit").val());
    form.append("expired_date", $("#expired_date_edit").val());
    form.append("harga_satuan", $("#harga_satuan_edit").val());


    $.ajax({
        type: "POST",
        url: `${endpoint}/stock-in-detail/${id}`,
        cache: false,
        processData: false,
        contentType: false,
        data: form,
        headers: {
            Authorization: `Bearer ${token}`,
            Accept: "application/json",
        },
        success: (res) => {
            swal({
                title: "success!",
                text: `${res.message}`,
                icon: "success",
                button: "OK",
            }).then(function () {
                userAdmin();
            });
        },
        error: (err) => {
            if (err.status === 422) {
                var message = err.responseJSON.errors;

                if (message.detail_aset_id !== undefined) {
                    $("#aset_id_edit_validation").html(
                        `${message.detail_aset_id}`
                    );
                } else {
                    $("#aset_id_edit_validation").html(``);
                }
            }
            console.log(err.status);
        },
    });
}

function editData(dataId) {
    $.ajax({
        url: `${endpoint}/stock-in-detail/id/${dataId}`,
        type: "GET",
        headers: {
            Authorization: `Bearer ${token}`,
        },
        success: (val) => {
            $("#id_value").val(val.id);
            $("#aset_id_select_edit").val(val.detail_aset_id);
            $("#aset_id_select_edit").val(val.detail_aset_id);
            $("#penyedia_id_select_edit").val(val.penyedia_id);
            $("#satuan_id_select_edit").val(val.satuan_id);
            $("#kemasan_id_select_edit").val(val.kemasan_id);
            $("#jumlah_edit").val(val.qty);
            $("#kode_edit").val(val.kode);
            $("#batch_edit").val(val.batch);
            $("#expired_date_edit").val(val.expired_date);
            $("#harga_satuan_edit").val(val.harga_satuan);
        },
    });
    var myModal = new bootstrap.Modal(
        document.getElementById("modalDataEditDetailStock"),
        {
            keyboard: false,
        }
    );
    myModal.show();
}

function showDetailStockIn(id) {
    return $("#drafts-table").DataTable({
        processing: true,
        serverSide: true,
        dom: "Bfrtip",
        ajax: {
            url: `${endpoint}/stock-in-detail/${id}`,
            type: "GET",
            headers: {
                Authorization: `Bearer ${token}`,
            },
            // data: e => console.log(e),
            // success: e => console.log(e),
        },
        columns: [
            {
                data: "DT_RowIndex",
                name: "DT_RowIndex",
            },
            {
                data: "nomor_aset",
                name: "nomor_aset",
            },
            {
                data: "kode",
                name: "kode",
            },
            {
                data: "nama_detail_aset",
                name: "nama_detail_aset",
            },
            {
                data: "batch",
                name: "batch",
            },
            {
                data: "harga_satuan",
                render: (a, b, row) => `Rp ${formatRupiah(row.harga_satuan)}`,
            },
            {
                data: "qty",
                name: "qty",
            },
            {
                data: "expired_date",
                name: "expired_date",
            },
            {
                data: "nama_penyedia",
                name: "nama_penyedia",
            },
            {
                data: "action",
                name: "action",
            },
        ],
    });
}

function editStockInMaster() {
    var id = $("#id_trx").val();
    $.ajax({
        url: `${endpoint}/stock-in/detail-id/${id}`,
        cache: false,
        processData: false,
        contentType: false,
        headers: {
            Authorization: `Bearer ${token}`,
        },
        success: (res) => {
            $("#no_berita_acara").val(res.no_berita_acara);
            $("#no_dokumen_pengiriman").val(res.no_dokumen_pengiriman);
            $("#jenis_id_select").val(res.jenis_id);
            $("#distributor_id_select").val(res.distributor_id);
            $("#tanggal_dokumen").val(res.tanggal_dokumen);
            $("#sumber_dana_id_select").val(res.sumber_dana_id);
            $("#dokumen_expedisi").val("");
            $("#dokumen_pendukung").val("");
            $("#dokumen_berita_acara").val("");
        },
        error: (err) => {
            console.log(err.status);
            if (err.status === 404) {
                window.location.href = "/not-found";
            }

            if (err.status === 401) {
                window.location.href = "/unauthorized";
            }
        },
    });
    var myModal = new bootstrap.Modal(document.getElementById("modalUpload"), {
        keyboard: false,
    });
    myModal.show();
}

function back() {
    return (window.location.href = `${window.location.origin}/stok-masuk`);
}

$("#harga_satuan").on("change keyup paste", function () {
    hargaSatuanValue = $("#harga_satuan").val();
    if (hargaSatuanValue === "") {
        hargaSatuanValue = "";
    }
});

$("#expired_date").on("change keyup paste", function () {
    expiredDateValue = $("#expired_date").val();
    if (expiredDateValue === "") {
        expiredDateValue = "";
    }
});

$("#jumlah").on("change keyup paste", function () {
    jumlahValue = $("#jumlah").val();
    if (jumlahValue === "") {
        jumlahValue = "";
    }
});

$("#kode").on("change keyup paste", function () {
    kodeValue = $("#kode").val();
    if (kodeValue === "") {
        kodeValue = "";
    }
});

$("#batch").on("change keyup paste", function () {
    batchValue = $("#batch").val();
    if (batchValue === "") {
        batchValue = "";
    }
});

function addData() {
    var id = $("#id_trx").val();
    var dokumen_berita_acara =
        $("#dokumen_berita_acara").prop("files")[0] === undefined
            ? null
            : $("#dokumen_berita_acara").prop("files")[0];
    var dokumen_expedisi =
        $("#dokumen_expedisi").prop("files")[0] === undefined
            ? null
            : $("#dokumen_expedisi").prop("files")[0];
    var dokumen_pendukung =
        $("#dokumen_pendukung").prop("files")[0] === undefined
            ? null
            : $("#dokumen_pendukung").prop("files")[0];
    console.log(dokumen_pendukung);
    var form = new FormData();
    form.append("no_berita_acara", $("#no_berita_acara").val());
    form.append("no_dokumen_pengiriman", $("#no_dokumen_pengiriman").val());
    form.append("tanggal_dokumen", $("#tanggal_dokumen").val());
    form.append("jenis_id", $("#jenis_id_select").val());
    form.append("distributor_id", $("#distributor_id_select").val());
    form.append("tahun_pengadaan", $("#tahun_pengadaan").val());
    form.append("sumber_dana_id", $("#sumber_dana_id_select").val());
    dokumen_berita_acara != null &&
        form.append("dokumen_berita_acara", dokumen_berita_acara);
    dokumen_expedisi != null &&
        form.append("dokumen_expedisi", dokumen_expedisi);
    dokumen_pendukung != null &&
        form.append("dokumen_pendukung", dokumen_pendukung);
    form.append("keterangan", $("#keterangan").val());
    $.ajax({
        type: "POST",
        url: `${endpoint}/stock-in/detail-id/${id}`,
        cache: false,
        processData: false,
        contentType: false,
        data: form,
        headers: {
            Authorization: `Bearer ${token}`,
        },
        success: (res) => {
            swal({
                title: "success!",
                text: `${res.message}`,
                icon: "success",
                button: "OK",
            }).then(() => {
                window.location.reload();
            });
        },
        error: (err) => {
            if (err.status === 422) {
                var message = err.responseJSON.errors;

                if (message.tahun_pengadaan !== undefined) {
                    $("#tahun_pengadaan_validation").html(
                        `${message.tahun_pengadaan}`
                    );
                } else {
                    $("#tahun_pengadaan_validation").html(``);
                }

                if (message.sumber_dana_id !== undefined) {
                    $("#sumber_dana_id_validation").html(
                        `${message.sumber_dana_id}`
                    );
                } else {
                    $("#sumber_dana_id_validation").html(``);
                }

                if (message.tanggal_dokumen !== undefined) {
                    $("#tanggal_dokumen_validation").html(
                        `${message.tanggal_dokumen}`
                    );
                } else {
                    $("#tanggal_dokumen_validation").html(``);
                }

                if (message.no_dokumen_pengiriman !== undefined) {
                    $("#no_dokumen_pengiriman_validation").html(
                        `${message.no_dokumen_pengiriman}`
                    );
                } else {
                    $("#no_dokumen_pengiriman_validation").html(``);
                }

                if (message.no_berita_acara !== undefined) {
                    $("#no_berita_acara_validation").html(
                        `${message.no_berita_acara}`
                    );
                } else {
                    $("#no_berita_acara_validation").html(``);
                }

                if (message.jenis_id !== undefined) {
                    $("#jenis_id_validation").html(`${message.jenis_id}`);
                } else {
                    $("#jenis_id_validation").html(``);
                }
                if (message.penyedia_id !== undefined) {
                    $("#penyedia_id_validation").html(`${message.penyedia_id}`);
                } else {
                    $("#penyedia_id_validation").html(``);
                }
                if (message.distributor_id !== undefined) {
                    $("#distributor_id_validation").html(
                        `${message.distributor_id}`
                    );
                } else {
                    $("#distributor_id_validation").html(``);
                }
                if (message.dokumen_berita_acara !== undefined) {
                    $("#dokumen_berita_acara_validation").html(
                        `${message.dokumen_berita_acara}`
                    );
                } else {
                    $("#dokumen_berita_acara_validation").html(``);
                }
                if (message.dokumen_expedisi !== undefined) {
                    $("#dokumen_expedisi_validation").html(
                        `${message.dokumen_expedisi}`
                    );
                } else {
                    $("#dokumen_expedisi_validation").html(``);
                }
                if (message.dokumen_pendukung !== undefined) {
                    $("#dokumen_pendukung_validation").html(
                        `${message.dokumen_pendukung}`
                    );
                } else {
                    $("#dokumen_pendukung_validation").html(``);
                }
            }
            console.log(err.status);
        },
    });
}

function addDataDetailStock() {
    var form = new FormData();
    form.append("stock_in_id", stock_in_id);
    form.append("detail_aset_id", asetIdValue);
    form.append("penyedia_id", penyediaValueId);
    form.append("satuan_id", satuanValue);
    form.append("kemasan_id", kemasanValueId);
    form.append("qty", $("#jumlah").val());
    form.append("kode", $("#kode").val());
    form.append("batch", $("#batch").val());
    form.append("expired_date", $("#expired_date").val());
    form.append("harga_satuan", $("#harga_satuan").val());
    $.ajax({
        type: "POST",
        url: `${endpoint}/stock-in-detail`,
        cache: false,
        processData: false,
        contentType: false,
        headers: {
            Authorization: `Bearer ${token}`,
        },
        data: form,
        success: (res) => {
            swal({
                title: "success!",
                text: `${res.message}`,
                icon: "success",
                button: "OK",
            }).then(() => {
                $("#drafts-table").DataTable().ajax.reload();
            });
        },
        error: (err) => {
            var status = err.status;
            if (status === 422) {
                var message = err?.responseJSON?.errors;

                if (message.duplicate !== undefined) {
                    swal({
                        title: "error!",
                        text: `${message.duplicate}`,
                        icon: "error",
                        button: "OK",
                    });
                }

                if (message.expired_date_limit !== undefined) {
                    swal({
                        title: "error!",
                        text: `${message.expired_date_limit}`,
                        icon: "error",
                        button: "OK",
                    });
                }

                if (message.detail_aset_id !== undefined) {
                    $("#aset_id_validation").html(`${message.detail_aset_id}`);
                } else {
                    $("#aset_id_validation").html(``);
                }
                if (message.penyedia_id !== undefined) {
                    $("#penyedia_id_validation").html(`${message.penyedia_id}`);
                } else {
                    $("#penyedia_id_validation").html(``);
                }
                if (message.satuan_id !== undefined) {
                    $("#satuan_id_validation").html(`${message.satuan_id}`);
                } else {
                    $("#satuan_id_validation").html(``);
                }
                if (message.kemasan_id !== undefined) {
                    $("#kemasan_id_validation").html(`${message.kemasan_id}`);
                } else {
                    $("#kemasan_id_validation").html(``);
                }
                if (message.qty !== undefined) {
                    $("#jumlah_validation").html(`${message.qty}`);
                } else {
                    $("#jumlah_validation").html(``);
                }
                if (message.kode !== undefined) {
                    $("#kode_validation").html(`${message.kode}`);
                } else {
                    $("#kode_validation").html(``);
                }
                if (message.batch !== undefined) {
                    $("#batch_validation").html(`${message.batch}`);
                } else {
                    $("#batch_validation").html(``);
                }
                if (message.expired_date !== undefined) {
                    $("#expired_date_validation").html(
                        `${message.expired_date}`
                    );
                } else {
                    $("#expired_date_validation").html(``);
                }
                if (message.harga_satuan !== undefined) {
                    $("#harga_satuan_validation").html(
                        `${message.harga_satuan}`
                    );
                } else {
                    $("#harga_satuan_validation").html(``);
                }
                if (message.aset_image !== undefined) {
                    $("#aset_image_validation").html(`${message.aset_image}`);
                } else {
                    $("#aset_image_validation").html(``);
                }
            }
        },
    });
}

function tambahData() {
    $("#button-add").css("display", "block");
    $("#button-edit").css("display", "none");
    $("#aset_id_select").val("");
    $("#aset_id_select").val("");
    $("#penyedia_id_select").val("");
    $("#satuan_id_select").val("");
    $("#kemasan_id_select").val("");
    $("#jumlah").val("");
    $("#kode").val("");
    $("#batch").val("");
    $("#expired_date").val("");
    $("#expired_date").attr("disabled", false);
    $("#harga_satuan").val("");
    var myModal = new bootstrap.Modal(document.getElementById("modalData"), {
        keyboard: false,
    });
    myModal.show();
}

function deleteData(id) {
    swal({
        title: "warning!",
        text: `Akah anda yakin akan menghapus data ini?!`,
        icon: "warning",
        button: "OK",
    }).then(function (value) {
        if (value) {
            $.ajax({
                type: "DELETE",
                url: `${endpoint}/stock-in-detail/${id}`,
                cache: false,
                processData: false,
                contentType: false,
                headers: {
                    Authorization: `Bearer ${token}`,
                },
                success: function (event) {
                    swal({
                        title: "Success!",
                        text: `${event.message}`,
                        icon: "success",
                        button: "OK",
                    });
                    $("#drafts-table").DataTable().ajax.reload();
                    console.log(event);
                },
                error: function (err) {
                    console.log(err.responseJSON);
                    swal({
                        title: "Error!",
                        text: `${err}`,
                        icon: "error",
                        button: "OK",
                    });
                },
            });
        }
    });
}

function logout() {
    swal({
        title: "peringatan!",
        text: `Apakah anda yakin akan melakukan logout?`,
        icon: "warning",
        button: "OK",
    }).then(function (result) {
        if (result) {
            localStorage.removeItem("token");
            window.location.href = "/login";
        }
    });
}
