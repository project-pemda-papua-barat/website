(function ($) {
    "use strict";
    var content = $('#authvalidation');
    var endpoint = `${window.location.origin}/service`;
    var token = localStorage.getItem('token');

    $(document).ready(function () {
        content.html('Loading...');
        $.ajax({
            type: 'get',
            url: `${endpoint}/profile`,
            cache: false,
            processData: false,
            contentType: false,
            headers: {
                "Authorization": `Bearer ${token}`,
            },
            success: function (result) {
                console.log(result.role_name)
                if (result.role_name === "Super Admin") {
                    content.html(`<a href="#" class="dropdown-toggle active" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <img class="user-avatar rounded-circle active" src="${result.profile_pict}" alt="User Avatar">
                    <span class="ml-2">${result.name}</span>
                </a>
                <div class="user-menu dropdown-menu">
                    <a class="nav-link" href="#"><i class="fa fa- user"></i>My Profile</a>
                    <a class="nav-link" href="#"><i class="fa fa- user"></i>Notifications <span class="count">0</span></a>
                    <a class="nav-link" href="#"><i class="fa fa -cog"></i>Settings</a>
                    <a class="nav-link" href="#" onclick="logout()"><i class="fa fa-power -off"></i>Logout</a>
                </div>
                        `);
                } else {
                    window.location.href = "/unauthorized";
                }

                console.log(result);
            },
            error: function (err) {
                if (err.status == 401) {
                    window.location.href = "/login";
                }
            }
        });
    });

})(jQuery);

function logout() {
    swal({
        title: 'peringatan!',
        text: `Apakah anda yakin akan melakukan logout?`,
        icon: 'warning',
        button: 'OK'
    }).then(function (result) {
        if (result) {
            localStorage.removeItem('token');
            window.location.href = "/login";
        }
    });

}