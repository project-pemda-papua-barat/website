var dataTemporary = [];
var asetIdValue = "";
var distributorValue = "";
var unitValue = "";
var endpoint = `${window.location.origin}/service`;
var token = localStorage.getItem('token');

(function ($) {
    "use strict";
    var content = $('#authvalidation');
    var date = new Date;
    var tahun = date.getFullYear();

    $(document).ready(function () {
        $('#year').html(tahun);
        let date = new Date;
        let year = date.getFullYear();
        let month =
        date.getMonth() + 1 < 10
            ? `0${date.getMonth() + 1}`
            : date.getMonth() + 1;
    let dates = date.getDate() < 10 ? `0${date.getDate()}` : date.getDate();
    let hour =
        date.getHours() < 10 ? `0${date.getHours()}` : date.getHours();
    let minute =
        date.getMinutes() < 10
            ? `0${date.getMinutes()}`
            : date.getMinutes();
    let second =
        date.getSeconds() < 10
            ? `0${date.getSeconds()}`
            : date.getSeconds();
        let num_txt = `${year}${month}${dates}${hour}${minute}${second}`
        $('#no_transaksi').val(num_txt);
        let listTahun = [];
        listTahun.push(year, year + 1);
        console.log(listTahun);
        $.map(listTahun, function (e) {
            $('#tahun_pengadaan').append($('<option>', {
                value: e,
                text: e
            }));
        })

        $('#content-sub-item').addClass('d-none');
        // auth
        $.ajax({
            type: 'get',
            url: `${endpoint}/profile`,
            cache: false,
            processData: false,
            contentType: false,
            headers: {
                "Authorization": `Bearer ${token}`,
            },
            success: function (result) {
                if (result.role_name != 'User Unit') {
                    window.location.href = "/unauthorized";
                }
                content.html(`<a href="#" class="dropdown-toggle active" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <img class="user-avatar rounded-circle active" src="${result.profile_pict}" alt="User Avatar">
                <span class="ml-2">${result.name}</span>
            </a>
            <div class="user-menu dropdown-menu">
                <a class="nav-link" href="#"><i class="fa fa- user"></i>My Profile</a>
                <a class="nav-link" href="#"><i class="fa fa- user"></i>Notifications <span class="count">0</span></a>
                <a class="nav-link" href="#"><i class="fa fa -cog"></i>Settings</a>
                <a class="nav-link" href="#" onclick="logout()"><i class="fa fa-power -off"></i>Logout</a>
            </div>
                    `);
            },
            error: function (err) {
                if (err.status == 401) {
                    window.location.href = "/login";
                }
            }
        });

        // distributor jenis
        $('#distributor_id_select').select2({
            placeholder: "Pilih  Distributor",
            allowClear: true,
        }).on("select2:selecting", function (e) {
            distributorValue = e.params.args.data.id;
        }).on("select2:clear", function () {
            $("#update-harga-terakhir").html(``);
            distributorValue = "";
        });

        // distributor jenis
        $('#unit_id_select').select2({
            placeholder: "Pilih  Unit Tujuan",
            allowClear: true,
        }).on("select2:selecting", function (e) {
            unitValue = e.params.args.data.id;
        }).on("select2:clear", function () {
            unitValue = "";
        });



    });

})(jQuery);

function addData() {
    var dokumen_berita_acara = $('#dokumen_berita_acara').prop('files')[0];
    var dokumen_expedisi = $('#dokumen_expedisi').prop('files')[0];
    var dokumen_pendukung = $('#dokumen_pendukung').prop('files')[0];
    var form = new FormData();
    form.append('no_transaksi', $('#no_transaksi').val());
    form.append('no_berita_acara', $('#no_berita_acara').val());
    form.append('no_dokumen_pengiriman', $('#no_dokumen_pengiriman').val());
    form.append('tanggal_dokumen', $('#tanggal_dokumen').val());
    form.append('distributor_id', distributorValue);
    form.append('to_unit_id', unitValue);
    dokumen_berita_acara !== undefined && form.append('dokumen_berita_acara', dokumen_berita_acara);
    dokumen_expedisi !== undefined && form.append('dokumen_expedisi', dokumen_expedisi);
    dokumen_pendukung !== undefined && form.append('dokumen_pendukung', dokumen_pendukung);
    form.append('keterangan', $('#keterangan').val());
    $.ajax({
        type: 'POST',
        url: `${endpoint}/stock-out`,
        cache: false,
        processData: false,
        contentType: false,
        data: form,
        headers: {
            "Authorization": `Bearer ${token}`
        },
        success: function (result) {
            swal({
                title: 'Success!',
                text: `${result.message}`,
                icon: 'success',
                button: 'OK'
            });
            window.location.href = `${window.location.origin}/stok-keluar/tambah/draft/${$('#no_transaksi').val()}`
            console.log(result)
        },
        error: function (err) {
            var status = err.status;
            if (status === 422) {
                var message = err.responseJSON.errors;


                if (message.tanggal_dokumen !== undefined) {
                    $('#tanggal_dokumen_validation').html(`${message.tanggal_dokumen}`);
                } else {
                    $('#tanggal_dokumen_validation').html(``)
                }

                if (message.no_dokumen_pengiriman !== undefined) {
                    $('#no_dokumen_pengiriman_validation').html(`${message.no_dokumen_pengiriman}`);
                } else {
                    $('#no_dokumen_pengiriman_validation').html(``)
                }

                if (message.no_berita_acara !== undefined) {
                    $('#no_berita_acara_validation').html(`${message.no_berita_acara}`);
                } else {
                    $('#no_berita_acara_validation').html(``)
                }

                if (message.to_unit_id !== undefined) {
                    $('#to_unit_id_validation').html(`${message.to_unit_id}`);
                } else {
                    $('#to_unit_id_validation').html(``)
                }
                if (message.penyedia_id !== undefined) {
                    $('#penyedia_id_validation').html(`${message.penyedia_id}`);
                } else {
                    $('#penyedia_id_validation').html(``)
                }
                if (message.distributor_id !== undefined) {
                    $('#distributor_id_validation').html(`${message.distributor_id}`);
                } else {
                    $('#distributor_id_validation').html(``)
                }
                if (message.dokumen_berita_acara !== undefined) {
                    $('#dokumen_berita_acara_validation').html(`${message.dokumen_berita_acara}`);
                } else {
                    $('#dokumen_berita_acara_validation').html(``)
                }
                if (message.dokumen_expedisi !== undefined) {
                    $('#dokumen_expedisi_validation').html(`${message.dokumen_expedisi}`);
                } else {
                    $('#dokumen_expedisi_validation').html(``)
                }
                if (message.dokumen_pendukung !== undefined) {
                    $('#dokumen_pendukung_validation').html(`${message.dokumen_pendukung}`);
                } else {
                    $('#dokumen_pendukung_validation').html(``)
                }
            }

            console.log(status)
        }
    });
}

function showFormUploadDoc() {
    var myModal = new bootstrap.Modal(document.getElementById('modalUpload'), {
        keyboard: false
    });
    myModal.show();
}

async function updateToDraft() {
    console.log('update to draft');
    let data = [];
    var maping = [];
    await fetch(`${endpoint}/stock-in/unit`, {
        headers: {
            'Content-Type': 'application/json',
            "Authorization": `Bearer ${token}`,
        },
    })
        .then((response) => response.json())
        .then(val => data.push(...val.data))

    await data.length > 0 ? await swal({
        title: 'warning!',
        text: `Apakah anda yakin akan menyimpan data-data ini sebagai draft?`,
        icon: 'warning',
        button: 'OK'
    }).then(value => {
        if (value) {
            data.map(d => maping.push({ id: d.id }));

            $.ajax({
                type: 'POST',
                url: `${endpoint}/stock-out/to-draft`,
                cache: false,
                processData: false,
                contentType: false,
                data: JSON.stringify({ "data": maping }),
                headers: {
                    "Authorization": `Bearer ${token}`,
                    "Content-Type": "application/json"
                },
                success: function (result) {
                    swal({
                        title: 'Success!',
                        text: `${result.message}`,
                        icon: 'success',
                        button: 'OK'
                    });
                    $('#draft-table').DataTable().ajax.reload();
                    console.log(result)
                },
                error: function (err) {
                    var status = err.status;
                    if (status === 422) {
                        var message = err.responseJSON.errors;
                        console.log(message);
                    }

                    console.log(status)
                }
            });
            console.log(maping);
        }
    }) : await swal({
        title: 'warning!',
        text: `Anda belum input aset!`,
        icon: 'warning',
        button: 'OK'
    });

}

function back() {
    return window.location.href = `${window.location.origin}/stok-keluar`;
}

$("#harga_satuan").on('change keyup paste', function () {
    hargaSatuanValue = $("#harga_satuan").val();
    if (hargaSatuanValue === "") {
        hargaSatuanValue = "";
    };
});

$("#expired_date").on('change keyup paste', function () {
    expiredDateValue = $("#expired_date").val();
    if (expiredDateValue === "") {
        expiredDateValue = "";
    };
});

$("#jumlah").on('change keyup paste', function () {
    jumlahValue = $("#jumlah").val();
    if (jumlahValue === "") {
        jumlahValue = "";
    };
});

$("#kode").on('change keyup paste', function () {
    kodeValue = $("#kode").val();
    if (kodeValue === "") {
        kodeValue = "";
    };
});

$("#batch").on('change keyup paste', function () {
    batchValue = $("#batch").val();
    if (batchValue === "") {
        batchValue = "";
    };
});

function validate(a) {
    return typeof a;
}

function messageNull(nullValue) {
    return `${nullValue} belum disii!`;
}


function tambahData() {
    $("#button-add").css("display", "block");
    $("#button-edit").css("display", "none");
    var myModal = new bootstrap.Modal(document.getElementById('modalData'), {
        keyboard: false
    });
    myModal.show();
}

function deleteStockOut(id) {
    swal({
        title: 'warning!',
        text: `Akah anda yakin akan menghapus data ini?!`,
        icon: 'warning',
        button: 'OK'
    }).then(function (value) {
        if (value) {
            $.ajax({
                type: 'DELETE',
                url: `${endpoint}/stock-in/unit/${id}`,
                cache: false,
                processData: false,
                contentType: false,
                headers: {
                    "Authorization": `Bearer ${token}`,
                },
                success: function (event) {
                    swal({
                        title: 'Success!',
                        text: `${event.message}`,
                        icon: 'success',
                        button: 'OK'
                    });
                    $('#draft-table').DataTable().ajax.reload();
                    console.log(event)
                },
                error: function (err) {
                    console.log(err.responseJSON)
                    swal({
                        title: 'Error!',
                        text: `${err}`,
                        icon: 'error',
                        button: 'OK'
                    });
                }
            });
        }
    });
}

function logout() {
    swal({
        title: 'peringatan!',
        text: `Apakah anda yakin akan melakukan logout?`,
        icon: 'warning',
        button: 'OK'
    }).then(function (result) {
        if (result) {
            localStorage.removeItem('token');
            window.location.href = "/login";
        }
    });

}