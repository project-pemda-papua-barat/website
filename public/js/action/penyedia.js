var endpoint = `${window.location.origin}/service`;
var token = localStorage.getItem('token');
$(document).ready(function ($) {
    "use strict";
    var date = new Date();
    var year = date.getFullYear();
    $('#year').html(year);

    $('#penyedia-table').DataTable({
        processing: true,
        serverSide: true,
        dom: 'Bfrtip',
        buttons: [{
            text: 'Tamba Data >',
            className: 'btn btn-primary',
            action: function (e, dt, node, config) {
                $('#nama_penyedia').attr("disabled", false);
                $('#alamat_penyedia').attr("disabled", false);
                $('#no_contact_penyedia').attr("disabled", false);
                $('#keterangan').attr("disabled", false);
                $('#is_active_status').html(``);
                $("#button-add").css("display", "block");
                $("#button-edit").css("display", "none");
                $('#nama_penyedia').val("");
                $('#alamat_penyedia').val("");
                $('#detail_alamat').val("");
                $('#lat').val("");
                $('#long').val("");
                $('#no_contact_penyedia').val("");
                $('#nama_penyedia_validation').html(``);
                $('#alamat_penyedia_validation').html(``);
                $('#no_contact_penyedia_validation').html(``);
                $('#keterangan').val("");
                var myModal = new bootstrap.Modal(document.getElementById('modalPenyedia'), {
                    keyboard: false
                });
                myModal.show();
            }
        }],
        ajax: {
            url: `${endpoint}/penyedia`,
            type: 'GET',
            headers: {
                "Authorization": `Bearer ${token}`,
            },
            // data: e => console.log(e),
            // success: e => console.log(e),
        },
        columns: [{
            data: "DT_RowIndex",
            name: "DT_RowIndex",
        },
        {
            data: 'nama_penyedia',
            name: 'nama_penyedia'
        },
        {
            data: 'alamat_penyedia',
            name: 'alamat_penyedia'
        },
        {
            data: 'detail_alamat',
            name: 'detail_alamat'
        },
        {
            data: 'no_contact_penyedia',
            name: 'no_contact_penyedia'
        },
        {
            data: 'keterangan',
            name: 'keterangan'
        },
        {
            data: 'is_active',
            render: function (data) {
                if (data == 0) {
                    return "Tidak Aktif"
                } else {
                    return "Aktif"
                }

            }
        },
        {
            data: 'action',
            name: 'action'
        },
        ],
    });
});

function addPeyedia() {
    var form = new FormData();
    form.append('nama_penyedia', $('#nama_penyedia').val());
    form.append('alamat_penyedia', $('#alamat_penyedia').val());
    form.append('no_contact_penyedia', $('#no_contact_penyedia').val());
    form.append('detail_alamat', $('#detail_alamat').val());
    form.append('lat', $('#lat').val());
    form.append('long', $('#long').val());
    form.append('keterangan', $('#keterangan').val());
    $.ajax({
        type: 'post',
        url: `${endpoint}/penyedia`,
        cache: false,
        processData: false,
        contentType: false,
        data: form,
        headers: {
            "Authorization": `Bearer ${token}`,
        },
        success: function (result) {
            swal({
                title: 'Success!',
                text: `${result.message}`,
                icon: 'success',
                button: 'OK'
            }).then(function () {
                $('#penyedia-table').DataTable().ajax.reload();
            });
            console.log(result)
        },
        error: function (err) {
            var status = err.status;
            if (status === 422) {
                var message = err.responseJSON.errors;
                if (message.nama_penyedia !== undefined) {
                    $('#nama_penyedia_validation').html(`${message.nama_penyedia}`);
                } else {
                    $('#nama_penyedia_validation').html(``)
                }
                if (message.alamat_penyedia !== undefined) {
                    $('#alamat_penyedia_validation').html(`${message.alamat_penyedia}`);
                } else {
                    $('#alamat_penyedia_validation').html(``)
                }
                if (message.no_contact_penyedia !== undefined) {
                    $('#no_contact_penyedia_validation').html(`${message.no_contact_penyedia}`);
                } else {
                    $('#no_contact_penyedia_validation').html(``)
                }
                if (message.detail_alamat !== undefined) {
                    $('#detail_alamat_penyedia_validation').html(`${message.detail_alamat}`);
                } else {
                    $('#detail_alamat_penyedia_validation').html(``)
                }
                console.log(message);
            }

            console.log(status)
        }
    });
}

function editDataPenyedia(id) {
    $("#button-add").css("display", "none");
    $("#button-edit").css("display", "block");
    var myModal = new bootstrap.Modal(document.getElementById('modalPenyedia'), {
        keyboard: false
    });
    $.ajax({
        type: 'get',
        url: `${endpoint}/penyedia/${id}`,
        cache: false,
        processData: false,
        contentType: false,
        headers: {
            "Authorization": `Bearer ${token}`,
        },
        success: function (result) {
            if (result.is_active == '0') {
                $('#nama_penyedia').attr("disabled", true);
                $('#alamat_penyedia').attr("disabled", true);
                $('#no_contact_penyedia').attr("disabled", true);
                $('#detail_alamat').attr("disabled", true);
                $('#lat').attr("disabled", true);
                $('#long').attr("disabled", true);
                $('#keterangan').attr("disabled", true);
                $('#id_penyedia').val(result.id);
                $('#nama_penyedia').val(result.nama_penyedia);
                $('#alamat_penyedia').val(result.alamat_penyedia);
                $('#detail_alamat').val(result.detail_alamat);
                $('#lat').val(result.lat);
                $('#long').val(result.long);
                $('#no_contact_penyedia').val(result.no_contact_penyedia);
                $('#keterangan').val(result.keterangan);
                $('#button-edit').attr("disabled", true);
            } else {
                $('#nama_penyedia').attr("disabled", false);
                $('#alamat_penyedia').attr("disabled", false);
                $('#no_contact_penyedia').attr("disabled", false);
                $('#keterangan').attr("disabled", false);
                $('#button-edit').attr("disabled", false);
                $('#detail_alamat').attr("disabled", false);
                $('#id_penyedia').val(result.id);
                $('#detail_alamat').val(result.detail_alamat);
                $('#lat').val(result.lat);
                $('#long').val(result.long);
                $('#nama_penyedia').val(result.nama_penyedia);
                $('#alamat_penyedia').val(result.alamat_penyedia);
                $('#no_contact_penyedia').val(result.no_contact_penyedia);
                $('#keterangan').val(result.keterangan);
            }

            $('#is_active_status').html(`
                <div class="col col-md-3"><label for="keterangan" class="switch switch-default switch-primary mr-2">Status Penyedia</label></div>
                <div class="col-12 col-md-9"><input type="checkbox" class="switch-input" ${result.is_active == '1' ? 'checked' : null} id="is_active" name="is_active"> <span class="switch-label"></span> <span class="switch-handle"></span></div>
                `);
            $('#is_active').on('change', function (event) {
                swal({
                    title: 'peringatan!',
                    text: `Apakah anda yakin akan me-nonaltifkan data penyedia ${result.nama_penyedia}?`,
                    icon: 'warning',
                    button: 'OK'
                }).then(function (results) {
                    if (results) {
                        $.ajax({
                            type: 'put',
                            url: `${endpoint}/penyedia/${$('#id_penyedia').val()}`,
                            cache: false,
                            processData: false,
                            contentType: false,
                            data: JSON.stringify({
                                'nama_penyedia': result.nama_penyedia,
                                'alamat_penyedia': result.alamat_penyedia,
                                'no_contact_penyedia': result.no_contact_penyedia,
                                'keterangan': result.keterangan,
                                'is_active': result.is_active == '0' ? 1 : 0
                            }),
                            headers: {
                                "Authorization": `Bearer ${token}`,
                                "Content-Type": "application/json"
                            },
                            success: function (result) {
                                swal({
                                    title: 'Success!',
                                    text: `${result.message}`,
                                    icon: 'success',
                                    button: 'OK'
                                }).then(function () {
                                    $('#penyedia-table').DataTable().ajax.reload();
                                    myModal.hide();
                                });
                                console.log(result)
                            },
                        })

                    }

                });
            });
            console.log(result)
        },
        error: function (err) {
            console.log(err)
        }
    });

    myModal.show();
}

function editPenyedia() {
    var id = $('#id_penyedia').val();
    $.ajax({
        type: 'put',
        url: `${endpoint}/penyedia/${id}`,
        cache: false,
        processData: false,
        contentType: false,
        data: JSON.stringify({
            'nama_penyedia': $('#nama_penyedia').val(),
            'alamat_penyedia': $('#alamat_penyedia').val(),
            'no_contact_penyedia': $('#no_contact_penyedia').val(),
            'keterangan': $('#keterangan').val(),
            'detail_alamat': $('#detail_alamat').val(),
            'lat': $('#lat').val(),
            'long': $('#long').val(),
        }),
        headers: {
            "Authorization": `Bearer ${token}`,
            "Content-Type": "application/json"
        },
        success: function (result) {
            swal({
                title: 'Success!',
                text: `${result.message}`,
                icon: 'success',
                button: 'OK'
            }).then(function () {
                $('#penyedia-table').DataTable().ajax.reload();
                var myModal = new bootstrap.Modal(document.getElementById('modalPenyedia'), {
                    keyboard: false
                });
                myModal.hide();
            });
            console.log(result)
        },
        error: function (err) {
            var status = err.status;
            if (status === 422) {
                var message = err.responseJSON.errors;
                if (message.nama_penyedia !== undefined) {
                    $('#nama_penyedia_validation').html(`${message.nama_penyedia}`);
                } else {
                    $('#nama_penyedia_validation').html(``)
                }
                if (message.alamat_penyedia !== undefined) {
                    $('#alamat_penyedia_validation').html(`${message.alamat_penyedia}`);
                } else {
                    $('#alamat_penyedia_validation').html(``)
                }
                if (message.no_contact_penyedia !== undefined) {
                    $('#no_contact_penyedia_validation').html(`${message.no_contact_penyedia}`);
                } else {
                    $('#no_contact_penyedia_validation').html(``)
                }
                console.log(message);
            }

            console.log(status)
        }
    });
}

function deleteDataPenyedia(id) {
    swal({
        title: 'Peringatan!',
        text: `Apakah anda yakin akan menghapus data ini?`,
        icon: 'warning',
        button: 'OK'
    }).then(function (value) {
        if (value) {
            $.ajax({
                type: 'delete',
                url: `${endpoint}/penyedia/${id}`,
                cache: false,
                processData: false,
                contentType: false,
                headers: {
                    "Authorization": `Bearer ${token}`,
                    "Content-Type": "application/json"
                },
                success: function (result) {
                    swal({
                        title: 'Success!',
                        text: `${result.message}`,
                        icon: 'success',
                        button: 'OK'
                    }).then(function () {
                        $('#penyedia-table').DataTable().ajax.reload();
                    });
                    console.log(result)
                },
                error: function (err) {
                    var status = err.status;

                    console.log(status)
                }
            });
        }
    });

}