var endpoint = `${window.location.origin}/service`;
var token = localStorage.getItem('token');
$(document).ready(function ($) {
    "use strict";
    var date = new Date();
    var year = date.getFullYear();
    $('#year').html(year);

    $('#sumber-dana-table').DataTable({
        processing: true,
        serverSide: true,
        dom: 'Bfrtip',
        buttons: [{
            text: 'Tamba Data >',
            className: 'btn btn-primary',
            action: function (e, dt, node, config) {
                $('#nama_sumber_dana').attr("disabled", false);
                $('#jenis_sumber_dana').attr("disabled", false);
                $('#keterangan').attr("disabled", false);
                $('#is_active_status').html(``);
                $("#button-add").css("display", "block");
                $("#button-edit").css("display", "none");
                $('#nama_sumber_dana').val("");
                $('#jenis_sumber_dana').val("");
                $('#no_contact_sumber_dana').val("");
                $('#nama_sumber_dana_validation').html(``);
                $('#jenis_sumber_dana_validation').html(``);
                $('#keterangan').val("");
                var myModal = new bootstrap.Modal(document.getElementById('modalSumberDana'), {
                    keyboard: false
                });
                myModal.show();
            }
        }],
        ajax: {
            url: `${endpoint}/sumber-dana`,
            type: 'GET',
            headers: {
                "Authorization": `Bearer ${token}`,
            },
            // data: e => console.log(e),
            // success: e => console.log(e),
        },
        columns: [{
            data: "DT_RowIndex",
            name: "DT_RowIndex",
        },
        {
            data: 'nama_sumber_dana',
            name: 'nama_sumber_dana'
        },
        {
            data: 'jenis_sumber_dana',
            name: 'jenis_sumber_dana'
        },
        {
            data: 'keterangan',
            name: 'keterangan'
        },
        {
            data: 'is_active',
            render: function (data) {
                if (data == 0) {
                    return "Tidak Aktif"
                } else {
                    return "Aktif"
                }

            }
        },
        {
            data: 'action',
            name: 'action'
        },
        ],
    });
});

function addSumberDana() {
    var form = new FormData();
    form.append('nama_sumber_dana', $('#nama_sumber_dana').val());
    form.append('jenis_sumber_dana', $('#jenis_sumber_dana').val());
    form.append('keterangan', $('#keterangan').val());
    $.ajax({
        type: 'post',
        url: `${endpoint}/sumber-dana`,
        cache: false,
        processData: false,
        contentType: false,
        data: form,
        headers: {
            "Authorization": `Bearer ${token}`,
        },
        success: function (result) {
            swal({
                title: 'Success!',
                text: `${result.message}`,
                icon: 'success',
                button: 'OK'
            }).then(function () {
                $('#sumber-dana-table').DataTable().ajax.reload();
            });
            console.log(result)
        },
        error: function (err) {
            var status = err.status;
            if (status === 422) {
                var message = err.responseJSON.errors;
                if (message.nama_sumber_dana !== undefined) {
                    $('#nama_sumber_dana_validation').html(`${message.nama_sumber_dana}`);
                } else {
                    $('#nama_sumber_dana_validation').html(``)
                }
                if (message.jenis_sumber_dana !== undefined) {
                    $('#jenis_sumber_dana_validation').html(`${message.jenis_sumber_dana}`);
                } else {
                    $('#jenis_sumber_dana_validation').html(``)
                }
                console.log(message);
            }

            console.log(status)
        }
    });
}

function editDataSumberDana(id) {
    $("#button-add").css("display", "none");
    $("#button-edit").css("display", "block");
    var myModal = new bootstrap.Modal(document.getElementById('modalSumberDana'), {
        keyboard: false
    });
    $.ajax({
        type: 'get',
        url: `${endpoint}/sumber-dana/${id}`,
        cache: false,
        processData: false,
        contentType: false,
        headers: {
            "Authorization": `Bearer ${token}`,
        },
        success: function (result) {
            if (result.is_active == '0') {
                $('#nama_sumber_dana').attr("disabled", true);
                $('#jenis_sumber_dana').attr("disabled", true);
                $('#keterangan').attr("disabled", true);
                $('#id_sumber_dana').val(result.id);
                $('#nama_sumber_dana').val(result.nama_sumber_dana);
                $('#alamat_sumber_dana').val(result.alamat_sumber_dana);
                $('#keterangan').val(result.keterangan);
                $('#button-edit').attr("disabled", true);
            } else {
                $('#nama_sumber_dana').attr("disabled", false);
                $('#jenis_sumber_dana').attr("disabled", false);
                $('#keterangan').attr("disabled", false);
                $('#button-edit').attr("disabled", false);
                $('#id_sumber_dana').val(result.id);
                $('#nama_sumber_dana').val(result.nama_sumber_dana);
                $('#jenis_sumber_dana').val(result.jenis_sumber_dana);
                $('#keterangan').val(result.keterangan);
            }

            $('#is_active_status').html(`
                <div class="col col-md-3"><label for="keterangan" class="switch switch-default switch-primary mr-2">Status distributor</label></div>
                <div class="col-12 col-md-9"><input type="checkbox" class="switch-input" ${result.is_active == '1' ? 'checked' : null} id="is_active" name="is_active"> <span class="switch-label"></span> <span class="switch-handle"></span></div>
                `);
            $('#is_active').on('change', function (event) {
                swal({
                    title: 'peringatan!',
                    text: `Apakah anda yakin akan me-nonaltifkan data sumber dana ${result.nama_sumber_dana}?`,
                    icon: 'warning',
                    button: 'OK'
                }).then(function (results) {
                    if (results) {
                        $.ajax({
                            type: 'put',
                            url: `${endpoint}/sumber-dana/${$('#id_sumber_dana').val()}`,
                            cache: false,
                            processData: false,
                            contentType: false,
                            data: JSON.stringify({
                                'nama_sumber_dana': result.nama_sumber_dana,
                                'jenis_sumber_dana': result.jenis_sumber_dana,
                                'keterangan': result.keterangan,
                                'is_active': result.is_active == '0' ? 1 : 0
                            }),
                            headers: {
                                "Authorization": `Bearer ${token}`,
                                "Content-Type": "application/json"
                            },
                            success: function (result) {
                                swal({
                                    title: 'Success!',
                                    text: `${result.message}`,
                                    icon: 'success',
                                    button: 'OK'
                                }).then(function () {
                                    $('#sumber-dana-table').DataTable().ajax.reload();
                                    myModal.hide();
                                });
                                console.log(result)
                            },
                        })

                    }

                });
            });
            console.log(result)
        },
        error: function (err) {
            console.log(err)
        }
    });

    myModal.show();
}

function editSumberDana() {
    var id = $('#id_sumber_dana').val();
    $.ajax({
        type: 'put',
        url: `${endpoint}/sumber-dana/${id}`,
        cache: false,
        processData: false,
        contentType: false,
        data: JSON.stringify({
            'nama_sumber_dana': $('#nama_sumber_dana').val(),
            'jenis_sumber_dana': $('#jenis_sumber_dana').val(),
            'keterangan': $('#keterangan').val(),
        }),
        headers: {
            "Authorization": `Bearer ${token}`,
            "Content-Type": "application/json"
        },
        success: function (result) {
            swal({
                title: 'Success!',
                text: `${result.message}`,
                icon: 'success',
                button: 'OK'
            }).then(function () {
                $('#sumber-dana-table').DataTable().ajax.reload();
                var myModal = new bootstrap.Modal(document.getElementById('modalSumberDana'), {
                    keyboard: false
                });
                myModal.hide();
            });
            console.log(result)
        },
        error: function (err) {
            var status = err.status;
            if (status === 422) {
                var message = err.responseJSON.errors;
                if (message.nama_distributor !== undefined) {
                    $('#nama_sumber_dana_validation').html(`${message.nama_sumber_dana}`);
                } else {
                    $('#nama_sumber_dana_validation').html(``)
                }
                if (message.alamat_sumber_dana !== undefined) {
                    $('#jenis_sumber_dana_validation').html(`${message.jenis_sumber_dana}`);
                } else {
                    $('#jenis_sumber_dana_validation').html(``)
                }
                console.log(message);
            }

            console.log(status)
        }
    });
}

function deleteDataSumberDana(id) {
    swal({
        title: 'Peringatan!',
        text: `Apakah anda yakin akan menghapus data ini?`,
        icon: 'danger',
        button: 'OK'
    }).then(function (value) {
        if (value) {
            $.ajax({
                type: 'delete',
                url: `${endpoint}/sumber-dana/${id}`,
                cache: false,
                processData: false,
                contentType: false,
                headers: {
                    "Authorization": `Bearer ${token}`,
                    "Content-Type": "application/json"
                },
                success: function (result) {
                    swal({
                        title: 'Success!',
                        text: `${result.message}`,
                        icon: 'success',
                        button: 'OK'
                    }).then(function () {
                        $('#sumber-dana-table').DataTable().ajax.reload();
                    });
                    console.log(result)
                },
                error: function (err) {
                    var status = err.status;
                    console.log(status)
                }
            });
        }
    })

}