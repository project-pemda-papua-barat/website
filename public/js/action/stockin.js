var dataTemporary = [];
var jenisProgramValue = "";
var sumberDanaValue = "";
var distributorValue = "";
var endpoint = `${window.location.origin}/service`;
var token = localStorage.getItem("token");

(function ($) {
    "use strict";
    var content = $("#authvalidation");
    var date = new Date();
    var tahun = date.getFullYear();

    $(document).ready(function () {
        $("#year").html(tahun);
        let date = new Date();
        let year = date.getFullYear();
        let month =
            date.getMonth() + 1 < 10
                ? `0${date.getMonth() + 1}`
                : date.getMonth() + 1;
        let dates = date.getDate() < 10 ? `0${date.getDate()}` : date.getDate();
        let hour =
            date.getHours() < 10 ? `0${date.getHours()}` : date.getHours();
        let minute =
            date.getMinutes() < 10
                ? `0${date.getMinutes()}`
                : date.getMinutes();
        let second =
            date.getSeconds() < 10
                ? `0${date.getSeconds()}`
                : date.getSeconds();
        let num_txt = `${year}${month}${dates}${hour}${minute}${second}`;
        $("#no_transaksi").val(num_txt);
        let listTahun = [];
        listTahun.push(year, year + 1);
        console.log(listTahun);
        $.map(listTahun, function (e) {
            $("#tahun_pengadaan").append(
                $("<option>", {
                    value: e,
                    text: e,
                })
            );
        });

        $("#content-sub-item").addClass("d-none");
        // auth
        $.ajax({
            type: "get",
            url: `${endpoint}/profile`,
            cache: false,
            processData: false,
            contentType: false,
            headers: {
                Authorization: `Bearer ${token}`,
            },
            success: function (result) {
                if (result.role_name != "User Unit") {
                    window.location.href = "/unauthorized";
                }
                content.html(`<a href="#" class="dropdown-toggle active" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <img class="user-avatar rounded-circle active" src="${result.profile_pict}" alt="User Avatar">
                <span class="ml-2">${result.name}</span>
            </a>
            <div class="user-menu dropdown-menu">
                <a class="nav-link" href="#"><i class="fa fa- user"></i>My Profile</a>
                <a class="nav-link" href="#"><i class="fa fa- user"></i>Notifications <span class="count">0</span></a>
                <a class="nav-link" href="#"><i class="fa fa -cog"></i>Settings</a>
                <a class="nav-link" href="#" onclick="logout()"><i class="fa fa-power -off"></i>Logout</a>
            </div>
                    `);

                console.log(result);
            },
            error: function (err) {
                if (err.status == 401) {
                    window.location.href = "/login";
                }
            },
        });

        // select jenis
        $("#jenis_id_select")
            .select2({
                placeholder: "Pilih  Jenis Program",
                allowClear: true,
            })
            .on("select2:selecting", function (e) {
                jenisProgramValue = e.params.args.data.id;
            })
            .on("select2:clear", function (e) {
                jenisProgramValue = "";
            });

        // select sumber dana
        $("#sumber_dana_id_select")
            .select2({
                placeholder: "Pilih  Sumber Dana",
                allowClear: true,
            })
            .on("select2:selecting", function (e) {
                sumberDanaValue = e.params.args.data.id;
            })
            .on("select2:clear", function (e) {
                sumberDanaValue = "";
            });

        // distributor jenis
        $("#distributor_id_select")
            .select2({
                placeholder: "Pilih  Distributor",
                allowClear: true,
            })
            .on("select2:selecting", function (e) {
                distributorValue = e.params.args.data.id;
            })
            .on("select2:clear", function (e) {
                distributorValue = "";
            });
    });
})(jQuery);

function addData() {
    var dokumen_berita_acara = $("#dokumen_berita_acara").prop("files")[0];
    var dokumen_expedisi = $("#dokumen_expedisi").prop("files")[0];
    var dokumen_pendukung = $("#dokumen_pendukung").prop("files")[0];
    var form = new FormData();
    form.append("no_transaksi", $("#no_transaksi").val());
    form.append("no_berita_acara", $("#no_berita_acara").val());
    form.append("no_dokumen_pengiriman", $("#no_dokumen_pengiriman").val());
    form.append("tanggal_dokumen", $("#tanggal_dokumen").val());
    form.append("jenis_id", jenisProgramValue);
    form.append("distributor_id", distributorValue);
    form.append("tahun_pengadaan", $("#tahun_pengadaan").val());
    form.append("sumber_dana_id", sumberDanaValue);
    dokumen_berita_acara !== undefined &&
        form.append("dokumen_berita_acara", dokumen_berita_acara);
    dokumen_expedisi !== undefined &&
        form.append("dokumen_expedisi", dokumen_expedisi);
    dokumen_pendukung !== undefined &&
        form.append("dokumen_pendukung", dokumen_pendukung);
    form.append("keterangan", $("#keterangan").val());
    $.ajax({
        type: "POST",
        url: `${endpoint}/stock-in`,
        cache: false,
        processData: false,
        contentType: false,
        data: form,
        headers: {
            Authorization: `Bearer ${token}`,
        },
        success: function (result) {
            swal({
                title: "Success!",
                text: `${result.message}`,
                icon: "success",
                button: "OK",
            });
            window.location.href = `${
                window.location.origin
            }/stok-masuk/tambah/draft/${$("#no_transaksi").val()}`;
            console.log(result);
        },
        error: function (err) {
            var status = err.status;
            if (status === 422) {
                var message = err.responseJSON.errors;

                if (message.tahun_pengadaan !== undefined) {
                    $("#tahun_pengadaan_validation").html(
                        `${message.tahun_pengadaan}`
                    );
                } else {
                    $("#tahun_pengadaan_validation").html(``);
                }

                if (message.sumber_dana_id !== undefined) {
                    $("#sumber_dana_id_validation").html(
                        `${message.sumber_dana_id}`
                    );
                } else {
                    $("#sumber_dana_id_validation").html(``);
                }

                if (message.tanggal_dokumen !== undefined) {
                    $("#tanggal_dokumen_validation").html(
                        `${message.tanggal_dokumen}`
                    );
                } else {
                    $("#tanggal_dokumen_validation").html(``);
                }

                if (message.no_dokumen_pengiriman !== undefined) {
                    $("#no_dokumen_pengiriman_validation").html(
                        `${message.no_dokumen_pengiriman}`
                    );
                } else {
                    $("#no_dokumen_pengiriman_validation").html(``);
                }

                if (message.no_berita_acara !== undefined) {
                    $("#no_berita_acara_validation").html(
                        `${message.no_berita_acara}`
                    );
                } else {
                    $("#no_berita_acara_validation").html(``);
                }

                if (message.jenis_id !== undefined) {
                    $("#jenis_id_validation").html(`${message.jenis_id}`);
                } else {
                    $("#jenis_id_validation").html(``);
                }
                if (message.penyedia_id !== undefined) {
                    $("#penyedia_id_validation").html(`${message.penyedia_id}`);
                } else {
                    $("#penyedia_id_validation").html(``);
                }
                if (message.distributor_id !== undefined) {
                    $("#distributor_id_validation").html(
                        `${message.distributor_id}`
                    );
                } else {
                    $("#distributor_id_validation").html(``);
                }
                if (message.dokumen_berita_acara !== undefined) {
                    $("#dokumen_berita_acara_validation").html(
                        `${message.dokumen_berita_acara}`
                    );
                } else {
                    $("#dokumen_berita_acara_validation").html(``);
                }
                if (message.dokumen_expedisi !== undefined) {
                    $("#dokumen_expedisi_validation").html(
                        `${message.dokumen_expedisi}`
                    );
                } else {
                    $("#dokumen_expedisi_validation").html(``);
                }
                if (message.dokumen_pendukung !== undefined) {
                    $("#dokumen_pendukung_validation").html(
                        `${message.dokumen_pendukung}`
                    );
                } else {
                    $("#dokumen_pendukung_validation").html(``);
                }
            }

            console.log(status);
        },
    });
}

function back() {
    return (window.location.href = `${window.location.origin}/stok-masuk`);
}
function logout() {
    swal({
        title: "peringatan!",
        text: `Apakah anda yakin akan melakukan logout?`,
        icon: "warning",
        button: "OK",
    }).then(function (result) {
        if (result) {
            localStorage.removeItem("token");
            window.location.href = "/login";
        }
    });
}
