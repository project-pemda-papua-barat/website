var endpoint = `${window.location.origin}/service`;
var token = localStorage.getItem("token");
var date = new Date();
var tahun = date.getFullYear();

(function ($) {
    var content = $("#authvalidation");

    $(document).ready(function () {
        $("#year").html(tahun);
        content.html("Loading...");
        $.ajax({
            type: "get",
            url: `${endpoint}/profile`,
            cache: false,
            processData: false,
            contentType: false,
            headers: {
                Authorization: `Bearer ${token}`,
            },
            success: function (result) {
                content.html(`<a href="#" class="dropdown-toggle active" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <img class="user-avatar rounded-circle active" src="${result.profile_pict}" alt="User Avatar">
                    <span class="ml-2">${result.name}</span>
                    </a>
                    <div class="user-menu dropdown-menu">
                        <a class="nav-link" href="#"><i class="fa fa- user"></i>My Profile</a>
                        <a class="nav-link" href="#"><i class="fa fa- user"></i>Notifications <span class="count">0</span></a>
                        <a class="nav-link" href="#"><i class="fa fa -cog"></i>Settings</a>
                        <a class="nav-link" href="#" onclick="logout()"><i class="fa fa-power -off"></i>Logout</a>
                    </div>
                `);

                result.role_name !== "Supervisor Unit" &&
                    swal({
                        title: "peringatan!",
                        text: `Apakah tidak punya hak akses ke halaman ini`,
                        icon: "warning",
                        button: "OK",
                    }).then(() => (window.location.href = "/dashboard"));

                getDataStafPengelolaInstalasiFarmasi();
                getKabid();
                getKadis();
            },
            error: function (err) {
                if (err.status == 401) {
                    window.location.href = "/login";
                }
            },
        });
    });
})(jQuery);

function deleteSigner(id) {
    swal({
        title: "Peringatan!",
        text: `Apakah anda yakin menghapus data ini?`,
        icon: `warning`,
        button: "OK",
    }).then((value) => {
        if (value) {
            $.ajax({
                type: "DELETE",
                url: `${endpoint}/signer/${id}`,
                cache: false,
                processData: false,
                contentType: false,
                headers: {
                    Authorization: `Bearer ${token}`,
                },
                success: function (result) {
                    swal({
                        title: `${result.status}`,
                        text: `${result.message}`,
                        icon: `${result.status}`,
                        button: "OK",
                    });
                    getDataStafPengelolaInstalasiFarmasi();
                    getKabid();
                    getKadis();
                },
                error: function (err) {
                    if (err.status == 401) {
                        window.location.href = "/login";
                    }
                },
            });
        }
    });
}

function assignToSigner(id) {
    $.ajax({
        type: "POST",
        url: `${endpoint}/signer/asign-jabatan/${id}`,
        cache: false,
        processData: false,
        contentType: false,
        headers: {
            Authorization: `Bearer ${token}`,
        },
        success: function (result) {
            swal({
                title: "Success!",
                text: `${result.message}`,
                icon: `success`,
                button: "OK",
            });
            getDataStafPengelolaInstalasiFarmasi();
            getKabid();
            getKadis();
        },
        error: function (err) {
            if (err.status == 401) {
                window.location.href = "/login";
            }
        },
    });
}

// Kadis
function getKadis() {
    $("#kadis").html(
        `<span class="text-center text-success">Loading...</span>`
    );
    $.ajax({
        type: "get",
        url: `${endpoint}/signer/jabatan/Kepala Dinas Kesehatan MayBrat`,
        cache: false,
        processData: false,
        contentType: false,
        headers: {
            Authorization: `Bearer ${token}`,
        },
        success: function (result) {
            var dataTable = $.map(
                result,
                (res, index) => `<tr>
                <td>${index + 1}</td>
                <td>${res.nama_penanda_tangan}</td>
                <td>${res.nip}</td>
                <td>${res.jabatan}</td>
                <td>${
                    res.selected == "true" ? "terpilih" : "tidak terpilih"
                }</td>
                <td>
                    <button type="button" onclick="assignToSigner(${
                        res.id
                    })" class="btn btn-rounded btn-success my-1">Tandai Penanda Tangan</button><br/>
                    <button type="button" onclick="deleteSigner(${
                        res.id
                    })" class="btn btn-rounded btn-danger my-1">Hapus Penanda Tangan</button>
                </td>
            </tr>`
            );
            $("#kadis").html(dataTable);
        },
        error: function (err) {
            if (err.status == 401) {
                window.location.href = "/login";
            }
        },
    });
}

// kabid
function getKabid() {
    $("#kabid").html(
        `<span class="text-center text-success">Loading...</span>`
    );
    $.ajax({
        type: "get",
        url: `${endpoint}/signer/jabatan/Kepala Bidang Sumber Daya Kesehatan`,
        cache: false,
        processData: false,
        contentType: false,
        headers: {
            Authorization: `Bearer ${token}`,
        },
        success: function (result) {
            var dataTable = $.map(
                result,
                (res, index) => `<tr>
                <td>${index + 1}</td>
                <td>${res.nama_penanda_tangan}</td>
                <td>${res.nip}</td>
                <td>${res.jabatan}</td>
                <td>${
                    res.selected == "true" ? "terpilih" : "tidak terpilih"
                }</td>
                <td>
                    <button type="button" onclick="assignToSigner(${
                        res.id
                    })" class="btn btn-rounded btn-success my-1">Tandai Penanda Tangan</button><br/>
                    <button type="button" onclick="deleteSigner(${
                        res.id
                    })" class="btn btn-rounded btn-danger my-1">Hapus Penanda Tangan</button>
                </td>
            </tr>`
            );
            $("#kabid").html(dataTable);
        },
        error: function (err) {
            if (err.status == 401) {
                window.location.href = "/login";
            }
        },
    });
}

// staf
function getDataStafPengelolaInstalasiFarmasi() {
    $("#staff").html(
        `<span class="text-center text-success">Loading...</span>`
    );
    $.ajax({
        type: "get",
        url: `${endpoint}/signer/jabatan/Staf Pengelola Instalasi Farmasi`,
        cache: false,
        processData: false,
        contentType: false,
        headers: {
            Authorization: `Bearer ${token}`,
        },
        success: function (result) {
            var dataTable = $.map(
                result,
                (res, index) => `<tr>
                <td>${index + 1}</td>
                <td>${res.nama_penanda_tangan}</td>
                <td>${res.nip}</td>
                <td>${res.jabatan}</td>
                <td>${
                    res.selected == "true" ? "terpilih" : "tidak terpilih"
                }</td>
                <td>
                    <button type="button" onclick="assignToSigner(${
                        res.id
                    })" class="btn btn-rounded btn-success my-1">Tandai Penanda Tangan</button><br/>
                    <button type="button" onclick="deleteSigner(${
                        res.id
                    })" class="btn btn-rounded btn-danger my-1">Hapus Penanda Tangan</button>
                </td>
            </tr>`
            );
            $("#staff").html(dataTable);
        },
        error: function (err) {
            if (err.status == 401) {
                window.location.href = "/login";
            }
        },
    });
}

function addKepalaDinas() {
    let form = new FormData();
    form.append("nama_penanda_tangan", $("#nama_penanda_tangan_kadis").val());
    form.append("nip", $("#nip_kadis").val());
    form.append("jabatan", "Kepala Dinas Kesehatan MayBrat");
    $.ajax({
        type: "POST",
        url: `${endpoint}/signer`,
        cache: false,
        processData: false,
        contentType: false,
        data: form,
        headers: {
            Authorization: `Bearer ${token}`,
        },
        success: function (result) {
            swal({
                title: "Success!",
                text: `${result.message}`,
                icon: `success`,
                button: "OK",
            });
            return getKadis();
        },
        error: function (err) {
            if (err.status === 422) {
                var msg = err.responseJSON.errors;

                if (msg.nip !== undefined) {
                    $("#nip_kadis_validation").html(`${msg.nip}`);
                } else {
                    $("#nip_kadis_validation").html("");
                }

                if (msg.nama_penanda_tangan !== undefined) {
                    $("#nama_penanda_tangan_kadis_validation").html(
                        `${msg.nama_penanda_tangan}`
                    );
                } else {
                    $("#nama_penanda_tangan_kadis_validation").html("");
                }
            }

            if (err.status == 401) {
                window.location.href = "/login";
            }
        },
    });
}

function addKabid() {
    let form = new FormData();
    form.append("nama_penanda_tangan", $("#nama_penanda_tangan_kabid").val());
    form.append("nip", $("#nip_kabid").val());
    form.append("jabatan", "Kepala Bidang Sumber Daya Kesehatan");
    $.ajax({
        type: "POST",
        url: `${endpoint}/signer`,
        cache: false,
        processData: false,
        contentType: false,
        data: form,
        headers: {
            Authorization: `Bearer ${token}`,
        },
        success: function (result) {
            swal({
                title: "Success!",
                text: `${result.message}`,
                icon: `success`,
                button: "OK",
            });
            return getKabid();
        },
        error: function (err) {
            if (err.status === 422) {
                var msg = err.responseJSON.errors;

                if (msg.nip !== undefined) {
                    $("#nip_kabid_validation").html(`${msg.nip}`);
                } else {
                    $("#nip_kabid_validation").html("");
                }

                if (msg.nama_penanda_tangan !== undefined) {
                    $("#nama_penanda_tangan_kabid_validation").html(
                        `${msg.nama_penanda_tangan}`
                    );
                } else {
                    $("#nama_penanda_tangan_kabid_validation").html("");
                }
            }

            if (err.status == 401) {
                window.location.href = "/login";
            }
        },
    });
}

function addStaff() {
    let form = new FormData();
    form.append("nama_penanda_tangan", $("#nama_penanda_tangan").val());
    form.append("nip", $("#nip").val());
    form.append("jabatan", "Staf Pengelola Instalasi Farmasi");
    $.ajax({
        type: "POST",
        url: `${endpoint}/signer`,
        cache: false,
        processData: false,
        contentType: false,
        data: form,
        headers: {
            Authorization: `Bearer ${token}`,
        },
        success: function (result) {
            swal({
                title: "Success!",
                text: `${result.message}`,
                icon: `${result.success}`,
                button: "OK",
            });
            return getDataStafPengelolaInstalasiFarmasi();
        },
        error: function (err) {
            if (err.status === 422) {
                var msg = err.responseJSON.errors;

                if (msg.nip !== undefined) {
                    $("#nip_validation").html(`${msg.nip}`);
                } else {
                    $("#nip_validation").html("");
                }

                if (msg.nama_penanda_tangan !== undefined) {
                    $("#nama_penanda_tangan_validation").html(
                        `${msg.nama_penanda_tangan}`
                    );
                } else {
                    $("#nama_penanda_tangan_validation").html("");
                }
            }

            if (err.status == 401) {
                window.location.href = "/login";
            }
        },
    });
}

function showFormAddDataKepalaDinas() {
    let buttonEdit = document.getElementById("button-unit-edit");
    buttonEdit.classList.add("d-none");
    $("#nama_penanda_tangan_kadis").val("");
    $("#nip_kadis").val("");
    var myModal = new bootstrap.Modal(document.getElementById("ModalKepalaDinas"), {
        keyboard: false,
    });
    myModal.show();
}

function showFormAddDataKabid() {
    let buttonEdit = document.getElementById("button-unit-edit");
    buttonEdit.classList.add("d-none");
    $("#nama_penanda_tangan").val("");
    $("#nip").val("");
    $("#jabatan").val("");
    var myModal = new bootstrap.Modal(document.getElementById("ModalKabid"), {
        keyboard: false,
    });
    myModal.show();
}

function showFormAddData() {
    let buttonEdit = document.getElementById("button-edit");
    buttonEdit.classList.add("d-none");
    // $('#id_staff').val("");
    $("#nama_penanda_tangan").val("");
    $("#nip").val("");
    $("#jabatan").val("");
    var myModal = new bootstrap.Modal(document.getElementById("ModalStaff"), {
        keyboard: false,
    });
    myModal.show();
}

function logout() {
    swal({
        title: "peringatan!",
        text: `Apakah anda yakin akan melakukan logout?`,
        icon: "warning",
        button: "OK",
    }).then(function (result) {
        if (result) {
            localStorage.removeItem("token");
            window.location.href = "/login";
        }
    });
}
