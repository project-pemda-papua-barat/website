var endpoint = `${window.location.origin}/service`;
var token = localStorage.getItem('token');

var headI = `<table id="aset-table" class="table table-striped table-bordered">
<thead>
    <tr>
        <th>No</th>
        <th>Kode Aset</th>
        <th>Nama Aset</th>
        <th>Action</th>
    </tr>
</thead>
</table>`

var headII = `<table id="aset-ii-table" class="table table-striped table-bordered">
<thead>
    <tr>
        <th>No</th>
        <th>Kode Aset</th>
        <th>Nama Aset</th>
        <th>Action</th>
    </tr>
</thead>
</table>
<div class="container">
<button type="button" onclick="backFirstLayer()" class="btn btn-outline btn-info"><< Kembali Ke Layer Utama</button>
</div>`

var headIII = `<table id="aset-iii-table" class="table table-striped table-bordered">
<thead>
    <tr>
        <th>No</th>
        <th>Kode Aset</th>
        <th>Nama Aset</th>
        <th>Action</th>
    </tr>
</thead>
</table>
<div class="container">
<button type="button" onclick="backFirstLayer()" class="btn btn-outline btn-info"><< Kembali Ke Layer Utama</button>
</div>`

var headIV = `<table id="aset-iv-table" class="table table-striped table-bordered">
<thead>
    <tr>
        <th>No</th>
        <th>Kode Aset</th>
        <th>Nama Aset</th>
        <th>Action</th>
    </tr>
</thead>
</table>
<div class="container">
<button type="button" onclick="backFirstLayer()" class="btn btn-outline btn-info"><< Kembali Ke Layer Utama</button>
</div>`

var headV = `<table id="aset-v-table" class="table table-striped table-bordered">
<thead>
    <tr>
        <th>No</th>
        <th>Kode Aset</th>
        <th>Nama Aset</th>
        <th>Action</th>
    </tr>
</thead>
</table>
<div class="container">
<button type="button" onclick="backFirstLayer()" class="btn btn-outline btn-info"><< Kembali Ke Layer Utama</button>
</div>`

var headVI = `<table id="aset-vi-table" class="table table-striped table-bordered">
<thead>
    <tr>
        <th>No</th>
        <th>Kode Aset</th>
        <th>Nama Aset</th>
        <th>Action</th>
    </tr>
</thead>
</table>
<div class="container">
<button type="button" onclick="backFirstLayer()" class="btn btn-outline btn-info"><< Kembali Ke Layer Utama</button>
</div>`

var headVII = `<table id="aset-vii-table" class="table table-striped table-bordered">
<thead>
    <tr>
        <th>No</th>
        <th>Kode Aset</th>
        <th>Nama Aset</th>
        <th>ID Data</th>
        <th>Action</th>
    </tr>
</thead>
</table>
<div class="container">
<button type="button" onclick="backFirstLayer()" class="btn btn-outline btn-info"><< Kembali Ke Layer Utama</button>
</div>`

$(document).ready(function ($) {
    "use strict";
    var date = new Date();
    var year = date.getFullYear();
    $('#year').html(year);
    $('#content').html('<span class="text-center">loading...</span>');
    $('#content').html(headI);
    $('#aset-table').DataTable({
        processing: true,
        serverSide: true,
        dom: 'Bfrtip',
        buttons: [{
            text: 'Tamba Data >',
            className: 'btn btn-primary',
            action: function (e, dt, node, config) {
                $('#nama_aset_i').attr("disabled", false);
                $('#kode_aset_i').attr("disabled", false);
                $("#button-add").css("display", "block");
                $("#button-edit").css("display", "none");
                $('#nama_aset_i').val("");
                $('#kode_aset_i').val("");
                $('#nama_aset_i_validation').html(``);
                $('#kode_aset_i_validation').html(``);
                $('#keterangan').val("");
                var myModal = new bootstrap.Modal(document.getElementById('modalAsetI'), {
                    keyboard: false
                });
                myModal.show();
            }
        }],
        ajax: {
            url: `${endpoint}/aset-i`,
            type: 'GET',
            headers: {
                "Authorization": `Bearer ${token}`,
            },
            // data: e => console.log(e),
            // success: e => console.log(e),
        },
        columns: [{
            data: "DT_RowIndex",
            name: "DT_RowIndex",
        },
        {
            data: 'kode_aset_i',
            name: 'kode_aset_i',
        },
        {
            data: 'nama_aset_i',
            render: function (data, _, row) {
                return `<a href="#" onclick="moveAsetII('${row.kode_aset_i}')" class="text-success">${data}</a>`
            }
        },
        {
            data: 'action',
            name: 'action'
        },
        ],
    });

});

// Back to aset i
function backFirstLayer() {
    $('#content').html('<span class="text-center">loading...</span>');
    $('#content').html(headI);
    $('#aset-table').DataTable({
        processing: true,
        serverSide: true,
        dom: 'Bfrtip',
        buttons: [{
            text: 'Tamba Data >',
            className: 'btn btn-primary',
            action: function (e, dt, node, config) {
                $('#nama_aset_i').attr("disabled", false);
                $('#kode_aset_i').attr("disabled", false);
                $("#button-add").css("display", "block");
                $("#button-edit").css("display", "none");
                $('#nama_aset_i').val("");
                $('#kode_aset_i').val("");
                $('#nama_aset_i_validation').html(``);
                $('#kode_aset_i_validation').html(``);
                $('#keterangan').val("");
                var myModal = new bootstrap.Modal(document.getElementById('modalAsetI'), {
                    keyboard: false
                });
                myModal.show();
            }
        }],
        ajax: {
            url: `${endpoint}/aset-i`,
            type: 'GET',
            headers: {
                "Authorization": `Bearer ${token}`,
            },
            // data: e => console.log(e),
            // success: e => console.log(e),
        },
        columns: [{
            data: "DT_RowIndex",
            name: "DT_RowIndex",
        },
        {
            data: 'kode_aset_i',
            name: 'kode_aset_i',
        },
        {
            data: 'nama_aset_i',
            render: function (data, _, row) {
                return `<a href="#" onclick="moveAsetII('${row.kode_aset_i}')" class="text-success">${data}</a>`
            }
        },
        {
            data: 'action',
            name: 'action'
        },
        ],
    });
};

// function aset vii
function addAsetVII() {
    var form = new FormData();
    form.append('nama_aset_vii', $('#nama_aset_vii').val());
    form.append('kode_aset_vii', $('#kode_aset_vii').val());
    form.append('data_id', $('#data_id').val());
    form.append('asetI_id', $('#lv7_asetI_id').val());
    form.append('asetII_id', $('#lv7_asetII_id').val());
    form.append('asetIII_id', $('#lv7_asetIII_id').val());
    form.append('asetIV_id', $('#lv7_asetIV_id').val());
    form.append('asetV_id', $('#lv7_asetV_id').val());
    form.append('asetVI_id', $('#lv7_asetVI_id').val());
    $.ajax({
        type: 'post',
        url: `${endpoint}/aset-vii`,
        cache: false,
        processData: false,
        contentType: false,
        data: form,
        headers: {
            "Authorization": `Bearer ${token}`,
        },
        success: function (result) {
            swal({
                title: 'Success!',
                text: `${result.message}`,
                icon: 'success',
                button: 'OK'
            }).then(function () {
                $('#aset-vii-table').DataTable().ajax.reload();
            });
            console.log(result)
        },
        error: function (err) {
            var status = err.status;
            if (status === 422) {
                var message = err.responseJSON.errors;
                if (message.kode_aset_vii !== undefined) {
                    $('#kode_aset_vii_validation').html(`${message.kode_aset_vii}`);
                } else {
                    $('#kode_aset_vii_validation').html(``)
                }
                if (message.nama_aset_vii !== undefined) {
                    $('#nama_aset_vii_validation').html(`${message.nama_aset_vii}`);
                } else {
                    $('#nama_aset_vii_validation').html(``)
                }
                if (message.data_id !== undefined) {
                    $('#data_id_validation').html(`${message.data_id}`);
                } else {
                    $('#data_id_validation').html(``)
                }

                console.log(message);
            }

            console.log(status)
        }
    });
}

function moveAsetVII(asetI_id, asetII_id, asetIII_id, asetIV_id, asetV_id, asetVI_id) {
    $('#content').html('<span class="text-success text-center">loading...</span>');
    $('#content').html(headVII);
    $('#lv7_asetI_id').val(asetI_id);
    $('#lv7_asetII_id').val(asetII_id);
    $('#lv7_asetIII_id').val(asetIII_id);
    $('#lv7_asetIV_id').val(asetIV_id);
    $('#lv7_asetV_id').val(asetV_id);
    $('#lv7_asetVI_id').val(asetVI_id);
    $(document).ready(function ($) {
        "use strict";
        $('#aset-vii-table').DataTable({
            processing: true,
            serverSide: true,
            dom: 'Bfrtip',
            buttons: [{
                text: 'Tamba Data >',
                className: 'btn btn-primary',
                action: function (e, dt, node, config) {
                    // $('#nama_aset_i').attr("disabled", false);
                    // $('#kode_aset_i').attr("disabled", false);
                    $("#button-add-aset-vii").css("display", "block");
                    $("#button-edit-aset-vii").css("display", "none");
                    // $('#nama_aset_i').val("");
                    // $('#kode_aset_i').val("");
                    // $('#nama_aset_i_validation').html(``);
                    // $('#kode_aset_i_validation').html(``);
                    // $('#keterangan').val("");
                    var myModal = new bootstrap.Modal(document.getElementById('modalAsetVII'), {
                        keyboard: false
                    });
                    myModal.show();
                }
            }],
            ajax: {
                url: `${endpoint}/aset-vii/${asetI_id}/${asetII_id}/${asetIII_id}/${asetIV_id}/${asetV_id}/${asetVI_id}`,
                type: 'GET',
                headers: {
                    "Authorization": `Bearer ${token}`,
                },
                // data: e => console.log(e),
                // success: e => console.log(e),
            },
            columns: [{
                data: "DT_RowIndex",
                name: "DT_RowIndex",
            },
            {
                data: 'kode_aset_v',
                render: function (data, _, row) {
                    return `${row.asetI_id}.${row.asetII_id}.${row.asetIII_id}.${row.asetIV_id}.${row.asetV_id}.${row.asetVI_id}.${row.kode_aset_vii}`
                }
            },
            {
                data: 'nama_aset_vii',
                name: 'nama_aset_vii'
            },
            {
                data: 'data_id',
                name: 'data_id'
            },
            {
                data: 'action',
                name: 'action'
            },
            ],
        });
    });
}

// function aset vi
function addAsetVI() {
    var form = new FormData();
    form.append('nama_aset_vi', $('#nama_aset_vi').val());
    form.append('kode_aset_vi', $('#kode_aset_vi').val());
    form.append('asetI_id', $('#lv6_asetI_id').val());
    form.append('asetII_id', $('#lv6_asetII_id').val());
    form.append('asetIII_id', $('#lv6_asetIII_id').val());
    form.append('asetIV_id', $('#lv6_asetIV_id').val());
    form.append('asetV_id', $('#lv6_asetV_id').val());
    $.ajax({
        type: 'post',
        url: `${endpoint}/aset-vi`,
        cache: false,
        processData: false,
        contentType: false,
        data: form,
        headers: {
            "Authorization": `Bearer ${token}`,
        },
        success: function (result) {
            swal({
                title: 'Success!',
                text: `${result.message}`,
                icon: 'success',
                button: 'OK'
            }).then(function () {
                $('#aset-vi-table').DataTable().ajax.reload();
            });
            console.log(result)
        },
        error: function (err) {
            var status = err.status;
            if (status === 422) {
                var message = err.responseJSON.errors;
                if (message.kode_aset_vi !== undefined) {
                    $('#kode_aset_vi_validation').html(`${message.kode_aset_vi}`);
                } else {
                    $('#kode_aset_vi_validation').html(``)
                }
                if (message.nama_aset_vi !== undefined) {
                    $('#nama_aset_vi_validation').html(`${message.nama_aset_vi}`);
                } else {
                    $('#nama_aset_vi_validation').html(``)
                }

                console.log(message);
            }

            console.log(status)
        }
    });
}

function moveAsetVI(asetI_id, asetII_id, asetIII_id, asetIV_id, asetV_id) {
    $('#content').html('<span class="text-success text-center">loading...</span>');
    $('#content').html(headVI);
    $('#lv6_asetI_id').val(asetI_id);
    $('#lv6_asetII_id').val(asetII_id);
    $('#lv6_asetIII_id').val(asetIII_id);
    $('#lv6_asetIV_id').val(asetIV_id);
    $('#lv6_asetV_id').val(asetV_id);
    $(document).ready(function ($) {
        "use strict";
        $('#aset-vi-table').DataTable({
            processing: true,
            serverSide: true,
            dom: 'Bfrtip',
            buttons: [{
                text: 'Tamba Data >',
                className: 'btn btn-primary',
                action: function (e, dt, node, config) {
                    // $('#nama_aset_i').attr("disabled", false);
                    // $('#kode_aset_i').attr("disabled", false);
                    $("#button-add-aset-vi").css("display", "block");
                    $("#button-edit-aset-vi").css("display", "none");
                    // $('#nama_aset_i').val("");
                    // $('#kode_aset_i').val("");
                    // $('#nama_aset_i_validation').html(``);
                    // $('#kode_aset_i_validation').html(``);
                    // $('#keterangan').val("");
                    var myModal = new bootstrap.Modal(document.getElementById('modalAsetVI'), {
                        keyboard: false
                    });
                    myModal.show();
                }
            }],
            ajax: {
                url: `${endpoint}/aset-vi/${asetI_id}/${asetII_id}/${asetIII_id}/${asetIV_id}/${asetV_id}`,
                type: 'GET',
                headers: {
                    "Authorization": `Bearer ${token}`,
                },
                // data: e => console.log(e),
                // success: e => console.log(e),
            },
            columns: [{
                data: "DT_RowIndex",
                name: "DT_RowIndex",
            },
            {
                data: 'kode_aset_v',
                render: function (data, _, row) {
                    return `${row.asetI_id}.${row.asetII_id}.${row.asetIII_id}.${row.asetIV_id}.${row.asetV_id}.${row.kode_aset_vi}`
                }
            },
            {
                data: 'nama_aset_vi',
                render: function (data, _, row) {
                    return `<a href="#" onclick="moveAsetVII(${row.asetI_id},${row.asetII_id},${row.asetIII_id},${row.asetIV_id},${row.asetV_id},${row.kode_aset_vi})" class="text-success">${data}</a>`
                }
            },
            {
                data: 'action',
                name: 'action'
            },
            ],
        });
    });
}

// function aset v
function addAsetV() {
    var form = new FormData();
    form.append('nama_aset_v', $('#nama_aset_v').val());
    form.append('kode_aset_v', $('#kode_aset_v').val());
    form.append('asetI_id', $('#lv5_asetI_id').val());
    form.append('asetII_id', $('#lv5_asetII_id').val());
    form.append('asetIII_id', $('#lv5_asetIII_id').val());
    form.append('asetIV_id', $('#lv5_asetIV_id').val());
    $.ajax({
        type: 'post',
        url: `${endpoint}/aset-v`,
        cache: false,
        processData: false,
        contentType: false,
        data: form,
        headers: {
            "Authorization": `Bearer ${token}`,
        },
        success: function (result) {
            swal({
                title: 'Success!',
                text: `${result.message}`,
                icon: 'success',
                button: 'OK'
            }).then(function () {
                $('#aset-v-table').DataTable().ajax.reload();
            });
            console.log(result)
        },
        error: function (err) {
            var status = err.status;
            if (status === 422) {
                var message = err.responseJSON.errors;
                if (message.kode_aset_v !== undefined) {
                    $('#kode_aset_v_validation').html(`${message.kode_aset_v}`);
                } else {
                    $('#kode_aset_v_validation').html(``)
                }
                if (message.nama_aset_v !== undefined) {
                    $('#nama_aset_v_validation').html(`${message.nama_aset_v}`);
                } else {
                    $('#nama_aset_v_validation').html(``)
                }

                console.log(message);
            }

            console.log(status)
        }
    });
}

function moveAsetV(asetI_id, asetII_id, asetIII_id, asetIV_id) {
    $('#content').html('<span class="text-success text-center">loading...</span>');
    $('#content').html(headV);
    $('#lv5_asetI_id').val(asetI_id);
    $('#lv5_asetII_id').val(asetII_id);
    $('#lv5_asetIII_id').val(asetIII_id);
    $('#lv5_asetIV_id').val(asetIV_id);
    $(document).ready(function ($) {
        "use strict";
        $('#aset-v-table').DataTable({
            processing: true,
            serverSide: true,
            dom: 'Bfrtip',
            buttons: [{
                text: 'Tamba Data >',
                className: 'btn btn-primary',
                action: function (e, dt, node, config) {
                    // $('#nama_aset_i').attr("disabled", false);
                    // $('#kode_aset_i').attr("disabled", false);
                    $("#button-add-aset-v").css("display", "block");
                    $("#button-edit-aset-v").css("display", "none");
                    // $('#nama_aset_i').val("");
                    // $('#kode_aset_i').val("");
                    // $('#nama_aset_i_validation').html(``);
                    // $('#kode_aset_i_validation').html(``);
                    // $('#keterangan').val("");
                    var myModal = new bootstrap.Modal(document.getElementById('modalAsetV'), {
                        keyboard: false
                    });
                    myModal.show();
                }
            }],
            ajax: {
                url: `${endpoint}/aset-v/${asetI_id}/${asetII_id}/${asetIII_id}/${asetIV_id}`,
                type: 'GET',
                headers: {
                    "Authorization": `Bearer ${token}`,
                },
                // data: e => console.log(e),
                // success: e => console.log(e),
            },
            columns: [{
                data: "DT_RowIndex",
                name: "DT_RowIndex",
            },
            {
                data: 'kode_aset_v',
                render: function (data, _, row) {
                    return `${row.asetI_id}.${row.asetII_id}.${row.asetIII_id}.${row.asetIV_id}.${row.kode_aset_v}`
                }
            },
            {
                data: 'nama_aset_v',
                render: function (data, _, row) {
                    return `<a href="#" onclick="moveAsetVI(${row.asetI_id},${row.asetII_id},${row.asetIII_id},${row.asetIV_id},${row.kode_aset_v})" class="text-success">${data}</a>`
                }
            },
            {
                data: 'action',
                name: 'action'
            },
            ],
        });
    });
}

// function aset iv
function addAsetIV() {
    var form = new FormData();
    form.append('nama_aset_iv', $('#nama_aset_iv').val());
    form.append('kode_aset_iv', $('#kode_aset_iv').val());
    form.append('asetI_id', $('#lv4_asetI_id').val());
    form.append('asetII_id', $('#lv4_asetII_id').val());
    form.append('asetIII_id', $('#lv4_asetIII_id').val());
    $.ajax({
        type: 'post',
        url: `${endpoint}/aset-iv`,
        cache: false,
        processData: false,
        contentType: false,
        data: form,
        headers: {
            "Authorization": `Bearer ${token}`,
        },
        success: function (result) {
            swal({
                title: 'Success!',
                text: `${result.message}`,
                icon: 'success',
                button: 'OK'
            }).then(function () {
                $('#aset-iv-table').DataTable().ajax.reload();
            });
            console.log(result)
        },
        error: function (err) {
            var status = err.status;
            if (status === 422) {
                var message = err.responseJSON.errors;
                if (message.kode_aset_iv !== undefined) {
                    $('#kode_aset_iv_validation').html(`${message.kode_aset_iv}`);
                } else {
                    $('#kode_aset_iv_validation').html(``)
                }
                if (message.nama_aset_iv !== undefined) {
                    $('#nama_aset_iv_validation').html(`${message.nama_aset_iv}`);
                } else {
                    $('#nama_aset_iv_validation').html(``)
                }

                console.log(message);
            }

            console.log(status)
        }
    });
}

function moveAsetIV(asetI_id, asetII_id, asetIII_id) {
    $('#content').html('<span class="text-success text-center">loading...</span>');
    $('#content').html(headIV);
    $('#lv4_asetI_id').val(asetI_id);
    $('#lv4_asetII_id').val(asetII_id);
    $('#lv4_asetIII_id').val(asetIII_id);
    $(document).ready(function ($) {
        "use strict";
        $('#aset-iv-table').DataTable({
            processing: true,
            serverSide: true,
            dom: 'Bfrtip',
            buttons: [{
                text: 'Tamba Data >',
                className: 'btn btn-primary',
                action: function (e, dt, node, config) {
                    // $('#nama_aset_i').attr("disabled", false);
                    // $('#kode_aset_i').attr("disabled", false);
                    $("#button-add-aset-iv").css("display", "block");
                    $("#button-edit-aset-iv").css("display", "none");
                    // $('#nama_aset_i').val("");
                    // $('#kode_aset_i').val("");
                    // $('#nama_aset_i_validation').html(``);
                    // $('#kode_aset_i_validation').html(``);
                    // $('#keterangan').val("");
                    var myModal = new bootstrap.Modal(document.getElementById('modalAsetIV'), {
                        keyboard: false
                    });
                    myModal.show();
                }
            }],
            ajax: {
                url: `${endpoint}/aset-iv/${asetI_id}/${asetII_id}/${asetIII_id}`,
                type: 'GET',
                headers: {
                    "Authorization": `Bearer ${token}`,
                },
                // data: e => console.log(e),
                // success: e => console.log(e),
            },
            columns: [{
                data: "DT_RowIndex",
                name: "DT_RowIndex",
            },
            {
                data: 'kode_aset_iv',
                render: function (data, _, row) {
                    return `${row.asetI_id}.${row.asetII_id}.${row.asetIII_id}.${row.kode_aset_iv}`
                }
            },
            {
                data: 'nama_aset_iv',
                render: function (data, _, row) {
                    return `<a href="#" onclick="moveAsetV(${row.asetI_id}, ${row.asetII_id}, ${row.asetIII_id}, ${row.kode_aset_iv})" class="text-success">${data}</a>`
                }
            },
            {
                data: 'action',
                name: 'action'
            },
            ],
        });
    });
}


// function aset III

function addAsetIII() {
    var form = new FormData();
    form.append('nama_aset_iii', $('#nama_aset_iii').val());
    form.append('kode_aset_iii', $('#kode_aset_iii').val());
    form.append('asetI_id', $('#lv3_asetI_id').val());
    form.append('asetII_id', $('#lv3_asetII_id').val());
    $.ajax({
        type: 'post',
        url: `${endpoint}/aset-iii`,
        cache: false,
        processData: false,
        contentType: false,
        data: form,
        headers: {
            "Authorization": `Bearer ${token}`,
        },
        success: function (result) {
            swal({
                title: 'Success!',
                text: `${result.message}`,
                icon: 'success',
                button: 'OK'
            }).then(function () {
                $('#aset-iii-table').DataTable().ajax.reload();
            });
            console.log(result)
        },
        error: function (err) {
            var status = err.status;
            if (status === 422) {
                var message = err.responseJSON.errors;
                if (message.kode_aset_iii !== undefined) {
                    $('#kode_aset_iii_validation').html(`${message.kode_aset_iii}`);
                } else {
                    $('#kode_aset_iii_validation').html(``)
                }
                if (message.nama_aset_iii !== undefined) {
                    $('#nama_aset_iii_validation').html(`${message.nama_aset_iii}`);
                } else {
                    $('#nama_aset_iii_validation').html(``)
                }

                console.log(message);
            }

            console.log(status)
        }
    });
}

function moveAsetIII(asetI_id, asetII_id) {
    $('#content').html('<span class="text-success text-center">loading...</span>');
    $('#content').html(headIII);
    $('#lv3_asetI_id').val(asetI_id);
    $('#lv3_asetII_id').val(asetII_id);
    // $('#asetI_id').val(id);
    $(document).ready(function ($) {
        "use strict";
        $('#aset-iii-table').DataTable({
            processing: true,
            serverSide: true,
            dom: 'Bfrtip',
            buttons: [{
                text: 'Tamba Data >',
                className: 'btn btn-primary',
                action: function (e, dt, node, config) {
                    // $('#nama_aset_i').attr("disabled", false);
                    // $('#kode_aset_i').attr("disabled", false);
                    $("#button-add-aset-iii").css("display", "block");
                    $("#button-edit-aset-iii").css("display", "none");
                    // $('#nama_aset_i').val("");
                    // $('#kode_aset_i').val("");
                    // $('#nama_aset_i_validation').html(``);
                    // $('#kode_aset_i_validation').html(``);
                    // $('#keterangan').val("");
                    var myModal = new bootstrap.Modal(document.getElementById('modalAsetIII'), {
                        keyboard: false
                    });
                    myModal.show();
                }
            }],
            ajax: {
                url: `${endpoint}/aset-iii/${asetI_id}/${asetII_id}`,
                type: 'GET',
                headers: {
                    "Authorization": `Bearer ${token}`,
                },
                // data: e => console.log(e),
                // success: e => console.log(e),
            },
            columns: [{
                data: "DT_RowIndex",
                name: "DT_RowIndex",
            },
            {
                data: 'kode_aset_iii',
                render: function (data, _, row) {
                    return `${row.asetI_id}.${row.asetII_id}.${row.kode_aset_iii}`
                }
            },
            {
                data: 'nama_aset_iii',
                render: function (data, _, row) {
                    return `<a href="#" onclick="moveAsetIV(${asetI_id}, ${asetII_id}, ${row.kode_aset_iii})" class="text-success">${data}</a>`
                }
            },
            {
                data: 'action',
                name: 'action'
            },
            ],
        });
    });
}

// function Aset II

function addAsetII() {
    var form = new FormData();
    form.append('nama_aset_ii', $('#nama_aset_ii').val());
    form.append('kode_aset_ii', $('#kode_aset_ii').val());
    form.append('asetI_id', $('#lv2_asetI_id').val());
    $.ajax({
        type: 'post',
        url: `${endpoint}/aset-ii`,
        cache: false,
        processData: false,
        contentType: false,
        data: form,
        headers: {
            "Authorization": `Bearer ${token}`,
        },
        success: function (result) {
            swal({
                title: 'Success!',
                text: `${result.message}`,
                icon: 'success',
                button: 'OK'
            }).then(function () {
                $('#aset-ii-table').DataTable().ajax.reload();
            });
            console.log(result)
        },
        error: function (err) {
            var status = err.status;
            if (status === 422) {
                var message = err.responseJSON.errors;
                if (message.kode_aset_ii !== undefined) {
                    $('#kode_aset_ii_validation').html(`${message.kode_aset_ii}`);
                } else {
                    $('#kode_aset_ii_validation').html(``)
                }
                if (message.nama_aset_ii !== undefined) {
                    $('#nama_aset_ii_validation').html(`${message.nama_aset_ii}`);
                } else {
                    $('#nama_aset_ii_validation').html(``)
                }

                console.log(message);
            }

            console.log(status)
        }
    });
}

function moveAsetII(id) {
    $('#content').html('<span class="text-success text-center">loading...</span>');
    $('#content').html(headII);
    $('#lv2_asetI_id').val(id);
    $(document).ready(function ($) {
        "use strict";
        $('#aset-ii-table').DataTable({
            processing: true,
            serverSide: true,
            dom: 'Bfrtip',
            buttons: [{
                text: 'Tamba Data >',
                className: 'btn btn-primary',
                action: function (e, dt, node, config) {
                    // $('#nama_aset_i').attr("disabled", false);
                    // $('#kode_aset_i').attr("disabled", false);
                    $("#button-add-aset-ii").css("display", "block");
                    $("#button-edit-aset-ii").css("display", "none");
                    // $('#nama_aset_i').val("");
                    // $('#kode_aset_i').val("");
                    // $('#nama_aset_i_validation').html(``);
                    // $('#kode_aset_i_validation').html(``);
                    // $('#keterangan').val("");
                    var myModal = new bootstrap.Modal(document.getElementById('modalAsetII'), {
                        keyboard: false
                    });
                    myModal.show();
                }
            }],
            ajax: {
                url: `${endpoint}/aset-ii/${id}`,
                type: 'GET',
                headers: {
                    "Authorization": `Bearer ${token}`,
                },
                // data: e => console.log(e),
                // success: e => console.log(e),
            },
            columns: [{
                data: "DT_RowIndex",
                name: "DT_RowIndex",
            },
            {
                data: 'kode_aset_ii',
                render: function (data, _, row) {
                    return `${row.asetI_id}.${row.kode_aset_ii}`
                }
            },
            {
                data: 'nama_aset_ii',
                render: function (data, _, row) {
                    return `<a href="#" onclick="moveAsetIII(${row.asetI_id}, ${row.kode_aset_ii})" class="text-success">${data}</a>`
                }
            },
            {
                data: 'action',
                name: 'action'
            },
            ],
        });
    });
}

// function Aset I

function addAsetI() {
    var form = new FormData();
    form.append('nama_aset_i', $('#nama_aset_i').val());
    form.append('kode_aset_i', $('#kode_aset_i').val());
    $.ajax({
        type: 'post',
        url: `${endpoint}/aset-i`,
        cache: false,
        processData: false,
        contentType: false,
        data: form,
        headers: {
            "Authorization": `Bearer ${token}`,
        },
        success: function (result) {
            swal({
                title: 'Success!',
                text: `${result.message}`,
                icon: 'success',
                button: 'OK'
            }).then(function () {
                $('#aset-table').DataTable().ajax.reload();
            });
            console.log(result)
        },
        error: function (err) {
            var status = err.status;
            if (status === 422) {
                var message = err.responseJSON.errors;
                if (message.kode_aset_i !== undefined) {
                    $('#kode_aset_i_validation').html(`${message.kode_aset_i}`);
                } else {
                    $('#kode_aset_i_validation').html(``)
                }
                if (message.nama_aset_i !== undefined) {
                    $('#nama_aset_i_validation').html(`${message.nama_aset_i}`);
                } else {
                    $('#nama_aset_i_validation').html(``)
                }

                console.log(message);
            }

            console.log(status)
        }
    });
}

