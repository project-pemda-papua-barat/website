var endpoint = `${window.location.origin}/service`;
var token = localStorage.getItem('token');
$(document).ready(function ($) {
    "use strict";
    var date = new Date();
    var year = date.getFullYear();
    $('#year').html(year);

    $('#distributor-table').DataTable({
        processing: true,
        serverSide: true,
        dom: 'Bfrtip',
        buttons: [{
            text: 'Tamba Data >',
            className: 'btn btn-primary',
            action: function (e, dt, node, config) {
                $('#nama_distributor').attr("disabled", false);
                $('#alamat_distributor').attr("disabled", false);
                $('#no_contact_distributor').attr("disabled", false);
                $('#keterangan').attr("disabled", false);
                $('#is_active_status').html(``);
                $("#button-add").css("display", "block");
                $("#button-edit").css("display", "none");
                $('#nama_distributor').val("");
                $('#alamat_distributor').val("");
                $('#no_contact_distributor').val("");
                $('#nama_distributor_validation').html(``);
                $('#alamat_distributor_validation').html(``);
                $('#no_contact_distributor_validation').html(``);
                $('#keterangan').val("");
                var myModal = new bootstrap.Modal(document.getElementById('modalDistributor'), {
                    keyboard: false
                });
                myModal.show();
            }
        }],
        ajax: {
            url: `${endpoint}/distributor`,
            type: 'GET',
            headers: {
                "Authorization": `Bearer ${token}`,
            },
            // data: e => console.log(e),
            // success: e => console.log(e),
        },
        columns: [{
            data: "DT_RowIndex",
            name: "DT_RowIndex",
        },
        {
            data: 'nama_distributor',
            name: 'nama_distributor'
        },
        {
            data: 'alamat_distributor',
            name: 'alamat_distributor'
        },
        {
            data: 'detail_alamat',
            name: 'detail_alamat'
        },
        {
            data: 'no_contact_distributor',
            name: 'no_contact_distributor'
        },
        {
            data: 'keterangan',
            name: 'keterangan'
        },
        {
            data: 'is_active',
            render: function (data) {
                if (data == 0) {
                    return "Tidak Aktif"
                } else {
                    return "Aktif"
                }

            }
        },
        {
            data: 'action',
            name: 'action'
        },
        ],
    });
});

function addDistributor() {
    var form = new FormData();
    form.append('nama_distributor', $('#nama_distributor').val());
    form.append('alamat_distributor', $('#alamat_distributor').val());
    form.append('no_contact_distributor', $('#no_contact_distributor').val());
    form.append('lat', $('#lat').val());
    form.append('long', $('#long').val());
    form.append('detail_alamat', $('#detail_alamat').val());
    $.ajax({
        type: 'post',
        url: `${endpoint}/distributor`,
        cache: false,
        processData: false,
        contentType: false,
        data: form,
        headers: {
            "Authorization": `Bearer ${token}`,
        },
        success: function (result) {
            swal({
                title: 'Success!',
                text: `${result.message}`,
                icon: 'success',
                button: 'OK'
            }).then(function () {
                $('#distributor-table').DataTable().ajax.reload();
            });
            console.log(result)
        },
        error: function (err) {
            var status = err.status;
            if (status === 422) {
                var message = err.responseJSON.errors;
                if (message.nama_distributor !== undefined) {
                    $('#nama_distributor_validation').html(`${message.nama_distributor}`);
                } else {
                    $('#nama_distributor_validation').html(``)
                }
                if (message.alamat_distributor !== undefined) {
                    $('#alamat_distributor_validation').html(`${message.alamat_distributor}`);
                } else {
                    $('#alamat_distributor_validation').html(``)
                }
                if (message.no_contact_distributor !== undefined) {
                    $('#no_contact_distributor_validation').html(`${message.no_contact_distributor}`);
                } else {
                    $('#no_contact_distributor_validation').html(``)
                }
                if (message.detail_alamat !== undefined) {
                    $('#detail_alamat_distributor_validation').html(`${message.detail_alamat}`);
                } else {
                    $('#detail_alamat_distributor_validation').html(``)
                }
                console.log(message);
            }

            console.log(status)
        }
    });
}

function editDataDistributor(id) {
    $("#button-add").css("display", "none");
    $("#button-edit").css("display", "block");
    var myModal = new bootstrap.Modal(document.getElementById('modalDistributor'), {
        keyboard: false
    });
    $.ajax({
        type: 'get',
        url: `${endpoint}/distributor/${id}`,
        cache: false,
        processData: false,
        contentType: false,
        headers: {
            "Authorization": `Bearer ${token}`,
        },
        success: function (result) {
            if (result.is_active == '0') {
                $('#nama_distributor').attr("disabled", true);
                $('#alamat_distributor').attr("disabled", true);
                $('#no_contact_distributor').attr("disabled", true);
                $('#detail_alamat').attr("disabled", true);
                $('#lat').attr("disabled", true);
                $('#long').attr("disabled", true);
                $('#keterangan').attr("disabled", true);
                $('#id_distributor').val(result.id);
                $('#nama_distributor').val(result.nama_distributor);
                $('#alamat_distributor').val(result.alamat_distributor);
                $('#no_contact_distributor').val(result.no_contact_distributor);
                $('#keterangan').val(result.keterangan);
                $('#button-edit').attr("disabled", true);
            } else {
                $('#nama_distributor').attr("disabled", false);
                $('#alamat_distributor').attr("disabled", false);
                $('#no_contact_distributor').attr("disabled", false);
                $('#detail_alamat').attr("disabled", false);
                $('#lat').attr("disabled", false);
                $('#long').attr("disabled", false);
                $('#keterangan').attr("disabled", false);
                $('#button-edit').attr("disabled", false);
                $('#id_distributor').val(result.id);
                $('#detail_alamat').val(result.detail_alamat);
                $('#lat').val(result.lat);
                $('#long').val(result.long);
                $('#nama_distributor').val(result.nama_distributor);
                $('#alamat_distributor').val(result.alamat_distributor);
                $('#no_contact_distributor').val(result.no_contact_distributor);
                $('#keterangan').val(result.keterangan);
            }

            $('#is_active_status').html(`
                <div class="col col-md-3"><label for="keterangan" class="switch switch-default switch-primary mr-2">Status distributor</label></div>
                <div class="col-12 col-md-9"><input type="checkbox" class="switch-input" ${result.is_active == '1' ? 'checked' : null} id="is_active" name="is_active"> <span class="switch-label"></span> <span class="switch-handle"></span></div>
                `);
            $('#is_active').on('change', function (event) {
                swal({
                    title: 'peringatan!',
                    text: `Apakah anda yakin akan me-nonaltifkan data distributor ${result.nama_distributor}?`,
                    icon: 'warning',
                    button: 'OK'
                }).then(function (results) {
                    if (results) {
                        $.ajax({
                            type: 'put',
                            url: `${endpoint}/distributor/${$('#id_distributor').val()}`,
                            cache: false,
                            processData: false,
                            contentType: false,
                            data: JSON.stringify({
                                'nama_distributor': result.nama_distributor,
                                'alamat_distributor': result.alamat_distributor,
                                'no_contact_distributor': result.no_contact_distributor,
                                'detail_alamat': result.detail_alamat,
                                'lat': result.lat,
                                'long': result.long,
                                'keterangan': result.keterangan,
                                'is_active': result.is_active == '0' ? 1 : 0
                            }),
                            headers: {
                                "Authorization": `Bearer ${token}`,
                                "Content-Type": "application/json"
                            },
                            success: function (result) {
                                swal({
                                    title: 'Success!',
                                    text: `${result.message}`,
                                    icon: 'success',
                                    button: 'OK'
                                }).then(function () {
                                    $('#distributor-table').DataTable().ajax.reload();
                                    myModal.hide();
                                });
                                console.log(result)
                            },
                        })

                    }

                });
            });
            console.log(result)
        },
        error: function (err) {
            console.log(err)
        }
    });

    myModal.show();
}

function editDistributor() {
    var id = $('#id_distributor').val();
    $.ajax({
        type: 'put',
        url: `${endpoint}/distributor/${id}`,
        cache: false,
        processData: false,
        contentType: false,
        data: JSON.stringify({
            'nama_distributor': $('#nama_distributor').val(),
            'alamat_distributor': $('#alamat_distributor').val(),
            'no_contact_distributor': $('#no_contact_distributor').val(),
            'detail_alamat': $('#detail_alamat').val(),
            'keterangan': $('#keterangan').val(),
            'lat': $('#lat').val(),
            'long': $('#long').val(),

        }),
        headers: {
            "Authorization": `Bearer ${token}`,
            "Content-Type": "application/json"
        },
        success: function (result) {
            swal({
                title: 'Success!',
                text: `${result.message}`,
                icon: 'success',
                button: 'OK'
            }).then(function () {
                $('#distributor-table').DataTable().ajax.reload();
                var myModal = new bootstrap.Modal(document.getElementById('modalDistributor'), {
                    keyboard: false
                });
                myModal.hide();
            });
            console.log(result)
        },
        error: function (err) {
            var status = err.status;
            if (status === 422) {
                var message = err.responseJSON.errors;
                if (message.nama_distributor !== undefined) {
                    $('#nama_distributor_validation').html(`${message.nama_distributor}`);
                } else {
                    $('#nama_distributor_validation').html(``)
                }
                if (message.alamat_distributor !== undefined) {
                    $('#alamat_distributor_validation').html(`${message.alamat_distributor}`);
                } else {
                    $('#alamat_distributor_validation').html(``)
                }
                if (message.no_contact_distributor !== undefined) {
                    $('#no_contact_distributor_validation').html(`${message.no_contact_distributor}`);
                } else {
                    $('#no_contact_distributor_validation').html(``)
                }
                if (message.detail_alamat !== undefined) {
                    $('#detail_alamat_distributor_validation').html(`${message.detail_alamat}`);
                } else {
                    $('#detail_alamat_distributor_validation').html(``)
                }

                console.log(message);
            }

            console.log(status)
        }
    });
}

function deleteDataDistributor(id) {
    swal({
        title: 'Peringatan!',
        text: `Apakah anda yakin akan menghapus data ini?`,
        icon: 'danger',
        button: 'OK'
    }).then(function (value) {
        if (value) {
            $.ajax({
                type: 'delete',
                url: `${endpoint}/distributor/${id}`,
                cache: false,
                processData: false,
                contentType: false,
                headers: {
                    "Authorization": `Bearer ${token}`,
                    "Content-Type": "application/json"
                },
                success: function (result) {
                    swal({
                        title: 'Success!',
                        text: `${result.message}`,
                        icon: 'success',
                        button: 'OK'
                    }).then(function () {
                        $('#distributor-table').DataTable().ajax.reload();
                    });
                    console.log(result)
                },
                error: function (err) {
                    var status = err.status;
                    console.log(status)
                }
            });
        }
    });

}