var endpoint = `${window.location.origin}/service`;
var token = localStorage.getItem("token");
var asetIdValue = null;
var formatRupiah = (money) => {
    return new Intl.NumberFormat("id-ID", {
        minimumFractionDigits: 0,
    }).format(money);
};
var date = new Date();
$(document).ready(function ($) {
    "use strict";
    var year = date.getFullYear();
    $("#year").html(year);

    $("#aset_id_select")
        .select2({
            placeholder: "Pilih  Aset",
            dropdownParent: $("#modalDetailAset"),
            allowClear: true,
        })
        .on("select2:selecting", function (e) {
            asetIdValue = e.params.args.data.id;
            console.log(asetIdValue);
        })
        .on("select2:clear", function () {
            asetIdValue = null;
        });

    $("#harga-perolehan-table").DataTable({
        processing: true,
        serverSide: true,
        dom: "Bfrtip",
        buttons: [
            {
                text: "Tamba Data >",
                className: "btn btn-primary",
                action: function (e, dt, node, config) {
                    $("#keterangan").attr("disabled", false);
                    $("#is_active_status").html(``);
                    $("#button-add").css("display", "block");
                    $("#button-edit").css("display", "none");
                    $("#nama_detail_aset").val("");
                    $("#nama_detail_aset_validation").html(``);
                    $("#is_expired").val("");
                    $("#is_expired_validation").html(``);
                    $("#keterangan").val("");
                    $("#aset_image").val("");
                    asetIdValue = null;
                    var myModal = new bootstrap.Modal(
                        document.getElementById("modalDetailAset"),
                        {
                            keyboard: false,
                        }
                    );
                    myModal.show();
                },
            },
        ],
        ajax: {
            url: `${endpoint}/detail-aset/`,
            type: "GET",
            headers: {
                Authorization: `Bearer ${token}`,
            },
            // data: e => console.log(e),
            // success: e => console.log(e),
        },
        columns: [
            {
                data: "DT_RowIndex",
                name: "DT_RowIndex",
            },
            {
                data: "aset",
                render: function (data, _, row) {
                    return `${row.aset.asetI_id}.${row.aset.asetII_id}.${row.aset.asetIII_id}.${row.aset.asetIV_id}.${row.aset.asetV_id}.${row.aset.asetVI_id}.${row.aset.kode_aset_vii}`;
                },
            },
            {
                data: "aset",
                render: function (data, _, row) {
                    return `${row.aset.nama_aset_vii}`;
                },
            },
            {
                data: "aset",
                render: function (data, _, row) {
                    return `${row.aset.data_id}`;
                },
            },
            {
                data: "nama_detail_aset",
                name: "nama_detail_aset",
            },
            {
                data: "keterangan",
                name: "keterangan",
            },
            {
                data: "aset_image",
                render: (a, b, row) =>
                    `<img src="${row.aset_image}" class="img-thumbnail" />`,
            },
            {
                data: "created_by",
                render: (a, b, row) => `${row.created_by} / ${row.updated_by}`,
            },
            {
                data: "harga_perolehan",
                render: function (data, _, row) {
                    var datas =
                        row.harga_perolehan !== null
                            ? $.map(row.harga_perolehan, function (d) {
                                  return `<li>Rp ${formatRupiah(
                                      d.perubahan_harga
                                  )}</li>`;
                              }).join("")
                            : null;
                    return datas.length === 0
                        ? `<a href="#" onclick="editHargaPerolehanShow('${row.id}', '${row.nama_detail_aset}')" class="btn btn-outline-success">Tambah Update Harga</a><br/><i class='text-danger'>Belum menambah update harga</i>`
                        : `<a href="#" onclick="editHargaPerolehanShow('${row.id}',  '${row.nama_detail_aset}')" class="btn btn-outline-success">Tambah Update Harga</a><br/><ol class="mt-1 ml-3">${datas}</ol>`;
                },
            },
            {
                data: "action",
                name: "action",
            },
        ],
    });
});

function editDetailAsetAction() {
    let id = $("#detai_aset_id_value").val();
    var aset_image_edit = $("#aset_image_edit").prop("files")[0];
    var form = new FormData();
    form.append("aset_id", $("#aset_id_select_edit").val());
    form.append("nama_detail_aset", $("#nama_detail_aset_edit").val());
    form.append("keterangan", $("#keterangan_edit").val());
    form.append("is_expired", $("#is_expired_edit").val());
    aset_image_edit !== undefined && form.append("aset_image", aset_image_edit);
    $.ajax({
        type: "POST",
        url: `${endpoint}/detail-aset/${id}`,
        cache: false,
        processData: false,
        contentType: false,
        data: form,
        headers: {
            Authorization: `Bearer ${token}`,
            Accept: "application/json",
        },
        success: function (result) {
            swal({
                title: "Success!",
                text: `${result.message}`,
                icon: "success",
                button: "OK",
            }).then(() => window.location.reload());
            console.log(result);
        },
        error: function (err) {
            var status = err.status;
            if (status === 401) {
                window.location.href = "/unauthorized";
            }
            if (status === 422) {
                var message = err.responseJSON.errors;
                if (message.aset_id !== undefined) {
                    $("#aset_id_select_edit_validation").html(
                        `${message.aset_id}`
                    );
                } else {
                    $("#aset_id_select_edit_validation").html(``);
                }
                if (message.nama_detail_aset !== undefined) {
                    $("#nama_detail_aset_edit_validation").html(
                        `${message.nama_detail_aset}`
                    );
                } else {
                    $("#nama_detail_aset_edit_validation").html(``);
                }
                if (message.keterangan !== undefined) {
                    $("#keterangan_edit_validation").html(
                        `${message.keterangan}`
                    );
                } else {
                    $("#keterangan_edit_validation").html(``);
                }
                if (message.is_expired !== undefined) {
                    $("#is_expired_edit_validation").html(
                        `${message.is_expired}`
                    );
                } else {
                    $("#is_expired_edit_validation").html(``);
                }
                if (message.aset_image !== undefined) {
                    $("#aset_image_edit_validation").html(
                        `${message.aset_image}`
                    );
                } else {
                    $("#aset_image_edit_validation").html(``);
                }
                console.log(message);
            }

            console.log(status);
        },
    });
}

function editDetailAsetShow(id) {
    $.ajax({
        type: "GET",
        url: `${endpoint}/detail-aset/${id}`,
        cache: false,
        processData: false,
        contentType: false,
        headers: {
            Authorization: `Bearer ${token}`,
            "Content-Type": "application/json",
        },
        success: function (result) {
            $("#detai_aset_id_value").val(result.id);
            $("#aset_id_select_edit").val(result.aset_id);
            $("#nama_detail_aset_edit").val(result.nama_detail_aset);
            $("#keterangan_edit").val(result.keterangan);
            $("#is_expired_edit").val(result.is_expired);
        },
        error: function (err) {
            var status = err.status;
            if (status === 401) {
                window.location.href = "/unauthorized";
            }
            console.log(status);
        },
    });
    var myModal = new bootstrap.Modal(
        document.getElementById("modalDetailAsetEdit"),
        {
            keyboard: false,
        }
    );
    myModal.show();
}

function editHargaPerolehanShow(e, detailTitle) {
    console.log(e);
    $("#detail_aset_id").val(e);
    $("#button-unit-add").css("display", "none");
    $("#button-unit-edit").css("display", "block");
    $("#tanggal").val("");
    $("#perubahan_harga").val("");
    $("#detail-title").html(`${detailTitle}`);
    var myModal = new bootstrap.Modal(
        document.getElementById("modalTambahHargaPerolehan"),
        {
            keyboard: false,
        }
    );
    myModal.show();
}

function editHargaPerolehan() {
    $.ajax({
        type: "POST",
        url: `${endpoint}/perubahan-harga-aset/update-harga`,
        cache: false,
        processData: false,
        contentType: false,
        data: JSON.stringify({
            detail_aset_id: $("#detail_aset_id").val(),
            tanggal: $("#tanggal").val(),
            perubahan_harga: $("#perubahan_harga").val(),
        }),
        headers: {
            Authorization: `Bearer ${token}`,
            "Content-Type": "application/json",
        },
        success: function (result) {
            swal({
                title: "Success!",
                text: `${result.message}`,
                icon: "success",
                button: "OK",
            }).then(function () {
                $("#harga-perolehan-table").DataTable().ajax.reload();
                var myModal = new bootstrap.Modal(
                    document.getElementById("modalTambahHargaPerolehan"),
                    {
                        keyboard: false,
                    }
                );
                myModal.hide();
            });
            console.log(result);
        },
        error: function (err) {
            var status = err.status;
            if (status === 401) {
                window.location.href = "/unauthorized";
            }
            if (status === 422) {
                var message = err.responseJSON.errors;
                if (message.tanggal !== undefined) {
                    $("#tanggal_validation").html(`${message.tanggal}`);
                } else {
                    $("#tanggal_validation").html(``);
                }
                if (message.perubahan_harga !== undefined) {
                    $("#perubahan_harga_validation").html(
                        `${message.perubahan_harga}`
                    );
                } else {
                    $("#perubahan_harga_validation").html(``);
                }

                console.log(message);
            }

            console.log(status);
        },
    });
}

function detailHargaPerolehan(id) {
    var myModal = new bootstrap.Modal(
        document.getElementById("ListTambahHargaPerolehan"),
        {
            keyboard: false,
        }
    );
    myModal.show();
    $("#content-list").html(
        `<span class="text-success text-center">Loading...</span>`
    );
    $.ajax({
        type: "GET",
        url: `${endpoint}/perubahan-harga-aset/by-detail-aset/${id}`,
        cache: false,
        processData: false,
        contentType: false,
        headers: {
            Authorization: `Bearer ${token}`,
            "Content-Type": "application/json",
        },
        success: function (result) {
            if (result.length > 0) {
                var dataListHarga = $.map(result, function (d, index) {
                    return `<tr>
                        <td>${index + 1}</td>
                        <td>${d.tanggal}</td>
                        <td>Rp ${formatRupiah(d.perubahan_harga)}</td>
                        <td> ${d.created_by.name} / ${d.created_at} </td>
                    </tr>`;
                }).join(" ");
                $("#content-list").html(`
                <table  class="table table-striped table-bordered">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Tanggal</th>
                            <th>List Perubahan Harga</th>
                            <th>Dibuat Oleh / waktu</th>
                        </tr>
                    </thead>
                    <tbody>${dataListHarga}</tbody>
                </table>
                `);
            } else {
                $("#content-list").html(
                    `<div class="d-flex justify-content-center"><span class="text-danger text-center">Belum ada Harga Perolehan yang ditambahkan</span></div> `
                );
            }
        },
    });
}

function addDetailAset() {
    var aset_image = $("#aset_image").prop("files")[0];
    var form = new FormData();
    form.append("aset_id", asetIdValue);
    form.append("nama_detail_aset", $("#nama_detail_aset").val());
    form.append("keterangan", $("#keterangan").val());
    form.append("is_expired", $("#is_expired").val());
    aset_image !== undefined && form.append("aset_image", aset_image);
    $.ajax({
        type: "POST",
        url: `${endpoint}/detail-aset`,
        cache: false,
        processData: false,
        contentType: false,
        data: form,
        headers: {
            Authorization: `Bearer ${token}`,
        },
        success: function (result) {
            swal({
                title: "Success!",
                text: `${result.message}`,
                icon: "success",
                button: "OK",
            }).then(function () {
                $("#harga-perolehan-table").DataTable().ajax.reload();
                var myModal = new bootstrap.Modal(
                    document.getElementById("modalDetailAset"),
                    {
                        keyboard: false,
                    }
                );
                myModal.hide();
            });
            console.log(result);
        },
        error: function (err) {
            var status = err.status;
            if (status === 401) {
                window.location.href = "/unauthorized";
            }
            if (status === 422) {
                var message = err.responseJSON.errors;
                if (message.aset_id !== undefined) {
                    $("#aset_id_select_validation").html(`${message.aset_id}`);
                } else {
                    $("#aset_id_select_validation").html(``);
                }
                if (message.nama_detail_aset !== undefined) {
                    $("#nama_detail_aset_validation").html(
                        `${message.nama_detail_aset}`
                    );
                } else {
                    $("#nama_detail_aset_validation").html(``);
                }
                if (message.keterangan !== undefined) {
                    $("#keterangan_validation").html(`${message.keterangan}`);
                } else {
                    $("#keterangan_validation").html(``);
                }
                if (message.is_expired !== undefined) {
                    $("#is_expired_validation").html(`${message.is_expired}`);
                } else {
                    $("#is_expired_validation").html(``);
                }
                if (message.aset_image !== undefined) {
                    $("#aset_image_validation").html(`${message.aset_image}`);
                } else {
                    $("#aset_image_validation").html(``);
                }
                console.log(message);
            }

            console.log(status);
        },
    });
}
