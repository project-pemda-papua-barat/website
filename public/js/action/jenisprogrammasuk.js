var endpoint = `${window.location.origin}/service`;
var token = localStorage.getItem('token');
$(document).ready(function () {
    "use strict";
    var date = new Date();
    var year = date.getFullYear();
    $('#year').html(year);

    $('#jenis-program-masuk-table').DataTable({
        processing: true,
        serverSide: true,
        dom: 'Bfrtip',
        buttons: [{
            text: 'Tamba Data >',
            className: 'btn btn-primary',
            action: function (e, dt, node, config) {
                $('#nama_program').attr("disabled", false);
                $('#keterangan').attr("disabled", false);
                $('#is_active_status').html(``);
                $("#button-add").css("display", "block");
                $("#button-edit").css("display", "none");
                $('#nama_program').val("");
                $('#nama_program_validation').html(``);
                $('#keterangan').val("");
                var myModal = new bootstrap.Modal(document.getElementById('modalProgram'), {
                    keyboard: false
                });
                myModal.show();
            }
        }],
        ajax: {
            url: `${endpoint}/jenis-program-masuk`,
            type: 'GET',
            headers: {
                "Authorization": `Bearer ${token}`,
            },
            // data: e => console.log(e),
            // success: e => console.log(e),
        },
        columns: [{
            data: "DT_RowIndex",
            name: "DT_RowIndex",
        },
        {
            data: 'nama_program',
            name: 'nama_program'
        },
        {
            data: 'keterangan',
            name: 'keterangan'
        },
        {
            data: 'is_active',
            render: function (data) {
                if (data == 0) {
                    return "Tidak Aktif"
                } else {
                    return "Aktif"
                }

            }
        },
        {
            data: 'action',
            name: 'action'
        },
        ],
    });
});

function addProgram() {
    var form = new FormData();
    form.append('nama_program', $('#nama_program').val());
    form.append('keterangan', $('#keterangan').val());
    $.ajax({
        type: 'post',
        url: `${endpoint}/jenis-program-masuk`,
        cache: false,
        processData: false,
        contentType: false,
        data: form,
        headers: {
            "Authorization": `Bearer ${token}`,
        },
        success: function (result) {
            swal({
                title: 'Success!',
                text: `${result.message}`,
                icon: 'success',
                button: 'OK'
            }).then(function () {
                $('#jenis-program-masuk-table').DataTable().ajax.reload();
            });
            console.log(result)
        },
        error: function (err) {
            var status = err.status;
            if (status === 422) {
                var message = err.responseJSON.errors;
                if (message.nama_program !== undefined) {
                    $('#nama_program_validation').html(`${message.nama_program}`);
                } else {
                    $('#nama_program_validation').html(``)
                }
                console.log(message);
            }

            console.log(status)
        }
    });
}

function editProgramShow(id) {
    $("#button-add").css("display", "none");
    $("#button-edit").css("display", "block");
    $('#nama_program_validation').html(``);
    var myModal = new bootstrap.Modal(document.getElementById('modalProgram'), {
        keyboard: false
    });
    $.ajax({
        type: 'get',
        url: `${endpoint}/jenis-program-masuk/${id}`,
        cache: false,
        processData: false,
        contentType: false,
        headers: {
            "Authorization": `Bearer ${token}`,
        },
        success: function (result) {
            if (result.is_active == '0') {
                $('#nama_program').attr("disabled", true);
                $('#keterangan').attr("disabled", true);
                $('#id_program').val(result.id);
                $('#nama_program').val(result.nama_program);
                $('#keterangan').val(result.keterangan);
                $('#button-edit').attr("disabled", true);
            } else {
                $('#nama_program').attr("disabled", false);
                $('#keterangan').attr("disabled", false);
                $('#button-edit').attr("disabled", false);
                $('#id_program').val(result.id);
                $('#nama_program').val(result.nama_program);
                $('#keterangan').val(result.keterangan);
            }

            $('#is_active_status').html(`
                <div class="col col-md-3"><label for="keterangan" class="switch switch-default switch-primary mr-2">Status Program</label></div>
                <div class="col-12 col-md-9"><input type="checkbox" class="switch-input" ${result.is_active == '1' ? 'checked' : null} id="is_active" name="is_active"> <span class="switch-label"></span> <span class="switch-handle"></span></div>
                `);
            $('#is_active').on('change', function (event) {
                swal({
                    title: 'peringatan!',
                    text: `Apakah anda yakin akan me-nonaltifkan data ${result.nama_program}?`,
                    icon: 'warning',
                    button: 'OK'
                }).then(function (results) {
                    if (results) {
                        $.ajax({
                            type: 'put',
                            url: `${endpoint}/jenis-program-masuk/${$('#id_program').val()}`,
                            cache: false,
                            processData: false,
                            contentType: false,
                            data: JSON.stringify({
                                'nama_program': result.nama_program,
                                'keterangan': result.keterangan,
                                'is_active': result.is_active == 0 ? 1 : 0
                            }),
                            headers: {
                                "Authorization": `Bearer ${token}`,
                                "Content-Type": "application/json"
                            },
                            success: function (result) {
                                swal({
                                    title: 'Success!',
                                    text: `${result.message}`,
                                    icon: 'success',
                                    button: 'OK'
                                }).then(function () {
                                    $('#jenis-program-masuk-table').DataTable().ajax.reload();
                                    myModal.hide();
                                });
                                console.log(result)
                            },
                        })

                    }

                });
            });
            console.log(result)
        },
        error: function (err) {
            console.log(err)
        }
    });

    myModal.show();
}

function editProgram() {
    var id = $('#id_program').val();
    $.ajax({
        type: 'put',
        url: `${endpoint}/jenis-program-masuk/${id}`,
        cache: false,
        processData: false,
        contentType: false,
        data: JSON.stringify({
            'nama_program': $('#nama_program').val(),
            'keterangan': $('#keterangan').val(),
        }),
        headers: {
            "Authorization": `Bearer ${token}`,
            "Content-Type": "application/json"
        },
        success: function (result) {
            swal({
                title: 'Success!',
                text: `${result.message}`,
                icon: 'success',
                button: 'OK'
            }).then(function () {
                $('#jenis-program-masuk-table').DataTable().ajax.reload();
                var myModal = new bootstrap.Modal(document.getElementById('modalProgram'), {
                    keyboard: false
                });
                myModal.hide();
            });
            console.log(result)
        },
        error: function (err) {
            var status = err.status;
            if (status === 422) {
                var message = err.responseJSON.errors;
                if (message.nama_program !== undefined) {
                    $('#nama_program_validation').html(`${message.nama_program}`);
                } else {
                    $('#nama_program_validation').html(``)
                }
                console.log(message);
            }

            console.log(status)
        }
    });
}

function deleteProgram(id) {
    swal({
        title: 'Peringatan!',
        text: `Apakah anda yakin akan menghapus data ini?`,
        icon: 'warning',
        button: 'OK'
    }).then(function (value) {
        if (value) {
            $.ajax({
                type: 'delete',
                url: `${endpoint}/jenis-program-masuk/${id}`,
                cache: false,
                processData: false,
                contentType: false,
                headers: {
                    "Authorization": `Bearer ${token}`,
                    "Content-Type": "application/json"
                },
                success: function (result) {
                    swal({
                        title: 'Success!',
                        text: `${result.message}`,
                        icon: 'success',
                        button: 'OK'
                    }).then(function () {
                        $('#jenis-program-masuk-table').DataTable().ajax.reload();
                    });
                    console.log(result)
                },
                error: function (err) {
                    var status = err.status;

                    console.log(status)
                }
            });
        }
    });

}