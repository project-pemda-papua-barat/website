var asetValue = "";
var stock_out_id = "";
var endpoint = `${window.location.origin}/service`;
var token = localStorage.getItem('token');
var path = window.location.pathname.split('/');
(function ($) {
    "use strict";
    var content = $('#authvalidation');
    var date = new Date;
    var tahun = date.getFullYear();

    $(document).ready(function () {
        $('#year').html(tahun);
        let date = new Date;
        let year = date.getFullYear();
        let month = date.getMonth();
        let dates = date.getDate();
        let hour = date.getHours();
        let minute = date.getMinutes();
        let second = date.getSeconds();
        let num_txt = `${year}${month}${dates}${hour}${minute}${second}`
        $('#no_transaksi').val(num_txt);
        let listTahun = [];
        listTahun.push(year, year + 1);
        console.log(listTahun);
        $.map(listTahun, function (e) {
            $('#tahun_pengadaan').append($('<option>', {
                value: e,
                text: e
            }));
        });

        // select ASET to Stock Out
        $('#sotcked_data_select').select2({
            placeholder: "Pilih  Aset",
            dropdownParent: $('#modalData'),
            allowClear: true
        }).on("select2:selecting", function (e) {
            asetValue = e.params.args.data.id;

            $.ajax({
                url: `${endpoint}/stocked-center/${asetValue}`,
                type: 'GET',
                headers: {
                    "Authorization": `Bearer ${token}`,
                },
                success: function (data) {
                    // Transforms the top-level key of the response object from 'items' to 'results'
                    let maping = $.map(data, (d) => {
                        return `<li>Nomor Aset: ${d.nomor_aset}</li>
                                  <li>Nama Detail Aset: ${d.nama_detail_aset}</li>
                                  <li>Distributor: ${d.nama_distributor}</li>
                                  <li>Penyedia: ${d.nama_penyedia}</li>
                                  <li>Sumber Dana: ${d.nama_sumber_dana} </li>
                                  <li>Stok: ${d.qty} ${d.nama_satuan} </li>
                        `
                    }).join(",", " ");
                    $("#list-content").html(`<ul class="ml-3">${maping}</li>`)
                }
            })
        }).on("select2:clear", function (e) {
            asetValue = "";
            $("#list-content").html(``);
        });

        $('#content-draft').html('<span class="text-success text-center py-4">loading...</span>');
        // auth
        $.ajax({
            type: 'get',
            url: `${endpoint}/profile`,
            cache: false,
            processData: false,
            contentType: false,
            headers: {
                "Authorization": `Bearer ${token}`,
            },
            success: function (result) {
                if (result.role_name !== 'User Unit') {
                    window.location.href = '/unauthorized';
                }
                content.html(`<a href="#" class="dropdown-toggle active" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <img class="user-avatar rounded-circle active" src="${result.profile_pict}" alt="User Avatar">
                <span class="ml-2">${result.name}</span>
            </a>
            <div class="user-menu dropdown-menu">
                <a class="nav-link" href="#"><i class="fa fa- user"></i>My Profile</a>
                <a class="nav-link" href="#"><i class="fa fa- user"></i>Notifications <span class="count">0</span></a>
                <a class="nav-link" href="#"><i class="fa fa -cog"></i>Settings</a>
                <a class="nav-link" href="#" onclick="logout()"><i class="fa fa-power -off"></i>Logout</a>
            </div>
                    `);

                console.log(result.role_name);
                if (result.role_name === 'User Unit') {
                    userAdmin();
                }
            },
            error: function (err) {
                if (err.status == 401) {
                    window.location.href = "/login";
                }
            }
        });

        $.ajax({
            url: `${endpoint}/stocked-center`,
            type: 'GET',
            headers: {
                "Authorization": `Bearer ${token}`,
            },
            success: res => {
                $.each(res, function (i, item) {
                    $('#sotcked_data_select_edit').append($('<option>', {
                        value: item.id,
                        text: `${item.nomor_aset} ${item.nama_detail_aset} ${item.kode} - stok: ${item.qty} ${item.is_expired === "true" ? ` - expired: ${item.expired_date}` : ""}`
                    }));
                });

                $.each(res, function (i, item) {
                    $('#sotcked_data_select').append($('<option>', {
                        value: item.id,
                        text: `${item.nomor_aset} ${item.nama_detail_aset} ${item.kode} - stok: ${item.qty} ${item.is_expired === "true" ? ` - expired: ${item.expired_date}` : ""}`
                    }));
                });
            }
        });

    });

})(jQuery);

function userAdmin() {
    $.ajax({
        type: 'GET',
        url: `${endpoint}/stock-out/detail/${path[4]}`,
        cache: false,
        processData: false,
        contentType: false,
        headers: {
            "Authorization": `Bearer ${token}`
        },
        success: res => {
            jenisProgramValue = res.nama_program;
            stock_out_id = res[0].id;
            if (res[0].is_status === 'waiting approval' || res[0].is_status === 'approved') {
                back();
            }

            $('#content-draft').html(`
            <div class="card-header">
                <div class="row">
                    <div class="col-xl-6 col-md-6">
                        <div class="form-group row">
                            <label for="no_transaksi" class=" col-sm-6 font-weight-bold">Nomor Transaksi</label>
                            <div class="col-sm-6">
                                <p class="font-weight-bold">: ${res[0].no_transaksi}</p>
                                <input type="hidden" id="id_trx" name="id_trx" value="${res[0].id}"/>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-6 col-md-6">
                        <div class="form-group row">
                            <label for="no_transaksi" class=" col-sm-6 font-weight-bold">Nomor Berita Acara</label>
                            <div class="col-sm-6">
                                <p class="font-weight-bold">: ${res[0].no_berita_acara}</p>
                                <input type="hidden" id="id_trx" name="id_trx"/>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-6 col-md-6">
                        <div class="form-group row">
                            <label for="no_transaksi" class=" col-sm-6 font-weight-bold">Nomor Dokumen Pengiriman</label>
                            <div class="col-sm-6">
                                <p class="font-weight-bold">: ${res[0].no_dokumen_pengiriman}</p>
                                <input type="hidden" id="id_trx" name="id_trx"/>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-6 col-md-6">
                        <div class="form-group row">
                            <label for="no_transaksi" class=" col-sm-6 font-weight-bold">Nama Distributor</label>
                            <div class="col-sm-6">
                                <p class="font-weight-bold">: ${res[0].nama_distributor}</p>
                                <input type="hidden" id="id_trx" name="id_trx"/>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-6 col-md-6">
                        <div class="form-group row">
                            <label for="no_transaksi" class=" col-sm-6 font-weight-bold">Unit Tujuan</label>
                            <div class="col-sm-6">
                                <p class="font-weight-bold">: ${res[0].unit_tujuan}</p>
                                <input type="hidden" id="id_trx" name="id_trx"/>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-6 col-md-6">
                        <div class="form-group row">
                            <label for="no_transaksi" class=" col-sm-6 font-weight-bold">Petugas</label>
                            <div class="col-sm-6">
                                <p class="font-weight-bold">: ${res[0].petugas}</p>
                                <input type="hidden" id="id_trx" name="id_trx"/>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-12 col-md-12">
                        <div class="form-group row">
                            <label for="no_transaksi" class=" col-sm-6 font-weight-bold">Keterangan</label>
                            <div class="col-sm-6">
                                <p class="font-weight-bold">: ${res[0].keterangan === null ? "" : res[0].keterangan}</p>
                                <input type="hidden" id="id_trx" name="id_trx"/>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-8 col-md-12">
                        <div class="d-flex justify-content-start">
                        <a ${
                            res[0].dokumen_berita_acara ===
                            window.location.origin + "/upload/aset_dokumen"
                                ? `onclick="notAsetUploaded()" href="#" `
                                : `href="${res[0].dokumen_berita_acara}" target="_blank" `
                        } class="btn btn-outline-primary my-1">Dokumen Berita Acara</a>
                        <a ${
                            res[0].dokumen_expedisi ===
                            window.location.origin + "/upload/aset_dokumen"
                                ? "onclick='notAsetUploaded()' href='#' "
                                : `href="${res[0].dokumen_expedisi}" target="_blank" `
                        } class="btn btn-outline-warning my-1 mx-2">Dokumen Expedisi</a>
                        <a ${
                            res[0].dokumen_pendukung ===
                            window.location.origin + "/upload/aset_dokumen"
                                ? `onclick='notAsetUploaded()' href='#' `
                                : `href="${res[0].dokumen_pendukung}" target="_blank" `
                        } class="btn btn-outline-success my-1">Dokumen Pendukung</a>
                        </div>
                    </div>
                    <div class="col-xl-4 col-md-3">
                        <div class="d-flex justify-content-end">
                            <a href="#"  class="btn btn-outline-danger my-1 " onclick="editStockOutMaster(${res[0].id})">Edit</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-body">
                    <div class="container">
                        <div id="content-sub-item">
                            <table id="drafts-table" class="table table-striped table-bordered py-4">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Nomor Aset</th>
                                        <th>Kode</th>
                                        <th>Nama</th>
                                        <th>Stok Tersedia</th>
                                        <th>Permintaan</th>
                                        <th>Penyedia</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                            </table>
                            <div class="mt-3">
                                <div class="d-flex justify-content-start">
                                    <button class="btn btn-secondary" onclick="tambahData()">Tambah Data ke Daftar Keluar</button>
                                </div>
                                <div class="d-flex justify-content-end">
                                    <button type="button" class="btn btn-danger mx-2" onclick="back()">Kembali</button>
                                    <button class="btn btn-primary mx-2" type="button" onclick="saveAll()">Simpan</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            `);
            showDetailStockOut(res[0].id);
            // showDetailStockIn();
        },
        error: err => {
            console.log(err)
            console.log(err.status)
            if (err.status === 404) {
                window.location.href = '/not-found';
            }

            if (err.status === 401) {
                window.location.href = '/unauthorized';
            }
        }
    })
}

function notAsetUploaded() {
    return swal({
        title: "Dokumen tidak tersedia!",
        text: `Dokumen ini belum di upload, mohon upload terlebih dahulu!`,
        icon: "warning",
        button: "OK",
    });
}

function saveAll() {
    console.log('SAVE ALL');
    const form = new FormData;
    form.append('stock_out_id', stock_out_id);
    form.append('is_status', 'waiting approval');
    $.ajax({
        type: 'POST',
        url: `${endpoint}/stock-out/store-status`,
        cache: false,
        processData: false,
        contentType: false,
        headers: {
            'Accept': 'application/json',
            "Authorization": `Bearer ${token}`,
        },
        data: form,
        success: res => {
            swal({
                title: 'Sukses!',
                text: `${res.message}`,
                icon: 'success',
                button: 'OK'
            }).then(() => back());
        },
        error: err => {
            let message = err.responseJSON.message;
            swal({
                title: 'Gagal!',
                text: `${message}`,
                icon: 'error',
                button: 'OK'
            });
        }
    });
}

function editDataDetailStock() {
    var id = $('#id_value').val();
    var stock_out_id = $('#stock_out_id_value').val();
    var form = new FormData;
    form.append('stocked_center_id', $('#sotcked_data_select_edit').val());
    form.append('qty_out', $('#jumlah_edit').val());
    form.append('stock_out_id', stock_out_id);

    $.ajax({
        type: 'POST',
        url: `${endpoint}/stock-out-detail/edit/${id}`,
        cache: false,
        processData: false,
        contentType: false,
        data: form,
        headers: {
            "Authorization": `Bearer ${token}`,
        },
        success: res => {
            swal({
                title: 'success!',
                text: `${res.message_stock}`,
                icon: 'success',
                button: 'OK'
            }).then(function () {
                userAdmin();
            });
        },
        error: err => {
            if (err.status === 422) {
                var message = err.responseJSON.errors; 
                var message2 = err.responseJSON?.message_stock; 
                var message3 = err.responseJSON?.message_unique; 
                if (message2 !== undefined) {
                    swal({
                        title: 'error!',
                        text: `${message2}`,
                        icon: 'error',
                        button: 'OK'
                    })
                }
                if (message3 !== undefined) {
                    swal({
                        title: 'error!',
                        text: `${message3}`,
                        icon: 'error',
                        button: 'OK'
                    })
                }

                // if (message.detail_aset_id !== undefined) {
                //     $('#aset_id_edit_validation').html(`${message.detail_aset_id}`);
                // } else {
                //     $('#aset_id_edit_validation').html(``)
                // }

            }
            console.log(err.status)
        }
    });
}

function editData(dataId) {
    $.ajax({
        url: `${endpoint}/stock-out-detail/${dataId}`,
        type: 'GET',
        headers: {
            "Authorization": `Bearer ${token}`,
        },
        success: val => {
            $("#id_value").val(val.id);
            $("#stock_out_id_value").val(val.stock_out_id);
            $('#sotcked_data_select_edit').val(val.stocked_center_id);
            $('#jumlah_edit').val(val.qty_out);
        },
    })
    var myModal = new bootstrap.Modal(document.getElementById('modalDataEditDetailStock'), {
        keyboard: false
    });
    myModal.show();
}

function showDetailStockOut(id) {
    return $('#drafts-table').DataTable({
        processing: true,
        serverSide: true,
        dom: 'Bfrtip',
        ajax: {
            url: `${endpoint}/stock-out-detail/stock-out/${id}`,
            type: 'GET',
            headers: {
                "Authorization": `Bearer ${token}`,
            },
            // data: e => console.log(e),
            // success: e => console.log(e),
        },
        columns: [{
            data: "DT_RowIndex",
            name: "DT_RowIndex",
        },
        {
            data: 'nomor_aset',
            name: 'nomor_aset'
        },
        {
            data: 'kode',
            name: 'kode'
        },
        {
            data: 'nama_detail_aset',
            name: 'nama_detail_aset'
        },
        {
            data: 'qty',
            name: 'qty'
        },
        {
            data: 'qty_out',
            name: 'qty_out'
        },
        {
            data: 'nama_penyedia',
            name: 'nama_penyedia'
        },
        {
            data: 'action',
            name: 'action'
        },
        ],
    });
}

function editStockOutMaster() {
    var id = $('#id_trx').val();
    $.ajax({
        url: `${endpoint}/stock-out/detail-id/${id}`,
        cache: false,
        processData: false,
        contentType: false,
        headers: {
            "Authorization": `Bearer ${token}`
        },
        success: res => {

            $('#no_berita_acara').val(res.no_berita_acara);
            $('#no_dokumen_pengiriman').val(res.no_dokumen_pengiriman);
            $('#distributor_id_select').val(res.distributor_id);
            $('#tanggal_dokumen').val(res.tanggal_dokumen);
            $('#keterangan').val(res.keterangan);
            $('#to_unit_id_select').val(res.to_unit_id);
        },
        error: err => {
            console.log(err.status)
            if (err.status === 404) {
                window.location.href = '/not-found';
            }

            if (err.status === 401) {
                window.location.href = '/unauthorized';
            }
        }
    })
    var myModal = new bootstrap.Modal(document.getElementById('modalUpload'), {
        keyboard: false
    });
    myModal.show();
}

function back() {
    return window.location.href = `${window.location.origin}/stok-keluar`;
}


function addData() {
    var id = $('#id_trx').val();
    var dokumen_berita_acara = $('#dokumen_berita_acara').prop('files')[0];
    var dokumen_expedisi = $('#dokumen_expedisi').prop('files')[0];
    var dokumen_pendukung = $('#dokumen_pendukung').prop('files')[0];
    console.log(dokumen_pendukung)
    var form = new FormData();
    form.append('no_transaksi', $('#no_transaksi').val());
    form.append('no_berita_acara', $('#no_berita_acara').val());
    form.append('no_dokumen_pengiriman', $('#no_dokumen_pengiriman').val());
    form.append('tanggal_dokumen', $('#tanggal_dokumen').val());
    form.append('distributor_id', $("#distributor_id_select").val());
    form.append('to_unit_id', $("#to_unit_id_select").val());
    dokumen_berita_acara !== undefined && form.append('dokumen_berita_acara', dokumen_berita_acara);
    dokumen_expedisi !== undefined && form.append('dokumen_expedisi', dokumen_expedisi);
    dokumen_pendukung !== undefined && form.append('dokumen_pendukung', dokumen_pendukung);
    form.append('keterangan', $('#keterangan').val());
    $.ajax({
        type: 'POST',
        url: `${endpoint}/stock-out/detail-id/${id}`,
        cache: false,
        processData: false,
        contentType: false,
        data: form,
        headers: {
            "Authorization": `Bearer ${token}`,
        },
        success: res => {
            swal({
                title: 'success!',
                text: `${res.message}`,
                icon: 'success',
                button: 'OK'
            }).then(() => {
                window.location.reload();
            });
        },
        error: err => {
            if (err.status === 422) {
                var message = err.responseJSON.errors;

                if (message.tahun_pengadaan !== undefined) {
                    $('#tahun_pengadaan_validation').html(`${message.tahun_pengadaan}`);
                } else {
                    $('#tahun_pengadaan_validation').html(``)
                }

                if (message.sumber_dana_id !== undefined) {
                    $('#sumber_dana_id_validation').html(`${message.sumber_dana_id}`);
                } else {
                    $('#sumber_dana_id_validation').html(``)
                }

                if (message.tanggal_dokumen !== undefined) {
                    $('#tanggal_dokumen_validation').html(`${message.tanggal_dokumen}`);
                } else {
                    $('#tanggal_dokumen_validation').html(``)
                }

                if (message.no_dokumen_pengiriman !== undefined) {
                    $('#no_dokumen_pengiriman_validation').html(`${message.no_dokumen_pengiriman}`);
                } else {
                    $('#no_dokumen_pengiriman_validation').html(``)
                }

                if (message.no_berita_acara !== undefined) {
                    $('#no_berita_acara_validation').html(`${message.no_berita_acara}`);
                } else {
                    $('#no_berita_acara_validation').html(``)
                }

                if (message.jenis_id !== undefined) {
                    $('#jenis_id_validation').html(`${message.jenis_id}`);
                } else {
                    $('#jenis_id_validation').html(``)
                }
                if (message.penyedia_id !== undefined) {
                    $('#penyedia_id_validation').html(`${message.penyedia_id}`);
                } else {
                    $('#penyedia_id_validation').html(``)
                }
                if (message.distributor_id !== undefined) {
                    $('#distributor_id_validation').html(`${message.distributor_id}`);
                } else {
                    $('#distributor_id_validation').html(``)
                }
                if (message.dokumen_berita_acara !== undefined) {
                    $('#dokumen_berita_acara_validation').html(`${message.dokumen_berita_acara}`);
                } else {
                    $('#dokumen_berita_acara_validation').html(``)
                }
                if (message.dokumen_expedisi !== undefined) {
                    $('#dokumen_expedisi_validation').html(`${message.dokumen_expedisi}`);
                } else {
                    $('#dokumen_expedisi_validation').html(``)
                }
                if (message.dokumen_pendukung !== undefined) {
                    $('#dokumen_pendukung_validation').html(`${message.dokumen_pendukung}`);
                } else {
                    $('#dokumen_pendukung_validation').html(``)
                }
            }
            console.log(err.status)
        }
    });
}

function addDataDetailStock() {
    var form = new FormData();
    form.append('stocked_center_id', asetValue);
    form.append('stock_out_id', stock_out_id);
    form.append('qty_out', $('#jumlah').val());
    $.ajax({
        type: 'POST',
        url: `${endpoint}/stock-out-detail`,
        cache: false,
        processData: false,
        contentType: false,
        headers: {
            "Authorization": `Bearer ${token}`,
        },
        data: form,
        success: res => {
            swal({
                title: 'success!',
                text: `${res.message}`,
                icon: 'success',
                button: 'OK'
            }).then(() => {
                $('#drafts-table').DataTable().ajax.reload();
            });
        },
        error: err => {
            var status = err.status;
            if (status === 422) {
                var message = err.responseJSON.errors === undefined ? null : err.responseJSON.errors;
                var message_stock = err.responseJSON.message_stock === undefined ? null : err.responseJSON.message_stock;
                var message_unique = err.responseJSON.message_unique === undefined ? null : err.responseJSON.message_unique;

                if (message_stock !== null) {
                    swal({
                        title: 'error!',
                        text: `${message_stock}`,
                        icon: 'error',
                        button: 'OK'
                    })
                }
                if (message_unique !== null) {
                    swal({
                        title: 'error!',
                        text: `${message_unique}`,
                        icon: 'error',
                        button: 'OK'
                    })
                }

                if (message?.stocked_center_id !== undefined) {
                    $('#sotcked_data_select_validation').html(`${message?.stocked_center_id}`);
                } else {
                    $('#sotcked_data_select_validation').html(``)
                }
                if (message?.qty_out !== undefined) {
                    $('#jumlah_validation').html(`${message?.qty_out}`);
                } else {
                    $('#jumlah_validation').html(``)
                }
            }
        }
    })
}


function tambahData() {
    $("#button-add").css("display", "block");
    $("#button-edit").css("display", "none");
    var myModal = new bootstrap.Modal(document.getElementById('modalData'), {
        keyboard: false
    });
    myModal.show();
}

function deleteData(id) {
    swal({
        title: 'warning!',
        text: `Akah anda yakin akan menghapus data ini?!`,
        icon: 'warning',
        button: 'OK'
    }).then(function (value) {
        if (value) {
            $.ajax({
                type: 'DELETE',
                url: `${endpoint}/stock-out-detail/${id}`,
                cache: false,
                processData: false,
                contentType: false,
                headers: {
                    "Authorization": `Bearer ${token}`,
                },
                success: function (event) {
                    swal({
                        title: 'Success!',
                        text: `${event.message}`,
                        icon: 'success',
                        button: 'OK'
                    });
                    $('#drafts-table').DataTable().ajax.reload();
                    console.log(event)
                },
                error: function (err) {
                    console.log(err.responseJSON)
                    swal({
                        title: 'Error!',
                        text: `${err}`,
                        icon: 'error',
                        button: 'OK'
                    });
                }
            });
        }
    });
}

function logout() {
    swal({
        title: 'peringatan!',
        text: `Apakah anda yakin akan melakukan logout?`,
        icon: 'warning',
        button: 'OK'
    }).then(function (result) {
        if (result) {
            localStorage.removeItem('token');
            window.location.href = "/login";
        }
    });

}