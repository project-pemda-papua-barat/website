var endpoint = `${window.location.origin}/service`;
var loading = `<div id="spinner" class="show position-fixed translate-middle w-100 vh-100 top-50 start-50 d-flex align-items-center justify-content-center">
    <div class="spinner-border text-primary" style="width: 3rem; height: 3rem;" role="status">
        <span class="sr-only">Loading...</span>
    </div>
</div>`;

(function ($) {
    "use strict";

    var content = $('#userValidation');
    var token = localStorage.getItem('token');
    var contentLoad = `<div class="row h-100 align-items-center justify-content-center">
    <div class="col-12 col-sm-8 col-md-6 col-lg-5 col-xl-5 ">
        <div class="card p-4 p-sm-5 my-4 mx-4 shadow">
            <div class="card-content">
                <div class="text-center py-4">
                    <h5 class="text-primary">Login</h5>
                    <p>Masukan Email dan Password</p>
                </div>
                <span id="email-validation" class="text-danger"></span>
                <div class="form-floating mb-3">
                    <input type="text" class="form-control" id="floatingInput" placeholder="username" name="username">
                    <label for="floatingInput">Username</label>
                </div>
                <span id="password-validation" class="text-danger"></span>
                <div class="form-floating mb-4">
                    <input type="password" class="form-control" id="floatingPassword" placeholder="Password" name="password">
                    <label for="floatingPassword">Password</label>
                </div>
                <span id="username-password-validation" class="text-danger"></span>
                <div class="d-flex align-items-center justify-content-between mb-4">
                    <div class="form-check">
                        <input type="checkbox" class="form-check-input" id="exampleCheck1">
                        <label class="form-check-label" for="exampleCheck1">ingat saya</label>
                    </div>
                </div>
                <button type="button" onclick="signInButton()" class="btn btn-primary py-3 w-100 mb-4">Sign In</button>
            </div>
            <a class="text-center text-primary" href="#">Lupa Password</a>
            <a class="text-center text-primary" href="#">Download Manual Book</a>
        </div>
    </div>
</div>`

    $(document).ready(function () {
        content.html(loading);
        $.ajax({
            type: 'get',
            url: `${endpoint}/profile`,
            cache: false,
            processData: false,
            contentType: false,
            headers: {
                "Authorization": `Bearer ${token}`,
            },
            success: function (result) {
                window.location.href = "/dashboard";
            },
            error: function (err) {
                content.html(contentLoad);
            }
        });
    });


})(jQuery);

function signInButton() {

    $.ajax({
        type: 'POST',
        url: `${endpoint}/auth`,
        cache: false,
        processData: false,
        contentType: false,
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        data: JSON.stringify({
            'username': $("#floatingInput").val(),
            'password': $("#floatingPassword").val()
        }),
        success: function (result) {
            console.log(result);
            $("#password-validation").html(``);
            $("#email-validation").html(``);
            $("#username-password-validation").html(``);
            localStorage.setItem('token', result.token);
            window.location.href = "/dashboard";
        },
        error: function (err) {
            var status = err.status;
            if (status == 422) {
                var statusResponse = err.responseJSON.errors;
                $("#username-password-validation").html(``)
                if (statusResponse.username !== undefined) {
                    $("#email-validation").html(`${statusResponse.username}`);
                } else {
                    $("#email-validation").html(``);
                }

                if (statusResponse.password !== undefined) {
                    $("#password-validation").html(`${statusResponse.password}`);
                } else {
                    $("#password-validation").html(``);
                }
            }

            if (status === 401) {
                $("#password-validation").html(``);
                $("#email-validation").html(``);
                $("#username-password-validation").html(`${err.responseJSON.message}`)
                console.log(err.responseJSON)
            }
        }
    });
}