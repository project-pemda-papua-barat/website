<?php

namespace App\Models;

use DateTime;
use DateTimeInterface;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\Facades\DataTables;

class StockIn extends Model
{
    use HasFactory;

    protected $table = 'stock_ins';

    protected $fillable = [
        'tahun_pengadaan',
        'no_transaksi',
        'no_berita_acara',
        'no_dokumen_pengiriman',
        'tanggal_dokumen',
        'jenis_id',
        'distributor_id',
        'sumber_dana_id',
        'dokumen_berita_acara',
        'dokumen_expedisi',
        'dokumen_pendukung',
        'is_status',
        'keterangan',
        'created_by',
        'updated_by',
        'user_id',
        'unit_id'
    ];


    function onDetailHandTrx($no_trx, $unit_id)
    {
        $data = StockIn::join("distributor_models", function ($join) {
            $join->on('stock_ins.distributor_id', '=', 'distributor_models.id');
        })->join("sumber_dana_models", function ($join) {
            $join->on('stock_ins.sumber_dana_id', '=', 'sumber_dana_models.id');
        })->join("jenis_program_masuk_models", function ($join) {
            $join->on('stock_ins.jenis_id', '=', 'jenis_program_masuk_models.id');
        })->join("unit_models", function ($join) {
            $join->on('stock_ins.unit_id', '=', 'unit_models.id');
        })->join("users", function ($join) {
            $join->on('stock_ins.user_id', '=', 'users.id');
        })
            ->whereUnitId($unit_id)
            ->whereNoTransaksi($no_trx)
            ->select(
                "stock_ins.id",
                "stock_ins.no_transaksi",
                'stock_ins.tahun_pengadaan',
                "stock_ins.no_berita_acara",
                "stock_ins.no_dokumen_pengiriman",
                "stock_ins.is_status",
                "stock_ins.dokumen_berita_acara",
                "stock_ins.dokumen_expedisi",
                "stock_ins.dokumen_pendukung",
                "distributor_models.nama_distributor",
                "sumber_dana_models.nama_sumber_dana",
                "jenis_program_masuk_models.nama_program",
                "unit_models.nama_unit",
                "users.name as petugas",
                "stock_ins.keterangan",
                "stock_ins.created_at"
            )->get();
        $arr = [];
        foreach ($data as $d) {
            array_push($arr, [
                'id' => $d->id,
                'no_transaksi' => $d->no_transaksi,
                'tahun_pengadaan' => $d->tahun_pengadaan,
                "no_berita_acara" => $d->no_berita_acara,
                "no_dokumen_pengiriman" => $d->no_dokumen_pengiriman,
                "is_status" => $d->is_status,
                "dokumen_berita_acara" => asset('upload/aset_dokumen/' . $d->dokumen_berita_acara),
                "dokumen_expedisi" => asset('upload/aset_dokumen/' . $d->dokumen_expedisi),
                "dokumen_pendukung" => asset('upload/aset_dokumen/' . $d->dokumen_pendukung),
                "nama_distributor" =>  $d->nama_distributor,
                "nama_sumber_dana" =>  $d->nama_sumber_dana,
                "nama_program" => $d->nama_program,
                "nama_unit" => $d->nama_unit,
                "petugas" => $d->petugas,
                "keterangan" => $d->keterangan,
                "created_at" => $d->created_at->format('Y-m-d H:i:s')
            ]);
        }
        return $arr;
    }

    function onDetailHandTrxAll($profile)
    {
        $data = StockIn::join("distributor_models", function ($join) {
            $join->on('stock_ins.distributor_id', '=', 'distributor_models.id');
        })->join("sumber_dana_models", function ($join) {
            $join->on('stock_ins.sumber_dana_id', '=', 'sumber_dana_models.id');
        })->join("jenis_program_masuk_models", function ($join) {
            $join->on('stock_ins.jenis_id', '=', 'jenis_program_masuk_models.id');
        })->join("unit_models", function ($join) {
            $join->on('stock_ins.unit_id', '=', 'unit_models.id');
        })->join("users", function ($join) {
            $join->on('stock_ins.user_id', '=', 'users.id');
        })->whereUnitId($profile)
            ->select(
                "stock_ins.id",
                "stock_ins.no_transaksi",
                'stock_ins.tahun_pengadaan',
                "stock_ins.no_berita_acara",
                "stock_ins.no_dokumen_pengiriman",
                "stock_ins.is_status",
                "stock_ins.dokumen_berita_acara",
                "stock_ins.dokumen_expedisi",
                "stock_ins.dokumen_pendukung",
                "distributor_models.nama_distributor",
                "sumber_dana_models.nama_sumber_dana",
                "jenis_program_masuk_models.nama_program",
                "unit_models.nama_unit",
                "unit_models.id as unit_id",
                "users.name as petugas",
                "stock_ins.keterangan",
                "stock_ins.created_at"
            )->get();

        $arr = [];
        foreach ($data as $d) {
            array_push($arr, [
                'id' => $d->id,
                'no_transaksi' => $d->no_transaksi,
                'tahun_pengadaan' => $d->tahun_pengadaan,
                "no_berita_acara" => $d->no_berita_acara,
                "no_dokumen_pengiriman" => $d->no_dokumen_pengiriman,
                "is_status" => $d->is_status,
                "dokumen_berita_acara" => asset('upload/aset_dokumen/' . $d->dokumen_berita_acara),
                "dokumen_expedisi" => asset('upload/aset_dokumen/' . $d->dokumen_expedisi),
                "dokumen_pendukung" => asset('upload/aset_dokumen/' . $d->dokumen_pendukung),
                "nama_distributor" =>  $d->nama_distributor,
                "nama_sumber_dana" =>  $d->nama_sumber_dana,
                "nama_program" => $d->nama_program,
                "nama_unit" => $d->nama_unit,
                "unit_id" => $d->unit_id,
                "petugas" => $d->petugas,
                "keterangan" => $d->keterangan,
                "created_at" => $d->created_at->format('Y-m-d H:i:s'),
                "stock_in_detail" => $this->stockInDetail($d->id)
            ]);
        }

        return Datatables::of($arr)
            ->addIndexColumn()
            ->make(true);
    }

    function stockInDetail($data)
    {
        $subdata = StockInDetail::whereStockInId($data)->get();
        $total = 0;
        foreach ($subdata as $sub) {
            $total += ($sub->harga_satuan * $sub->qty);
        }
        $subDatas = [
            'total' => $subdata->count(),
            'total_harga_perolehan' => $total,
        ];
        return $subDatas;
    }

    function withDetail($id)
    {
        $subdata =  StockInDetail::join("detail_asets", function ($join) {
            $join->on('stock_in_details.detail_aset_id', '=', 'detail_asets.id');
        })->join("aset_v_i_i_s", function ($join) {
            $join->on('detail_asets.aset_id', '=', 'aset_v_i_i_s.id');
        })->join("kemasan_models", function ($join) {
            $join->on('stock_in_details.kemasan_id', '=', 'kemasan_models.id');
        })->join("satuan_models", function ($join) {
            $join->on('stock_in_details.satuan_id', '=', 'satuan_models.id');
        })->join("penyedia_models", function ($join) {
            $join->on('stock_in_details.penyedia_id', '=', 'penyedia_models.id');
        })->join("unit_models", function ($join) {
            $join->on('stock_in_details.unit_id', '=', 'unit_models.id');
        })->whereStockInId($id)->select(
            'stock_in_details.id',
            DB::raw('CONCAT(aset_v_i_i_s.asetI_id, aset_v_i_i_s.asetII_id, aset_v_i_i_s.asetIII_id, aset_v_i_i_s.asetIV_id, aset_v_i_i_s.asetV_id, aset_v_i_i_s.asetVI_id, aset_v_i_i_s.kode_aset_vii, "-", aset_v_i_i_s.data_id) AS nomor_aset'),
            "detail_asets.nama_detail_aset",
            "stock_in_details.kode as kodefikasi",
            'satuan_models.nama_satuan',
            'kemasan_models.nama_kemasan',
            "penyedia_models.nama_penyedia",
            "unit_models.nama_unit",
            "stock_in_details.qty",
            "stock_in_details.batch",
            "stock_in_details.expired_date",
            "stock_in_details.harga_satuan",
            "stock_in_details.created_at",
            "stock_in_details.updated_at"
        )->get();
        $subArr = [];
        foreach ($subdata as $d) {
            array_push($subArr, [
                'id' => $d->id,
                'nomor_aset' => $d->nomor_aset,
                "nama_detail_aset" => $d->nama_detail_aset,
                "kodefikasi" => $d->kodefikasi,
                "nama_kemasan" => $d->nama_kemasan,
                "nama_penyedia" =>  $d->nama_penyedia,
                "nama_unit" => $d->nama_unit,
                "expired_date" => $d->expired_date,
                "batch" => $d->batch,
                "qty" =>  $d->qty,
                "nama_satuan" => $d->nama_satuan,
                "harga_satuan" => $d->harga_satuan,
                "total" => $d->harga_satuan * $d->qty,
                "created_at" => $d->created_at->format('Y-m-d H:i:s')
            ]);
        }
        $data = StockIn::join("distributor_models", function ($join) {
            $join->on('stock_ins.distributor_id', '=', 'distributor_models.id');
        })->join("sumber_dana_models", function ($join) {
            $join->on('stock_ins.sumber_dana_id', '=', 'sumber_dana_models.id');
        })->join("jenis_program_masuk_models", function ($join) {
            $join->on('stock_ins.jenis_id', '=', 'jenis_program_masuk_models.id');
        })->join("unit_models", function ($join) {
            $join->on('stock_ins.unit_id', '=', 'unit_models.id');
        })->join("users", function ($join) {
            $join->on('stock_ins.user_id', '=', 'users.id');
        })->where('stock_ins.id', '=', $id)
            ->select(
                "stock_ins.id",
                "stock_ins.no_transaksi",
                'stock_ins.tahun_pengadaan',
                "stock_ins.no_berita_acara",
                "stock_ins.no_dokumen_pengiriman",
                "distributor_models.nama_distributor",
                "sumber_dana_models.nama_sumber_dana",
                "jenis_program_masuk_models.nama_program",
                "unit_models.nama_unit",
                "unit_models.address as alamat_unit",
                "users.name as petugas",
                "stock_ins.keterangan",
                "stock_ins.created_at"
            )->get();

        $arr = [];
        foreach ($data as $d) {
            array_push($arr, [
                'id' => $d->id,
                'no_transaksi' => $d->no_transaksi,
                'tahun_pengadaan' => $d->tahun_pengadaan,
                "no_berita_acara" => $d->no_berita_acara,
                "no_dokumen_pengiriman" => $d->no_dokumen_pengiriman,
                "is_status" => $d->is_status,
                "nama_distributor" =>  $d->nama_distributor,
                "nama_sumber_dana" =>  $d->nama_sumber_dana,
                "nama_program" => $d->nama_program,
                "nama_unit" => $d->nama_unit,
                "alamat_unit" => $d->alamat_unit,
                "petugas" => $d->petugas,
                "keterangan" => $d->keterangan,
                "created_at" => $d->created_at->format('d M Y'),
                "stock_in_detail" => $subArr,
                "total_harga" => $this->stockInDetail($d->id)['total_harga_perolehan']
            ]);
        }
        return $arr[0];
    }

    function aset()
    {
        return $this->hasOne(DetailAset::class, 'id', 'detail_aset_id');
    }

    function jenis()
    {
        return $this->hasOne(JenisProgramMasukModel::class, 'id', 'jenis_id');
    }

    function satuan()
    {
        return $this->hasOne(SatuanModel::class, 'id', 'satuan_id');
    }

    function unit()
    {
        return $this->hasOne(UnitModel::class, 'id', 'unit_id');
    }

    function petugas()
    {
        return $this->hasOne(User::class, 'id',  'user_id');
    }

    function sumberDana()
    {
        return $this->hasOne(SumberDanaModel::class, 'id', 'sumber_dana_id');
    }

    function penyedia()
    {
        return $this->hasOne(PenyediaModel::class, 'id', 'penyedia_id');
    }

    function updateBy()
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }

    function createdBy()
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }
}
