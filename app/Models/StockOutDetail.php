<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\Facades\DataTables;

class StockOutDetail extends Model
{
    use HasFactory;

    protected $fillable = [
        'stock_out_id',
        'stocked_center_id',
        'qty_out',
        'is_status',
        'unique_id'
    ];

    function detail(){
        return $this->getDataDetailStock();
    }

    function StockOutDetailByTrx($idStockOut)
    {
        $data = StockOutDetail::join('stocked_centers', function($join){
            $join->on('stock_out_details.stocked_center_id', '=', 'stocked_centers.id');
        })->join('detail_asets', function($join){
            $join->on('stocked_centers.detail_asets_id', 'detail_asets.id');
        })->join("aset_v_i_i_s", function ($join) {
            $join->on('detail_asets.aset_id', '=', 'aset_v_i_i_s.id');
        })->join("satuan_models", function ($join) {
            $join->on('stocked_centers.satuan_id', '=', 'satuan_models.id');
        })->join("penyedia_models", function ($join) {
            $join->on('stocked_centers.penyedia_id', '=', 'penyedia_models.id');
        })->join("jenis_program_masuk_models", function ($join) {
            $join->on('stocked_centers.jenis_id', '=', 'jenis_program_masuk_models.id');
        })->join("sumber_dana_models", function ($join) {
            $join->on('stocked_centers.sumber_dana_id', '=', 'sumber_dana_models.id');
        })->join("distributor_models", function ($join) {
            $join->on('stocked_centers.distributor_id', '=', 'distributor_models.id');
        })
        ->select(
            'stock_out_details.id',
            'detail_asets.nama_detail_aset',
            DB::raw("CONCAT(aset_v_i_i_s.asetI_id,aset_v_i_i_s.asetII_id,aset_v_i_i_s.asetIII_id,aset_v_i_i_s.asetIV_id,aset_v_i_i_s.asetV_id,aset_v_i_i_s.asetVI_id,aset_v_i_i_s.kode_aset_vii, ' - ', aset_v_i_i_s.data_id) as nomor_aset"),
            "penyedia_models.nama_penyedia",
            'jenis_program_masuk_models.nama_program',
            "sumber_dana_models.nama_sumber_dana",
            "distributor_models.nama_distributor",
            'stocked_centers.kode',
            'stocked_centers.qty',
            "stock_out_details.qty_out",
            'satuan_models.nama_satuan',
            'stock_out_details.created_at',
            'stock_out_details.updated_at',
        )
        ->where('stock_out_details.stock_out_id', '=', $idStockOut)
        ->get();

        $arr = [];
        foreach ($data as $d) {
            array_push($arr, [
                'id' => $d->id,
                'nama_detail_aset' => $d->nama_detail_aset,
                'nomor_aset' => $d->nomor_aset,
                "nama_penyedia" => $d->nama_penyedia,
                'nama_program' => $d->nama_program,
                "sumber_dana" => $d->nama_sumberdsana,
                "nama_distributor" => $d->nama_distributor,
                'kode' => $d->kode,
                'qty' => $d->qty,
                "qty_out" => $d->qty_out,
                'satuan' => $d->nama_satuan,
                "created_at" => $d->created_at->format('Y-m-d H:i:s'),
                "updated_at" => $d->updated_at->format('Y-m-d H:i:s'),
            ]);
        }
        return DataTables::of($arr)
            ->addIndexColumn()
            ->addColumn('action', function ($arr) {
                return '<button type="button" onclick="editData(' . $arr['id'] . ')" class="text-warning"><i class="fa fa-edit" aria-hidden="true"></i> </button><button type="button" onclick="deleteData(' . $arr['id'] . ')" class="text-danger"><i class="fa fa-trash" aria-hidden="true"></i></button>';
            })
            ->make(true);

    }



    protected function getDataDetailStock()
    {
        $data = StockOutDetail::join('stocked_centers', function($join){
            $join->on('stock_out_details.stocked_center_id', '=', 'stocked_centers.id');
        })
        ->select(
            'stock_out_details.id',
            'stocked_centers.*'
        )
        ->get();
        return $data;
    }

}
