<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\Facades\DataTables;

class StockOut extends Model
{
    use HasFactory;

    protected $table = 'stock_outs';

    protected $fillable = [
        'no_transaksi',
        'no_berita_acara',
        'no_dokumen_pengiriman',
        'tanggal_dokumen',
        'distributor_id',
        'dokumen_berita_acara',
        'dokumen_expedisi',
        'dokumen_pendukung',
        'is_status',
        'keterangan',
        'created_by',
        'updated_by',
        'user_id',
        'from_unit_id',
        'to_unit_id'
    ];

    function withDetailStockOutId($id)
    {        
        $data = StockOut::join("unit_models", function ($join) {
            $join->on('stock_outs.to_unit_id', '=', 'unit_models.id');
        })->join("distributor_models", function ($join) {
            $join->on('stock_outs.distributor_id', '=', 'distributor_models.id');
        })->where('stock_outs.id', '=', $id)
        ->select(
            'stock_outs.id',
            'stock_outs.no_transaksi',
            'stock_outs.no_berita_acara',
            'stock_outs.no_dokumen_pengiriman',
            'stock_outs.tanggal_dokumen',
            'distributor_models.nama_distributor',
            'unit_models.nama_unit as unit_tujuan',
            'unit_models.address as alamat_unit_tujuan'
        )->get();
        $arr = [];
        foreach($data as $d){
            array_push($arr, [
                'id' => $d->id,
                'no_transaksi' => $d->no_transaksi,
                'no_dokumen_pengiriman' => $d->no_dokumen_pengiriman,
                'tanggal_dokumen' => $d->tanggal_dokumen,
                'nama_distributor' => $d->nama_distributor,
                'unit_tujuan' => $d->unit_tujuan,
                'alamat_unit_tujuan' => $d->alamat_unit_tujuan,
                'total_harga' => $this->getTotal($id),
                'detail' => $this->getStockoutDetail($id)
            ]);
        }
        return $arr[0];
    }

    public function getStockoutDetail($id)
    {
        $data = StockOutDetail::join('stocked_centers', function($join){
            $join->on('stock_out_details.stocked_center_id', '=', 'stocked_centers.id');
        })->join('detail_asets', function($join){
            $join->on('stocked_centers.detail_asets_id', '=', 'detail_asets.id');
        })->join('aset_v_i_i_s', function($join){
            $join->on('detail_asets.aset_id', '=', 'aset_v_i_i_s.id');
        })->join('jenis_program_masuk_models', function ($join) {
            $join->on('stocked_centers.jenis_id', '=', 'jenis_program_masuk_models.id');
        })->join('satuan_models', function ($join) {
            $join->on('stocked_centers.satuan_id', '=', 'satuan_models.id');
        })->join("penyedia_models", function ($join) {
            $join->on('stocked_centers.penyedia_id', '=', 'penyedia_models.id');
        })->join("sumber_dana_models", function ($join) {
            $join->on('stocked_centers.sumber_dana_id', '=', 'sumber_dana_models.id');
        })->select(
            'stock_out_details.id',
            'stock_out_details.qty_out',
            'stocked_centers.harga_satuan',
            DB::raw("stock_out_details.qty_out * stocked_centers.harga_satuan as total"),
            DB::raw("CONCAT(aset_v_i_i_s.asetI_id,aset_v_i_i_s.asetII_id,aset_v_i_i_s.asetIII_id,aset_v_i_i_s.asetIV_id,aset_v_i_i_s.asetV_id,aset_v_i_i_s.asetVI_id,aset_v_i_i_s.kode_aset_vii, ' - ', aset_v_i_i_s.data_id) as nomor_aset"),
            'aset_v_i_i_s.nama_aset_vii as nama_aset',
            'detail_asets.nama_detail_aset',
            'jenis_program_masuk_models.nama_program',
            'satuan_models.kode_satuan',
            'penyedia_models.nama_penyedia',
            'sumber_dana_models.nama_sumber_dana'
        )->whereStockOutId($id)->get();
        $arr = [];
        foreach($data as $d){
            array_push($arr, [
                'id' => $d->id,
                'qty_out' => $d->qty_out,
                'harga_satuan' => $d->harga_satuan,
                'total' => $d->total,
                'nomor_aset' => $d->nomor_aset,
                'nama_aset' => $d->nama_aset,
                'nama_detail_aset' => $d->nama_detail_aset,
                'nama_program' => $d->nama_program,
                'kode_satuan' => $d->kode_satuan,
                'nama_penyedia' => $d->nama_penyedia,
                'nama_sumber_dana' => $d->nama_sumber_dana,
                'total'
            ]);
        }
        return $data;
    }

    function getTotal($id)
    {
        $total = 0;
        $subdata = StockOutDetail::join('stocked_centers', function($join){
            $join->on('stock_out_details.stocked_center_id', '=', 'stocked_centers.id');
        })->select('stock_out_details.id', 'stock_out_details.qty_out', 'stocked_centers.harga_satuan')->where('stock_out_details.stock_out_id', '=', $id)->get();
        foreach ($subdata as $sub) {
            $total += ($sub->harga_satuan * $sub->qty_out);
        }
        $subDatas = [
            'total_harga_perolehan' => $total,
        ];
        return $subDatas['total_harga_perolehan'];
    }

    function outHandTrxUnit($unit)
    {
        $data = StockOut::join('distributor_models', function ($join) {
            $join->on('stock_outs.distributor_id', '=', 'distributor_models.id');
        })->join('unit_models', function ($join) {
            $join->on('stock_outs.to_unit_id', '=', 'unit_models.id');
        })->join('users', function ($join) {
            $join->on('stock_outs.user_id', '=', 'users.id');
        })->whereFromUnitId($unit)
            ->select(
                'stock_outs.id',
                'stock_outs.no_transaksi',
                'stock_outs.no_berita_acara',
                'stock_outs.no_dokumen_pengiriman',
                'stock_outs.tanggal_dokumen',
                'stock_outs.dokumen_berita_acara',
                'stock_outs.dokumen_expedisi',
                'stock_outs.dokumen_pendukung',
                'stock_outs.is_status',
                'stock_outs.keterangan',
                'distributor_models.nama_distributor',
                'unit_models.nama_unit AS unit_tujuan',
                'unit_models.address AS alamat_unit',
                'users.name as petugas',
                'stock_outs.created_at',
                'stock_outs.updated_at'
            )->get();
        $arr = [];
        foreach ($data as $d) {
            array_push($arr, [
                'id' => $d->id,
                'no_transaksi' => $d->no_transaksi,
                'no_berita_acara' => $d->no_berita_acara,
                'no_dokumen_pengiriman' => $d->no_dokumen_pengiriman,
                'tanggal_dokumen' => $d->tanggal_dokumen,
                'dokumen_berita_acara' =>  asset('upload/aset_dokumen/' . $d->dokumen_berita_acara),
                'dokumen_expedisi' =>  asset('upload/aset_dokumen/' . $d->dokumen_expedisi),
                'dokumen_pendukung' =>  asset('upload/aset_dokumen/' . $d->dokumen_pendukung),
                'is_status' => $d->is_status,
                'keterangan' => $d->keterangan,
                'nama_distributor' => $d->nama_distributor,
                'unit_tujuan' => $d->unit_tujuan,
                'unit_asal' => $unit,
                'alamat_unit' =>  $d->alamat_unit,
                'petugas' => $d->petugas,
                'created_at' => $d->created_at->format('Y-m-d H:i:s'),
                'updated_at' => $d->updated_at->format('Y-m-d H:i:s')
            ]);
        }
        return DataTables::of($arr)
            ->addIndexColumn()
            ->make(true);
    }


    function outDetailHandTrxAll($profile, $no_trx)
    {
        $data = StockOut::join('distributor_models', function ($join) {
            $join->on('stock_outs.distributor_id', '=', 'distributor_models.id');
        })->join('users', function ($join) {
            $join->on('stock_outs.user_id', '=', 'users.id');
        })->join('unit_models', function ($join) {
            $join->on('stock_outs.to_unit_id', '=', 'unit_models.id');
        })
            ->whereNoTransaksi($no_trx)
            ->whereFromUnitId($profile)
            ->select(
                'stock_outs.id',
                'stock_outs.no_transaksi',
                'stock_outs.no_berita_acara',
                'stock_outs.no_dokumen_pengiriman',
                'stock_outs.tanggal_dokumen',
                'stock_outs.dokumen_berita_acara',
                'stock_outs.dokumen_expedisi',
                'stock_outs.dokumen_pendukung',
                'stock_outs.is_status',
                'stock_outs.keterangan',
                'distributor_models.nama_distributor',
                'unit_models.nama_unit as unit_tujuan',
                'users.name as created_by',
                'stock_outs.created_at',
                'stock_outs.updated_at'
            )->get();
            $arr = [];
            foreach ($data as $d) {
                array_push($arr, [
                    'id' => $d->id,
                    'no_transaksi' => $d->no_transaksi,
                    'no_berita_acara' => $d->no_berita_acara,
                    'no_dokumen_pengiriman' => $d->no_dokumen_pengiriman,
                    'tanggal_dokumen' => $d->tanggal_dokumen,
                    'dokumen_berita_acara' =>  asset('upload/aset_dokumen/' . $d->dokumen_berita_acara),
                    'dokumen_expedisi' =>  asset('upload/aset_dokumen/' . $d->dokumen_expedisi),
                    'dokumen_pendukung' =>  asset('upload/aset_dokumen/' . $d->dokumen_pendukung),
                    'is_status' => $d->is_status,
                    'keterangan' => $d->keterangan,
                    'nama_distributor' => $d->nama_distributor,
                    'unit_tujuan' => $d->unit_tujuan,
                    'alamat_unit' =>  $d->alamat_unit,
                    'petugas' => $d->created_by,
                    'created_at' => $d->created_at->format('Y-m-d H:i:s'),
                    'updated_at' => $d->updated_at->format('Y-m-d H:i:s')
                ]);
            }
        return $arr;
    }
}