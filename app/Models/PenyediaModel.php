<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PenyediaModel extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = [
        'nama_penyedia',
        'alamat_penyedia',
        'no_contact_penyedia',
        'is_active',
        'detail_alamat',
        'lat',
        'long',
        'updated_by',
        'created_by',
        'keterangan'
    ];
}
