<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AsetII extends Model
{
    use HasFactory;

    protected $fillable = ['asetI_id', 'kode_aset_ii', 'nama_aset_ii'];
}
