<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PenandaTanganModel extends Model
{
    use HasFactory;

    protected $fillable = [ 'unit_id' , 'nama_penanda_tangan', 'nip' , 'jabatan' ,'created_by' ,'updated_by' ,'selected', 'unique_id'];
}
