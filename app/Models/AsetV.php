<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AsetV extends Model
{
    use HasFactory;

    protected $fillable = ['asetI_id', 'asetII_id', 'asetIII_id', 'asetIV_id', 'kode_aset_v', 'nama_aset_v'];
}
