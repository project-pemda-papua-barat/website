<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UnitModel extends Model
{
    use HasFactory;

    protected $fillable = ['nama_unit', 'area_id', 'address', 'detail_address', 'lat', 'long', 'created_by', 'updated_by', 'is_active'];

    function area()
    {
        return $this->belongsTo(AreaModel::class);
    }

    function region()
    {
        return $this->belongsTo(RegionModel::class);
    }
}
