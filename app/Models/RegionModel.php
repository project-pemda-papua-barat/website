<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class RegionModel extends Model
{
    use HasFactory;

    protected $table = 'tbl_kodepos';


    function getRegion($provinsi)
    {
        return DB::select("CALL daerah(?)", [$provinsi]);
    }
}
