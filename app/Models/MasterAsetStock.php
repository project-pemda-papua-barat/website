<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;

class MasterAsetStock extends Model
{
    use HasFactory;

    protected $fillable = [
        'detail_asets_id',
        'jenis_id',
        'satuan_id',
        'penyedia_id',
        'sumber_dana_id',
        'distributor_id',
        'from_unit_id',
        'to_unit_id',
        'user_id',
        'no_transaksi',
        'kode',
        'batch',
        'expired_date',
        'harga_satuan',
        'tahun_pengadaan',
        'created_by',
        'updated_by',
        'qty'
    ];

    function unitName($id_form)
    {
        $unit =  UnitModel::find($id_form);
        return $unit->nama_unit;
    }

    function user($id_user)
    {
        $name = User::find($id_user);
        return $name->name;
    }



    function dataStockQueryUnit($start_date, $endDate, $unit_id)
    {
        $data = MasterAsetStock::join('detail_asets', function ($join) {
            $join->on('master_aset_stocks.detail_asets_id', '=', 'detail_asets.id');
        })->join("aset_v_i_i_s", function ($join) {
            $join->on('detail_asets.aset_id', '=', 'aset_v_i_i_s.id');
        })->join("penyedia_models", function ($join) {
            $join->on('master_aset_stocks.penyedia_id', '=', 'penyedia_models.id');
        })->join("satuan_models", function ($join) {
            $join->on('master_aset_stocks.satuan_id', '=', 'satuan_models.id');
        })->join("jenis_program_masuk_models", function ($join) {
            $join->on('master_aset_stocks.jenis_id', '=', 'jenis_program_masuk_models.id');
        })->join("sumber_dana_models", function ($join) {
            $join->on('master_aset_stocks.sumber_dana_id', '=', 'sumber_dana_models.id');
        })->join("distributor_models", function ($join) {
            $join->on('master_aset_stocks.distributor_id', '=', 'distributor_models.id');
        })->join("users", function ($join) {
            $join->on('master_aset_stocks.user_id', '=', 'users.id');
        })->whereDate('master_aset_stocks.created_at', '>=', $start_date)->whereDate('master_aset_stocks.created_at', '<=', $endDate)->whereFromUnitId($unit_id)->select(
            'master_aset_stocks.id',
            'detail_asets.nama_detail_aset',
            DB::raw('CONCAT(aset_v_i_i_s.asetI_id, aset_v_i_i_s.asetII_id, aset_v_i_i_s.asetIII_id, aset_v_i_i_s.asetIV_id, aset_v_i_i_s.asetV_id, aset_v_i_i_s.asetVI_id, aset_v_i_i_s.kode_aset_vii, " - ", aset_v_i_i_s.data_id) AS nomor_aset'),
            'detail_asets.keterangan',
            'master_aset_stocks.kode',
            'satuan_models.nama_satuan',
            'penyedia_models.nama_penyedia',
            "jenis_program_masuk_models.nama_program",
            "sumber_dana_models.nama_sumber_dana",
            "distributor_models.nama_distributor",
            "users.name as approved_by",
            "master_aset_stocks.from_unit_id",
            "master_aset_stocks.to_unit_id",
            "master_aset_stocks.no_transaksi",
            "master_aset_stocks.kode",
            "master_aset_stocks.batch",
            "master_aset_stocks.expired_date",
            "master_aset_stocks.harga_satuan",
            "master_aset_stocks.tahun_pengadaan",
            "master_aset_stocks.created_at",
            "master_aset_stocks.qty"
        )->get();
        $arr = [];
        foreach ($data as $d) {
            array_push($arr, [
                'nama_detail_aset' => $d->nama_detail_aset,
                'nomor_aset' => $d->nomor_aset,
                'keterangan' => $d->keterangan,
                'kode' => $d->kode,
                'nama_satuan' => $d->nama_satuan,
                'nama_penyedia' => $d->nama_penyedia,
                'nama_program' => $d->nama_program,
                'nama_sumber_dana' => $d->nama_sumber_dana,
                'nama_distributor' => $d->nama_distributor,
                'approved_by' => $d->approved_by,
                'from_unit' => $this->unitName($d->from_unit_id),
                'to_unit' => $this->unitName($d->to_unit_id),
                'no_transaksi' => $d->no_transaksi,
                'kode' => $d->kode,
                'batch' => $d->batch,
                'expired_date' => $d->expired_date,
                'harga_satuan' => $d->harga_satuan,
                'tahun_pengadaan' => $d->tahun_pengadaan,
                'created_at' => $d->created_at->format('Y-m-d H:i:s'),
                'total' => $d->qty * $d->harga_satuan,
                'qty' => $d->qty,
            ]);
        }
        return $arr;
    }

    function dataStockUnit($profile)
    {
        $data = MasterAsetStock::join('detail_asets', function ($join) {
            $join->on('master_aset_stocks.detail_asets_id', '=', 'detail_asets.id');
        })->join("aset_v_i_i_s", function ($join) {
            $join->on('detail_asets.aset_id', '=', 'aset_v_i_i_s.id');
        })->join("penyedia_models", function ($join) {
            $join->on('master_aset_stocks.penyedia_id', '=', 'penyedia_models.id');
        })->join("satuan_models", function ($join) {
            $join->on('master_aset_stocks.satuan_id', '=', 'satuan_models.id');
        })->join("jenis_program_masuk_models", function ($join) {
            $join->on('master_aset_stocks.jenis_id', '=', 'jenis_program_masuk_models.id');
        })->join("sumber_dana_models", function ($join) {
            $join->on('master_aset_stocks.sumber_dana_id', '=', 'sumber_dana_models.id');
        })->join("distributor_models", function ($join) {
            $join->on('master_aset_stocks.distributor_id', '=', 'distributor_models.id');
        })->join("users", function ($join) {
            $join->on('master_aset_stocks.user_id', '=', 'users.id');
        })->whereToUnitId($profile)->select(
            'master_aset_stocks.id',
            'detail_asets.nama_detail_aset',
            DB::raw('CONCAT(aset_v_i_i_s.asetI_id, aset_v_i_i_s.asetII_id, aset_v_i_i_s.asetIII_id, aset_v_i_i_s.asetIV_id, aset_v_i_i_s.asetV_id, aset_v_i_i_s.asetVI_id, aset_v_i_i_s.kode_aset_vii, " - ", aset_v_i_i_s.data_id) AS nomor_aset'),
            'detail_asets.keterangan',
            'master_aset_stocks.kode',
            'satuan_models.nama_satuan',
            'penyedia_models.nama_penyedia',
            "jenis_program_masuk_models.nama_program",
            "sumber_dana_models.nama_sumber_dana",
            "distributor_models.nama_distributor",
            "users.name as approved_by",
            "master_aset_stocks.from_unit_id",
            "master_aset_stocks.to_unit_id",
            "master_aset_stocks.no_transaksi",
            "master_aset_stocks.kode",
            "master_aset_stocks.batch",
            "master_aset_stocks.expired_date",
            "master_aset_stocks.harga_satuan",
            "master_aset_stocks.tahun_pengadaan",
            "master_aset_stocks.qty"
        )->get();
        $arr = [];
        foreach ($data as $d) {
            array_push($arr, [
                'nama_detail_aset' => $d->nama_detail_aset,
                'nomor_aset' => $d->nomor_aset,
                'keterangan' => $d->keterangan,
                'kode' => $d->kode,
                'nama_satuan' => $d->nama_satuan,
                'nama_penyedia' => $d->nama_penyedia,
                'nama_program' => $d->nama_program,
                'nama_sumber_dana' => $d->nama_sumber_dana,
                'nama_distributor' => $d->nama_distributor,
                'approved_by' => $d->approved_by,
                'from_unit' => $this->unitName($d->from_unit_id),
                'to_unit' => $this->unitName($d->to_unit_id),
                'no_transaksi' => $d->no_transaksi,
                'kode' => $d->kode,
                'batch' => $d->batch,
                'expired_date' => $d->expired_date,
                'harga_satuan' => $d->harga_satuan,
                'tahun_pengadaan' => $d->tahun_pengadaan,
                'qty' => $d->qty,
            ]);
        }
        return $arr;
    }
}
