<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FirstDocument extends Model
{
    use HasFactory;


    protected $fillable = ['file_name_first_doc'];
}
