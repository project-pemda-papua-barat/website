<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AsetVI extends Model
{
    use HasFactory;

    protected $fillable = ['asetI_id', 'asetII_id', 'asetIII_id', 'asetIV_id', 'asetV_id', 'kode_aset_vi', 'nama_aset_vi'];
}
