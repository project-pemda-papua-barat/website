<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ThirdDocument extends Model
{
    use HasFactory;

    protected $fillable = ['file_name_third_doc'];
}
