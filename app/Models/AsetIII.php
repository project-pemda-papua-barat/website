<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AsetIII extends Model
{
    use HasFactory;

    protected $table = 'aset_i_i_i_s';

    protected $fillable = ['asetI_id', 'asetII_id', 'kode_aset_iii', 'nama_aset_iii'];
}
