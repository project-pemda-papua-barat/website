<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class KemasanModel extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = [
        'nama_kemasan',
        'keterangan',
        'is_active',
        'created_by',
        'updated_by'
    ];
}
