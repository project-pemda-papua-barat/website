<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AsetIV extends Model
{
    use HasFactory;

    protected $fillable = ['asetI_id', 'asetII_id', 'asetIII_id', 'kode_aset_iv', 'nama_aset_iv'];
}
