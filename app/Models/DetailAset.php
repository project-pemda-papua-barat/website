<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\Facades\DataTables;

class DetailAset extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = ['aset_id', 'nama_detail_aset', 'aset_image', 'keterangan', 'is_expired', 'created_by', 'updated_by'];

    function aset()
    {
        return $this->hasOne(AsetVII::class, 'id', 'aset_id');
    }


    function data()
    {
        $data = DetailAset::with('aset')->orderBy('updated_at', 'desc')->get();
        $datas = [];

        foreach($data as $d){
            $created_by = User::find($d->created_by);
            $updatedby = User::find($d->updated_by);
            $hargaPerolehan = HargaPerolehan::whereDetailAsetId($d->id)->limit(3)->orderBy('created_at', 'desc')->get();
            array_push($datas, [
                "id" =>$d->id,
                "aset_id" =>$d->aset_id,
                "nama_detail_aset" =>$d->nama_detail_aset,
                "is_expired" =>$d->is_expired,
                "aset_image" =>$d->aset_image == null ? asset('img/no_image.jpg') : asset('upload/aset_image/' . $d->aset_image),
                "keterangan" =>$d->keterangan,
                "created_by" =>$created_by->name,
                "updated_by" => $updatedby->name,
                "created_at" =>$d->created_at->format('Y-m-d H:i:s'),
                "updated_at" =>$d->updated_at->format('Y-m-d H:i:s'),
                "aset" =>$d->aset,
                'harga_perolehan' => $hargaPerolehan
            ]);
        }
        return DataTables::of($datas)->addColumn('action', function ($da) {
            $button = '<a href="#" onclick="editDetailAsetShow(' . $da['id'] . ')" class="text-info m-2"><i class="fa fa-edit" aria-hidden="true"></i></a><a  href="#" onclick="detailHargaPerolehan(' . $da['id'] . ')" class="text-warning m-2"><i class="fa fa-eye" aria-hidden="true"></i>
            </a>';
            return $button;
        })
            ->addIndexColumn()
            ->rawColumns(['action'])
            ->make(true);
        return $data;
    }

    function detail()
    {
        return DetailAset::join('aset_v_i_i_s', function ($join) {
            return $join->on('detail_asets.aset_id', '=', 'aset_v_i_i_s.id');
        })->select(
            "detail_asets.id",
            "detail_asets.nama_detail_aset",
            "detail_asets.keterangan",
            "detail_asets.aset_image",
            "detail_asets.is_expired",
            DB::raw("CONCAT(aset_v_i_i_s.asetI_id,aset_v_i_i_s.asetII_id,aset_v_i_i_s.asetIII_id,aset_v_i_i_s.asetIV_id,aset_v_i_i_s.asetV_id,aset_v_i_i_s.asetVI_id,aset_v_i_i_s.kode_aset_vii) as nomor_aset"),
            "aset_v_i_i_s.data_id"
        )->get();
    }

}
