<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class HargaPerolehan extends Model
{
    use HasFactory;

    protected $table = 'harga_perolehans';

    protected $fillable = ['detail_aset_id', 'tanggal', 'perubahan_harga', 'created_by', 'updated_by'];

    function createdBy()
    {
        return $this->hasOne(User::class, 'id', 'created_by');
    }
    function updatedBy()
    {
        return $this->hasOne(User::class, 'id', 'updated_by');
    }
}
