<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AsetVII extends Model
{
    use HasFactory;

    protected $fillable = ['asetI_id', 'asetII_id', 'asetIII_id', 'asetIV_id', 'asetV_id', 'asetVI_id', 'kode_aset_vii', 'nama_aset_vii', 'data_id'];

    function update_harga()
    {
        return $this->hasMany(HargaPerolehan::class, 'aset_id');
    }
}
