<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SumberDanaModel extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = 'sumber_dana_models';

    protected $fillable = [
        'nama_sumber_dana',
        'jenis_sumber_dana',
        'keterangan',
        'is_active',
        'created_by',
        'updated_by'
    ];
}
