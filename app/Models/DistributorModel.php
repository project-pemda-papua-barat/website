<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DistributorModel extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = [
        'nama_distributor',
        'alamat_distributor',
        'no_contact_distributor',
        'keterangan',
        'detail_alamat',
        'lat',
        'long',
        'created_by',
        'updated_by'
    ];
}
