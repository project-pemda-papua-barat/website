<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\Facades\DataTables;

class StockInDetail extends Model
{
    use HasFactory;

    protected $table = 'stock_in_details';

    protected $fillable = [
        'stock_in_id',
        'detail_aset_id',
        'satuan_id',
        'kemasan_id',
        'penyedia_id',
        'unit_id',
        'qty',
        'kode',
        'batch',
        'expired_date',
        'harga_satuan',
        'is_status',
    ];

    function stockinwithDetail($stockinid)
    {
        $data = StockInDetail::join("detail_asets", function ($join) {
            $join->on('stock_in_details.detail_aset_id', '=', 'detail_asets.id');
        })->join("aset_v_i_i_s", function ($join) {
            $join->on('detail_asets.aset_id', '=', 'aset_v_i_i_s.id');
        })->join("kemasan_models", function ($join) {
            $join->on('stock_in_details.kemasan_id', '=', 'kemasan_models.id');
        })->join("satuan_models", function ($join) {
            $join->on('stock_in_details.satuan_id', '=', 'satuan_models.id');
        })->join("penyedia_models", function ($join) {
            $join->on('stock_in_details.penyedia_id', '=', 'penyedia_models.id');
        })->join("unit_models", function ($join) {
            $join->on('stock_in_details.unit_id', '=', 'unit_models.id');
        })->whereStockInId($stockinid)->select(
            'stock_in_details.id',
            DB::raw('CONCAT(aset_v_i_i_s.asetI_id, aset_v_i_i_s.asetII_id, aset_v_i_i_s.asetIII_id, aset_v_i_i_s.asetIV_id, aset_v_i_i_s.asetV_id, aset_v_i_i_s.asetVI_id, aset_v_i_i_s.kode_aset_vii, "-", aset_v_i_i_s.data_id) AS nomor_aset'),
            "detail_asets.nama_detail_aset",
            "stock_in_details.kode as kode_aset",
            'satuan_models.nama_satuan',
            'kemasan_models.nama_kemasan',
            "penyedia_models.nama_penyedia",
            "unit_models.nama_unit",
            "stock_in_details.qty",
            "stock_in_details.kode",
            "stock_in_details.batch",
            "stock_in_details.expired_date",
            "stock_in_details.harga_satuan",
            "stock_in_details.is_status",
            "stock_in_details.created_at",
            "stock_in_details.updated_at"
        )->get();

        $arr = [];
        foreach ($data as $d) {
            array_push($arr, [
                'id' => $d->id,
                'nama_detail_aset' => $d->nama_detail_aset,
                'nomor_aset' => $d->nomor_aset,
                "kode_aset" => $d->kode_aset,
                "nama_detail_aset" => $d->nama_detail_aset,
                'nama_satuan' => $d->nama_satuan,
                'nama_kemasan' => $d->nama_kemasan,
                "nama_penyedia" => $d->nama_penyedia,
                "nama_unit" => $d->nama_unit,
                "qty" => $d->qty,
                "kode" =>  $d->kode,
                "batch" =>  $d->batch,
                "expired_date" => $d->expired_date,
                "harga_satuan" => $d->harga_satuan,
                "is_status" => $d->is_status,
                "created_at" => $d->created_at->format('Y-m-d H:i:s'),
                "updated_at" => $d->updated_at->format('Y-m-d H:i:s')
            ]);
        }
        return DataTables::of($arr)
            ->addIndexColumn()
            ->addColumn('action', function ($arr) {
                return '<button type="button" onclick="editData(' . $arr['id'] . ')" class="text-warning"><i class="fa fa-edit" aria-hidden="true"></i></button><button type="button" onclick="deleteData(' . $arr['id'] . ')" class="text-danger"><i class="fa fa-trash" aria-hidden="true"></i></button>';
            })
            ->make(true);
    }
}
