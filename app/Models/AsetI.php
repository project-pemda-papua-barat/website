<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AsetI extends Model
{
    use HasFactory;

    protected $table = 'aset_i_s';

    protected $fillable = ['kode_aset_i', 'nama_aset_i'];
}
