<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AreaModel extends Model
{
    use HasFactory;

    protected $fillable = ['nama_area', 'created_by', 'updated_by', 'is_active', 'keterangan'];
}
