<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class StockedCenter extends Model
{
    use HasFactory;

    protected $fillable = [
        'detail_asets_id',
        'jenis_id',
        'satuan_id',
        'penyedia_id',
        'sumber_dana_id',
        'distributor_id',
        'from_unit_id',
        'to_unit_id',
        'user_id',
        'no_transaksi',
        'kode',
        'batch',
        'expired_date',
        'harga_satuan',
        'tahun_pengadaan',
        'is_expired',
        'qty'
    ];

    function stockedCenterData($unit)
    {
        
        $data = $this->stockedCenterDataDB($unit);
        return $data;
    }

    function stockedCenterDataDetail($unit, $id)
    {
        
        $data = $this->stockedCenterDataDetailDB($unit, $id);
        return $data;
    }

    protected function stockedCenterDataDetailDB($unit, $id)
    {
        $data = StockedCenter::join('detail_asets', function($join){
            $join->on('stocked_centers.detail_asets_id', '=', 'detail_asets.id');
        })->join("aset_v_i_i_s", function ($join) {
            $join->on('detail_asets.aset_id', '=', 'aset_v_i_i_s.id');
        })->join("jenis_program_masuk_models", function ($join) {
            $join->on('stocked_centers.jenis_id', '=', 'jenis_program_masuk_models.id');
        })->join("penyedia_models", function ($join) {
            $join->on('stocked_centers.penyedia_id', '=', 'penyedia_models.id');
        })->join("sumber_dana_models", function ($join) {
            $join->on('stocked_centers.sumber_dana_id', '=', 'sumber_dana_models.id');
        })->join("distributor_models", function ($join) {
            $join->on('stocked_centers.distributor_id', '=', 'distributor_models.id');
        })->join("satuan_models", function ($join) {
            $join->on('stocked_centers.satuan_id', '=', 'satuan_models.id');
        })
        ->select(
            'stocked_centers.id',
            'detail_asets.nama_detail_aset',
            DB::raw("CONCAT(aset_v_i_i_s.asetI_id,aset_v_i_i_s.asetII_id,aset_v_i_i_s.asetIII_id,aset_v_i_i_s.asetIV_id,aset_v_i_i_s.asetV_id,aset_v_i_i_s.asetVI_id,aset_v_i_i_s.kode_aset_vii, ' - ', aset_v_i_i_s.data_id) as nomor_aset"),
            'jenis_program_masuk_models.nama_program',
            "penyedia_models.nama_penyedia",
            "sumber_dana_models.nama_sumber_dana",
            "distributor_models.nama_distributor",
            'stocked_centers.kode',
            'stocked_centers.batch',
            'stocked_centers.batch',
            'stocked_centers.harga_satuan',
            'stocked_centers.is_expired',
            'stocked_centers.qty',            
            'satuan_models.nama_satuan',
        )
        ->whereToUnitId($unit)
        ->where('stocked_centers.id', '=', $id)
        ->get();
        return $data;
    }

    protected function stockedCenterDataDB($unit)
    {
        $data = StockedCenter::join('detail_asets', function($join){
            $join->on('stocked_centers.detail_asets_id', '=', 'detail_asets.id');
        })->join("aset_v_i_i_s", function ($join) {
            $join->on('detail_asets.aset_id', '=', 'aset_v_i_i_s.id');
        })->join("jenis_program_masuk_models", function ($join) {
            $join->on('stocked_centers.jenis_id', '=', 'jenis_program_masuk_models.id');
        })->join("penyedia_models", function ($join) {
            $join->on('stocked_centers.penyedia_id', '=', 'penyedia_models.id');
        })->join("sumber_dana_models", function ($join) {
            $join->on('stocked_centers.sumber_dana_id', '=', 'sumber_dana_models.id');
        })->join("distributor_models", function ($join) {
            $join->on('stocked_centers.distributor_id', '=', 'distributor_models.id');
        })->join("satuan_models", function ($join) {
            $join->on('stocked_centers.satuan_id', '=', 'satuan_models.id');
        })
        ->select(
            'stocked_centers.id',
            'detail_asets.nama_detail_aset',
            DB::raw("CONCAT(aset_v_i_i_s.asetI_id,aset_v_i_i_s.asetII_id,aset_v_i_i_s.asetIII_id,aset_v_i_i_s.asetIV_id,aset_v_i_i_s.asetV_id,aset_v_i_i_s.asetVI_id,aset_v_i_i_s.kode_aset_vii, ' - ', aset_v_i_i_s.data_id) as nomor_aset"),
            'jenis_program_masuk_models.nama_program',
            "penyedia_models.nama_penyedia",
            "sumber_dana_models.nama_sumber_dana",
            "distributor_models.nama_distributor",
            'stocked_centers.from_unit_id as unit_asal',
            'stocked_centers.to_unit_id as unit_tujuan',
            'stocked_centers.kode',
            'stocked_centers.batch',
            'stocked_centers.batch',
            'stocked_centers.harga_satuan',
            'stocked_centers.is_expired',
            'stocked_centers.qty',            
            'stocked_centers.expired_date',
            'satuan_models.nama_satuan',
        )->whereToUnitId($unit)->get();
        $arr = [];
        foreach ($data as $d) {
            array_push($arr, [
                "id" => $d->id,
                "nama_detail_aset" =>$d->nama_detail_aset,
                "nomor_aset" => $d->nomor_aset,
                "nama_program"=> $d->nama_program,
                "nama_penyedia"=> $d->nama_penyedia,
                "nama_sumber_dana"=> $d->nama_sumber_dana,
                "nama_distributor"=> $d->nama_distributor,
                "unit_asal"=> $this->unit($d->unit_asal),
                "unit_tujuan"=> $this->unit($d->unit_tujuan),
                "kode"=>$d->kode,
                "batch"=> $d->batch,
                "harga_satuan"=> $d->harga_satuan,
                "is_expired"=> $d->is_expired,
                "expired_date" => $d->expired_date,
                "qty"=> $d->qty,
                "nama_satuan"=> $d->nama_satuan
            ]);
        }
        return $arr;
    }

    protected function unit($unit)
    {
        $data = UnitModel::find($unit);
        return $data->nama_unit;
    }
}
