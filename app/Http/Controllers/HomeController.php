<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    function index()
    {
        return view('home.index');
    }
    
    function dashboard()
    {
        return view('dashboard.index');
    }

    function unauthorized()
    {
        return view('errors.401');
    }

    function notFound()
    {
        return view('errors.404');
    }

    function test()
    {
        return view('home.test');
    }
}
