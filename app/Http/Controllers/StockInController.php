<?php

namespace App\Http\Controllers;

use App\Models\DetailAset;
use App\Models\DistributorModel;
use App\Models\JenisProgramMasukModel;
use App\Models\KemasanModel;
use App\Models\PenyediaModel;
use App\Models\SatuanModel;
use App\Models\StockIn;
use App\Models\SumberDanaModel;
use Barryvdh\DomPDF\Facade\Pdf;
use Carbon\Carbon;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Session;

class StockInController extends Controller
{
    function index()
    {
        return view('stockin.index');
    }

    function tambah()
    {
        $listSumberDana =  $this->listSumberDana();
        $listJenis =  $this->listJenis();
        $listSDistributor =  $this->listDistributor();
        return view('stockin.tambah', compact('listSumberDana', 'listJenis', 'listSDistributor'));
    }

    function tambahDraf($no_transaksi)
    {
        $listJenis = $this->listJenis();
        $listSDistributor =  $this->listDistributor();
        $listSumberDana =  $this->listSumberDana();
        $detailAset = $this->listDetailAset();
        $penyedias = $this->listPenyedia();
        $satuans = $this->listSatuan();
        $kemasans = $this->listKemasan();
        // dd($satuans);
        return view(
            'stockin.draft',
            compact(
                'no_transaksi',
                'listJenis',
                'listSDistributor',
                'listSumberDana',
                'detailAset',
                'penyedias',
                'satuans',
                'kemasans'
            )
        );
    }

    function export($id, $unit_id)
    {
        $time = Carbon::now();
        $data = [
            'title' => 'SURAT BUKTI BARANG MASUK',
            'data' => $this->stockInView($id),
            'date' => $time->format('D M Y'),
            'time' => $time->format('H:i:s'),
            'signer' => $this->collectSingner($unit_id)
        ];

        $pdf = PDF::loadView('stockin.export', $data)->setPaper('A4', 'landscape');
        return $pdf->download('stock_in_' . $time . '.pdf');
    }

    private function stockInView($id)
    {
        $data = new StockIn();
        $print = $data->withDetail($id);
        return $print;
    }
}
