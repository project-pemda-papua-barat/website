<?php

namespace App\Http\Controllers;

use App\Models\StockOut;
use Barryvdh\DomPDF\Facade\Pdf;
use Carbon\Carbon;

class StockOutController extends Controller
{
    function index()
    {
        return view('stockout.index');
    }

    function tambah()
    {
        $listDistributor =  $this->listDistributor();
        $unit = $this->listUnit();
        return view('stockout.tambah', compact('listDistributor', 'unit'));
    }

    function tambahDraf($no_transaksi)
    {
        $listJenis = $this->listJenis();
        $listSDistributor =  $this->listDistributor();
        $listSumberDana =  $this->listSumberDana();
        $detailAset = $this->listDetailAset();
        $penyedias = $this->listPenyedia();
        $satuans = $this->listSatuan();
        $kemasans = $this->listKemasan();
        $unit = $this->listUnit();

        return view(
            'stockout.draft',
            compact(
                'no_transaksi',
                'listJenis',
                'listSDistributor',
                'listSumberDana',
                'detailAset',
                'penyedias',
                'satuans',
                'kemasans',
                'unit'
            )
        );
    }

    function export ($id, $unit_id)
    {
        $time = Carbon::now();
        $title = 'SURAT BUKTI BARANG KELUAR';
        $date = $time->format('D M Y');
        $waktu = $time->format('H:i:s');
        $data = $this->stockOutView($id);
        $signer = $this->collectSingner($unit_id);

        $pdf = PDF::loadView('stockout.export', compact('title', 'date', 'data', 'waktu', 'signer'))->setPaper('A4', 'landscape');

        return $pdf->download('stockout_' . $time . '.pdf');
    }

    private function stockOutView($id)
    {
        $data = new StockOut();
        $print = $data->withDetailStockOutId($id);
        return $print;
    }
}
