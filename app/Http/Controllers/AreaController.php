<?php

namespace App\Http\Controllers;

use App\Models\AreaModel;
use Illuminate\Http\Request;

class AreaController extends Controller
{
    public function index()
    {
        return view('area.index');
    }

    function tambahUnit()
    {
        return view('area.tambahunit');
    }

    function editUnit($id)
    {
        $lists = AreaModel::select('id', 'nama_area as text')->whereIsActive(1)->get();
        $selected = "selected";
        return view('area.editunit',  compact('lists', 'id'));
    }
}
