<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;

class ReportController extends Controller
{
    function stokGudang()
    {
        $unit = $this->listUnit();
        $penyedia = $this->listPenyedia();
        $distributor = $this->listDistributor();
        $sumberDana = $this->listSumberDana();
        $jenisProgram = $this->listJenis();
        $listAset = $this->listDetailAset();
        return view('report.stock.gudang', compact(
            'unit', 
            'penyedia', 
            'distributor', 
            'sumberDana', 
            'jenisProgram', 
            'listAset'
        ));
    }

    function print($data)
    {
        return view('report.stock.print', compact('data'));
        $pdf = App::make('dompdf.wrapper');
        $pdf->loadView('report.stock.print', compact('data'))
            ->setOption([
                'dpi' => 150, 
                'defaultFont' => 'sans-serif',
                 'isJavascriptEnabled' => false,
                 'isHtml5ParserEnabled' => false
            ])
            ->setPaper('F4', 'landscape')
            ->setWarnings(false);
        return $pdf->stream();
    }
}
