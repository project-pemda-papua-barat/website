<?php

namespace App\Http\Controllers\Mobile;

use App\Http\Controllers\Controller;
use App\Models\AreaModel;
use App\Models\DistributorModel;
use App\Models\JenisProgramMasukModel;
use App\Models\KemasanModel;
use App\Models\PenyediaModel;
use App\Models\SatuanModel;
use App\Models\SumberDanaModel;
use App\Models\UnitModel;
use Illuminate\Http\Request;
use Throwable;

class PaginationController extends Controller
{
    function __construct()
    {
        $this->middleware('auth:api');
    }

    // Penyedia
    public function penyedia($page)
    {
        try {
            $penyedia = PenyediaModel::orderBy('updated_at', 'DESC')->paginate($page);
            return $penyedia;
        } catch (Throwable $e) {
            report($e);
            $message = $e->getMessage();
            if ($message == 'Attempt to read property "id" on null') {
                return response()->json(['message' => "invalid Token!"], 401);
            }
        }
    }

    // Distibutor

    public function distributor($page)
    {
        try {
            $distributor = DistributorModel::orderBy('updated_at', 'DESC')->paginate($page);
            return $distributor;
        } catch (Throwable $e) {
            report($e);
            $message = $e->getMessage();
            if ($message == 'Attempt to read property "id" on null') {
                return response()->json(['message' => "invalid Token!"], 401);
            }
        }
    }

    // sumber Dana

    public function sumberDana($page)
    {
        try {
            $sumberDana = SumberDanaModel::orderBy('updated_at', 'DESC')->paginate($page);
            return $sumberDana;
        } catch (Throwable $e) {
            report($e);
            $message = $e->getMessage();
            if ($message == 'Attempt to read property "id" on null') {
                return response()->json(['message' => "invalid Token!"], 401);
            }
        }
    }

    // Area

    public function area($page)
    {
        try {
            $sumberDana = AreaModel::orderBy('updated_at', 'DESC')->paginate($page);
            return $sumberDana;
        } catch (Throwable $e) {
            report($e);
            $message = $e->getMessage();
            if ($message == 'Attempt to read property "id" on null') {
                return response()->json(['message' => "invalid Token!"], 401);
            }
        }
    }

    // Unit

    public function unit($page)
    {
        try {
            $unit = UnitModel::with('area')->orderBy('updated_at', 'DESC')->paginate($page);
            return $unit;
        } catch (Throwable $e) {
            report($e);
            $message = $e->getMessage();
            if ($message == 'Attempt to read property "id" on null') {
                return response()->json(['message' => "invalid Token!"], 401);
            }
        }
    }

    // Jenis PRogram Masuk

    public function jenisProgramMasuk($page)
    {
        try {
            $satuan = JenisProgramMasukModel::orderBy('updated_at', 'DESC')->paginate($page);
            return $satuan;
        } catch (Throwable $e) {
            report($e);
            $message = $e->getMessage();
            if ($message == 'Attempt to read property "id" on null') {
                return response()->json(['message' => "invalid Token!"], 401);
            }
        }
    }

    // Satuan

    public function satuan($page)
    {
        try {
            $program = SatuanModel::orderBy('updated_at', 'DESC')->paginate($page);
            return $program;
        } catch (Throwable $e) {
            report($e);
            $message = $e->getMessage();
            if ($message == 'Attempt to read property "id" on null') {
                return response()->json(['message' => "invalid Token!"], 401);
            }
        }
    }

    // Kemasan

    public function kemasan($page)
    {
        try {
            $program = KemasanModel::orderBy('updated_at', 'DESC')->paginate($page);
            return $program;
        } catch (Throwable $e) {
            report($e);
            $message = $e->getMessage();
            if ($message == 'Attempt to read property "id" on null') {
                return response()->json(['message' => "invalid Token!"], 401);
            }
        }
    }
}
