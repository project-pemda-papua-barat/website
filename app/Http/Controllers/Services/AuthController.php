<?php

namespace App\Http\Controllers\Services;

use App\Http\Controllers\Controller;
use App\Models\DetailProfile;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Throwable;
use Yajra\DataTables\Facades\DataTables;

class AuthController extends Controller
{

    function __construct()
    {
        $this->middleware('auth:api', ['except' => ['auth']]);
    }

    function auth(Request $request)
    {
        $request->validate([
            'username' => 'required|string',
            'password' => 'required|string'
        ]);
        $credentials = $request->only('username', 'password');

        
        $token = Auth::setTTL(43200)->attempt($credentials);
        
        if (!$token) {
            return response()->json([
                'status' => 'error',
                'message' => 'username or password incorrect!',
            ], 401);
        }
                
        $unit = User::whereUsername($request->username)->first();
        $value = $unit->squad_id;
        session(['unit_id' => $value]);

        return response()->json([
            'status' => 'success',
            'token' => $token,
            'type' => 'bearer',
        ]);
    }

    function register(Request $request)
    {
        $request->validate([
            'name' => 'required|string|max:255',
            'username' => 'required|string|max:255|unique:users,username',
            'email' => 'required|string|email|max:255|unique:users,email',
            'password' => 'required|string|min:6',
            'role_id' => 'required',
            'squad_id' => 'required',
            'address_user' => 'required|string',
            'profile_pict' => 'image|mimes:jpeg,png,jpg,webp|max:1024'
        ], [
            'profile_pict.mimes' => 'ekstensi yang diperbolehkan hanya .jpeg, .jpg dan .webp',
            'profile_pict.max' => 'Maksimal gambar hanya 1 mb'
        ]);

        $profile = $this->profile();
        $role_name = $profile->getData()->role_name;
        $image = $request->file('profile_pict') == null ? null : $request->file('profile_pict');
        $file_name = $image == null ? null : $image->hashName();

        // try {
        if ($role_name == "Super Admin") {

            $user = User::create([
                "name" => $request->name,
                "email" => $request->email,
                "username" => $request->username,
                "password" => Hash::make($request->password),
                "role_id" => $request->role_id,
                "squad_id" => $request->squad_id,
            ]);

            DetailProfile::create([
                'address_user' => $request->address_user,
                'user_id' => $user->id,
                'profile_pict' => $file_name,
                'lat' => $request->lat,
                'long' => $request->long,
            ]);

            if ($image != null) {
                $image->move(public_path('storage'), $file_name);
            }

            return response()->json([
                'status' => 'success',
                'message' => 'User created successfully',
            ]);
        } else {
            return response()->json(['message' => "token invalid, hanya bisa diakses oleh Super User!"], 401);
        }
        // } catch (Throwable $e) {
        //     report($e);
        //     $message = $e->getMessage();
        //     // return $message;
        //     if ($message == 'Attempt to read property "id" on null') {
        //         return response()->json(['message' => "invalid Token!"], 401);
        //     }
        // }
    }

    function users()
    {
        $profile = $this->profile();
        try {
            if ($profile->getData()->role_name == "Super Admin") {
                $users = User::with('role', 'squad')->get();
                return DataTables::of($users)->addColumn('action', function ($data) {
                    $button = '<a href="#" onclick="editDataArea(' . $data->id . ')" class="text-info m-2"><i class="fa fa-edit" aria-hidden="true"></i></a><a href="#" onclick="deleteArea(' . $data->id . ')" class="text-danger m-2"><i class="fa fa-trash" aria-hidden="true"></i></a>';
                    return $button;
                })
                    ->addIndexColumn()
                    ->rawColumns(['action'])
                    ->make(true);
            } else {
                return response()->json(['message' => "token invalid, hanya bisa diakses oleh Super Admin!"], 401);
            }
        } catch (Throwable $e) {
            report($e);
            $message = $e->getMessage();
            // return $message;
            if ($message == 'Attempt to read property "id" on null') {
                return response()->json(['message' => "invalid Token!"], 401);
            }
        }
    }

    public function refresh()
    {
        return $this->respondWithToken(auth()->refresh(true, true));
    }

    public function logout()
    {
        try {
            auth()->logout(true);
            return response()->json(['message' => 'Successfully logged out']);
        } catch (Throwable $e) {
            report($e);
            $message = $e->getMessage();
            if ($message == 'Attempt to read property "id" on null') {
                return response()->json(['message' => "invalid Token!"], 401);
            }
        }
    }

    function update(Request $request)
    {
        $request->validate([
            'address_user' => 'required|string',
            'profile_pict' => 'image|mimes:jpeg,png,jpg,webp|max:1024'
        ]);

        try {

            $profile = $this->profiles();
            $detail = DetailProfile::where('user_id', $profile['id'])->get();
            $image =  $request->file('profile_pict') == null ? null : $profile['username'] .  '.' .  $request->file('profile_pict')->extension();

            $detail[0]->address_user = $request->address_user;
            $image == null ? null : $detail[0]->profile_pict = $image;
            $detail[0]->save();

            if ($image != null) {
                $request->file('profile_pict')->move(public_path('storage'), $image);
            }

            return response()->json([
                'status' => 'success',
                'message' => 'Detail profile user has been updated successfully',
            ]);
        } catch (Throwable $e) {
            report($e);
            $message = $e->getMessage();
            if ($message == 'Attempt to read property "id" on null') {
                return response()->json(['message' => "invalid Token!"], 401);
            }
        }
    }

    public function profile()
    {
        $data = auth()->user();
        $user = User::where('users.id', $data->id)
            ->join('unit_models', function ($join) {
                return $join->on('unit_models.id', '=', 'users.squad_id');
            })
            ->join('role_users', function ($join) {
                return $join->on('role_users.id', '=', 'users.role_id');
            })
            ->join('detail_profiles', function ($join) {
                return $join->on('detail_profiles.user_id', '=', 'users.id');
            })
            ->select(
                'users.id',
                'users.name',
                'users.username',
                'users.email',
                'users.role_id',
                'unit_models.nama_unit as squad_name',
                'unit_models.address as squad_address',
                'unit_models.id as squad_id',
                // 'squads.squad_phone',
                'role_users.role_name',
                'detail_profiles.profile_pict',
                'detail_profiles.address_user',
            )->get();

        return response()->json([
            'id' => $user[0]->id,
            'name' => $user[0]->name,
            'username' => $user[0]->username,
            'email' => $user[0]->email,
            'role_id' => $user[0]->role_id,
            'squad_name' => $user[0]->squad_name,
            // 'squad_unit' => $user[0]->squad_unit,
            'squad_address' => $user[0]->squad_address,
            // 'squad_phone' => $user[0]->squad_phone,
            'role_name' => $user[0]->role_name,
            'profile_pict' => $user[0]->profile_pict === null ? asset('img/default.png') : asset('storage/' . $user[0]->profile_pict),
            'address_user' => $user[0]->address_user,
        ]);

        // dd($user[0]->profile_pict);
    }
}
