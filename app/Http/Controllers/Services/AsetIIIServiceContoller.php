<?php

namespace App\Http\Controllers\Services;

use App\Http\Controllers\Controller;
use App\Models\AsetIII;
use Illuminate\Http\Request;
use Throwable;
use Yajra\DataTables\Facades\DataTables;

class AsetIIIServiceContoller extends Controller
{
    function __construct()
    {
        $this->middleware('auth:api');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id_asetI, $id_asetII)
    {
        try {
            $aset = AsetIII::where('asetI_id', '=', $id_asetI)->where('asetII_id',  '=', $id_asetII)->get();
            return DataTables::of($aset)->addColumn('action', function ($data) {
                $button = '<a href="#" onclick="editAsetIIIShow(' . $data->id . ')" class="text-info m-2"><i class="fa fa-edit" aria-hidden="true"></i>';
                return $button;
            })
                ->addIndexColumn()
                ->rawColumns(['action'])
                ->make(true);
        } catch (Throwable $e) {
            report($e);
            $message = $e->getMessage();
            if ($message == 'Attempt to read property "id" on null') {
                return response()->json(['message' => "invalid Token!"], 401);
            }
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'asetI_id' => 'required',
            'asetII_id' => 'required',
            'kode_aset_iii' => 'required',
            'nama_aset_iii' => 'required|max:255',
        ]);

        try {

            $profile = $this->profile()[0];
            // return $profile;

            if ($profile->role_name === "Supervisor Pusat") {

                AsetIII::create([
                    'asetI_id' => $request->asetI_id,
                    'asetII_id' => $request->asetII_id,
                    'kode_aset_iii' => $request->kode_aset_iii,
                    'nama_aset_iii' => $request->nama_aset_iii
                ]);


                return response()->json([
                    'status' => 'success',
                    'message' => 'Aset III created successfully',
                ]);
            }
            return response()->json(['message' => "token invalid, hanya bisa diakses oleh Supervisor Pusat!"], 401);
        } catch (Throwable $e) {
            report($e);
            $message = $e->getMessage();
            if ($message == 'Attempt to read property "id" on null') {
                return response()->json(['message' => "invalid Token!"], 401);
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
