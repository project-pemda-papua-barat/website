<?php

namespace App\Http\Controllers\Services;

use App\Http\Controllers\Controller;
use App\Models\StockedCenter;
use Illuminate\Http\Request;
use Throwable;

class StockedCenterServiceController extends Controller
{
    function __construct()
    {
        $this->middleware('auth:api');
        $this->stockedData = new StockedCenter();
    }

    protected function index()
    {
        try {
           $profile = $this->profile()[0];
           $data = $this->stockedData->stockedCenterData($profile->unit_id);
            return $data;
        } catch (Throwable $e) {
            report($e);
            $message = $e->getMessage();
            if ($message == 'Attempt to read property "id" on null') {
                return response()->json(['message' => "invalid Token!"], 401);
            }
        }
    }

    protected function detail($id)
    {
        try {
           $profile = $this->profile()[0];
           $data = $this->stockedData->stockedCenterDataDetail($profile->unit_id, $id);
            return $data;
        } catch (Throwable $e) {
            report($e);
            $message = $e->getMessage();
            if ($message == 'Attempt to read property "id" on null') {
                return response()->json(['message' => "invalid Token!"], 401);
            }
        }
    }
}
