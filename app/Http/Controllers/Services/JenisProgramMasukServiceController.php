<?php

namespace App\Http\Controllers\Services;

use App\Http\Controllers\Controller;
use App\Models\JenisProgramMasukModel;
use App\Models\User;
use Illuminate\Http\Request;
use Throwable;
use Yajra\DataTables\Facades\DataTables;

class JenisProgramMasukServiceController extends Controller
{
    function __construct()
    {
        $this->middleware('auth:api');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $program = JenisProgramMasukModel::orderBy('updated_at', 'DESC')->get();
            return DataTables::of($program)->addColumn('action', function ($data) {
                $button = '<a href="#" onclick="editProgramShow(' . $data->id . ')" class="text-info m-2"><i class="fa fa-edit" aria-hidden="true"></i></a><a href="#" onclick="deleteProgram(' . $data->id . ')" class="text-danger m-2"><i class="fa fa-trash" aria-hidden="true"></i>
                </a>';
                return $button;
            })
                ->addIndexColumn()
                ->rawColumns(['action'])
                ->make(true);
        } catch (Throwable $e) {
            report($e);
            $message = $e->getMessage();
            if ($message == 'Attempt to read property "id" on null') {
                return response()->json(['message' => "invalid Token!"], 401);
            }
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'nama_program' => 'required|string|max:255|unique:jenis_program_masuk_models,nama_program',
        ]);

        try {

            $profile = $this->profile()[0];

            if ($profile->role_name == "Supervisor Pusat") {
                JenisProgramMasukModel::create([
                    'nama_program' => $request->nama_program,
                    'keterangan' => $request->keterangan,
                    'created_by' => $profile->id,
                    'updated_by' => $profile->id
                ]);

                return response()->json([
                    'status' => 'success',
                    'message' => 'Jenis Program Masuk is created successfully',
                ]);
            }
            return response()->json(['message' => "token invalid, hanya bisa diakses oleh Super User!"], 401);
        } catch (Throwable $e) {
            report($e);
            $message = $e->getMessage();
            if ($message == 'Attempt to read property "id" on null') {
                return response()->json(['message' => "invalid Token!"], 401);
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $sumberDana = JenisProgramMasukModel::find($id);
            return $sumberDana;
        } catch (Throwable $e) {
            report($e);
            $message = $e->getMessage();
            if ($message == 'Attempt to read property "id" on null') {
                return response()->json(['message' => "invalid Token!"], 401);
            }
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'nama_program' => 'required|string|max:255',
        ]);

        try {

            $profile = $this->profile()[0];

            if ($profile->role_name === "Supervisor Pusat") {

                $data = JenisProgramMasukModel::find($id);
                $data->nama_program = $request->nama_program;
                $data->keterangan = $request->keterangan;
                $data->updated_by = $profile->id;
                $request->is_active === null ? null : $data->is_active = $request->is_active;
                $data->save();

                return response()->json([
                    'status' => 'success',
                    'message' => 'Jenis Program Masuk updated successfully',
                    'data' => $data
                ]);
            }
            return response()->json(['message' => "token invalid, hanya bisa diakses oleh Super User!"], 401);
        } catch (Throwable $e) {
            report($e);
            $message = $e->getMessage();
            if ($message == 'Attempt to read property "id" on null') {
                return response()->json(['message' => "invalid Token!"], 401);
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {

            $profile = $this->profile()[0];

            if ($profile->role_name === "Supervisor Pusat") {

                $program = JenisProgramMasukModel::find($id);
                $program->delete();

                return response()->json([
                    'status' => 'success',
                    'message' => 'Distributor deleted successfully',
                ]);
            }
            return response()->json(['message' => "token invalid, hanya bisa diakses oleh Supervisor Pusat!"], 401);
        } catch (Throwable $e) {
            report($e);
            $message = $e->getMessage();
            if ($message == 'Attempt to read property "id" on null') {
                return response()->json(['message' => "invalid Token!"], 401);
            }
        }
    }

    function list()
    {
        $data = JenisProgramMasukModel::select('id', 'nama_program as text')->get();
        return $data;
    }

    function profile()
    {
        $data = auth()->user();
        $user = User::where('users.id', $data->id)
            ->join('squads', function ($join) {
                return $join->on('squads.id', '=', 'users.squad_id');
            })
            ->join('role_users', function ($join) {
                return $join->on('role_users.id', '=', 'users.role_id');
            })
            ->select(
                'users.id',
                'users.name',
                'users.username',
                'users.email',
                'squads.squad_name',
                'squads.squad_unit',
                'squads.squad_address',
                'squads.squad_phone',
                'role_users.role_name'
            )->get();

        return $user;
    }
}
