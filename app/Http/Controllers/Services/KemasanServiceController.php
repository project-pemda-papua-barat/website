<?php

namespace App\Http\Controllers\Services;

use App\Http\Controllers\Controller;
use App\Models\KemasanModel;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Throwable;
use Yajra\DataTables\Facades\DataTables;

class KemasanServiceController extends Controller
{
    function __construct()
    {
        $this->middleware('auth:api');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $kemasan = KemasanModel::orderBy('updated_at', 'DESC')->get();
            return DataTables::of($kemasan)->addColumn('action', function ($data) {
                $button = '<a href="#" onclick="editDataKemasan(' . $data->id . ')" class="text-info m-2"><i class="fa fa-edit" aria-hidden="true"></i></a><a href="#" onclick="deleteDataKemasan(' . $data->id . ')" class="text-danger m-2"><i class="fa fa-trash" aria-hidden="true"></i>
                </a>';
                return $button;
            })
                ->addIndexColumn()
                ->rawColumns(['action'])
                ->make(true);
        } catch (Throwable $e) {
            report($e);
            $message = $e->getMessage();
            if ($message == 'Attempt to read property "id" on null') {
                return response()->json(['message' => "invalid Token!"], 401);
            }
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'nama_kemasan' => 'required|string|max:255|unique:kemasan_models,nama_kemasan',
        ]);

        try {

            $profile = $this->profile()[0];
            $id = $profile->id;

            if ($profile->role_name === "Supervisor Pusat") {

                DB::table('kemasan_models')->insert([
                    'nama_kemasan' => $request->nama_kemasan,
                    'keterangan' => $request->keterangan,
                    'created_by' => $id,
                    'updated_by' => $id,
                ]);

                return response()->json([
                    'status' => 'success',
                    'message' => 'Kemasan created successfully',
                    'request' => [
                        'nama_kemasan' => $request->nama_kemasan,
                        'created_by' => $id,
                        'updated_by' => $id,
                        'keterangan' => $request->keterangan
                    ]
                ]);
            }
            return response()->json(['message' => "token invalid, hanya bisa diakses oleh Supervisor Pusat!"], 401);
        } catch (Throwable $e) {
            report($e);
            $message = $e->getMessage();
            if ($message == 'Attempt to read property "id" on null') {
                return response()->json(['message' => "invalid Token!"], 401);
            }
        }
    }

    function list()
    {
        $data = KemasanModel::select('id', 'nama_kemasan as text')->get();
        return $data;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $satuan = KemasanModel::find($id);
            return $satuan;
        } catch (Throwable $e) {
            report($e);
            $message = $e->getMessage();
            if ($message == 'Attempt to read property "id" on null') {
                return response()->json(['message' => "invalid Token!"], 401);
            }
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'nama_kemasan' => 'required|string',
        ]);

        try {

            $profile = $this->profile()[0];

            if ($profile->role_name === "Supervisor Pusat") {

                $data = KemasanModel::find($id);
                $data->nama_kemasan = $request->nama_kemasan;
                $data->keterangan = $request->keterangan;
                $data->updated_by = $profile->id;
                $request->is_active === null ? null : $data->is_active = $request->is_active;
                $data->save();

                return response()->json([
                    'status' => 'success',
                    'message' => 'Kemasan updated successfully',
                ]);
            }
            return response()->json(['message' => "token invalid, hanya bisa diakses oleh Super User!"], 401);
        } catch (Throwable $e) {
            report($e);
            $message = $e->getMessage();
            if ($message == 'Attempt to read property "id" on null') {
                return response()->json(['message' => "invalid Token!"], 401);
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {

            $profile = $this->profile()[0];

            if ($profile->role_name === "Supervisor Pusat") {

                $kemasan = KemasanModel::find($id);
                $kemasan->delete();

                return response()->json([
                    'status' => 'success',
                    'message' => 'Kemasan deleted successfully',
                ]);
            }
            return response()->json(['message' => "token invalid, hanya bisa diakses oleh Supervisor Pusat!"], 401);
        } catch (Throwable $e) {
            report($e);
            $message = $e->getMessage();
            if ($message == 'Attempt to read property "id" on null') {
                return response()->json(['message' => "invalid Token!"], 401);
            }
        }
    }

    function profile()
    {
        $data = auth()->user();
        $user = User::where('users.id', $data->id)
            ->join('squads', function ($join) {
                return $join->on('squads.id', '=', 'users.squad_id');
            })
            ->join('role_users', function ($join) {
                return $join->on('role_users.id', '=', 'users.role_id');
            })
            ->select(
                'users.id',
                'users.name',
                'users.username',
                'users.email',
                'squads.squad_name',
                'squads.squad_unit',
                'squads.squad_address',
                'squads.squad_phone',
                'role_users.role_name'
            )->get();

        return $user;
    }
}
