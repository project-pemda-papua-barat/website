<?php

namespace App\Http\Controllers\Services;

use App\Http\Controllers\Controller;
use App\Models\DetailProfile;
use App\Models\User;
use Illuminate\Http\Request;
use Throwable;

class DetailProfileServiceController extends Controller
{
    function __construct()
    {
        $this->middleware('auth:api');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $profile = $this->profile();
        $request['user_id'] = $profile[0]->id;

        $request->validate([
            'address_user' => 'required|string',
            'user_id' => 'required|unique:detail_profiles,user_id',
            'profile_pict' => 'image|mimes:jpeg,png,jpg,webp|max:1024'
        ]);

        $image = $request->file('profile_pict') == null ? null : $request->file('profile_pict');
        $file_name = $image == null ? null : $image->hashName();

        DetailProfile::create([
            'address_user' => $request->address_user,
            'user_id' => $request['user_id'],
            'profile_pict' => $file_name,
        ]);

        if ($image != null) {
            $image->move(public_path('storage'), $file_name);
        }

        return response()->json([
            'status' => 'success',
            'message' => 'detail created successfully'
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    function profile()
    {
        $data = auth()->user();
        $user = User::where('users.id', $data->id)
            ->join('squads', function ($join) {
                return $join->on('squads.id', '=', 'users.squad_id');
            })
            ->join('role_users', function ($join) {
                return $join->on('role_users.id', '=', 'users.role_id');
            })
            // ->join('detail_profiles', function ($join) {
            //     return $join->on('detail_profiles.user_id', '=', 'users.id');
            // })
            ->select(
                'users.id',
                'users.name',
                'users.username',
                'users.email',
                'squads.squad_name',
                'squads.squad_unit',
                'squads.squad_address',
                'squads.squad_phone',
                'role_users.role_name',
                // 'detail_profiles.profile_pict',
                // 'detail_profiles.address_user',
            )->get();

        return $user;

        // dd($user[0]->profile_pict);
    }
}
