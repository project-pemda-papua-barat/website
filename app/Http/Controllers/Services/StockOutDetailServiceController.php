<?php

namespace App\Http\Controllers\Services;

use App\Http\Controllers\Controller;
use App\Models\StockedCenter;
use App\Models\StockOut;
use App\Models\StockOutDetail;
use Illuminate\Http\Request;
use Throwable;

class StockOutDetailServiceController extends Controller
{
    function __construct()
    {
        $this->middleware('auth:api');
        $this->stockOutDetail = new StockOutDetail();
    }

    function index()
    {
        $data = $this->stockOutDetail->detail();
        return $data;
    }

    function detail($id)
    {
        $data = $this->stockOutDetail->find($id);
        return $data;
    }

    function byIndexTrx($idStockOut)
    {
        $data = $this->stockOutDetail->StockOutDetailByTrx($idStockOut);
        return $data;
    }

    function store(Request $request)
    {
        $request->validate([
            'stock_out_id' => 'required|numeric',
            'stocked_center_id' => 'required|numeric',
            'qty_out' => 'required|numeric'
        ], [
            'stocked_center_id.required' => 'anda belum memilih aset yang akan dikeluarkan!'
        ]);

        try {

            $profile = $this->profile()[0];

            if ($profile->role_name == "User Unit") {
                return $this->stockedCheckCreate(
                    $request->stock_out_id,
                    $request->stocked_center_id,
                    $request->qty_out
                );
            }

            return response()->json(['message' => "token invalid, hanya bisa diakses oleh Admin Unit!"], 401);

        } catch (Throwable $e) {
            report($e);
            $message = $e->getMessage();
            if ($message == 'Attempt to read property "id" on null') {
                return response()->json(['message' => "invalid Token!"], 401);
            }
        }
    }

    function update(Request $request, $id)
    {
        $request->validate([
            'stock_out_id' => 'required|numeric',
            'stocked_center_id' => 'required|numeric',
            'qty_out' => 'required|numeric'
        ], [
            'stocked_center_id.required' => 'anda belum memilih aset yang akan dikeluarkan!'
        ]);

        try {

            $profile = $this->profile()[0];

            if ($profile->role_name == "User Unit") {

                return $this->stockedCheckUpdate(
                    $id,
                    $request->stock_out_id,
                    $request->stocked_center_id,
                    $request->qty_out
                );
            }

            return response()->json(['message' => "token invalid, hanya bisa diakses oleh Admin Unit!"], 401);

        } catch (Throwable $e) {
            report($e);
            $message = $e->getMessage();
            if ($message == 'Attempt to read property "id" on null') {
                return response()->json(['message' => "invalid Token!"], 401);
            }
        }
    }

    protected function stockedCheckUpdate(
        $id,
        $stock_out_id,
        $stocked_center_id,
        $qty_out
     ){
         
         $stockOutDetailId = $this->stockOutDetail->find($id);
         $nomor_tiket = StockOut::find($stock_out_id);
         $responseStockMelebihiKapasitas = response()->json(['message_stock' => 'stok out request melebihi stock yg tersedia'], 422);

         if($stockOutDetailId  == null){
            return response()->json(['message_stock' => 'data not found!'], 404);
         }

        // check stock center id dlu dengan input
        if($stockOutDetailId->stocked_center_id == $stocked_center_id){
            // cek stock tersedia
            $stockedCenter = StockedCenter::find($stockOutDetailId->stocked_center_id);
            if($stockedCenter->qty < $qty_out){
                return $responseStockMelebihiKapasitas;
            }

            $stockOutDetailId->qty_out = $qty_out;
            $stockOutDetailId->save();
            return response()->json(['message_stock' => 'stok out detail berhasil diupdate'], 200);

        }

        // jika stock center tidak sama dengan input
        if($stockOutDetailId->stocked_center_id != $stocked_center_id){
            // check unique
            $nomor_unique = $nomor_tiket->no_transaksi . '_' . $stocked_center_id;
            $dataUnittersediaCek = $this->stockOutDetail->whereUniqueId($nomor_unique)->get();
            if(count($dataUnittersediaCek) > 0 ){
                return response()->json(['message_unique' => 'tidak boleh ada data yang double!'], 422);
            }

            // check stock nya
            $stockedCenterNew = StockedCenter::find($stocked_center_id);
            if($stockedCenterNew->qty < $qty_out){
                return $responseStockMelebihiKapasitas;
            }

            // update stock out detail
            $stockOutDetailId->qty_out = $qty_out;
            $stockOutDetailId->stocked_center_id = $stocked_center_id;
            $stockOutDetailId->unique_id = $nomor_unique;
            $stockOutDetailId->save();
            return response()->json(['message_stock' => 'stok out detail berhasil diupdate'], 200);
        }
     }

    protected function stockedCheckCreate(
        $stock_out_id,
        $stocked_center_id,
        $qty_out
    ) {
        $data = StockedCenter::find($stocked_center_id);
        $stockOut = StockOut::find($stock_out_id);
        $uniqeRequest = $stockOut->no_transaksi . "_" . $data->id;
        $unique = $this->stockOutDetail::whereUniqueId($uniqeRequest)->get();

        if ($data->qty < $qty_out) {
            return response()->json(['message_stock' => 'stok out request melebihi stock yg tersedia'], 422);
        }

        if (count($unique) > 0) {
            return response()->json(['message_unique' => 'anda sudah menambahkan data ini, silahkan masukan data lain atau hapus data sebelumnya'], 422);
        }

        try {

            $profile = $this->profile()[0];

            if ($profile->role_name == "User Unit") {
                $this->stockOutDetail->create([
                    'stock_out_id' => $stock_out_id,
                    'stocked_center_id' => $stocked_center_id,
                    'qty_out' => $qty_out,
                    'is_status' => 'can deleted',
                    'unique_id' => $uniqeRequest
                ]);

                return response()->json([
                    'status' => 'success',
                    'message' => 'Detail stockout  created successfully'
                ]);
            }

            return response()->json(['message' => "token invalid, hanya bisa diakses oleh Admin Unit!"], 401);
        } catch (Throwable $e) {
            report($e);
            $message = $e->getMessage();
            if ($message == 'Attempt to read property "id" on null') {
                return response()->json(['message' => "invalid Token!"], 401);
            }
        }
    }

    protected function destroy($id)
    {
        try {

            $profile = $this->profile()[0];

            if ($profile->role_name === "User Unit") {

                $data = $this->stockOutDetail->find($id);

                if ($data == null) {
                    return response()->json([
                        'status' => 'Error',
                        'message' => 'Data Not Found!',
                    ], 404);
                }

                $data->delete();
                return response()->json([
                    'status' => 'success',
                    'message' => 'detail aset deleted successfully',
                ]);
            }
            return response()->json(['message' => "token invalid, hanya bisa diakses oleh User Unit!"], 401);
        } catch (Throwable $e) {
            report($e);
            $message = $e->getMessage();
            if ($message == 'Attempt to read property "id" on null') {
                return response()->json(['message' => "invalid Token!"], 401);
            }
        }
    }
}
