<?php

namespace App\Http\Controllers\Services;

use App\Http\Controllers\Controller;
use App\Models\AsetI;
use App\Models\User;
use Illuminate\Http\Request;
use Throwable;
use Yajra\DataTables\Facades\DataTables;

class AsetIServiceController extends Controller
{
    function __construct()
    {
        $this->middleware('auth:api');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $aset = AsetI::all();
            return DataTables::of($aset)->addColumn('action', function ($data) {
                $button = '<a href="#" onclick="editAsetIShow(' . $data->id . ')" class="text-info m-2"><i class="fa fa-edit" aria-hidden="true"></i>';
                return $button;
            })
                ->addIndexColumn()
                ->rawColumns(['action'])
                ->make(true);
        } catch (Throwable $e) {
            report($e);
            $message = $e->getMessage();
            if ($message == 'Attempt to read property "id" on null') {
                return response()->json(['message' => "invalid Token!"], 401);
            }
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'kode_aset_i' => 'required|string|max:255|unique:aset_i_s,kode_aset_i',
            'nama_aset_i' => 'required|string|max:255|unique:aset_i_s,nama_aset_i',
        ]);

        try {

            $profile = $this->profile()[0];
            // return $profile;

            if ($profile->role_name === "Supervisor Pusat") {

                AsetI::create([
                    'kode_aset_i' => $request->kode_aset_i,
                    'nama_aset_i' => $request->nama_aset_i
                ]);


                return response()->json([
                    'status' => 'success',
                    'message' => 'Aset I created successfully',
                ]);
            }
            return response()->json(['message' => "token invalid, hanya bisa diakses oleh Supervisor Pusat!"], 401);
        } catch (Throwable $e) {
            report($e);
            $message = $e->getMessage();
            if ($message == 'Attempt to read property "id" on null') {
                return response()->json(['message' => "invalid Token!"], 401);
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\AsetI  $asetI
     * @return \Illuminate\Http\Response
     */
    public function show(AsetI $asetI)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\AsetI  $asetI
     * @return \Illuminate\Http\Response
     */
    public function edit(AsetI $asetI)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\AsetI  $asetI
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, AsetI $asetI)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\AsetI  $asetI
     * @return \Illuminate\Http\Response
     */
    public function destroy(AsetI $asetI)
    {
        //
    }


    function profile()
    {
        $data = auth()->user();
        $user = User::where('users.id', $data->id)
            ->join('squads', function ($join) {
                return $join->on('squads.id', '=', 'users.squad_id');
            })
            ->join('role_users', function ($join) {
                return $join->on('role_users.id', '=', 'users.role_id');
            })
            ->select(
                'users.id',
                'users.name',
                'users.username',
                'users.email',
                'squads.squad_name',
                'squads.squad_unit',
                'squads.squad_address',
                'squads.squad_phone',
                'role_users.role_name'
            )->get();

        return $user;
    }
}
