<?php

namespace App\Http\Controllers\Services;

use App\Http\Controllers\Controller;
use App\Models\AsetV;
use Illuminate\Http\Request;
use Throwable;
use Yajra\DataTables\Facades\DataTables;

class AsetVServiceController extends Controller
{
    function __construct()
    {
        $this->middleware('auth:api');
    }

    function index($id_asetI, $id_asetII, $id_asetIII, $id_asetIV)
    {
        try {
            $aset = AsetV::where('asetI_id', '=', $id_asetI)->where('asetII_id',  '=', $id_asetII)->where('asetIII_id',  '=', $id_asetIII)->where('asetIV_id',  '=', $id_asetIV)->get();
            return DataTables::of($aset)->addColumn('action', function ($data) {
                $button = '<a href="#" onclick="editAsetVShow(' . $data->id . ')" class="text-info m-2"><i class="fa fa-edit" aria-hidden="true"></i>';
                return $button;
            })
                ->addIndexColumn()
                ->rawColumns(['action'])
                ->make(true);
        } catch (Throwable $e) {
            report($e);
            $message = $e->getMessage();
            if ($message == 'Attempt to read property "id" on null') {
                return response()->json(['message' => "invalid Token!"], 401);
            }
        }
    }

    public function store(Request $request)
    {
        $request->validate([
            'asetI_id' => 'required',
            'asetII_id' => 'required',
            'asetIII_id' => 'required',
            'asetIV_id' => 'required',
            'kode_aset_v' => 'required',
            'nama_aset_v' => 'required|string|max:255|unique:aset_v_s,nama_aset_v',
        ]);

        try {

            $profile = $this->profile()[0];
            // return $profile;

            if ($profile->role_name === "Supervisor Pusat") {

                AsetV::create([
                    'asetI_id' => $request->asetI_id,
                    'asetII_id' => $request->asetII_id,
                    'asetIII_id' => $request->asetIII_id,
                    'asetIV_id' => $request->asetIV_id,
                    'kode_aset_v' => $request->kode_aset_v,
                    'nama_aset_v' => $request->nama_aset_v
                ]);


                return response()->json([
                    'status' => 'success',
                    'message' => 'Aset V created successfully',
                ]);
            }
            return response()->json(['message' => "token invalid, hanya bisa diakses oleh Supervisor Pusat!"], 401);
        } catch (Throwable $e) {
            report($e);
            $message = $e->getMessage();
            if ($message == 'Attempt to read property "id" on null') {
                return response()->json(['message' => "invalid Token!"], 401);
            }
        }
    }
}
