<?php

namespace App\Http\Controllers\Services;

use App\Http\Controllers\Controller;
use App\Models\DetailAset;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Throwable;

class DetailAsetServiceController extends Controller
{
    function __construct()
    {
        $this->middleware('auth:api');
        $this->listAset = new DetailAset();
    }

    protected function index()
    {
        $data = $this->listAset->data();
        return $data;
    }

    protected function list()
    {
        $data = $this->listAset->detail();
        $list = [];

        foreach ($data as $d) {
            array_push($list, [
                'id' => $d->id,
                'text' => $d->nama_detail_aset . " " . $d->keterangan . " - Nomor Aset: " .  $d->nomor_aset . " - Data ID: " . $d->data_id,
                'is_expired' => $d->is_expired
            ]);
        }

        return $list;
    }

    protected function store(Request $request)
    {
        $request->validate([
            'aset_id' => 'required',
            'nama_detail_aset' => 'required|string|max:255|unique:detail_asets,nama_detail_aset',
            'keterangan' => 'required',
            'is_expired' => 'required',
            'aset_image' => 'image|mimes:png,jpg,jpeg|max:1048'
        ]);

        try {

            $profile = $this->profile()[0];
            $aset_image = $request->file('aset_image');
            $aset_image_ext = $aset_image === null ? null : 'aset_image_' . Carbon::now()->format('ymdHis') . '.' . $aset_image->extension();

            if ($profile->role_name == "Supervisor Pusat" || $profile->role_name == "Supervisor Unit") {
                $this->listAset->create([
                    'aset_id' => $request->aset_id,
                    'nama_detail_aset' => $request->nama_detail_aset,
                    'keterangan' => $request->keterangan,
                    'is_expired' => $request->is_expired,
                    'created_by' => $profile->id,
                    'updated_by' => $profile->id,
                    'aset_image' => $aset_image_ext
                ]);

                if ($aset_image != null) {
                    $aset_image->move(public_path('upload/aset_image'), $aset_image_ext);
                }


                return response()->json([
                    'status' => 'success',
                    'message' => 'Aset Detail is created successfully',
                ]);
            }
            return response()->json(['message' => "token invalid, hanya bisa diakses oleh Supervisor Pusat atau Supervisor Unit!"], 401);
        } catch (Throwable $e) {
            report($e);
            $message = $e->getMessage();
            if ($message == 'Attempt to read property "id" on null') {
                return response()->json(['message' => "invalid Token!"], 401);
            }
        }
    }

    protected function detail($id)
    {
        $data = $this->listAset->find($id);
        return $data;
    }

    protected function update(Request $request, $id)
    {
        $request->validate([
            'aset_id' => 'required',
            'nama_detail_aset' => 'required|string|max:255',
            'keterangan' => 'required',
            'is_expired' => 'required',
            'aset_image' => 'image|mimes:png,jpg,jpeg|max:1048'
        ]);

        try {

            $profile = $this->profile()[0];

            if ($profile->role_name == "Supervisor Pusat" || $profile->role_name == "Supervisor Unit") {
                $aset_image = $request->file('aset_image');
                $aset_image_ext = $aset_image === null ? null : 'aset_image_' . Carbon::now()->format('ymdHis') . '.' . $aset_image->extension();

                $detailAset = $this->listAset->find($id);
                $detailAset->nama_detail_aset = $request->nama_detail_aset;
                $detailAset->keterangan = $request->keterangan;
                $detailAset->is_expired = $request->is_expired;
                $detailAset->updated_by = $profile->id;

                if ($aset_image !== null) {
                    $aset_image->move(public_path('upload/aset_image'),  $detailAset->aset_image == null ? $aset_image_ext : $detailAset->aset_image);

                    if ($detailAset->aset_image === null) {
                        $detailAset->aset_image = $aset_image_ext;
                    }
                }

                $detailAset->save();


                return response()->json([
                    'status' => 'success',
                    'message' => 'Aset Detail is UPDATED successfully',
                ]);
            }
            return response()->json(['message' => "token invalid, hanya bisa diakses oleh Supervisor Pusat atau Supervisor Unit!"], 401);
        } catch (Throwable $e) {
            report($e);
            $message = $e->getMessage();
            if ($message == 'Attempt to read property "id" on null') {
                return response()->json(['message' => "invalid Token!"], 401);
            }
        }
    }
}
