<?php

namespace App\Http\Controllers\Services;

use App\Http\Controllers\Controller;
use App\Models\MasterAsetStock;
use Illuminate\Http\Request;
use Throwable;

class MasterStockServiceController extends Controller
{
    function __construct()
    {
        $this->middleware('auth:api');
        $this->masterStock = new MasterAsetStock();
    }

    protected function index()
    {
        try {
            $profile = $this->profile()[0];
            $data = $this->masterStock->dataStockUnit($profile->unit_id);
            return $data;
        } catch (Throwable $e) {
            report($e);
            $message = $e->getMessage();
            if ($message == 'Attempt to read property "id" on null') {
                return response()->json(['message' => "invalid Token!"], 401);
            }
        }
    }
}
