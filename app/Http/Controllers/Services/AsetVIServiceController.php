<?php

namespace App\Http\Controllers\Services;

use App\Http\Controllers\Controller;
use App\Models\AsetVI;
use Illuminate\Http\Request;
use Throwable;
use Yajra\DataTables\Facades\DataTables;

class AsetVIServiceController extends Controller
{
    function __construct()
    {
        $this->middleware('auth:api');
    }

    function index($id_asetI, $id_asetII, $id_asetIII, $id_asetIV, $id_asetV)
    {
        try {
            $aset = AsetVI::where('asetI_id', '=', $id_asetI)
                ->where('asetII_id',  '=', $id_asetII)
                ->where('asetIII_id',  '=', $id_asetIII)
                ->where('asetIV_id',  '=', $id_asetIV)
                ->where('asetV_id',  '=', $id_asetV)->get();
            return DataTables::of($aset)->addColumn('action', function ($data) {
                $button = '<a href="#" onclick="editAsetVIShow(' . $data->id . ')" class="text-info m-2"><i class="fa fa-edit" aria-hidden="true"></i>';
                return $button;
            })
                ->addIndexColumn()
                ->rawColumns(['action'])
                ->make(true);
        } catch (Throwable $e) {
            report($e);
            $message = $e->getMessage();
            if ($message == 'Attempt to read property "id" on null') {
                return response()->json(['message' => "invalid Token!"], 401);
            }
        }
    }

    public function store(Request $request)
    {
        $request->validate([
            'asetI_id' => 'required',
            'asetII_id' => 'required',
            'asetIII_id' => 'required',
            'asetIV_id' => 'required',
            'asetV_id' => 'required',
            'kode_aset_vi' => 'required',
            'nama_aset_vi' => 'required|string',
        ]);

        try {

            $profile = $this->profile()[0];
            // return $profile;

            if ($profile->role_name == "Supervisor Pusat") {

                AsetVI::create([
                    'asetI_id' => $request->asetI_id,
                    'asetII_id' => $request->asetII_id,
                    'asetIII_id' => $request->asetIII_id,
                    'asetIV_id' => $request->asetIV_id,
                    'asetV_id' => $request->asetV_id,
                    'kode_aset_vi' => $request->kode_aset_vi,
                    'nama_aset_vi' => $request->nama_aset_vi
                ]);


                return response()->json([
                    'status' => 'success',
                    'message' => 'Aset VI created successfully',
                ]);
            }
            return response()->json(['message' => "token invalid, hanya bisa diakses oleh Supervisor Pusat!"], 401);
        } catch (Throwable $e) {
            report($e);
            $message = $e->getMessage();
            if ($message == 'Attempt to read property "id" on null') {
                return response()->json(['message' => "invalid Token!"], 401);
            }
        }
    }
}
