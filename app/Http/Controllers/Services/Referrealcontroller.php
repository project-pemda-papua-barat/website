<?php

namespace App\Http\Controllers\Services;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class Referrealcontroller extends Controller
{
    function __construct()
    {
        $this->middleware('auth:api');
    }

    function store(Request $request)
    {
        return response()->json(
            [
                'status' => 'success',
                'message' => 'refferal sukses disimpan!',
                'data' => [
                    'referredTrx' => $request->referredTrx,
                    'identifier' => $request->identifier,
                    'identifierValue' => $request->identifierValue,
                    'referralCode' => $request->referralCode
                ]
            ]
        );
    }
}
