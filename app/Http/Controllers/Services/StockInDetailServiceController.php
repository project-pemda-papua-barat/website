<?php

namespace App\Http\Controllers\Services;

use App\Http\Controllers\Controller;
use App\Models\DetailAset;
use App\Models\MasterAsetStock;
use App\Models\StockedCenter;
use App\Models\StockIn;
use App\Models\StockInDetail;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Throwable;
use Illuminate\Support\Facades\File;

class StockInDetailServiceController extends Controller
{
    function __construct()
    {
        $this->middleware('auth:api');
        $this->stockInDetail = new StockInDetail();
        $this->stockin = new StockIn();
    }

    protected function storeStatus(Request $request)
    {
        $request->validate([
            'is_status' => 'required|string',
            'stock_in_id' => 'required'
        ]);

        try {
            $profile = $this->profile()[0];


            if ($profile->role_name == "User Unit") {
                $data = $this->stockin->find($request->stock_in_id);

                if ($data == null) {
                    return response()->json(['message' => 'data tidak tersedia'], 404);
                }

                if ($data->dokumen_berita_acara == null) {
                    return response()->json(['message' => 'minimal upload dokumen berita acara terlebih dahulu'], 404);
                }

                $detail = $this->stockInDetail->whereStockInId($request->stock_in_id)->get();

                if ($detail->count() === 0) {
                    return response()->json(['message' => 'Anda belum menambahkan detail Aset!'], 404);
                }


                if ($data->is_status === "can deleted" || $data->is_status === "harus direvisi") {
                    $data->is_status = $request->is_status;
                    $data->save();

                    return response()->json(['message' => 'Stock in berhasil diupdate menunggu review atau approve spv unit! ']);
                }

                return response()->json(['message' => 'data tidak bisa diubah', 'user' => 'Unit Admin'], 401);
            }

            if ($profile->role_name = "Supervisor Unit") {
                $data = $this->stockin->find($request->stock_in_id);

                if ($data == null) {
                    return response()->json(['message' => 'data tidak tersedia'], 404);
                }

                if ($data->is_status === 'waiting approval') {

                    $data->is_status = $request->is_status;
                    $data->keterangan = $request->keterangan;
                    $data->save();

                    if ($request->is_status === 'approved') {
                        return $this->storeToMasterStock(
                            $data->id,
                            $data->jenis_id,
                            $data->sumber_dana_id,
                            $data->distributor_id,
                            $profile->id,
                            $data->no_transaksi,
                            $data->tahun_pengadaan
                        );
                    }

                    return response()->json(['message' => 'Stock in berhasil diupdate menunggu direvisi admin unit! ']);
                }

                return response()->json(['message' => 'data tidak bisa diubah', 'user' => 'Spv Unit'], 401);
            }

            return response()->json(['message' => "token invalid, hanya bisa diakses oleh Admin Unit atau Supervisor Unit!"], 401);
        } catch (Throwable $e) {
            $message = $e->getMessage();
            if ($message == 'Attempt to read property "id" on null') {
                return response()->json(['message' => "invalid Token!"], 401);
            }
        }
    }

    protected function storeToMasterStock(
        $stockinid,
        $jenisId,
        $sumberDanaId,
        $distributorId,
        $profileId,
        $notrx,
        $tahun_pengadaan
    ) {

        $data = $this->stockInDetail->whereStockInId($stockinid)->get();

        for ($i = 1; $i <= count($data); $i++) {
            $detailAsetIdStatusExpired = DetailAset::find($data[$i - 1]->detail_aset_id);
            $dataStocked = StockedCenter::where(
                "detail_asets_id",
                $data[$i - 1]->detail_aset_id,
            )->where(
                "jenis_id",
                '=',
                $jenisId,
            )->where(
                "satuan_id",
                '=',
                $data[$i - 1]->satuan_id,
            )->where(
                "penyedia_id",
                '=',
                $data[$i - 1]->penyedia_id,
            )->where(
                "sumber_dana_id",
                '=',
                $sumberDanaId,
            )->where(
                "distributor_id",
                '=',
                $distributorId,
            )->where(
                "from_unit_id",
                '=',
                $data[$i - 1]->unit_id,
            )->where(
                "to_unit_id",
                '=',
                $data[$i - 1]->unit_id,
            )->where(
                "kode",
                '=',
                $data[$i - 1]->kode,
            )->get();

            if (count($dataStocked) === 0) {
                StockedCenter::create([
                    "detail_asets_id" => $data[$i - 1]->detail_aset_id,
                    "jenis_id" => $jenisId,
                    "satuan_id" => $data[$i - 1]->satuan_id,
                    "penyedia_id" => $data[$i - 1]->penyedia_id,
                    'sumber_dana_id' => $sumberDanaId,
                    'distributor_id' => $distributorId,
                    'from_unit_id' => $data[$i - 1]->unit_id,
                    'to_unit_id' => $data[$i - 1]->unit_id,
                    'kode' => $data[$i - 1]->kode,
                    'batch' => $data[$i - 1]->batch,
                    'expired_date' => $data[$i - 1]->expired_date,
                    'harga_satuan' => $data[$i - 1]->harga_satuan,
                    'is_expired' => $detailAsetIdStatusExpired->is_expired,
                    'qty' => $data[$i - 1]->qty
                ]);
            }

            if (count($dataStocked) !== 0 &&  $detailAsetIdStatusExpired->is_expired === 'false') {
                StockedCenter::where(
                    "detail_asets_id",
                    '=',
                    $data[$i - 1]->detail_aset_id,
                )->where(
                    "jenis_id",
                    '=',
                    $jenisId,
                )->where(
                    "satuan_id",
                    '=',
                    $data[$i - 1]->satuan_id,
                )->where(
                    "penyedia_id",
                    '=',
                    $data[$i - 1]->penyedia_id,
                )->where(
                    "sumber_dana_id",
                    '=',
                    $sumberDanaId,
                )->where(
                    "distributor_id",
                    '=',
                    $distributorId,
                )->where(
                    "from_unit_id",
                    '=',
                    $data[$i - 1]->unit_id,
                )->where(
                    "to_unit_id",
                    '=',
                    $data[$i - 1]->unit_id,
                )->where(
                    "kode",
                    '=',
                    $data[$i - 1]->kode,
                )->update([
                    'qty' => $dataStocked[0]->qty + $data[$i - 1]->qty
                ]);
            }

            if (count($dataStocked) !== 0 &&  $detailAsetIdStatusExpired->is_expired === 'true') {
                StockedCenter::create([
                    "detail_asets_id" => $data[$i - 1]->detail_aset_id,
                    "jenis_id" => $jenisId,
                    "satuan_id" => $data[$i - 1]->satuan_id,
                    "penyedia_id" => $data[$i - 1]->penyedia_id,
                    'sumber_dana_id' => $sumberDanaId,
                    'distributor_id' => $distributorId,
                    'from_unit_id' => $data[$i - 1]->unit_id,
                    'to_unit_id' => $data[$i - 1]->unit_id,
                    'kode' => $data[$i - 1]->kode,
                    'batch' => $data[$i - 1]->batch,
                    'expired_date' => $data[$i - 1]->expired_date,
                    'harga_satuan' => $data[$i - 1]->harga_satuan,
                    'is_expired' => $detailAsetIdStatusExpired->is_expired,
                    'qty' => $data[$i - 1]->qty
                ]);
            }


            MasterAsetStock::create([
                "detail_asets_id" => $data[$i - 1]->detail_aset_id,
                "jenis_id" => $jenisId,
                "satuan_id" => $data[$i - 1]->satuan_id,
                "penyedia_id" => $data[$i - 1]->penyedia_id,
                'sumber_dana_id' => $sumberDanaId,
                'distributor_id' => $distributorId,
                'from_unit_id' => $data[$i - 1]->unit_id,
                'to_unit_id' => $data[$i - 1]->unit_id,
                'user_id' => $profileId,
                'no_transaksi' => $notrx,
                'kode' => $data[$i - 1]->kode,
                'batch' => $data[$i - 1]->batch,
                'expired_date' => $data[$i - 1]->expired_date,
                'harga_satuan' => $data[$i - 1]->harga_satuan,
                'tahun_pengadaan' => $tahun_pengadaan,
                'created_by' => $profileId,
                'updated_by' => $profileId,
                'qty' => $data[$i - 1]->qty
            ]);
        }

        return response()->json(['message' => 'Approved! Stock in berhasil ditambahkan ke master Stok! ']);
    }


    protected function detailListAset(Request $request)
    {
        $request->validate(['stock_in_id' => 'required']);
        try {
            $profile = $this->profile()[0];
            if ($profile->role_name == "User Unit") {
                $data = $this->stockInDetail->whereStockInId($request->stock_in_id)->get();
                return response()->json(['data' => count($data)]);
            }

            return response()->json(['message' => "token invalid, hanya bisa diakses oleh Admin Unit!"], 401);
        } catch (Throwable $e) {
            $message = $e->getMessage();
            if ($message == 'Attempt to read property "id" on null') {
                return response()->json(['message' => "invalid Token!"], 401);
            }
        }
    }

    protected function stockinwithDetail($stockinid)
    {
        $data = $this->stockInDetail->stockinwithDetail($stockinid);
        return $data;
    }

    protected function detail($id)
    {
        $data = $this->stockInDetail->find($id);
        return $data;
    }

    protected function update(Request $request, $id)
    {
        $request->validate([
            'detail_aset_id' => 'required',
            'satuan_id' => 'required',
            'kemasan_id' => 'required',
            'penyedia_id' => 'required',
            'qty' => 'required',
            'kode' => 'required',
            'batch' => 'required',
            'expired_date' => 'required|date',
            'harga_satuan' => 'required',
            'aset_image' => 'image|mimes:png,jpg,jpeg|max:2048'
        ]);

        try {
            $profile = $this->profile()[0];
            if ($profile->role_name == "User Unit") {
                $aset_image = $request->file('aset_image');

                $data = $this->detail($id);

                $data->detail_aset_id = $request->detail_aset_id;
                $data->satuan_id = $request->satuan_id;
                $data->kemasan_id = $request->kemasan_id;
                $data->penyedia_id = $request->penyedia_id;
                $data->qty = $request->qty;
                $data->kode = $request->kode;
                $data->batch = $request->batch;
                $data->expired_date = $request->expired_date;
                $data->harga_satuan = $request->harga_satuan;

                $data->save();

                $aset_image !== null ? $aset_image->move(public_path('upload/aset_image'),  $data->aset_image) : null;

                return response()->json(['message' => "Stocked detail edited successfully!"], 200);
            }

            return response()->json(['message' => "token invalid, hanya bisa diakses oleh Admin Unit!"], 401);
        } catch (Throwable $e) {
            $message = $e->getMessage();
            if ($message == 'Attempt to read property "id" on null') {
                return response()->json(['message' => "invalid Token!"], 401);
            }
        }
    }

    protected function store(Request $request)
    {
        $request->validate([
            'stock_in_id' => 'required',
            'detail_aset_id' => 'required',
            'satuan_id' => 'required',
            'kemasan_id' => 'required',
            'penyedia_id' => 'required',
            'qty' => 'required',
            'kode' => 'required',
            'batch' => 'required',
            'expired_date' => 'required|date',
            'harga_satuan' => 'required'
        ]);

        try {

            $profile = $this->profile()[0];
            $expired_time_limit = Carbon::parse(Carbon::now())->diffInDays($request->expired_date);

            // check data double
            $checkDoubleData = $this->stockInDetail->where('stock_in_id', '=', $request->stock_in_id)->where(
                'detail_aset_id',
                '=',
                $request->detail_aset_id
            )->get();

            if ($expired_time_limit < 31) {
                return response()->json([
                    'errors' => [
                        'expired_date_limit' => 'tanggal expire yang anda masukan minimal 30 hr kedepan dari hari ini!'
                    ]
                ], 422);
            }

            if ($checkDoubleData->count() > 0) {
                return response()->json([
                    'errors' => [
                        'duplicate' => 'detail aset ini sudah ditambahkan silahkan pilih aset yang lain!'
                    ]
                ], 422);
            }

            if ($profile->role_name == "User Unit") {
                $this->stockInDetail->create([
                    'stock_in_id' => $request->stock_in_id,
                    'detail_aset_id' => $request->detail_aset_id,
                    'satuan_id' => $request->satuan_id,
                    'kemasan_id' => $request->kemasan_id,
                    'penyedia_id' => $request->penyedia_id,
                    'qty' => $request->qty,
                    'kode' => $request->kode,
                    'batch' => $request->batch,
                    'expired_date' => $request->expired_date,
                    'harga_satuan' => $request->harga_satuan,
                    'is_status' => 'draft',
                    'unit_id' => $profile->unit_id,
                ]);

                return response()->json([
                    'status' => 'success',
                    'message' => 'Detail stocked  created successfully'
                ]);
            }

            return response()->json(['message' => "token invalid, hanya bisa diakses oleh Admin Unit!"], 401);
        } catch (Throwable $e) {
            report($e);
            $message = $e->getMessage();
            if ($message == 'Attempt to read property "id" on null') {
                return response()->json(['message' => "invalid Token!"], 401);
            }
        }
    }

    protected function destroy($id)
    {
        try {

            $profile = $this->profile()[0];

            if ($profile->role_name === "User Unit") {

                $data = $this->stockInDetail->find($id);
                $path = public_path() . '/upload/aset_image/' . $data->aset_image;

                if ($data->is_status === 'waiting approval' || $data->is_status === 'approved') {
                    return response()->json(["message" => "Status Detail Stock In Sudah Tidak bisa dihapus!"], 422);
                }


                File::delete($path);

                $data->delete();
                return response()->json([
                    'status' => 'success',
                    'message' => 'detail aset deleted successfully',
                ]);
            }
            return response()->json(['message' => "token invalid, hanya bisa diakses oleh User Unit!"], 401);
        } catch (Throwable $e) {
            report($e);
            $message = $e->getMessage();
            if ($message == 'Attempt to read property "id" on null') {
                return response()->json(['message' => "invalid Token!"], 401);
            }
        }
    }
}
