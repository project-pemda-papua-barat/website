<?php

namespace App\Http\Controllers\Services;

use App\Http\Controllers\Controller;
use App\Models\RegionModel;
use App\Models\User;
use Illuminate\Http\Request;
use Throwable;
use Yajra\DataTables\Facades\DataTables;

class RegionServiceController extends Controller
{
    function __construct()
    {
        $this->middleware('auth:api');
    }

    function index($provinsi)
    {
        try {
            $sumberDana = new RegionModel();
            $datas = $sumberDana->getRegion($provinsi);
            return $datas;
        } catch (Throwable $e) {
            report($e);
            $message = $e->getMessage();
            if ($message == 'Attempt to read property "id" on null') {
                return response()->json(['message' => "invalid Token!"], 401);
            }
            return $e;
        }
    }

    function profile()
    {
        $data = auth()->user();
        $user = User::where('users.id', $data->id)
            ->join('squads', function ($join) {
                return $join->on('squads.id', '=', 'users.squad_id');
            })
            ->join('role_users', function ($join) {
                return $join->on('role_users.id', '=', 'users.role_id');
            })
            ->select(
                'users.id',
                'users.name',
                'users.username',
                'users.email',
                'squads.squad_name',
                'squads.squad_unit',
                'squads.squad_address',
                'squads.squad_phone',
                'role_users.role_name'
            )->get();

        return $user;
    }
}
