<?php

namespace App\Http\Controllers\Services;

use App\Http\Controllers\Controller;
use App\Models\AreaModel;
use App\Models\UnitModel;
use App\Models\User;
use Illuminate\Http\Request;
use Throwable;
use Yajra\DataTables\Facades\DataTables;

class UnitServicesController extends Controller
{
    function __construct()
    {
        $this->middleware('auth:api');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $area = UnitModel::with('area')->orderBy('updated_at', 'DESC')->get();
            return DataTables::of($area)->addColumn('action', function ($data) {
                $button = '<a href="#" onclick="editUnitShow(' . $data->id . ')" class="text-info m-2"><i class="fa fa-edit" aria-hidden="true"></i></a><a href="#" onclick="deleteUnit(' . $data->id . ')" class="text-danger m-2"><i class="fa fa-trash" aria-hidden="true"></i>
                </a>';
                return $button;
            })
                ->addIndexColumn()
                ->rawColumns(['action'])
                ->make(true);
        } catch (Throwable $e) {
            report($e);
            $message = $e->getMessage();
            if ($message == 'Attempt to read property "id" on null') {
                return response()->json(['message' => "invalid Token!"], 401);
            }
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'nama_unit' => 'required|string|max:255|unique:unit_models,nama_unit',
            'area_id_select' => 'required',
            'address' => 'required',
            'detail_address' => 'required',
            'lat' => 'required',
            'long' => 'required',
        ]);

        try {

            $profile = $this->profile()[0];

            if ($profile->role_name === "Supervisor Pusat") {

                UnitModel::create([
                    'nama_unit' => $request->nama_unit,
                    'area_id' => $request->area_id_select,
                    'address' => $request->address,
                    'detail_address' => $request->detail_address,
                    'lat' => $request->lat,
                    'long' => $request->long,
                    'created_by' => $profile->id,
                    'updated_by' => $profile->id
                ]);

                return response()->json([
                    'status' => 'success',
                    'message' => 'Unit Data is created successfully',
                ]);
            }
            return response()->json(['message' => "token invalid, hanya bisa diakses oleh Supervisor Pusat!"], 401);
        } catch (Throwable $e) {
            report($e);
            $message = $e->getMessage();
            if ($message == 'Attempt to read property "id" on null') {
                return response()->json(['message' => "invalid Token!"], 401);
            }
        }
    }

    function listArea()
    {
        $list = AreaModel::select('id', 'nama_area as text')->whereIsActive(1)->get();
        return $list;
    }
    function listUnit()
    {
        $list = UnitModel::select('id', 'nama_unit as text')->whereIsActive(1)->get();
        return $list;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $unit = UnitModel::with('area')->find($id);
            return $unit;
        } catch (Throwable $e) {
            report($e);
            $message = $e->getMessage();
            if ($message == 'Attempt to read property "id" on null') {
                return response()->json(['message' => "invalid Token!"], 401);
            }
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'nama_unit' => 'required|string|max:255',
            'area_id' => 'required',
            'address' => 'required',
            'detail_address' => 'required',
            'lat' => 'required',
            'long' => 'required',
        ]);
        $profile = $this->profile()[0];

        if ($profile->role_name == "Supervisor Pusat") {
            // 'nama_unit', 'area_id', 'address', 'detail_address', 'lat', 'long', 'created_by', 'updated_by'
            $data = UnitModel::find($id);
            $data->nama_unit = $request->nama_unit;
            $data->area_id = $request->area_id;
            $data->address = $request->address;
            $data->detail_address = $request->detail_address;
            $data->lat = $request->lat;
            $data->long = $request->long;
            $data->updated_by = $profile->id;
            $request->is_active === null ? null : $data->is_active = $request->is_active;
            $data->save();

            return response()->json([
                'status' => 'success',
                'message' => 'Area updated successfully',
            ]);
        }
        return response()->json(['message' => "token invalid, hanya bisa diakses oleh Supervisor Pusat!"], 401);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    function profile()
    {
        $data = auth()->user();
        $user = User::where('users.id', $data->id)
            ->join('squads', function ($join) {
                return $join->on('squads.id', '=', 'users.squad_id');
            })
            ->join('role_users', function ($join) {
                return $join->on('role_users.id', '=', 'users.role_id');
            })
            ->select(
                'users.id',
                'users.name',
                'users.username',
                'users.email',
                'squads.squad_name',
                'squads.squad_unit',
                'squads.squad_address',
                'squads.squad_phone',
                'role_users.role_name'
            )->get();

        return $user;
    }
}
