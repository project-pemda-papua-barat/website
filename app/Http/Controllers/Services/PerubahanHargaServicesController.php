<?php

namespace App\Http\Controllers\Services;

use App\Http\Controllers\Controller;
use App\Models\AsetVII;
use App\Models\HargaPerolehan;
use App\Models\User;
use Illuminate\Http\Request;
use Throwable;

class PerubahanHargaServicesController extends Controller
{
    function __construct()
    {
        $this->middleware('auth:api');
    }

    function hargaByAset($id)
    {
        $data = HargaPerolehan::with('createdBy')->whereDetailAsetId($id)->get();
        return $data;
    }

    function search($aset)
    {
        $data = AsetVII::whereNamaAsetVii($aset)->with('update_harga')->get();
        return $data;
    }

    function lastUpdatePrice($id)
    {
        $data = HargaPerolehan::whereDetailAsetId($id)->latest()->first();
        return $data;
    }

    function store(Request $request)
    {
        $request->validate([
            'detail_aset_id' => 'required',
            'tanggal' => 'required|date_format:Y-m-d',
            'perubahan_harga' => 'required|numeric'
        ]);

        try {

        $profile = $this->profile()[0];

        if ($profile->role_name == "Supervisor Pusat" || $profile->role_name == "Supervisor Unit") {
            HargaPerolehan::create([
                'detail_aset_id' => $request->detail_aset_id,
                'tanggal' => $request->tanggal,
                'perubahan_harga' => $request->perubahan_harga,
                'created_by' => $profile->id,
                'updated_by' => $profile->id
            ]);
            return response()->json([
                'status' => 'success',
                'message' => 'Update Perubahan Harga Berhasil!',
            ]);
        }
        return response()->json(['message' => "token invalid, hanya bisa diakses oleh Supervisor Pusat atau Supervisor Unit!"], 401);

        } catch (Throwable $e) {
            report($e);
            $message = $e->getMessage();
            if ($message == 'Attempt to read property "id" on null') {
                return response()->json(['message' => "invalid Token!"], 401);
            }
        }
    }
}
