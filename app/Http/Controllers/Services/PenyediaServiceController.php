<?php

namespace App\Http\Controllers\Services;

use App\Http\Controllers\Controller;
use App\Models\PenyediaModel;
use App\Models\User;
use Illuminate\Http\Request;
use Throwable;
use Yajra\DataTables\Facades\DataTables;

class PenyediaServiceController extends Controller
{
    function __construct()
    {
        $this->middleware('auth:api');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $penyedia = PenyediaModel::orderBy('updated_at', 'DESC')->get();
            return DataTables::of($penyedia)->addColumn('action', function ($data) {
                $button = '<a href="#" onclick="editDataPenyedia(' . $data->id . ')" class="text-info m-2"><i class="fa fa-edit" aria-hidden="true"></i></a> <a  href="#" onclick="deleteDataPenyedia(' . $data->id . ')" class="text-danger m-2"><i class="fa fa-trash" aria-hidden="true"></i>
                </a>';
                return $button;
            })
                ->addIndexColumn()
                ->rawColumns(['action'])
                ->make(true);
        } catch (Throwable $e) {
            report($e);
            $message = $e->getMessage();
            if ($message == 'Attempt to read property "id" on null') {
                return response()->json(['message' => "invalid Token!"], 401);
            }
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'nama_penyedia' => 'required|string|max:255|unique:penyedia_models,nama_penyedia',
            'alamat_penyedia' => 'required|string',
            'no_contact_penyedia' => 'required|string|digits_between:9,14|unique:penyedia_models,no_contact_penyedia',
            'detail_alamat' => 'required',
            'lat' => 'required',
            'long' => 'required'
        ]);

        try {

            $profile = $this->profile()[0];

            if ($profile->role_name === "Supervisor Pusat") {


                PenyediaModel::create([
                    'nama_penyedia' => $request->nama_penyedia,
                    'alamat_penyedia' => $request->alamat_penyedia,
                    'no_contact_penyedia' => $request->no_contact_penyedia,
                    'detail_alamat' => $request->detail_alamat,
                    'lat' => $request->lat,
                    'long' => $request->long,
                    'created_by' => $profile->id,
                    'updated_by' => $profile->id,
                    'keterangan' => $request->keterangan,
                ]);

                return response()->json([
                    'status' => 'success',
                    'message' => 'Penyedia created successfully',
                ]);
            }
            return response()->json(['message' => "token invalid, hanya bisa diakses oleh Supervisor Pusat!"], 401);
        } catch (Throwable $e) {
            report($e);
            $message = $e->getMessage();
            if ($message == 'Attempt to read property "id" on null') {
                return response()->json(['message' => "invalid Token!"], 401);
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $penyedia = PenyediaModel::find($id);
            return $penyedia;
        } catch (Throwable $e) {
            report($e);
            $message = $e->getMessage();
            if ($message == 'Attempt to read property "id" on null') {
                return response()->json(['message' => "invalid Token!"], 401);
            }
        }
    }

    function list()
    {
        $data = PenyediaModel::select('id', 'nama_penyedia as text')->get();
        return $data;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'nama_penyedia' => 'required|string',
            'alamat_penyedia' => 'required|string',
            'no_contact_penyedia' => 'required|string|digits_between:9,14',
            'detail_alamat' => 'required',
            'lat' => 'required',
            'long' => 'required'
        ]);

        try {

            $profile = $this->profile()[0];

            if ($profile->role_name === "Supervisor Pusat") {

                $data = PenyediaModel::find($id);
                $data->nama_penyedia = $request->nama_penyedia;
                $data->alamat_penyedia = $request->alamat_penyedia;
                $data->no_contact_penyedia = $request->no_contact_penyedia;
                $data->detail_alamat = $request->detail_alamat;
                $data->lat = $request->lat;
                $data->long = $request->long;
                $data->keterangan = $request->keterangan;
                $data->updated_by = $profile->id;
                $request->is_active === null ? null : $data->is_active = $request->is_active;
                $data->save();

                return response()->json([
                    'status' => 'success',
                    'message' => 'Penyedia updated successfully',
                ]);
            }
            return response()->json(['message' => "token invalid, hanya bisa diakses oleh Superviser Pusat!"], 401);
        } catch (Throwable $e) {
            report($e);
            $message = $e->getMessage();
            if ($message == 'Attempt to read property "id" on null') {
                return response()->json(['message' => "invalid Token!"], 401);
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {

            $profile = $this->profile()[0];

            if ($profile->role_name === "Supervisor Pusat") {

                $penyedia = PenyediaModel::find($id);
                $penyedia->delete();

                return response()->json([
                    'status' => 'success',
                    'message' => 'Penyedia deleted successfully',
                ]);
            }
            return response()->json(['message' => "token invalid, hanya bisa diakses oleh Supervisor Pusat!"], 401);
        } catch (Throwable $e) {
            report($e);
            $message = $e->getMessage();
            if ($message == 'Attempt to read property "id" on null') {
                return response()->json(['message' => "invalid Token!"], 401);
            }
        }
    }

    function profile()
    {
        $data = auth()->user();
        $user = User::where('users.id', $data->id)
            ->join('squads', function ($join) {
                return $join->on('squads.id', '=', 'users.squad_id');
            })
            ->join('role_users', function ($join) {
                return $join->on('role_users.id', '=', 'users.role_id');
            })
            ->select(
                'users.id',
                'users.name',
                'users.username',
                'users.email',
                'squads.squad_name',
                'squads.squad_unit',
                'squads.squad_address',
                'squads.squad_phone',
                'role_users.role_name'
            )->get();

        return $user;
    }
}
