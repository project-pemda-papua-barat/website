<?php

namespace App\Http\Controllers\Services;

use App\Http\Controllers\Controller;
use App\Models\AsetIV;
use Illuminate\Http\Request;
use Throwable;
use Yajra\DataTables\Facades\DataTables;

class AsetIVServiceController extends Controller
{
    function __construct()
    {
        $this->middleware('auth:api');
    }

    public function index($id_asetI, $id_asetII, $id_asetIII)
    {
        try {
            $aset = AsetIV::where('asetI_id', $id_asetI)->where('asetII_id', $id_asetII)->where('asetIII_id', $id_asetIII)->get();
            return DataTables::of($aset)->addColumn('action', function ($data) {
                $button = '<a href="#" onclick="editAsetIVShow(' . $data->id . ')" class="text-info m-2"><i class="fa fa-edit" aria-hidden="true"></i></a>';
                return $button;
            })
                ->addIndexColumn()
                ->rawColumns(['action'])
                ->make(true);
        } catch (Throwable $e) {
            report($e);
            $message = $e->getMessage();
            if ($message == 'Attempt to read property "id" on null') {
                return response()->json(['message' => "invalid Token!"], 401);
            }
        }
    }

    public function store(Request $request)
    {
        $request->validate([
            'asetI_id' => 'required',
            'asetII_id' => 'required',
            'asetIII_id' => 'required',
            'kode_aset_iv' => 'required',
            'nama_aset_iv' => 'required',
        ]);

        try {

            $profile = $this->profile()[0];
            // return $profile;

            if ($profile->role_name === "Supervisor Pusat") {

                AsetIV::create([
                    'asetI_id' => $request->asetI_id,
                    'asetII_id' => $request->asetII_id,
                    'asetIII_id' => $request->asetIII_id,
                    'kode_aset_iv' => $request->kode_aset_iv,
                    'nama_aset_iv' => $request->nama_aset_iv
                ]);


                return response()->json([
                    'status' => 'success',
                    'message' => 'Aset IV created successfully',
                ]);
            }
            return response()->json(['message' => "token invalid, hanya bisa diakses oleh Supervisor Pusat!"], 401);
        } catch (Throwable $e) {
            report($e);
            $message = $e->getMessage();
            if ($message == 'Attempt to read property "id" on null') {
                return response()->json(['message' => "invalid Token!"], 401);
            }
        }
    }
}
