<?php

namespace App\Http\Controllers\Services;

use App\Http\Controllers\Controller;
use App\Models\StockIn;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Throwable;
use Illuminate\Support\Facades\File;


class StockInServiceController extends Controller
{
    function __construct()
    {
        $this->middleware('auth:api');
        $this->onHand = new StockIn();
    }

    protected function index()
    {
        try {
            $profile = $this->profile()[0];
            if ($profile->role_name == "User Unit" || "Supervisor Unit") {
                $data = $this->onHand?->onDetailHandTrxAll($profile->unit_id);
                return $data;
            }

            return response()->json(['message' => "token invalid, hanya bisa diakses oleh Admin Unit atau Supervisor Unit!"], 401);
        } catch (Throwable $e) {
            $message = $e->getMessage();
            if ($message == 'Attempt to read property "id" on null') {
                return response()->json(['message' => "invalid Token!"], 401);
            }
        }
    }

    protected function detail($id)
    {
        $data = $this->onHand->find($id);
        return response()->json($data);
    }

    protected function detailStockIn($no_trx)
    {
        try {
            $profile = $this->profile()[0];
            if ($profile->role_name == "User Unit") {
                $data = $this->onHand?->onDetailHandTrx($no_trx, $profile->unit_id);
                if (count($data) == 0) {
                    return response()->json(['message' => 'Data not Found!'], 404);
                }
                return $data;
            }

            return response()->json(['message' => "token invalid, hanya bisa diakses oleh Admin Unit!"], 401);
        } catch (Throwable $e) {
            $message = $e->getMessage();
            if ($message == 'Attempt to read property "id" on null') {
                return response()->json(['message' => "invalid Token!"], 401);
            }
        }
    }

    protected function editStockIn(Request $request, $id)
    {
        $request->validate([
            'tahun_pengadaan' => 'required',
            'no_berita_acara' => 'required',
            'no_dokumen_pengiriman' => 'required',
            'tanggal_dokumen' => 'required',
            'jenis_id' => 'required',
            'distributor_id' => 'required',
            'sumber_dana_id' => 'required',
            'dokumen_berita_acara' => 'mimes:pdf|max:2048',
            'dokumen_expedisi' => 'mimes:pdf|max:2048',
            'dokumen_pendukung' => 'mimes:pdf|max:2048',
        ]);

        try {
            $profile = $this->profile()[0];
            if ($profile->role_name == "User Unit") {

                $dokumen_berita_acara = $request->file('dokumen_berita_acara');
                $dokumen_berita_acara_ext = $dokumen_berita_acara === null ? null : 'dokumen_berita_acara_' . Carbon::now()->format('ymdHis') . '.' . $dokumen_berita_acara->extension();

                $dokumen_expedisi = $request->file('dokumen_expedisi');
                $dokumen_expedisi_ext = $dokumen_expedisi === null ? null : 'dokumen_expedisi_' . Carbon::now()->format('ymdHis') . '.' . $dokumen_expedisi->extension();

                $dokumen_pendukung = $request->file('dokumen_pendukung');
                $dokumen_pendukung_ext = $dokumen_pendukung === null ? null : 'dokumen_pendukung_' . Carbon::now()->format('ymdHis') . '.' . $dokumen_pendukung->extension();

                $data = $this->onHand?->find($id);
                $data->tahun_pengadaan = $request->tahun_pengadaan;
                $data->no_berita_acara = $request->no_berita_acara;
                $data->no_dokumen_pengiriman = $request->no_dokumen_pengiriman;
                $data->tanggal_dokumen = $request->tanggal_dokumen;
                $data->jenis_id = $request->jenis_id;
                $data->distributor_id = $request->distributor_id;
                $data->sumber_dana_id = $request->sumber_dana_id;
                $data->keterangan = $request->keterangan;

                if ($dokumen_berita_acara != null) {
                    $dokumen_berita_acara->move(public_path('upload/aset_dokumen'),  $data->dokumen_berita_acara == null ? $dokumen_berita_acara_ext : $data->dokumen_berita_acara);

                    if ($data->dokumen_berita_acara == null) {
                        $data->dokumen_berita_acara = $dokumen_berita_acara_ext;
                    }
                }

                if ($dokumen_expedisi != null) {
                    $dokumen_expedisi->move(public_path('upload/aset_dokumen'),  $data->dokumen_expedisi == null ? $dokumen_expedisi_ext : $data->dokumen_expedisi);

                    if ($data->dokumen_expedisi == null) {
                        $data->dokumen_expedisi = $dokumen_expedisi_ext;
                    }
                }

                if ($dokumen_pendukung != null) {
                    $dokumen_pendukung->move(public_path('upload/aset_dokumen'),  $data->dokumen_pendukung == null ? $dokumen_pendukung_ext : $data->dokumen_pendukung);

                    if ($data->dokumen_pendukung == null) {
                        $data->dokumen_pendukung = $dokumen_pendukung_ext;
                    }
                }

                $data->save();

                return response()->json(['message' => "Stocked (nomor transaksi) edited successfully!"], 200);
            }

            return response()->json(['message' => "token invalid, hanya bisa diakses oleh Admin Unit!"], 401);
        } catch (Throwable $e) {
            $message = $e->getMessage();
            if ($message == 'Attempt to read property "id" on null') {
                return response()->json(['message' => "invalid Token!"], 401);
            }
        }
    }

    protected function store(Request $request)
    {
        $request->validate([
            'tahun_pengadaan' => 'required',
            'no_transaksi' => 'required',
            'no_berita_acara' => 'required',
            'no_dokumen_pengiriman' => 'required',
            'tanggal_dokumen' => 'required',
            'jenis_id' => 'required',
            'distributor_id' => 'required',
            'sumber_dana_id' => 'required',
            'dokumen_berita_acara' => 'mimes:pdf|max:2048',
            'dokumen_expedisi' => 'mimes:pdf|max:2048',
            'dokumen_pendukung' => 'mimes:pdf|max:2048',
        ]);

        try {

            $profile = $this->profile()[0];
            $dokumen_berita_acara = $request->file('dokumen_berita_acara');
            $dokumen_berita_acara_ext = $dokumen_berita_acara === null ? null : 'dokumen_berita_acara_' . Carbon::now()->format('ymdHis') . '.' . $dokumen_berita_acara->extension();

            $dokumen_expedisi = $request->file('dokumen_expedisi');
            $dokumen_expedisi_ext = $dokumen_expedisi === null ? null : 'dokumen_expedisi_' . Carbon::now()->format('ymdHis') . '.' . $dokumen_expedisi->extension();

            $dokumen_pendukung = $request->file('dokumen_pendukung');
            $dokumen_pendukung_ext = $dokumen_pendukung === null ? null : 'dokumen_pendukung_' . Carbon::now()->format('ymdHis') . '.' . $dokumen_pendukung->extension();


            if ($profile->role_name == "User Unit") {
                StockIn::create([
                    'tahun_pengadaan' => $request->tahun_pengadaan,
                    'no_transaksi' => $request->no_transaksi,
                    'no_berita_acara' => $request->no_berita_acara,
                    'no_dokumen_pengiriman' => $request->no_dokumen_pengiriman,
                    'tanggal_dokumen' => $request->tanggal_dokumen,
                    'jenis_id' => $request->jenis_id,
                    'distributor_id' => $request->distributor_id,
                    'sumber_dana_id' => $request->sumber_dana_id,
                    'dokumen_berita_acara' => $dokumen_berita_acara_ext,
                    'dokumen_expedisi' => $dokumen_expedisi_ext,
                    'dokumen_pendukung' => $dokumen_pendukung_ext,
                    'is_status' => 'can deleted',
                    'keterangan' => $request->keterangan,
                    'created_by' => $profile->id,
                    'updated_by' => $profile->id,
                    'user_id' => $profile->id,
                    'unit_id' => $profile->unit_id
                ]);

                if ($dokumen_berita_acara != null) {
                    $dokumen_berita_acara->move(public_path('upload/aset_dokumen'), $dokumen_berita_acara_ext);
                }

                if ($dokumen_expedisi != null) {
                    $dokumen_expedisi->move(public_path('upload/aset_dokumen'),  $dokumen_expedisi_ext);
                }

                if ($dokumen_pendukung != null) {
                    $dokumen_pendukung->move(public_path('upload/aset_dokumen'),  $dokumen_pendukung_ext);
                }


                return response()->json([
                    'status' => 'success',
                    'message' => 'Stocked (nomor transaksi) created successfully'
                ]);
            }

            return response()->json(['message' => "token invalid, hanya bisa diakses oleh Admin Unit!"], 401);
        } catch (Throwable $e) {
            report($e);
            $message = $e->getMessage();
            if ($message == 'Attempt to read property "id" on null') {
                return response()->json(['message' => "invalid Token!"], 401);
            }
        }
    }

    protected function destroy($id)
    {
        try {

            $profile = $this->profile()[0];

            if ($profile->role_name === "User Unit") {

                $data = $this->onHand->find($id);
                $path = public_path() . '/upload/aset_dokumen/' . $data->dokumen_berita_acara;
                $path2 = public_path() . '/upload/aset_dokumen/' . $data->dokumen_expedisi;
                $path3 = public_path() . '/upload/aset_dokumen/' . $data->dokumen_pendukung;

                if ($data->is_status === 'waiting approval' || $data->is_status === 'approved') {
                    return response()->json(["message" => "Status Stock In Sudah Tidak bisa dihapus!"], 422);
                }

                if (isset($path)) File::delete($path);
                if (isset($path2)) File::delete($path2);
                if (isset($path3)) File::delete($path3);

                $data->delete();
                return response()->json([
                    'status' => 'success',
                    'message' => 'Stock In Data deleted successfully',
                ]);
            }
            return response()->json(['message' => "token invalid, hanya bisa diakses oleh User Unit!"], 401);
        } catch (Throwable $e) {
            report($e);
            $message = $e->getMessage();
            if ($message == 'Attempt to read property "id" on null') {
                return response()->json(['message' => "invalid Token!"], 401);
            }
        }
    }
}
