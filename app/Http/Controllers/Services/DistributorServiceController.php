<?php

namespace App\Http\Controllers\Services;

use App\Http\Controllers\Controller;
use App\Models\DistributorModel;
use App\Models\User;
use Illuminate\Http\Request;
use Throwable;
use Yajra\DataTables\Facades\DataTables;

class DistributorServiceController extends Controller
{
    function __construct()
    {
        $this->middleware('auth:api');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $penyedia = DistributorModel::orderBy('updated_at', 'DESC')->get();
            return DataTables::of($penyedia)->addColumn('action', function ($data) {
                $button = '<a href="#" onclick="editDataDistributor(' . $data->id . ')" class="text-info m-2">
                <i class="fa fa-edit" aria-hidden="true"></i>
                </a><a href="#" onclick="deleteDataDistributor(' . $data->id . ')" class="text-danger m-2"><i class="fa fa-trash" aria-hidden="true"></i>
                </a>';
                return $button;
            })
                ->addIndexColumn()
                ->rawColumns(['action'])
                ->make(true);
        } catch (Throwable $e) {
            report($e);
            $message = $e->getMessage();
            if ($message == 'Attempt to read property "id" on null') {
                return response()->json(['message' => "invalid Token!"], 401);
            }
        }
    }

    function list()
    {
        $data = DistributorModel::select('id', 'nama_distributor as text')->get();
        return $data;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'nama_distributor' => 'required|string|max:255|unique:distributor_models,nama_distributor',
            'alamat_distributor' => 'required|string',
            'no_contact_distributor' => 'required|string|digits_between:9,14|unique:distributor_models,no_contact_distributor',
            'detail_alamat' => 'required',
            'lat' => 'required',
            'long' => 'required',
        ]);

        try {

            $profile = $this->profile()[0];

            if ($profile->role_name === "Supervisor Pusat") {


                DistributorModel::create([
                    'nama_distributor' => $request->nama_distributor,
                    'alamat_distributor' => $request->alamat_distributor,
                    'no_contact_distributor' => $request->no_contact_distributor,
                    'detail_alamat' => $request->detail_alamat,
                    'lat' => $request->lat,
                    'long' => $request->long,
                    'created_by' => $profile->id,
                    'updated_by' => $profile->id,
                    'keterangan' => $request->keterangan,
                ]);

                return response()->json([
                    'status' => 'success',
                    'message' => 'distributor created successfully',
                ]);
            }
            return response()->json(['message' => "token invalid, hanya bisa diakses oleh Supervisor Pusat!"], 401);
        } catch (Throwable $e) {
            report($e);
            $message = $e->getMessage();
            if ($message == 'Attempt to read property "id" on null') {
                return response()->json(['message' => "invalid Token!"], 401);
            }
        }
    }



    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $penyedia = DistributorModel::find($id);
            return $penyedia;
        } catch (Throwable $e) {
            report($e);
            $message = $e->getMessage();
            if ($message == 'Attempt to read property "id" on null') {
                return response()->json(['message' => "invalid Token!"], 401);
            }
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'nama_distributor' => 'required|string',
            'alamat_distributor' => 'required|string',
            'no_contact_distributor' => 'required|string|digits_between:9,14',
            'detail_alamat' => 'required',
            'lat' => 'required',
            'long' => 'required',
        ]);

        try {

            $profile = $this->profile()[0];

            if ($profile->role_name === "Supervisor Pusat") {

                $data = DistributorModel::find($id);
                $data->nama_distributor = $request->nama_distributor;
                $data->alamat_distributor = $request->alamat_distributor;
                $data->no_contact_distributor = $request->no_contact_distributor;
                $data->detail_alamat = $request->detail_alamat;
                $data->lat = $request->lat;
                $data->long = $request->long;
                $data->keterangan = $request->keterangan;
                $data->updated_by = $profile->id;
                $request->is_active === null ? null : $data->is_active = $request->is_active;
                $data->save();

                return response()->json([
                    'status' => 'success',
                    'message' => 'distributor updated successfully',
                ]);
            }
            return response()->json(['message' => "token invalid, hanya bisa diakses oleh Super User!"], 401);
        } catch (Throwable $e) {
            report($e);
            $message = $e->getMessage();
            if ($message == 'Attempt to read property "id" on null') {
                return response()->json(['message' => "invalid Token!"], 401);
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {

            $profile = $this->profile()[0];

            if ($profile->role_name === "Supervisor Pusat") {

                $penyedia = DistributorModel::find($id);
                $penyedia->delete();

                return response()->json([
                    'status' => 'success',
                    'message' => 'Distributor deleted successfully',
                ]);
            }
            return response()->json(['message' => "token invalid, hanya bisa diakses oleh Supervisor Pusat!"], 401);
        } catch (Throwable $e) {
            report($e);
            $message = $e->getMessage();
            if ($message == 'Attempt to read property "id" on null') {
                return response()->json(['message' => "invalid Token!"], 401);
            }
        }
    }
}
