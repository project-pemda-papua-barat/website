<?php

namespace App\Http\Controllers\Services;

use App\Http\Controllers\Controller;
use App\Models\MasterAsetStock;
use App\Models\StockedCenter;
use App\Models\StockOut;
use App\Models\StockOutDetail;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Throwable;
use Illuminate\Support\Facades\File;

class StockoutServiceController extends Controller
{
    function __construct()
    {
        $this->middleware('auth:api');
        $this->outHand = new StockOut();
        $this->outHandDetail = new StockOutDetail();
        $this->stockedData = new StockedCenter();
        $this->masterStocked = new MasterAsetStock();
    }

    protected function index($no_trx)
    {
        try {
            $profile = $this->profile()[0];
            if ($profile->role_name == "User Unit" || "Supervisor Unit") {
                $data = $this->outHand?->outDetailHandTrxAll($profile->unit_id, $no_trx);
                return $data;
            }

            return response()->json(['message' => "token invalid, hanya bisa diakses oleh Admin Unit atau Supervisor Unit!"], 401);
        } catch (Throwable $e) {
            $message = $e->getMessage();
            if ($message == 'Attempt to read property "id" on null') {
                return response()->json(['message' => "invalid Token!"], 401);
            }
        }
    }

    protected function storeStatus(Request $request)
    {
        $request->validate([
            'is_status' => 'required|string',
            'stock_out_id' => 'required'
        ]);

        try {
            $profile = $this->profile()[0];


            if ($profile->role_name == "User Unit") {
                $data = $this->outHand->find($request->stock_out_id);

                if ($data == null) {
                    return response()->json(['message' => 'data tidak tersedia'], 404);
                }

                if ($data->dokumen_berita_acara == null) {
                    return response()->json(['message' => 'minimal upload dokumen berita acara terlebih dahulu'], 404);
                }

                $detail = $this->outHandDetail->whereStockOutId($request->stock_out_id)->get();

                if($detail->count() === 0){
                    return response()->json(['message' => 'Anda belum menambahkan detail Aset!'], 404);
                }

                if ($data->is_status === "can deleted" || $data->is_status === "harus direvisi") {
                    $data->is_status = $request->is_status;
                    $data->save();

                    return response()->json(['message' => 'Stock in berhasil diupdate menunggu review atau approve spv unit! ']);
                }

                return response()->json(['message' => 'data tidak bisa diubah', 'user' => 'Unit Admin'], 401);
            }

            if ($profile->role_name = "Supervisor Unit") {
                $data = $this->outHand->find($request->stock_out_id);

                if ($data == null) {
                    return response()->json(['message' => 'data tidak tersedia'], 404);
                }

                if ($data->is_status === 'waiting approval') {

                    if ($request->is_status === 'approved') {
                        return $this->storeToMasterStockInStockOut($data);
                    }

                    $data->is_status = $request->is_status;
                    $data->keterangan = $request->keterangan;
                    $data->save();

                    return response()->json(['message' => 'Stock out berhasil diupdate menunggu direvisi admin unit! ']);
                }

                return response()->json(['message' => 'data tidak bisa diubah', 'user' => 'Spv Unit'], 401);
            }

            return response()->json(['message' => "token invalid, hanya bisa diakses oleh Admin Unit atau Supervisor Unit!"], 401);
        } catch (Throwable $e) {
            $message = $e->getMessage();
            if ($message == 'Attempt to read property "id" on null') {
                return response()->json(['message' => "invalid Token!"], 401);
            }
        }
    }

    protected function storeToMasterStockInStockOut($data)
    {
        // return $data;
        $detailData = $this->outHandDetail->whereStockOutId($data->id)->get();
        $statusQuantity = [];

        // cek jumlah stock tersedia di stocked data
        foreach($detailData as $detail){
            $dataStock = $this->stockedData->find($detail->stocked_center_id);
            $stocked = $dataStock->qty - $detail->qty_out;
            array_push($statusQuantity, $stocked );
        }
        
        // jika qty available
        $chekQty = Arr::where($statusQuantity, function($value){
            return $value < 0;
        });
        

        if(count($chekQty) > 0){
            // terdapat stock yang kurang atau kosong!
            return response()->json(['message' => 'detail wajib direvisi! terdapat stok detail aset yang kurang dari request atau habis!', 'user' => 'Spv Unit'], 401);
        } else {
            $push = [];
            foreach($detailData as $detail){
                $dataStock = $this->stockedData->find($detail->stocked_center_id);
                $dataStock->qty = $dataStock->qty - $detail->qty_out;
                $dataStock->save();

                // nanti tambah store ke master aset
                $this->masterStocked::create([
                    "detail_asets_id" => $dataStock->detail_asets_id,
                    "jenis_id" => $dataStock->jenis_id,
                    "satuan_id" => $dataStock->satuan_id,
                    "penyedia_id" => $dataStock->penyedia_id,
                    'sumber_dana_id' => $dataStock->sumber_dana_id,
                    'distributor_id' => $dataStock->distributor_id,
                    'from_unit_id' => $dataStock->from_unit_id,
                    'to_unit_id' => $data->to_unit_id,
                    'user_id' =>$data->user_id,
                    'no_transaksi' => $data->no_transaksi,
                    'kode' => $dataStock->kode,
                    'batch' => $dataStock->batch,
                    'expired_date' => $dataStock->expired_date, 
                    'harga_satuan' => $dataStock->harga_satuan,
                    'tahun_pengadaan' => 2002,
                    'created_by' => $data->created_by,
                    'updated_by' => $data->updated_by,
                    'qty' => -$detail->qty_out
                ]);
                
            }
    
            $dataStockOut = $this->outHand->find($data->id);
            $dataStockOut->keterangan = "approved";
            $dataStockOut->is_status = "approved";
            $dataStockOut->save();

            return response()->json(['message' => 'Approved! OnHand berhasil Update master Stok! ']);
        }

    }


    protected function update(Request $request, $id)
    {
        $request->validate([
            'no_berita_acara' => 'required',
            'no_dokumen_pengiriman' => 'required',
            'tanggal_dokumen' => 'required',
            'distributor_id' => 'required',
            'dokumen_berita_acara' => 'mimes:pdf|max:2048',
            'dokumen_expedisi' => 'mimes:pdf|max:2048',
            'dokumen_pendukung' => 'mimes:pdf|max:2048',
            'to_unit_id' => 'required',
        ]);

        try {

            $profile = $this->profile()[0];
            $dokumen_berita_acara = $request->file('dokumen_berita_acara');
            $dokumen_expedisi = $request->file('dokumen_expedisi');
            $dokumen_pendukung = $request->file('dokumen_pendukung');


            if ($profile->role_name == "User Unit") {

                $edit = $this->outHand->find($id);
                $edit->no_berita_acara = $request->no_berita_acara;
                $edit->no_dokumen_pengiriman = $request->no_dokumen_pengiriman;
                $edit->tanggal_dokumen= $request->tanggal_dokumen;
                $edit->distributor_id= $request->distributor_id;
                $edit->keterangan= $request->keterangan;
                $edit->updated_by= $profile->id;
                $edit->user_id= $profile->id;
                $edit->to_unit_id= $request->to_unit_id;
                $edit->keterangan = $request->keterangan;

                
                if($dokumen_berita_acara !== null){
                    if($edit->dokumen_berita_acara === null) {
                        $name =  'dokumen_berita_acara_stok_keluar_' . Carbon::now()->format('ymdHis') . '.' . $dokumen_berita_acara->extension();
                        $edit->dokumen_berita_acara = $name;
                       $dokumen_berita_acara->move(public_path('upload/aset_dokumen'),  $name);
                    } else {
                        $dokumen_berita_acara->move(public_path('upload/aset_dokumen'),  $edit->dokumen_berita_acara);
                    }
                }

                if($dokumen_expedisi !== null){
                    if($edit->dokumen_expedisi === null) {
                        $name2 =  'dokumen_expedisi_stok_keluar_' . Carbon::now()->format('ymdHis') . '.' . $dokumen_expedisi->extension();
                        $edit->dokumen_expedisi = $name2;
                       $dokumen_expedisi->move(public_path('upload/aset_dokumen'),  $name2);
                    } else {
                        $dokumen_expedisi->move(public_path('upload/aset_dokumen'),  $edit->dokumen_expedisi);
                    }
                }

                if($dokumen_pendukung !== null){
                    if($edit->dokumen_pendukung === null) {
                        $name3 =  'dokumen_pendukung_stok_keluar_' . Carbon::now()->format('ymdHis') . '.' . $dokumen_pendukung->extension();
                        $edit->dokumen_pendukung = $name3;
                       $dokumen_pendukung->move(public_path('upload/aset_dokumen'),  $name3);
                    } else {
                        $dokumen_pendukung->move(public_path('upload/aset_dokumen'),  $edit->dokumen_pendukung);
                    }
                }

                $edit->save();

                return response()->json([
                    'status' => 'success',
                    'message' => 'Stocked (nomor transaksi keluar) edited successfully'
                ]);
            }

            return response()->json(['message' => "token invalid, hanya bisa diakses oleh Admin Unit!"], 401);
        } catch (Throwable $e) {
            report($e);
            $message = $e->getMessage();
            if ($message == 'Attempt to read property "id" on null') {
                return response()->json(['message' => "invalid Token!"], 401);
            }
        }
    }

    protected function destroy($id)
    {
        try {

            $profile = $this->profile()[0];

            if ($profile->role_name === "User Unit") {

                $data = $this->outHand->find($id);
                $path = public_path() . '/upload/aset_dokumen/' . $data->dokumen_berita_acara;
                $path2 = public_path() . '/upload/aset_dokumen/' . $data->dokumen_expedisi;
                $path3 = public_path() . '/upload/aset_dokumen/' . $data->dokumen_pendukung;

                if ($data->is_status === 'waiting approval' || $data->is_status === 'approved') {
                    return response()->json(["message" => "Status Stock In Sudah Tidak bisa dihapus!"], 422);
                }

                $path != null  ?? File::delete($path);
                $path2 != null ?? File::delete($path2);
                $path3 != null ?? File::delete($path3);

                $data->delete();
                return response()->json([
                    'status' => 'success',
                    'message' => 'Stock out Data deleted successfully',
                ]);
            }
            return response()->json(['message' => "token invalid, hanya bisa diakses oleh User Unit!"], 401);
        } catch (Throwable $e) {
            report($e);
            $message = $e->getMessage();
            if ($message == 'Attempt to read property "id" on null') {
                return response()->json(['message' => "invalid Token!"], 401);
            }
        }
    }

    protected function detail($id)
    {
        $data = $this->outHand->find($id);
        return response()->json($data);
    }

    protected function unit()
    {
        try {
            $profile = $this->profile()[0];
            if ($profile->role_name == "User Unit" || "Supervisor Unit") {
                $data = $this->outHand?->outHandTrxUnit($profile->unit_id);
                return $data;
            }

            return response()->json(['message' => "token invalid, hanya bisa diakses oleh Admin Unit atau Supervisor Unit!"], 401);
        } catch (Throwable $e) {
            $message = $e->getMessage();
            if ($message == 'Attempt to read property "id" on null') {
                return response()->json(['message' => "invalid Token!"], 401);
            }
        }
    }

    protected function store(Request $request)
    {
        $request->validate([
            'no_transaksi' => 'required',
            'no_berita_acara' => 'required',
            'no_dokumen_pengiriman' => 'required',
            'tanggal_dokumen' => 'required',
            'distributor_id' => 'required',
            'dokumen_berita_acara' => 'mimes:pdf|max:2048',
            'dokumen_expedisi' => 'mimes:pdf|max:2048',
            'dokumen_pendukung' => 'mimes:pdf|max:2048',
            'to_unit_id' => 'required',
        ]);

        try {

            $profile = $this->profile()[0];
            $dokumen_berita_acara = $request->file('dokumen_berita_acara');
            $dokumen_berita_acara_ext =   $dokumen_berita_acara === null ? null : 'dokumen_berita_acara_stok_keluar' . Carbon::now()->format('ymdHis') . '.' . $dokumen_berita_acara->extension();
            $dokumen_expedisi = $request->file('dokumen_expedisi');
            $dokumen_expedisi_ext =  $dokumen_expedisi === null ? null : 'dokumen_expedisi_stok_keluar' . Carbon::now()->format('ymdHis') . '.' . $dokumen_expedisi->extension();
            $dokumen_pendukung = $request->file('dokumen_pendukung');
            $dokumen_pendukung_ext =  $dokumen_pendukung === null ? null : 'dokumen_pendukung_stok_keluar' . Carbon::now()->format('ymdHis') . '.' . $dokumen_pendukung->extension();


            if ($profile->role_name == "User Unit") {

                $this->outHand->create([
                    'no_transaksi' => $request->no_transaksi,
                    'no_berita_acara' => $request->no_berita_acara,
                    'no_dokumen_pengiriman' => $request->no_dokumen_pengiriman,
                    'tanggal_dokumen' => $request->tanggal_dokumen,
                    'distributor_id' => $request->distributor_id,
                    'dokumen_berita_acara' => $dokumen_berita_acara_ext,
                    'dokumen_expedisi' => $dokumen_expedisi_ext,
                    'dokumen_pendukung' => $dokumen_pendukung_ext,
                    'is_status' => 'can deleted',
                    'keterangan' => $request->keterangan,
                    'created_by' => $profile->id,
                    'updated_by' => $profile->id,
                    'user_id' => $profile->id,
                    'from_unit_id' => $profile->unit_id,
                    'to_unit_id' => $request->to_unit_id
                ]);

                $dokumen_berita_acara === null ? null : $dokumen_berita_acara->move(public_path('upload/aset_dokumen'), $dokumen_berita_acara_ext);
                $dokumen_expedisi === null ? null : $dokumen_expedisi->move(public_path('upload/aset_dokumen'), $dokumen_expedisi_ext);
                $dokumen_pendukung === null ? null : $dokumen_pendukung->move(public_path('upload/aset_dokumen'), $dokumen_pendukung_ext);


                return response()->json([
                    'status' => 'success',
                    'message' => 'Stocked (nomor transaksi keluar) created successfully'
                ]);
            }

            return response()->json(['message' => "token invalid, hanya bisa diakses oleh Admin Unit!"], 401);
        } catch (Throwable $e) {
            report($e);
            $message = $e->getMessage();
            if ($message == 'Attempt to read property "id" on null') {
                return response()->json(['message' => "invalid Token!"], 401);
            }
        }
    }
}
