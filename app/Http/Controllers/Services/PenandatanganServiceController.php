<?php

namespace App\Http\Controllers\Services;

use App\Http\Controllers\Controller;
use App\Models\PenandaTanganModel;
use Illuminate\Http\Request;
use Throwable;

class PenandatanganServiceController extends Controller
{
    function __construct()
    {
        $this->middleware('auth:api');
        $this->signer = new PenandaTanganModel();
    }

    protected function storeToPaperConfig($id)
    {
        try {
            $profile = $this->profile()[0];

            if ($profile->role_name === "Supervisor Unit") {
                $storeData = $this->signer->find($id);
                $jabatanData = $this->signer->whereJabatan($storeData->jabatan)->get();

                foreach($jabatanData as $jabatan) {
                    $dataBaru = $this->signer->find($jabatan->id );

                    if($jabatan->id == $id){
                        $dataBaru->selected = 'true';
                        $dataBaru->save();
                    }
                    
                    if($jabatan->id !=   $id){
                        $dataBaru->selected = 'false';
                        $dataBaru->save();
                    }
                }

                return response()->json([
                    'message' => $storeData->nama_penanda_tangan. " berhasil di assign ke penanda tangan dengan jabatannya sebagai " . $storeData->jabatan], 200);
            }
            return response()->json(['message' => "token invalid, hanya bisa diakses oleh Supervisor Unit!"], 401);

        } catch (Throwable $e) {
            report($e);
            $message = $e->getMessage();
            if ($message == 'Attempt to read property "id" on null') {
                return response()->json(['message' => "invalid Token!"], 401);
            }
        }
    }

    protected function find($jabatan)
    {
        try {
            $profile = $this->profile()[0];
           
            if ($profile->role_name === "Supervisor Unit") {
                $storeData = $this->signer->whereJabatan($jabatan)->whereUnitId($profile->unit_id)->get();
                return $storeData;
            }
            return response()->json(['message' => "token invalid, hanya bisa diakses oleh Supervisor Unit!"], 401);

        } catch (Throwable $e) {
            report($e);
            $message = $e->getMessage();
            if ($message == 'Attempt to read property "id" on null') {
                return response()->json(['message' => "invalid Token!"], 401);
            }
        }
    }

    protected function store(Request $request)
    {
        $request->validate([
            'nama_penanda_tangan' => 'required|string|min:3|max:100',
            'nip' => 'required|min:10|max:25|unique:penanda_tangan_models,nip',
            'jabatan' => 'required',
        ]);

        try {
            $profile = $this->profile()[0];
            $response = response()->json([
                'status' => 'success',
                'message' => 'Penandatangan Baru berhasil dibuat',
            ]);
            $notUniqe = response()->json([
                'status' => 'fail',
                'message' => 'Penandatangan ini sudah tersedia',
            ], 422);

            if ($profile->role_name === "Supervisor Unit") {
                $countData = $this->signer->whereUniqueId($request->nama_penanda_tangan . $request->nip . $request->jabatan)->get();
                $storeData = $this->signer->whereJabatan($request->jabatan)->get();

                if (count($countData) > 0) {
                    return $notUniqe;
                }

                if (count($storeData) > 0) {
                    $this->signer->create([
                        'nama_penanda_tangan' => $request->nama_penanda_tangan,
                        'nip' => $request->nip,
                        'jabatan' => $request->jabatan,
                        'unique_id' => $request->nama_penanda_tangan . $request->nip . $request->jabatan,
                        'created_by' => $profile->id,
                        'updated_by' => $profile->id,
                        'unit_id' => $profile->unit_id,
                        'selected' => 'false'
                    ]);

                    return $response;
                }
                
                $this->signer->create([
                    'nama_penanda_tangan' => $request->nama_penanda_tangan,
                    'nip' => $request->nip,
                    'jabatan' => $request->jabatan,
                    'unique_id' => $request->nama_penanda_tangan . $request->nip . $request->jabatan,
                    'created_by' => $profile->id,
                    'updated_by' => $profile->id,
                    'unit_id' => $profile->unit_id,
                    'selected' => 'true'
                ]);

                return $response;                
            }
            return response()->json(['message' => "token invalid, hanya bisa diakses oleh Supervisor Unit!"], 401);

        } catch (Throwable $e) {
            report($e);
            $message = $e->getMessage();
            if ($message == 'Attempt to read property "id" on null') {
                return response()->json(['message' => "invalid Token!"], 401);
            }
        }
    }

    public function destroy($id)
    {
        try {

            $profile = $this->profile()[0];

            if ($profile->role_name === "Supervisor Unit") {

                $signer = $this->signer->find($id);
                if($signer->selected == 'true'){
                    return response()->json([
                        'status' => 'error',
                        'message' => 'Status penanda tangan ini sedang aktif! tidak bisa dihapus!',
                    ]);
                }
                $signer->delete();

                return response()->json([
                    'status' => 'success',
                    'message' => 'Penanda tangan Berhasil Dihapus',
                ]);
            }
            return response()->json(['message' => "token invalid, hanya bisa diakses oleh Supervisor Unit!"], 401);
        } catch (Throwable $e) {
            report($e);
            $message = $e->getMessage();
            if ($message == 'Attempt to read property "id" on null') {
                return response()->json(['message' => "invalid Token!"], 401);
            }
        }
    }
}
