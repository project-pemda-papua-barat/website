<?php

namespace App\Http\Controllers\Services;

use App\Http\Controllers\Controller;
use App\Models\MasterAsetStock;
use Illuminate\Http\Request;

class ReportStockServiceController extends Controller
{
    function __construct()
    {
        $this->middleware('auth:api');
        $this->masterStock = new MasterAsetStock();
    }

    function filter(Request $request)
    {
        $request->validate([
            'start_date' => 'required|date',
            'end_date' => 'required|date',
            'unit_id' => 'required'
        ]);

        $data = $this->masterStock->dataStockQueryUnit($request->start_date . ' 00:00:00', $request->end_date . ' 23:59:59', $request->unit_id);

        return response($data);
    }
}
