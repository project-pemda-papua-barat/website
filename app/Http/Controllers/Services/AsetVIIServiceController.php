<?php

namespace App\Http\Controllers\Services;

use App\Http\Controllers\Controller;
use App\Models\AsetVII;
use Illuminate\Http\Request;
use Throwable;
use Yajra\DataTables\Facades\DataTables;

class AsetVIIServiceController extends Controller
{
    function __construct()
    {
        $this->middleware('auth:api');
    }

    function index($id_asetI, $id_asetII, $id_asetIII, $id_asetIV, $id_asetV, $id_asetVI)
    {
        try {
            $aset = AsetVII::where('asetI_id', '=', $id_asetI)
                ->where('asetII_id',  '=', $id_asetII)
                ->where('asetIII_id',  '=', $id_asetIII)
                ->where('asetIV_id',  '=', $id_asetIV)
                ->where('asetV_id',  '=', $id_asetV)
                ->where('asetVI_id',  '=', $id_asetVI)->get();
            return DataTables::of($aset)->addColumn('action', function ($data) {
                $button = '<a href="#" onclick="editAsetVIIShow(' . $data->id . ')" class="text-info m-2"><i class="fa fa-edit" aria-hidden="true"></i>';
                return $button;
            })
                ->addIndexColumn()
                ->rawColumns(['action'])
                ->make(true);
        } catch (Throwable $e) {
            report($e);
            $message = $e->getMessage();
            if ($message == 'Attempt to read property "id" on null') {
                return response()->json(['message' => "invalid Token!"], 401);
            }
        }
    }

    public function store(Request $request)
    {
        $request->validate([
            'asetI_id' => 'required',
            'asetII_id' => 'required',
            'asetIII_id' => 'required',
            'asetIV_id' => 'required',
            'asetV_id' => 'required',
            'asetVI_id' => 'required',
            'kode_aset_vii' => 'required',
            'nama_aset_vii' => 'required',
            'data_id' => 'required|unique:aset_v_i_i_s,data_id'
        ]);

        try {

            $profile = $this->profile()[0];
            // return $profile;

            if ($profile->role_name == "Supervisor Pusat") {

                AsetVII::create([
                    'asetI_id' => $request->asetI_id,
                    'asetII_id' => $request->asetII_id,
                    'asetIII_id' => $request->asetIII_id,
                    'asetIV_id' => $request->asetIV_id,
                    'asetV_id' => $request->asetV_id,
                    'asetVI_id' => $request->asetVI_id,
                    'kode_aset_vii' => $request->kode_aset_vii,
                    'nama_aset_vii' => $request->nama_aset_vii,
                    'data_id' => $request->data_id
                ]);


                return response()->json([
                    'status' => 'success',
                    'message' => 'Aset VII created successfully',
                ]);
            }
            return response()->json(['message' => "token invalid, hanya bisa diakses oleh Supervisor Pusat!"], 401);
        } catch (Throwable $e) {
            report($e);
            $message = $e->getMessage();
            if ($message == 'Attempt to read property "id" on null') {
                return response()->json(['message' => "invalid Token!"], 401);
            }
        }
    }

    function list()
    {
        $aset = AsetVII::all();
        $listAset = [];
        foreach ($aset as $a => $key) {
            array_push(
                $listAset,
                [
                    'id' => $key->id,
                    'nomor_aset' => $key->asetI_id . $key->asetII_id . $key->asetIII_id . $key->asetIV_id . $key->asetV_id . $key->asetVI_id . $key->kode_aset_vii,
                    'nama_aset' => $key->nama_aset_vii,
                    'id_aset' => $key->data_id
                ]
            );
        }
        return $listAset;
    }
}
