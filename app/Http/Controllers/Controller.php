<?php

namespace App\Http\Controllers;

use App\Models\DetailAset;
use App\Models\DistributorModel;
use App\Models\JenisProgramMasukModel;
use App\Models\KemasanModel;
use App\Models\PenandaTanganModel;
use App\Models\PenyediaModel;
use App\Models\SatuanModel;
use App\Models\SumberDanaModel;
use App\Models\UnitModel;
use App\Models\User;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Throwable;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    protected function profile()
    {
        $data = auth()->user();
        $user = User::where('users.id', $data->id)
            ->join('unit_models', function ($join) {
                return $join->on('unit_models.id', '=', 'users.squad_id');
            })
            ->join('role_users', function ($join) {
                return $join->on('role_users.id', '=', 'users.role_id');
            })
            ->select(
                'users.id',
                'users.name',
                'users.username',
                'users.email',
                'users.role_id',
                'unit_models.nama_unit as squads.squad_name',
                // 'squads.squad_unit',
                'unit_models.id as unit_id',
                'unit_models.address as squad_address',
                // 'squads.squad_phone',
                'role_users.role_name'
            )->get();

        return $user;
    }

    protected function collectSingner($unit_id){
        $storeData = PenandaTanganModel::whereSelected('true')->whereUnitId($unit_id)->get();
        return $storeData;
    }

    protected function listDistributor()
    {
        $listSDistributor =  DistributorModel::select('id', 'nama_distributor as text')->get();
        return $listSDistributor;
    }

    protected function listUnit()
    {
        $data = UnitModel::select('id', 'nama_unit as text')->get();
        return $data;
    }

    protected function listKemasan()
    {
        $data = KemasanModel::select('id', 'nama_kemasan as text')->get();
        return $data;
    }

    protected function listJenis()
    {
        $listJenis = JenisProgramMasukModel::select('id', 'nama_program as text')->get();
        return $listJenis;
    }

    protected function listSumberDana()
    {
        $listSumberDana =  SumberDanaModel::select('id', 'nama_sumber_dana as text')->get();
        return $listSumberDana;
    }


    protected function listSatuan()
    {
        $data = SatuanModel::select('id', 'kode_satuan as text')->get();
        return $data;
    }

    protected function listPenyedia()
    {
        $data = PenyediaModel::select('id', 'nama_penyedia as text')->get();
        return $data;
    }

    protected function listDetailAset()
    {
        $listAset = new DetailAset();
        $data = $listAset->detail();
        $list = [];

        foreach ($data as $d) {
            array_push($list, [
                'id' => $d->id,
                'text' => $d->nama_detail_aset . "-" . $d->keterangan . " - Nomor Aset: " .  $d->nomor_aset . " - Data ID: " . $d->data_id,
                'is_expired' => $d->is_expired
            ]);
        }

        return $list;
    }
}
