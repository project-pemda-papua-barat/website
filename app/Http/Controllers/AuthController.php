<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    function index()
    {
        return view('auth.index');
    }

    function forgotPassword()
    {
        return view('auth.forgotPassword');
    }
}
