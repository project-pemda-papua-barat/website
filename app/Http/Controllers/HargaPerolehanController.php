<?php

namespace App\Http\Controllers;

use App\Models\AsetVII;
use Illuminate\Http\Request;

class HargaPerolehanController extends Controller
{
    function index()
    {
        $listAset = $this->aset();
        return view('hargaperolehan.index', compact('listAset'));
    }

    protected function aset()
    {
        $aset = AsetVII::all();
        $listAset = [];
        foreach ($aset as $a => $key) {
            array_push(
                $listAset,
                [
                    'id' => $key->id,
                    'nomor_aset' => $key->asetI_id . $key->asetII_id . $key->asetIII_id . $key->asetIV_id . $key->asetV_id . $key->asetVI_id . $key->kode_aset_vii,
                    'nama_aset' => $key->nama_aset_vii,
                    'id_aset' => $key->data_id
                ]
            );
        }
        return $listAset;
    }
}
