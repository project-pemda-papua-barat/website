<?php

namespace App\View\Components;

use Illuminate\View\Component;

class ModalUnit extends Component
{
    public $idModal;
    public $title;
    public $buttonAdd;
    public $buttonEdit;
    public $actionAdd;
    public $actionEdit;
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct(
        $idModal,
        $title,
        $buttonAdd,
        $buttonEdit,
        $actionAdd,
        $actionEdit
    ) {
        $this->idModal = $idModal;
        $this->title = $title;
        $this->buttonAdd = $buttonAdd;
        $this->buttonEdit = $buttonEdit;
        $this->actionAdd = $actionAdd;
        $this->actionEdit = $actionEdit;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.modal-unit');
    }
}
