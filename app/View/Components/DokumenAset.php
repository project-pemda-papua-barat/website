<?php

namespace App\View\Components;

use Illuminate\View\Component;

class DokumenAset extends Component
{
    public $idModal;
    public $title;
    public $titleButton;
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($idModal, $title, $titleButton)
    {
        $this->idModal = $idModal;
        $this->title = $title;
        $this->titleButton =  $titleButton;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.dokumen-aset');
    }
}
